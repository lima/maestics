################################################################################
# Automatically-generated file. Do not edit!
################################################################################

FPP_SRCS := 
F03_SRCS := 
FOR_SRCS := 
F95_SRCS := 
ASM_SRCS := 
F77_SRCS := 
F90_SRCS := 
F08_SRCS := 
O_SRCS := 
S_UPPER_SRCS := 
FTN_SRCS := 
OBJ_SRCS := 
F_SRCS := 
FIX_SRCS := 
C_SRCS := 
EXECUTABLES := 
OBJS := 
C_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
src/Projects \
src \
src/initializations \
src/inputs/Climate \
src/inputs/LAI \
src/inputs/Plant \
src/inputs/Root \
src/inputs/Soil \
src/inputs/Snow \
src/inputs \
src/inputs/USM \
src/inputs/crop_management_parameters \
src/inputs/data_previousCycle \
src/inputs/generals_parameters \
src/inputs/initializations \
src/inputs/optimisation \
src/inputs/outputs_management \
src/outputs/Balance \
src/outputs/daily_outputs \
src/outputs/data_end_ofCycle \
src/outputs/profile \
src/outputs/report \
src/stics_day/Development \
src/stics_day/Lixivation \
src/stics_day \
src/stics_day/contibutions_N_H2O_residues \
src/stics_day/cut_crops \
src/stics_day/decision_support \
src/stics_day/growth \
src/stics_day/micro_climate \
src/stics_day/offer_requirement_H2o \
src/stics_day/offer_requirement_N \
src/stics_day/stress \
src/utilities \
src/utilities/climate \
src/utilities/dates_management \
src/utilities/soil \
src/utilities/System \
src/utilities/Files \
src/utilities/SticsFiles \
src/outputs/history \

