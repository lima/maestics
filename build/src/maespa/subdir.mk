################################################################################
# Automatically-generated file. Do not edit! CBF added 30/05/24
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
F90_SRCS += \
default_conditions.f90 \
maestcom.f90 \
metcom.f90 \
getmet.f90 \
maindeclarations.f90 \
inout.f90 \
maespa.f90 \
physiol.f90 \
radn.f90 \
unstor.f90 \
utils.f90 \
watbal.f90 

OBJS += \
./src/maespa/switches.o \
./src/maespa/default_conditions.o \
./src/maespa/maestcom.o \
./src/maespa/metcom.o \
./src/maespa/getmet.o \
./src/maespa/maindeclarations.o \
./src/maespa/inout.o \
./src/maespa/maespa.o \
./src/maespa/physiol.o \
./src/maespa/radn.o \
./src/maespa/unstor.o \
./src/maespa/utils.o \
./src/maespa/watbal.o

# Each subdirectory must supply rules for building sources it contributes
%.o: %.f90
	@echo 'Building file: $<'
	@echo 'Invoking: GNU Fortran Compiler'
	@echo "$@"
	gfortran -O3 -march=native -finline -finline-functions -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


default_conditions.o: switches.o
getmet.o: maestcom.o metcom.o switches.o 
maindeclarations.o: maestcom.o
inout.o: switches.o maestcom.o 
maespa.o: maestcom.o metcom.o switches.o maindeclarations.o
physiol.o: maestcom.o metcom.o
radn2.o: maestcom.o
unstor.o: maestcom.f90
utils.o: maestcom.f90
watbal.o: maestcom.f90 switches.f90


