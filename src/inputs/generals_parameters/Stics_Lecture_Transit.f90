!*********************************************************************
!     lecture et initialisation des parametres nouvelle version
!     en attente d'etre redirige dans leurs fichiers respectifs
!     fichier paramv6.par
!     23/11/07 ajout des parametres d'adaptation au CC de la MO
!********************************************************************
! subroutine of general parameters v6
! - reading of the generals parameters of the file tempoparv6.sti
SUBROUTINE Stics_Lecture_Transit(t,P_nbplantes)

USE Stics
USE Messages
USE SticsFiles

implicit none

    type(Stics_Transit_), intent(INOUT) :: t  
    integer , intent(IN)::P_nbplantes  ! // PARAMETER // number of simulated plants // SD // P_USM/USMXML // 0 


      character(len=30) :: nomVar

      type(File_), pointer :: par_file
      logical :: file_open, exists
      integer :: fpar
      integer :: ret



      call open_file('parnew',par_file, file_open)
      
      exists = existFile(par_file)
    if (.NOT.exists) then
        call EnvoyerMsgHistorique("New formalisms parameters file doesn't exist : ",char(par_file%path))
        call EnvoyerMsgErreur( "New formalisms parameters  file doesn't exist : ",char(par_file%path) )
    end if
    
    if ( .NOT.file_open ) then
        call EnvoyerMsgHistorique("Error opening new formalisms parameters file: ", char(par_file%path))
        call EnvoyerMsgErreur( "Error opening new formalisms parameters file: ", char(par_file%path) )
    end if
 
    if ( any((/.NOT.exists,.NOT.file_open/)) ) then
       call exit(9)
    end if
    

     fpar = par_file%unit
 

! DR 08/11/2016 on ne evut pas pouvoir utiliser les formalismes d'effet du CC sur la denit et nit avant que joel revisit les formalismes
    t%P_code_adaptCC_miner = 2
    t%P_code_adaptCC_nit = 2
    t%P_code_adaptCC_denit = 2
    t%P_code_adapt_MO_CC = 2


    ! lecture des parametres
!specificites cultures fauchees
!***********************************
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_codetempfauche
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_coefracoupe(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_coefracoupe(2)
!specificites Quinoa
!***********************************
      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_codepluiepoquet
      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_nbjoursrrversirrig
!dynamique des talles
!***********************
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_swfacmin
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_codetranspitalle
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_codedyntalle(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_SurfApex(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_SeuilMorTalle(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_SigmaDisTalle(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_VitReconsPeupl(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_SeuilReconsPeupl(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_MaxTalle(1)
!! dr 10/06/2010 nouevaux parametres
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_SeuilLAIapex(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_tigefeuilcoupe(1)
! DR et ML et SYL 15/06/09
! ************************
! introduction de la fin des modifications de Sylvain (nadine et FR)
! dans le cadre du projet PERMED
! ### SYL 19-02-2009
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_codedyntalle(2)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_SurfApex(2)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_SeuilMorTalle(2)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_SigmaDisTalle(2)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_VitReconsPeupl(2)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_SeuilReconsPeupl(2)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_MaxTalle(2)
! ! dr 10/06/2010 nouevaux parametres
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_SeuilLAIapex(2)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_tigefeuilcoupe(2)
! ###
! DR et ML et SYL 15/06/09 FIN introduction de la fin des modifications de Sylvain
! DR et ML et SYL 15/06/09
! ************************
! introduction de la fin des modifications de Sylvain (nadine et FR)
! dans le cadre du projet PERMED
! #### SYL 26/02/2009

!Deplafonnement des reserves pour le cycle reproducteur
!*******************************************************
! DR 10/06/2010 on indexe sur le nb de plantes
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_resplmax(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_resplmax(2)
!calcul du stade de debut montaison pour les prairies perennes
!**************************************************************
! DR 10/06/2010 nouveaux parametres de 71
! DR et ML et SYL 16/06/09
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_codemontaison(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_codemontaison(2)
! ####
! DR et ML et SYL 15/06/09 FIN introduction de la fin des modifications de Sylvain
!Prise en compte du CC sur les matieres organiques
!****************************************************
! DR 08/11/2016 je ne les lis plus je mets les codes =2 en constante  en attendant que joel et bruno regardent les formailsmes de jorge de plus pres
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_code_adapt_MO_CC
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_periode_adapt_CC
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_an_debut_serie_histo
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_an_fin_serie_histo
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_param_tmoy_histo
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_code_adaptCC_miner
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_code_adaptCC_nit
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_code_adaptCC_denit
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_TREFdenit1
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_TREFdenit2
!test humidite dans decision semis
!********************************************
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_nbj_pr_apres_semis
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_eau_mini_decisemis
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_humirac_decisemis
!pilotage des fertilisations
!**********************************
! DR 05/04/2011 on les passe dans paramv6
      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_codecalferti
      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_ratiolN
      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_dosimxN
      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_codetesthumN      ! => Stics_Communs ?? A classer ou dupliquer

      ! Bruno : les 3 nouveaux param

      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_codeNmindec
      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_rapNmindec
      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_fNmindecmin

! ML 17/10/2013 on ajoute le code de couplage avce le module pathogene
! DR 07/02/2014 j'enleve la possibilite d'appel a Mila qui fait l'objet d'une version branch Mila
! je laisse les codes de calcul de duree d'humectation qui peut servir a tous
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_codepatho
! ML 29102012 on ajoute les 2 codes de calcul de la temp de rosee et de la duree d'humectation
      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_codetrosee
      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_codeSWDRH
!DR 10/02/2015 j'ajoute une option pour les tours d'eau en cas d'irrigation automaitique
!     read (fpar,*,end=80,err=250) nomVar
!     read (fpar,*,end=90,err=250) t%P_codedate_irrigauto
!     read (fpar,*,end=80,err=250) nomVar
!     read (fpar,*,end=90,err=250) t%P_datedeb_irrigauto
!     read (fpar,*,end=80,err=250) nomVar
!     read (fpar,*,end=90,err=250) t%P_datefin_irrigauto
!DR 16/11/2015 pour Julie Constantin on ajoute a des stades
!     read (fpar,*,end=80,err=250) nomVar
!     read (fpar,*,end=90,err=250) t%P_stage_start_irrigauto
!     read (fpar,*,end=80,err=250) nomVar
!     read (fpar,*,end=90,err=250) t%P_stage_end_irrigauto

! DR 06/05/2015 je rajoute un code pouyr tester la mortalite des racines
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_codemortalracine

 ! Ajout Joel 4/2/15
 ! Parametres nit denit et N2O
 ! Joel et DR 19/10/2016 creation des options nitrification et denitrification
 !DR 08/11/2016 je migre les parametres de nitrifiaction et denitrifiaction dans les param_gen pour ne pas faire de doublonsavce ceux
 ! deja existants dans le param_gen

! DR 05/02/2016 pour regles de semis agmipWheat
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_type_project
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_rules_sowing_AgMIP
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_Flag_Agmip_rap

      ! DR 23/03/2014 pour Constance et pour la preochaine version on peut faire plusieurs eclaircissages
      ! DR 23/03/2016 pour le rendre generique je le sort dans param_newform
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_option_thinning
! DR 30/03/2016 je met a part la possiblite d'apporter plusieurs types deengrais , c'est au dela de la notion de paturage
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_option_engrais_multiple
      ! DR 29/03/2016 option pour activer la gestion en paturage
      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_option_pature
! DR 07/04/2016 les parametres de l'option pature
      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_coderes_pature
      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_pertes_restit_ext
      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_Crespc_pature
      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_Nminres_pature
      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_eaures_pature
      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_coef_calcul_qres
      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_engrais_pature
      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_coef_calcul_doseN




      call EnvoyerMsgHistorique('codeNmindec',t%P_codeNmindec)
      call EnvoyerMsgHistorique('rapNmindec',t%P_rapNmindec)
      call EnvoyerMsgHistorique('fNmindecmin ',t%P_fNmindecmin )


 ! Ajout Loic et Bruno fevrier 2014
 !    valeurs initiales des nouveaux pools
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=80,err=250) t%P_maperenne0
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=80,err=250) t%P_QNperenne0
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=80,err=250) t%P_QNrestemp0
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=80,err=250) t%P_msrac0
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=80,err=250) t%P_QNrac0
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=80,err=250) t%P_codejourdes
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=80,err=250) t%P_codetauxexportfauche
! fin ajout mars 2017
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=80,err=250) t%P_code_gdh_Wang
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=80,err=250) t%P_tdoptdeb    ! Optimal temperature for calculating duration between dormancy and bud break
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=80,err=250) t%P_code_hautfauche_dyn
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=80,err=250) t%P_Hautfauche  ! Cutting height of forage crops
      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_code_CsurNsol_dynamic  ! option to calulate Csursol dynamic
! DR 18/06/2021 le codeFinert est a virer car Les 2 tests de function ne sont pas valid�s
! DR 23/08/2019 j'introduit le Finert de Bruno "proprement" pour comparaison avec trunk
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_codeFinert
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_Finert1  ! C/N soil factor 1 (nd)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_Finert2  ! C/N soil factor 2 (nd)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_codeFunctionFinert
! Bruno et Florent juin 2018 parametre P_humirac
      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_humirac  ! 1 = la fonction F_humirac atteint un plateau (ancien code) / 2 = la fonction n'atteint pas de plateau (identique a la phase germination-levee)
! fin ajout
!  DR 04/03/2019 j'ajoute un code dans param_newform pour desactiver le nouveau calcul de BM pour l'evaluation SMS
! voir ce qu'on fait avce les CAS , pour le moment je l'indice sur la plante
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_code_auto_profres(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_resk(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_resz(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_code_auto_profres(2)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_resk(2)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_resz(2)

 ! DR 12/06/2019 ajout de l'option to mix the humus on the depth itrav1-itrav2 (1) or on the depth 1-itrav2 (2) // 1,2
! PL, 12/04/2022: option inutile maintenant
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_code_depth_mixed_humus

 ! DR 12/06/2019 ajout de l'option de calcul du stock initial d'N selon bruno (2) or selon v9. (1) // 1,2
! DR 11/04/2022 on commente le calcul facon 9.1 qui ne sera plus utilis�
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_code_stock_BM
 ! Ajout Loic Avril 2021 : code pour activer les options ISOP + renseignement du pourcentage de legumineuses
      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_code_ISOP
      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_code_pct_legume
      read (fpar,*,end=80,err=250) nomVar
      read (fpar,*,end=90,err=250) t%P_pct_legume

!dr et ml 17/10/2013 forcage de codeSWDRH si couplage avce Mila
! dr 07/02/2014 j'enleve aussi le forcage devevnu inutile
!      if (t%P_codepatho .eq.1)then
!        call EnvoyerMsgHistorique('(/)')
!        call EnvoyerMsgHistorique(408)
!        t%P_codeSWDRH=1
!      endif



    ! domi - 27/01/04 - je rajoute un test sur P_ratiol<0
      if (t%P_ratiolN < 0.0)then
        call EnvoyerMsgHistorique('(/)')
        call EnvoyerMsgHistorique(395)
        t%P_ratiolN=1.0
      endif

    ! PB - 16/03/2004
    ! en mode de simulation CAS on ne peut pas calculer automatiquement les fertilisations
      if (P_nbplantes > 1) t%P_codecalferti = 2

! dr 05/04/2011 sont passe dans paramv6
      if (t%P_codecalferti == 1) then
        call EnvoyerMsgHistorique('P_ratiolN ',t%P_ratiolN)
        call EnvoyerMsgHistorique('P_codetesthumN ',t%P_codetesthumN)
      endif

      ! DR 22/06/2015 j'ajoute un test sur les dates de tour d'eau
!      if (t%P_codedate_irrigauto == 1 .and. t%P_datedeb_irrigauto > t%P_datefin_irrigauto) call EnvoyerMsgHistorique(408)

! 31/05/2018 manquait la fermeture du fichier param_newform
      ret = closeFile(par_file)
      return


80    call EnvoyerMsgErreur('End of new formalisms parameters file :  missing lines ')
      call EnvoyerMsgHistorique('End of new formalisms parameters file :  missing lines ')
      call exit(9)
  
90    call EnvoyerMsgErreur('End of new formalisms parameters file :  missing value for ',nomVar)
      call EnvoyerMsgHistorique('End of new formalisms parameters file :  missing value for ',nomVar)
      call exit(9)
  

250   call EnvoyerMsgErreur('Error reading new formalisms parameters file !')
      call EnvoyerMsgHistorique('Error reading new formalisms parameters file !')
      call exit(9)



end subroutine Stics_Lecture_Transit
 
 
