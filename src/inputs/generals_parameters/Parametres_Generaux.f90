! Module of general parameters
! - Description of the structure Parametres_Generaux_
! - reading of the generals parameters
! DR 14/02/2020 prise en compte de la participation de Nicolas Beaudoin sur les algos concerant
!  la prise en compte des cailloux dans Stics
module Parametres_Generaux


USE Stics, only: nb_residus_max   ! nb_residus_max defini dans Stics
USE SticsFiles
integer, parameter :: METHOD_STICS_V6 = 6         ! Code symbolique. define the use herited reading method of Stics V6
integer, parameter :: METHOD_XML_V7 = 7           ! Code symbolique.  define the use herited reading method XML / Javastics
integer, parameter :: PG_LECTURE_OK = 1       ! Code de retour. the reading of the plant file made without error.
integer, parameter :: PG_LECTURE_ERREUR_NO_METHOD = -1      ! Code retour. Error : the method choose is unknown.


type Parametres_Generaux_

  integer :: P_codeh2oact  ! // PARAMETER // code to activate  water stress effect on the crop: yes (1), no (2) // code 1/2 // PARAM // 0
  integer :: P_codeinnact  ! // PARAMETER // code activating  nitrogen stress effect on the crop: yes (1), no (2) // code 1/2 // PARAM // 0
  integer :: P_codhnappe  ! // PARAMETER // mode of calculation of watertable level // code 1/2 // PARAM // 0
  integer :: P_codeminopt  ! // PARAMETER // option to maintain a constant water content in bare soil rainfall and PET ni): yes (1), no (2)  // code 1/2 // PARAM // 0
  integer :: P_codeprofmes  ! // PARAMETER // option of depth of calculation for water and nitrogen stocks (1=P_profmes or 2=soil depth) // code 1/2 // PARAM // 0
  integer :: P_codeactimulch  ! // PARAMETER // activation of the accounting for natural mulching in the partitioning of soil evaporation within the soil profile: yes (1), no (2) // code 1/2 // PARAM // 0
  integer :: codeulaivernal  
  integer :: P_codetycailloux  ! // PARAMETER // stones type code // code 1 to 10 // PARAM // 0
  integer :: P_codetypeng  ! // PARAMETER // fertiliser type code // code 1 to 8 // PARAM // 0
  integer :: P_codetypres  ! // PARAMETER // organic residue tyope code // code 1 to 8 // PARAM // 0
  integer :: P_iniprofil  ! // PARAMETER // Option of smoothing out (function spline) the initial nitrogen and water profile: yes (1), no (0) // code 0/1 // PARAM // 0

  real :: P_beta  ! // PARAMETER // parameter of increase of maximal transpiration when occurs a water stress // SD // PARAM // 1
  real :: P_lvopt  ! // PARAMETER // Optimum root density // cm root.cm-3 soil // PARAM // 1
  ! Loic Fevrier 2021 : New mineralisation model Clivot
  real :: P_Gmin1  ! // PARAMETER // Mineralization parameters of the new model : mineralization rate constant (day-1) // PARAM // 1
  real :: P_Gmin2  ! // PARAMETER // Mineralization parameters of the new model : clay content factor (%-1) // PARAM // 1
  real :: P_Gmin3  ! // PARAMETER // Mineralization parameters of the new model : CaCO3 content factor (%-1) // PARAM // 1
  real :: P_Gmin4  ! // PARAMETER // Mineralization parameters of the new model : pH factor 1 (pH-1) // PARAM // 1
  real :: P_Gmin5  ! // PARAMETER // Mineralization parameters of the new model : pH factor 2 (pH) // PARAM // 1
  real :: P_Gmin6  ! // PARAMETER // Mineralization parameters of the new model : C/N soil factor 1 (nd) // PARAM // 1
  real :: P_Gmin7  ! // PARAMETER // Mineralization parameters of the new model : C/N soil factor 2 (nd) // PARAM // 1
  ! Loic Fevrier 2021 : New mineralisation model Clivot (FIN)
  real :: P_Wh  ! // PARAMETER // ratio N/C of humus // g g-1 // PARAM // 1
  real :: P_TREFh  ! // PARAMETER // temperature of reference for the soil mineralization parameters  // degree C // PARAM // 1
  real :: P_difN  ! // PARAMETER // coefficient de diffusion apparente du nitrate dans le sol humide // cm2 jour-1 // PARAM // 1
  real :: P_plNmin  ! // PARAMETER // Minimal amount of precipitation to manage a fertilization // mm day-1 // PARAM // 1
  real :: P_proprac  ! // PARAMETER // Ratio root mass / mass of aerial parts at harvest  // g.g -1 // PARAM // 1
  real :: P_coefb  ! // PARAMETER // parameter defining radiation effect on  conversion efficiency // SD // PARAM // 1
  real :: P_irrlev  ! // PARAMETER // amount of irrigation applied automatically on the sowing day when the model calculates irrigation, to allow germination // mm // PARAM // 1
  real :: P_distdrain  ! // PARAMETER // distance to the drain to calculate watertable height // cm // PARAM // 1
  real :: P_dacohes  ! // PARAMETER // bulk density under which root growth is reduced due to a lack of cohesion d // g cm-3 // PARAM // 1
  real :: P_daseuilhaut  ! // PARAMETER // Threshold of bulk density of soil below that the root growth  no more possible // g cm-3 // PARAM // 1
  real :: P_daseuilbas  ! // PARAMETER // Threshold of bulk density of soil below that the root growth is not limited // g cm-3 // PARAM // 1
  real :: P_QNpltminINN  ! // PARAMETER // minimal amount of nitrogen in the plant allowing INN computing // kg ha-1 // PARAM // 1
  real :: P_pHminvol  ! // PARAMETER // P_pH above which the fertilizer volatilisation is null // P_pH // PARAM // 1
  real :: P_pHmaxvol  ! // PARAMETER //  P_pH beyond which the fertilizer volatilisation is maximale // P_pH // PARAM // 1
  real :: P_Vabs2  ! // PARAMETER // nitrogen uptake rate for which  fertilizer losts of  are divided by 2 // kg/ha/jour // PARAM // 1
  real :: P_Xorgmax  ! // PARAMETER // maximal amount of oraganised nitrogen coming from the mineral fertilizer  // kg.ha-1 // PARAM // 1
  real :: P_hminm  ! // PARAMETER // moisture (proportion of field capacity) below which mineralisation rate is nil // g eau g-1 sol // PARAM // 1
  real :: P_hoptm  ! // PARAMETER // moisture (proportion of field capacity) above which mineralisation rate is maximum // g eau g-1 sol // PARAM // 1
  real :: P_hminn  ! // PARAMETER // moisture (proportion of field capacity) below which nitrification rate is nil // g eau g-1 sol // PARAM // 1
  real :: P_hoptn  ! // PARAMETER // moisture (proportion of field capacity) at which nitrification rate is maximum // g eau g-1 sol // PARAM // 1
  real :: P_pHminnit  ! // PARAMETER // effect of the P_pH on nitrification (threshold mini) // P_pH // PARAM // 1
  real :: P_pHmaxnit  ! // PARAMETER // effect of the P_pH on nitrification (threshold maxi) // P_pH // PARAM // 1
  real :: P_tnitopt  ! // PARAMETER // cardinal temperature for nitrification // degree C // PARAM // 1
  real :: P_tnitmax  ! // PARAMETER // cardinal temperature for nitrification // degree C // PARAM // 1
  real :: P_tnitmin  ! // PARAMETER // cardinal temperature for nitrification // degree C // PARAM // 1
  real :: P_pminruis  ! // PARAMETER // Minimal amount of precipitation to start a drip  // mm day-1 // PARAM // 1
  real :: P_diftherm  ! // PARAMETER // soil thermal diffusivity // cm2 s-1 // PARAM // 1
  real :: P_Bformnappe  ! // PARAMETER // coefficient of water table shape (artificial drained soil) // SD // PARAM // 1
  real :: P_rdrain  ! // PARAMETER // drain radius // cm // PARAM // 1
  real :: P_hcccx(10)  ! // PARAMETER // field capacity moisture of each pebble type  (table) // % ponderal // PARAM // 1
  real :: P_masvolcx(10)  ! // PARAMETER // volumetric mass (bulk) of pebbles // g cm-3 // PARAM // 1
  real :: P_engamm(8)  ! // PARAMETER // proportion of ammonium in the fertilizer // SD // PARAM // 1
  real :: P_voleng(8)  ! // PARAMETER // maximal fraction of mineral fertilizer that can be volatilized  // SD // PARAM // 1
  real :: P_orgeng(8)  ! // PARAMETER // maximal quantity of mineral fertilizer that can be organized in the soil (fraction for type 8) // kg ha-1 // PARAM // 1
  real :: P_deneng(8)  ! // PARAMETER // proportion of the mineral fertilizer that can be denitrified (useful if codenit not active) // SD // PARAM // 1
  real :: P_parsurrg  ! // PARAMETER // coefficient PAR/RG for the calculation of PAR  // * // PARAM // 1

! Joel 4/2/15
! parametres nit-denit-N2O
! DR 8/11/2016 passe dans les param generaux
  real    :: P_pHminden        ! pH below which the denitrification N2O molar fraction is maximum
  real    :: P_pHmaxden        ! pH beyond which the denitrification N2O molar fraction is minimum
  real    :: P_Kd              ! Affinity constant for NO3 in denitrification (mg N/L)
  real    :: P_kdesat          ! rate constant of desaturation (d-1)
  real    :: P_wfpsc           ! wfps threshold beyond which denitrification occurs                 0.62
  real    :: P_vnitmax         ! maximum nitrification rate if michaelis_menten option used (mg N kg-1 d-1)                         20.
  real    :: P_Kamm            ! affinity constant for NH4 in nitrification if michaelis_menten option used (mg N/l)                24.
  real    :: P_nh4_min         ! minimum (fixed ?) NH4 concentration found in soil (mg N/kg)                   1.0
! 19/10/2016 ajout options et nouveux params
  integer :: P_code_vnit      ! choice of nitrification rate dependence on NH4 (linear or Michaelis-Menten)
  real    :: P_fnx_soil          ! potential proportion of NH4 nitrified each day if linear model
  integer :: P_code_tnit      ! choice of temperature function for nitrification (piecewise linear or gaussian)
  real    :: P_tnitopt_gauss     ! optimum temperature for nitrification
  real    :: P_scale_tnitopt     ! parameter related to the range of optimum temperature for nitrification
  integer :: P_code_rationit  ! choice of constant or variable N2O ratio for nitrification
!  real :: P_rationit_constant ! constant value of N2O ratio for nitrification
  integer :: P_code_hourly_wfps_nit ! choice of activating or not hourly WFPS calculation for nit
  real    :: P_tdenitopt_gauss   ! optimum temperature for denitrification
  real    :: P_scale_tdenitopt   ! parameter related to the range of optimum temperature for denitrification
  integer :: P_code_pdenit    ! choice of denitrification potential (soil parameter or calculated from Corg)
  real    :: P_cmin_pdenit       ! Corg value below which denitrification potential is constant and min
  real    :: P_cmax_pdenit       ! Corg value above which denitrification potential is constant and max
  real    :: P_min_pdenit        ! min value of denitrification potential
  real    :: P_max_pdenit        ! max value of denitrification potential
  integer :: P_code_ratiodenit ! constant value of N2O ratio for denitrification
!  real :: P_ratiodenit_constant ! constant value of N2O ratio for denitrification
  integer :: P_code_hourly_wfps_denit ! choice of activating or not hourly WFPS calculation for denit


! Residus
  integer :: nbresidus = 21  ! todo : separer racines fines = 21, racines grosses = 22

  real, dimension(nb_residus_max) :: P_kbio  ! // PARAMETER // constant of mortality rate of microbial biomass // day -1 // PARAM // 1
  real, dimension(nb_residus_max) :: P_yres  ! // PARAMETER // Carbon assimilation yield of the microflora // g g-1 // PARAM // 1
  real, dimension(nb_residus_max) :: P_akres  ! // PARAMETER // parameter of organic residues decomposition: kres=P_akres+P_bkres/P_CsurNres // day-1 // PARAM // 1
  real, dimension(nb_residus_max) :: P_bkres  ! // PARAMETER // parameter of organic residues decomposition: kres=P_akres+P_bkres/P_CsurNres // g g-1 // PARAM // 1
  real, dimension(nb_residus_max) :: P_awb  ! // PARAMETER // parameter  of organic residues decomposition: CsurNbio=P_awb+P_bwb/P_CsurNres // SD // PARAM // 1
  real, dimension(nb_residus_max) :: P_bwb  ! // PARAMETER // parameter of organic residues decomposition: CsurNbio=P_awb+P_bwb/P_CsurNres // g g-1 // PARAM // 1
  real, dimension(nb_residus_max) :: P_cwb  ! // PARAMETER // Minimum ratio C/N of microbial biomass in the relationship: CsurNbio=P_awb+P_bwb/P_CsurNres // g g-1 // PARAM // 1
  real, dimension(nb_residus_max) :: P_ahres  ! // PARAMETER // parameter of organic residues humification: hres=1-P_ahres*P_CsurNres/(P_bhres+P_CsurNres) // g g-1 // PARAM // 1
  real, dimension(nb_residus_max) :: P_bhres  ! // PARAMETER // parameter of organic residues humification: hres=1-P_ahres*P_CsurNres/(P_bhres+P_CsurNres) // g g-1 // PARAM // 1
  real, dimension(nb_residus_max) :: P_CNresmin  ! // PARAMETER // minimum observed value of ratio C/N of organic residues  // g g-1 // PARAM // 1
  real, dimension(nb_residus_max) :: P_CNresmax  ! // PARAMETER // maximum observed value of ratio C/N of organic residues // g g-1 // PARAM // 1
  real, dimension(nb_residus_max) :: P_CroCo  ! // PARAMETER // parameter of organic residues decomposition  //  SD// PARAM // 1
  real, dimension(nb_residus_max) :: P_qmulchruis0  ! // PARAMETER // Amount of mulch to annul the drip // t ha-1 // PARAM // 1
  real, dimension(nb_residus_max) :: P_mouillabilmulch  ! // PARAMETER // maximum wettability of crop mulch // mm t-1 ha // PARAM // 1
  real, dimension(nb_residus_max) :: P_kcouvmlch  ! // PARAMETER // Extinction Coefficient reliant la quantite de paillis vegetal au taux de couverture du sol // * // PARAM // 1
  real, dimension(nb_residus_max) :: P_albedomulchresidus  ! // PARAMETER // P_albedo of crop mulch // SD // PARAM // 1
  real, dimension(nb_residus_max) :: P_Qmulchdec  ! // PARAMETER // maximal amount of decomposing mulch // t C.ha-1 // PARAM // 1

  integer :: P_codesymbiose  ! // PARAMETER // option of calculation of symbiotic fixation // code 1/2 // PARAM // 0

! NB le 15/02/06 parametres DST
  real :: P_proflabour  ! // PARAMETER // soil minimal depth for ploughing when soil compaction is activated // cm // PARAM // 1
  real :: P_proftravmin  ! // PARAMETER // soil minimal depth for chisel tillage when soil compaction is activated // cm // PARAM // 1
  real :: P_trefr  ! // PARAMETER // temperature of reference for the soil mineralization parameters  // degree C // PARAM // 1

  real :: P_FTEMh  ! // PARAMETER // Parameter 2 of the temperature function on the decomposition rate of humus // degree K-1 // PARAM // 1
  real :: P_FTEMr  ! // PARAMETER // Parameter 2 of the temperature function on the decomposition rate of organic residues // degree K-1 // PARAM // 1
  real :: P_FTEMra  ! // PARAMETER // Parameter 1 of the temperature function on the decomposition rate of organic residues // * // PARAM // 1
  real :: P_FTEMha  ! // PARAMETER // Parameter 1 of the temperature function on the decomposition rate of humus // * // PARAM // 1
  real :: P_fhminsat  ! // PARAMETER // soil mineralisation rate at water saturation // SD // PARAM // 1
  real :: P_fnx  ! // PARAMETER // ! potential proportion of NH4 nitrified each day if linear model // day-1 // PARAM // 1
  real :: P_rationit  ! // PARAMETER // ratio between N2O emisson and total nitrification // kg.ha-1.j-1 // PARAM // 1
  real :: P_ratiodenit  ! // PARAMETER // ratio between N2O emisson and total denitrification // kg.ha-1.j-1 // PARAM // 1

  real :: P_prophumtasssem  ! // PARAMETER // field capacity proportion above witch compaction may occur (to delay sowing) // SD // PARAM // 1
  real :: P_prophumtassrec  ! // PARAMETER // field capacity proportion above witch compaction may occur (to delay harvest) // SD // PARAM // 1

  integer :: P_codeinitprec  ! // PARAMETER // reinitializing initial status in case of chaining simulations : yes (1), no (2) // code 1/2 // PARAM // 0
  integer :: P_flagEcriture  ! // PARAMETER // option for writing the output files (1 = mod_history.sti, 2=daily outputs,4= report outputs, 8=balance outputs,16 = profile outputs, 32 = debug  outputs; 64= screen outputs, 128 = agmip outputs) add them to have several types of outputs //  code 1/2 // PARAM // 1
  integer :: P_codesensibilite  ! // PARAMETER // code to activate the sensitivity analysis version of the model: yes (1), no (2) // code 1/2 // PARAM // 0
  integer :: P_codefrmur  ! // PARAMETER // code defining the maturity status of the fruits in the output  variable CHARGEFRUIT (1 = including ripe fruits (last box N);  2 = excluding ripe fruits (first N-1 boxes)) // code 1/2 // PARAM // 0

! ** OFFRNODU
  integer :: P_codefxn  ! // PARAMETER // option to activate the chosen way to compute fxN // code 1/2 // PARAM // 0

  integer :: P_codemsfinal  ! // PARAMETER // option defining the biomass and yield conservation after harvest (1 = yes (values maintained equal to harvest) ; 2 = no (values set at 0)) // code 1/2 // PARAM // 0

  real :: P_psihumin  ! // PARAMETER // soil potential corresponding to wilting point // Mpa // PARAM // 1
  real :: P_psihucc  ! // PARAMETER // soil potential corresponding to field capacity  // Mpa // PARAM // 1

  integer :: P_codeoutscient

  integer :: P_codeseprapport  ! // PARAMETER // choice of the kind of column separator in the rapport.sti file: separator chosen in P_separateurrapport (2), space (1) // code 1/2 // STATION // 0
  character :: P_separateurrapport  ! // PARAMETER // column separator in rapport.sti file // caractere // PARAM // 0

!: micro-climat ??
  integer :: P_codemicheur  ! // PARAMETER // option of calculation of hourly microclimatic outputs (output file humidite.sti) yes (1) no (2) // code 1/2 // PARAM // 0

  real :: P_alphapH  ! // PARAMETER // maximal soil pH variation per unit of inorganic N added with slurry // kg-1 ha //PARAM //1
  real :: P_dpHvolmax  ! // PARAMETER // maximal P_pH increase following the application of organic residue sutch as slurry // SD // PARAM // 1
  real :: P_pHvols  ! // PARAMETER // maximal soil P_pH above which soil P_pH is not affected by addition of organic residue (sutch as slurry)  // SD // PARAM // 1
  real :: P_fredkN  ! // PARAMETER // reduction factor of decomposition rate of residues when mineral N is limiting // SD // PARAM // 1
  real :: P_fredlN  ! // PARAMETER // reduction factor of decomposition rate of biomass when mineral N is limiting // SD // PARAM // 1
  real :: P_fNCBiomin  ! // PARAMETER // maximal reduction factor of the ratio N/C of the microbial biomass when nitrogen limits decomposition (between 0 and 1) // SD // PARAM // 1
! 22/06/09 FIN introduction des modifications de BM

!  26/07/2012 le trio on ajoute 2 parametres dans param_gen
  real :: P_tnitopt2    ! // PARAMETER // optimal temperature (2/2) for nitrification // grade C
  real :: P_y0msrac     ! // PARAMETER // minimal amount of root mass at harvest (when aerial biomass is nil)  // t.ha-1 // PARAM // 1
  real :: P_fredNsup    ! // PARAMETER // additional reduction factor of residues decomposition rate when mineral N is very limited in soil // SD // PARAM // 1
  real :: P_Primingmax  ! // PARAMETER // maximum priming ratio (relative to SOM decomposition rate) // SD //PARAM // 1

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  integer :: P_codesnow !< // PARAMETER // code to activate the calculation of snow depth , and recaclulation of tmin, tmax, rainfall : yes (1), no (2) // code 1/2 // PARAM // 0
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  ! DR 28/05/2019 new parameter
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  real :: P_tmin_mineralisation !> // PARAMETER// minimal temperature for mineralisation // degrees // PAAM// 1
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  
end type Parametres_Generaux_



CONTAINS






! ********************************************************
!! *    lecture et initialisation des parametres generaux *
!! *    fichier param.par                                 *
!! *    version 5.1 - 23/02/2004                          *
! ********************************************************

subroutine Lecture_Parametres_Generaux(pg) 

USE Messages

  implicit none

!: ARGUMENTS

  type(Parametres_Generaux_), intent(OUT) :: pg  


!: VARIABLES LOCALES
  integer :: icx  !  
  integer :: ieng  !  
  integer :: ires ! variables de boucle  
  integer :: eof           ! variable de gestion des erreurs de lecture  

  character(len=30) :: nomVar

  type(File_), pointer :: par_file
  logical :: file_open, exists
  integer :: fpar
  integer :: ret
  call open_file('par',par_file, file_open)
      
      exists = existFile(par_file)
    if (.NOT.exists) then
        call EnvoyerMsgHistorique("General parameters file doesn't exist : ",char(par_file%path))
        call EnvoyerMsgErreur( "General parameters  file doesn't exist : ",char(par_file%path) )
    end if
    
    if ( .NOT.file_open ) then
        call EnvoyerMsgHistorique("Error opening general parameters file: ", char(par_file%path))
        call EnvoyerMsgErreur( "Error opening general parameters file: ", char(par_file%path) )
    end if
 
    if ( any((/.NOT.exists,.NOT.file_open/)) ) then
       call exit(9)
    end if
    

  fpar = par_file%unit

! simulation options
! **********************
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_codeinnact
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_codeh2oact
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_codeminopt
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_iniprofil
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_codeprofmes
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_codeinitprec

  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_codemsfinal  ! 1 = maintien de variables recolte apres recolte; 2 = annulation
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_codeactimulch
!: si codeulaivernal = 1 la vernalisation joue sur ulai, si = 0 joue pas
! *- on met en dur dans initial.for : codeulaivernal = 1
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_codefrmur
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_codemicheur
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_codeoutscient
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_codeseprapport
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,fmt='(A1)', iostat=eof,end=90,err=250) pg%P_separateurrapport
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_codesensibilite
  read (fpar,*,end=80,err=250) nomVar 
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_codesnow
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  read (fpar,*,end=80,err=250) nomVar 
  ! DR 29/08/2012 maintenant je lis flagecriture a la  place de codesig
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_flagEcriture
 

! radiation interception
! **********************

  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_parsurrg
!
! shoot growth
! **********************
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_coefb
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_proprac
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_y0msrac
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_dacohes
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_daseuilbas
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_daseuilhaut
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_beta
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_lvopt
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_difN
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_plNmin
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_irrlev
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_QNpltminINN
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_codesymbiose
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_codefxn
  read (fpar,*,end=80,err=250) nomVar
! Humified SOM mineralisation
! ***********************************
! DR 298/05/2019 this new parameter is add for coldest conditions (fixed to 0 before)
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_tmin_mineralisation
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_FTEMh
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_FTEMha
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_TREFh
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_FTEMr
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_FTEMra
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_trefr
  ! Loic Fevrier 2021 : New mineralisation model Clivot
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_Gmin1
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_Gmin2
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_Gmin3
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_Gmin4
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_Gmin5
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_Gmin6
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_Gmin7
  ! Loic Fevrier 2021 : New mineralisation model Clivot (FIN)
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_Wh
  read (fpar,*,end=80,err=250) nomVar
! Parameters of fertiliser losses
! ***********************************
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_pHminvol
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_pHmaxvol
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_Vabs2
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_Xorgmax
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_hminm
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_hoptm
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_alphapH
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_dpHvolmax
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_pHvols

  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_fhminsat

  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_fredkN
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_fredlN

! P_fNCBiomin = facteur de reduction du rapport N/C de la biomasse en conditions de limitation en N
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_fNCBiomin
! 26/07/2012 on ajoute 2 parametres de mineralisation de BM
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_fredNsup
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_Primingmax

! DR 08/11/2016 je mets ici les nouvelles options de formalisme denit et nit et les parametres qui vont avce qui etaient dans le param_newform
! Nitrification
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_hminn
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_hoptn
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_pHminnit
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_pHmaxnit
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_NH4_min

 ! denitrification
 read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_pHminden
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_pHmaxden
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_wfpsc
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_tdenitopt_gauss
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_scale_tdenitopt
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_Kd
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_kdesat

 ! Nitrification
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_code_vnit
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_fnx
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_vnitmax
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_Kamm
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_code_tnit
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_tnitmin
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_tnitopt
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_tnitopt2
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_tnitmax
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_tnitopt_gauss
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_scale_tnitopt
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_code_rationit
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_rationit
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_code_hourly_wfps_nit

! Denitrification
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_code_pdenit
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_cmin_pdenit
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_cmax_pdenit
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_min_pdenit
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_max_pdenit
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_code_ratiodenit
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_ratiodenit
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_code_hourly_wfps_denit

! soil
!**************
! pertes gazeuses: denitrification et volatilisation
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_pminruis
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_diftherm
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_Bformnappe
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_rdrain
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_psihumin
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_psihucc
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_prophumtasssem
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_prophumtassrec
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_codhnappe
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_distdrain
  ! technics
  !***************
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_proflabour
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_proftravmin
  ! typology
  !***************
! types of pebbles
!*****************
  read (fpar,*,end=80,err=250) nomVar
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_codetycailloux
  read (fpar,*,end=80,err=250) nomVar
  do icx = 1,10
    read (fpar,*,iostat=eof,end=90,err=250) pg%P_masvolcx(icx)
    read (fpar,*,end=80,err=250) nomVar
    read (fpar,*,iostat=eof,end=90,err=250) pg%P_hcccx(icx)
    read (fpar,*,end=80,err=250) nomVar
  end do

! types of mineral fertilisers (8 types)
!********************************
  read (fpar,*,iostat=eof,end=90,err=250) pg%P_codetypeng
  read (fpar,*,end=80,err=250) nomVar
  do ieng = 1,8
    read (fpar,*,iostat=eof,end=90,err=250) pg%P_engamm(ieng)
    read (fpar,*,end=80,err=250) nomVar
    read (fpar,*,iostat=eof,end=90,err=250) pg%P_orgeng(ieng)
    read (fpar,*,end=80,err=250) nomVar
    read (fpar,*,iostat=eof,end=90,err=250) pg%P_deneng(ieng)
    read (fpar,*,end=80,err=250) nomVar
    read (fpar,*,iostat=eof,end=90,err=250) pg%P_voleng(ieng)
    read (fpar,*,end=80,err=250) nomVar
  end do

!types of residues
!**************************

  read (fpar,*,iostat=eof,end=90,err=250) pg%P_codetypres

  do ires=1, pg%nbresidus
    read (fpar,*,iostat=eof,end=70,err=250)
    read (fpar,*,iostat=eof,end=90,err=250) pg%P_CroCo(ires)
    read (fpar,*,end=80,err=250) nomVar
    read (fpar,*,iostat=eof,end=90,err=250) pg%P_akres(ires)
    read (fpar,*,end=80,err=250) nomVar
    read (fpar,*,iostat=eof,end=90,err=250) pg%P_bkres(ires)
    read (fpar,*,end=80,err=250) nomVar
    read (fpar,*,iostat=eof,end=90,err=250) pg%P_awb(ires)
    read (fpar,*,end=80,err=250) nomVar
    read (fpar,*,iostat=eof,end=90,err=250) pg%P_bwb(ires)
    read (fpar,*,end=80,err=250) nomVar
    read (fpar,*,iostat=eof,end=90,err=250) pg%P_cwb(ires)
    read (fpar,*,end=80,err=250) nomVar
    read (fpar,*,iostat=eof,end=90,err=250) pg%P_ahres(ires)
    read (fpar,*,end=80,err=250) nomVar
    read (fpar,*,iostat=eof,end=90,err=250) pg%P_bhres(ires)
    read (fpar,*,end=80,err=250) nomVar
    read (fpar,*,iostat=eof,end=90,err=250) pg%P_kbio(ires)
    read (fpar,*,end=80,err=250) nomVar
    read (fpar,*,iostat=eof,end=90,err=250) pg%P_yres(ires)
    read (fpar,*,end=80,err=250) nomVar
    read (fpar,*,iostat=eof,end=90,err=250) pg%P_CNresmin(ires)
    read (fpar,*,end=80,err=250) nomVar
    read (fpar,*,iostat=eof,end=90,err=250) pg%P_cnresmax(ires)
    read (fpar,*,end=80,err=250) nomVar
! DR 02/02/2011 on deplace ces parametres dans les residus
    read (fpar,*,iostat=eof,end=90,err=250) pg%P_qmulchruis0(ires)
    read (fpar,*,end=80,err=250) nomVar
    read (fpar,*,iostat=eof,end=90,err=250) pg%P_mouillabilmulch(ires)
    read (fpar,*,end=80,err=250) nomVar
    read (fpar,*,iostat=eof,end=90,err=250) pg%P_kcouvmlch(ires)
    read (fpar,*,end=80,err=250) nomVar
    read (fpar,*,iostat=eof,end=90,err=250) pg%P_albedomulchresidus(ires)


    read (fpar,*,end=80,err=250) nomVar
    read (fpar,*,iostat=eof,end=90,err=250) pg%P_Qmulchdec(ires)

  end do

  ret = closeFile(par_file) 

  return

70  ret = closeFile(par_file)  
    return
80  call EnvoyerMsgErreur('End of general parameters file :  missing lines ')
    call EnvoyerMsgHistorique('End of general parameters file :  missing lines ')
    !call EnvoyerMsgHistorique(105)
    call exit(9)

90  call EnvoyerMsgErreur('End of general parameters file :  missing value for ',nomVar)
    call EnvoyerMsgHistorique('End of general parameters file :  missing value for ',nomVar)
    call exit(9)
! on a rencontre une erreur lors de la lecture des parametres
250 call EnvoyerMsgErreur('Error reading general parameters file !')
    call EnvoyerMsgHistorique('Error reading general parameters file !')
    !call EnvoyerMsgHistorique(105)
    call exit(9)




  end subroutine Lecture_Parametres_Generaux



subroutine Parametres_Generaux_Zero(pg)

type(Parametres_Generaux_), intent(OUT) :: pg  

  pg%P_codeh2oact = 0
  pg%P_codeinnact = 0
  pg%P_codhnappe = 0
  pg%P_codeminopt = 0
  pg%P_codeprofmes = 0
  pg%P_codeactimulch = 0
  pg%codeulaivernal = 0
  pg%P_codetycailloux = 0
  pg%P_codetypeng = 0
  pg%P_codetypres = 0
  pg%P_iniprofil = 0
  pg%P_beta = 0.
  pg%P_lvopt = 0.
  pg%P_Wh = 0.
  pg%P_FTEMh = 0.
  pg%P_TREFh = 0.
  pg%P_difN = 0.
  pg%P_plNmin = 0.
  pg%P_proprac = 0.
  pg%P_coefb = 0.
  pg%P_irrlev = 0.
  pg%P_distdrain = 0.
  pg%P_dacohes = 0.
  pg%P_daseuilhaut = 0.
  pg%P_daseuilbas = 0.
  pg%P_QNpltminINN = 0.
  pg%P_pHminvol = 0.
  pg%P_pHmaxvol = 0.
  pg%P_Vabs2 = 0.
  pg%P_Xorgmax = 0.
  pg%P_hminm = 0.
  pg%P_hoptm = 0.
  pg%P_hminn = 0.
  pg%P_hoptn = 0.
  pg%P_fnx = 0.
  pg%P_pHminnit = 0.
  pg%P_pHmaxnit = 0.
  pg%P_tnitopt = 0.
  pg%P_tnitmax = 0.
  pg%P_tnitmin = 0.
  pg%P_pminruis = 0.
  pg%P_diftherm = 0.
  pg%P_Bformnappe = 0.
  pg%P_rdrain = 0.
  pg%P_hcccx(:) = 0.
  pg%P_masvolcx(:) = 0.
  pg%P_engamm(:) = 0.
  pg%P_voleng(:) = 0.
  pg%P_orgeng(:) = 0.
  pg%P_deneng(:) = 0.
  pg%P_codesymbiose = 0
  pg%P_proflabour = 0.
  pg%P_proftravmin = 0.
  pg%P_trefr = 0.
  pg%P_FTEMr = 0.
  pg%P_FTEMra = 0.
  pg%P_FTEMha = 0.
  pg%P_rationit = 0.
  pg%P_ratiodenit = 0.
  pg%P_alphapH = 0.
  pg%P_dpHvolmax = 0.
  pg%P_pHvols = 0.
  pg%P_prophumtasssem = 0.
  pg%P_prophumtassrec = 0.
  pg%P_fhminsat = 0.
  pg%P_codeinitprec = 0
  pg%P_flagEcriture = 0
  pg%P_codesensibilite = 0
  pg%P_codefrmur = 0
  pg%P_codefxn = 0
  pg%P_codemsfinal = 0
  pg%P_psihumin = 0.
  pg%P_psihucc = 0.
  pg%P_codeoutscient = 0
  pg%P_codeseprapport = 0
  pg%P_separateurrapport = ''
  pg%P_codemicheur = 0
  pg%P_fredkN = 0.
  pg%P_fredlN = 0.

  ! DR 08/11/2016 les parametres nit et denit sont dans le param_newform
pg%P_pHminden=0.
pg%P_pHmaxden=0.
pg%P_wfpsc=0.
pg%P_nh4_min=0.
pg%P_code_vnit=0
pg%P_vnitmax=0.
pg%P_Kamm=0.
pg%P_code_tnit=0
pg%P_tnitopt_gauss=0.
pg%P_scale_tnitopt=0.
pg%P_code_rationit=0
pg%P_code_hourly_wfps_nit=0
pg%P_kdesat=0.
pg%P_Kd=0.
pg%P_tdenitopt_gauss=0.
pg%P_scale_tdenitopt=0.
pg%P_code_pdenit=0
pg%P_cmin_pdenit=0.
pg%P_cmax_pdenit=0.
pg%P_min_pdenit=0.
pg%P_max_pdenit=0.
pg%P_code_ratiodenit=0
pg%P_code_hourly_wfps_denit=0

! codesnow !!!!!!!!!!!!!!!!!!!!!!!
pg%P_codesnow=0
!!!!!!!!!!!!!!!!!!!!!!!!

! DR 03122020 merge trunk
! DR 04/06/2019 ajout d'un nouveau parametre P_tmin_mineralisation!!!!!!!
pg%P_tmin_mineralisation = 0
!!!!!!!!!!!!!!!!!!!!!!!!
  ! Loic Fevrier 2021 : New mineralisation model Clivot
  pg%P_Gmin1=0.
  pg%P_Gmin2=0.
  pg%P_Gmin3=0.
  pg%P_Gmin4=0.
  pg%P_Gmin5=0.
  pg%P_Gmin6=0.
  pg%P_Gmin7=0.
  ! Loic Fevrier 2021 : New mineralisation model Clivot (FIN)


return
end subroutine Parametres_Generaux_Zero

end module Parametres_Generaux
 
 
