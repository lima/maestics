! Module of USM
!- Description  of the structure USM_
!- reading of the simulation description (files: plant, soil, itk, ...) in travail.usm
module USM

!: On utilise le module Sorties
  USE Messages


!: On force la declaration de toutes les variables
  implicit none
  ! 18/09/2012 j'enleve les allocatable et je mets une taille maxi
  integer, parameter  :: nb_plant_max = 2


!: The derived type'USM_'
  type USM_
! DR 18/07/2012 je redimmensionne les noms des fichiers de 25 a 50 pour prendre en compte les noms de fichiers pour optimistics
    character(len=12)       :: P_codesimul !   // PARAMETER // simulation code (culture ou feuille=lai force) // SD // P_USM/USMXML // 0
    integer                 :: codoptim   ! code optimisation : 0 (pas d'opti), 1 (opti plante principale), 2 (opti plante associee)
    integer                 :: P_codesuite  !   // PARAMETER // code for successive P_USM ( 1=yes, 0=no) // SD // P_USM/USMXML // 0
    character(len=40)       :: P_nomSimulation !  // PARAMETER // name of the P_USM // SD // P_USM // 0
    integer                 :: P_iwater !   // PARAMETER // julian day of the beginning of the simulation // jour julien // P_USM // 1
    integer                 :: P_ifwater !   // PARAMETER // julian day of the end of simulation // julian day // P_USM // 1
    integer                 :: P_nbplantes !  // PARAMETER // number of simulated plants // SD // P_USM/USMXML // 0

    character(len=50)       :: P_ficInit !  // PARAMETER // name of the initialization file  // SD // P_USM // 0
    integer                 :: P_ichsl  !   // PARAMETER // soil numer in the  param.soil  file // SD // P_USM // 1
    character(len=50)       :: P_nomSol !  // PARAMETER // name of soil // SD // P_USM/USMXML // 0
    character(len=50)       :: P_ficStation !  // PARAMETER // name of station file // SD // P_USM // 0
    character(len=50)       :: P_wdata1 !   // PARAMETER // name of the beginning climate file // SD // P_USM // 0
    character(len=50)       :: P_wdata2 !   // PARAMETER // name of the ending climate file // SD // P_USM // 0
    integer                 :: nbans ! le nombre d'annees de la simulation
    integer                 :: P_culturean !  // PARAMETER // crop status 1 = over 1 calendar year ,other than 1  = on two calendar years (winter crop in northern hemisphere) // code 0/1 // P_USM/USMXML // 0
    integer                 :: P_ifwater0
    integer                 :: P_culturean0

!    character(len=50),dimension(:),allocatable :: P_fplt !   // PARAMETER // name of the plant file // SD // P_USM/USMXML // 0
!    character(len=50),dimension(:),allocatable :: P_ftec !   // PARAMETER // name of the technique file // SD // P_USM/USMXML // 0
!    character(len=50),dimension(:),allocatable :: P_flai !   // PARAMETER // name of the file LAI // SD // P_USM/USMXML // 0
  ! 18/09/2012 j'enleve les allocatable et je mets une taille maxi
    character(len=50),dimension(nb_plant_max) :: P_fplt !   // PARAMETER // name of the plant file // SD // P_USM/USMXML // 0
    character(len=50),dimension(nb_plant_max) :: P_ftec !   // PARAMETER // name of the technique file // SD // P_USM/USMXML // 0
    character(len=50),dimension(nb_plant_max) :: P_flai !   // PARAMETER // name of the file LAI // SD // P_USM/USMXML // 0

    integer :: file_Method_Optimistics_Station; !   // method to read the name of the station file for optimistics
    integer :: file_Method_Optimistics_Init; !   // method to read the name of the Init file for optimistics
    integer :: file_Method_Optimistics_Sol; !   // method to read the name of the sol file for optimistics
    integer,dimension(nb_plant_max) :: file_Method_Optimistics_Tec; !   // method to read the name of the tec file for optimistics
    integer,dimension(nb_plant_max) :: file_Method_Optimistics_Plt; !   // method to read the name of the plt file for optimistics
    integer,dimension(nb_plant_max) :: file_Method_Optimistics_lai; !   // method to read the name of the lai file for optimistics


  end type USM_

contains

!****f* USM_Lire
! NAME
!   USM_Lire - Fonction de lecture d'une P_USM
!
! DESCRIPTION
!
!***
  !subroutine USM_Lire(usm, path, pathusm )
  subroutine USM_Lire(usm) !, path, pathusm )

!  USE Stics , only : path, pathusm
!  USE Stics


 !     type(stics_communs_), intent(in) :: sc !enabling_record
      type(USM_), intent(INOUT) :: usm  ! // PARAMETER // name of the usm // SD // USMXML // 0 




      character(len=25) :: vartmp ! variable de lecture temporaire.  
      integer :: i ! variable temporaire  
      integer :: i_xml
! DR le 21/06/2017 pour David et la calibration Atcha , je force le LAI pour qi le fichier est present sinon on ne peut pas faire d'optimisation
!  ou de lancement en usms independantes
! ******* forcage LAI a regarder quand on modifiera l'usm pour ajouter un code : codeforcage_lai
!      integer :: i_lai
    ! on utilise une variable logique pour tester l'existence du fichier
      logical::exists  


    type(File_), pointer :: usm_file
    logical :: file_open
    integer :: fusm
    integer :: ret
    
    !call open_file(stics_files,'usm',usm_file, file_open)
    call open_file('usm',usm_file, file_open)
    exists = existFile(usm_file)
    
    if (.NOT.exists) then
        call EnvoyerMsgErreur( "Usm file doesn't exist : ",char(usm_file%path) )
    end if
    
    if ( .NOT.file_open ) then
        call EnvoyerMsgErreur( "Error opening usm file: ", char(usm_file%path) )
    end if
 
    if ( any((/.NOT.exists,.NOT.file_open/)) ) then
       call exit(9)
    end if

    fusm = usm_file%unit

! ** Le type de simulation ('feuille' pour forcer le lai a partir d'un fichier)
! ** lecture d'un code determinant le mode de simulation
! *- P_codesimul = 'culture' --> mode normal
! *- P_codesimul = 'feuille' --> mode feuille (1 annee maxi)
      read(fusm,*,err = 250, end = 90) vartmp
      read(fusm,*,err = 250, end = 90) usm%P_codesimul


!: Lecture du code d'optimisation
      read(fusm,*,err = 250, end = 90) vartmp
      read(fusm,*,err = 250, end = 90) usm%codoptim

!: Lecture du code de poursuite de simulation
      read(fusm,*,err = 250, end = 90) vartmp
      read(fusm,*,err = 250, end = 90) usm%P_codesuite

!: Lecture des parametres de la simulation
      read(fusm,*,err = 250, end = 90) vartmp
      read(fusm,*,err = 250, end = 90) usm%P_nbplantes

      read(fusm,*,err = 250, end = 90) vartmp
      read(fusm,*,err = 250, end = 90) usm%P_nomSimulation

      read(fusm,*,err = 250, end = 90) vartmp
      read(fusm,*,err = 250, end = 90) usm%P_iwater

      read(fusm,*,err = 250, end = 90) vartmp
      read(fusm,*,err = 250, end = 90) usm%P_ifwater

      read(fusm,*,err = 250, end = 90) vartmp
      read(fusm,*,err = 250, end = 90) usm%P_ficInit

      read(fusm,*,err = 250, end = 90) vartmp
      read(fusm,*,err = 250, end = 90) usm%P_ichsl

      read(fusm,*,err = 250, end = 90) vartmp
      read(fusm,*,err = 250, end = 90) usm%P_nomsol

      read(fusm,*,err = 250, end = 90) vartmp
      read(fusm,*,err = 250, end = 90) usm%P_ficStation

      read(fusm,*,err = 250, end = 90) vartmp
      read(fusm,*,err = 250, end = 90) usm%P_wdata1

      read(fusm,*,err = 250, end = 90) vartmp
      read(fusm,*,err = 250, end = 90) usm%P_wdata2

      read(fusm,*,err = 250, end = 90) vartmp
      read(fusm,*,err = 250, end = 90) usm%nbans

      read(fusm,*,err = 250, end = 90) vartmp
      read(fusm,*,err = 250, end = 90) usm%P_culturean



      do i = 1, usm%P_nbplantes

        read(fusm,*,err = 250, end = 90) vartmp
        read(fusm,*,err = 250, end = 90) usm%P_fplt(i)
      ! fic_plante
        i_xml=index(usm%P_fplt(i),'.xml')
        if(i_xml.eq.0)usm%file_Method_Optimistics_Plt(i)=1

        read(fusm,*,err = 250, end = 90) vartmp
        read(fusm,*,err = 250, end = 90) usm%P_ftec(i)
      ! fictec
        i_xml=index(usm%P_ftec(i),'.xml')
        if(i_xml.eq.0)usm%file_Method_Optimistics_Tec(i)=1

! DR 23/10/07 je crois que Pul l'avait mis en attente il faut le reactiver
        read(fusm,*,err = 250, end = 90) vartmp
        read(fusm,*,err = 250, end = 90) usm%P_flai(i)
      ! ficlai !!!regarder car ce n'est pas un xml
        i_xml=index(usm%P_flai(i),'.xml')
        if(i_xml.eq.0)usm%file_Method_Optimistics_lai(i)=1

! DR le 21/06/2017 pour David et la calibration Atcha , je force le LAI pour qi le fichier est present sinon on ne peut pas faire d'optimisation
!  ou de lancement en usms independantes
! ******* forcage LAI a regarder quand on modifiera l'usm pour ajouter un code : codeforcage_lai
!        i_lai=index(usm%P_flai(i),'.lai')
!        if(i_lai.ne.0.and.usm%P_flai(i).ne.'default.lai'.and.usm%P_flai(i).ne.'null')then
!            usm%P_codesimul='feuille'
!            call EnvoyerMsgHistorique(157)
!        endif
      end do

! DR 17/07/2012 pour gerer les lectures de fichiers parametres pour Optimistics on regarde si les noms de fichiers sont des xml ou pas
! fic_init
      i_xml=index(usm%P_ficInit,'.xml')
      if(i_xml.eq.0)usm%file_Method_Optimistics_Init=1
      ! fic_station
      i_xml=index(usm%P_ficStation,'.xml')
      if(i_xml.eq.0)usm%file_Method_Optimistics_Station=1
! DR 10/09/2012 j'ajoute le nom du fichier sol , ici c'etait le nom du sol mais il ne servait a rien donc on peut l'utiliser
      ! fic_sol
      i_xml=index(usm%P_nomsol,'.sol')
      if(i_xml>0) usm%file_Method_Optimistics_Sol=1


! DR 24/09/2018 pour l'histoire des pbs d'enchainement en serie climatique je conserve le P_ifwater initial
      usm%P_ifwater0 = usm%P_ifwater
      usm%P_culturean0 = usm%P_culturean
      ret = closeFile(usm_file)
      return



90    call EnvoyerMsgHistorique('End of usm file :  missing value for ',vartmp)
      call EnvoyerMsgErreur('End of usm file :  missing value for ',vartmp)
      call exit(9)
      
250   call EnvoyerMsgHistorique('Error reading usm file: ', char(usm_file%path))
      call EnvoyerMsgErreur('Error reading usm file: ', char(usm_file%path))
      call exit(9)
      
      
!       \O/        __O__
!        |           |   
!       / \         / \  

  end subroutine USM_Lire

end module USM
 
 
