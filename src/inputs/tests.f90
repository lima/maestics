module Tests

! DR Module to test the parameters after the reading and stop the running if necessary

USE iso_varying_string
USE Messages

interface test_parameters
module procedure c1_p1_test_parameters,c1_p2_test_parameters,test_parameters_superieur
end interface test_parameters

logical::test=.FALSE.

contains


subroutine test_parameters_superieur(value_param_min,value_param_max,etiq)
! sp de test pour verifier que le min est < au max
    real, intent(IN)               :: value_param_min
    real, intent(IN)               :: value_param_max
    integer, intent(IN)            :: etiq

    if (.NOT.TEST) return

    if (value_param_min .gt.value_param_max)then
            call EnvoyerMsgHistorique(etiq)
            call EnvoyerMsgHistorique('min',value_param_min)
            call EnvoyerMsgHistorique('max',value_param_max)
            call exit(9)
    endif
end subroutine test_parameters_superieur

subroutine c1_p1_test_parameters(value_code,value_param_c1,etiq1,value_param_c2,etiq2)
! pour l'instant ne sert a rien , a voir
    integer, intent(IN)            :: value_code
    real, intent(IN)               :: value_param_c1
    integer, intent(IN)            :: etiq1
    real, intent(IN)               :: value_param_c2
    integer, intent(IN)            :: etiq2

    if (.NOT.TEST) return

    if (value_code .eq. 1)then
         if(int(value_param_c1).eq.-999)then
            call EnvoyerMsgHistorique(etiq1,value_param_c1)
         endif
    endif
    if (value_code .eq. 2)then
         if(int(value_param_c1).eq.-999)then
            call EnvoyerMsgHistorique(etiq2,value_param_c2)
         endif
    endif

end subroutine c1_p1_test_parameters

subroutine c1_p2_test_parameters(code_name,value_code, &
             value_c1_p1,value_c1_p2,value_c1_p3,value_c1_p4,value_c1_p5,value_c1_p6,value_c1_p7,&
             value_c1_p8,value_c1_p9,value_c1_p10,value_c1_p11,value_c1_p12, etiq_c1,            &
             value_c2_p1,value_c2_p2,value_c2_p3,value_c2_p4,value_c2_p5,value_c2_p6,value_c2_p7,&
             value_c2_p8,value_c2_p9,value_c2_p10,value_c2_p11,value_c2_p12, etiq_c2,            &
             value_c3_p1,value_c3_p2,value_c3_p3,value_c3_p4,value_c3_p5,value_c3_p6,value_c3_p7,&
             value_c3_p8,value_c3_p9,value_c3_p10,value_c3_p11,value_c3_p12, etiq_c3)

! sp de test sur la valeur -999 des parametres normalement presents car option activee
! 3 modalites a l'option possibles
! 12 parametres par modalites possibles
    character(len=*), intent(IN)   :: code_name
    integer, intent(IN)            :: value_code
    real, intent(IN), optional     :: value_c1_p1
    real, intent(IN), optional     :: value_c1_p2
    real, intent(IN), optional     :: value_c1_p3
    real, intent(IN), optional     :: value_c1_p4
    real, intent(IN), optional     :: value_c1_p5
    real, intent(IN), optional     :: value_c1_p6
    real, intent(IN), optional     :: value_c1_p7
    real, intent(IN), optional     :: value_c1_p8
    real, intent(IN), optional     :: value_c1_p9
    real, intent(IN), optional     :: value_c1_p10
    real, intent(IN), optional     :: value_c1_p11
    real, intent(IN), optional     :: value_c1_p12
    integer, intent(IN), optional           :: etiq_c1
    real, intent(IN), optional   :: value_c2_p1
    real, intent(IN), optional   :: value_c2_p2
    real, intent(IN), optional   :: value_c2_p3
    real, intent(IN), optional   :: value_c2_p4
    real, intent(IN), optional   :: value_c2_p5
    real, intent(IN), optional   :: value_c2_p6
    real, intent(IN), optional   :: value_c2_p7
    real, intent(IN), optional   :: value_c2_p8
    real, intent(IN), optional   :: value_c2_p9
    real, intent(IN), optional   :: value_c2_p10
    real, intent(IN), optional   :: value_c2_p11
    real, intent(IN), optional   :: value_c2_p12
    integer, intent(IN) ,optional           :: etiq_c2
    real, intent(IN), optional   :: value_c3_p1
    real, intent(IN), optional   :: value_c3_p2
    real, intent(IN), optional   :: value_c3_p3
    real, intent(IN), optional   :: value_c3_p4
    real, intent(IN), optional   :: value_c3_p5
    real, intent(IN), optional   :: value_c3_p6
    real, intent(IN), optional   :: value_c3_p7
    real, intent(IN), optional   :: value_c3_p8
    real, intent(IN), optional   :: value_c3_p9
    real, intent(IN), optional   :: value_c3_p10
    real, intent(IN), optional   :: value_c3_p11
    real, intent(IN), optional   :: value_c3_p12
    integer, intent(IN) ,optional           :: etiq_c3

    if (.NOT.TEST) return

    if (value_code .eq. 1)then
 !        call envoi_message (code_name,value_code, &
          call envoi_message (code_name, &
             value_c1_p1,value_c1_p2,value_c1_p3,value_c1_p4,value_c1_p5,value_c1_p6,value_c1_p7,&
             value_c1_p8,value_c1_p9,value_c1_p10,value_c1_p11,value_c1_p12, etiq_c1)
    endif
    if (value_code .eq. 2)then
       !   call envoi_message (code_name,value_code, &
         call envoi_message (code_name, &
             value_c2_p1,value_c2_p2,value_c2_p3,value_c2_p4,value_c2_p5,value_c2_p6,value_c2_p7,&
             value_c2_p8,value_c2_p9,value_c2_p10,value_c2_p11,value_c2_p12, etiq_c2)
    endif
    if (value_code .eq. 3)then
     !     call envoi_message (code_name,value_code, &
          call envoi_message (code_name, &
             value_c3_p1,value_c3_p2,value_c3_p3,value_c3_p4,value_c3_p5,value_c3_p6,value_c3_p7,&
             value_c3_p8,value_c3_p9,value_c3_p10,value_c3_p11,value_c3_p12, etiq_c3)
    endif

end subroutine c1_p2_test_parameters

!DR 03122020 merge trunk
!subroutine envoi_message (code_name, value_code, value_p1, value_p2, value_p3, value_p4, value_p5,  &
subroutine envoi_message (code_name, value_p1, value_p2, value_p3, value_p4, value_p5,  &
                          value_p6,value_p7, value_p8, value_p9, value_p10, value_p11, value_p12, etiq)
! gere l'envoie des messages suivant les etiquettes
    character(len=*), intent(IN)   :: code_name
!    integer, intent(IN)            :: value_code  ! non utilisee

    real, intent(IN),optional               :: value_p1
    real, intent(IN),optional               :: value_p2
    real, intent(IN),optional               :: value_p3
    real, intent(IN),optional               :: value_p4
    real, intent(IN),optional               :: value_p5
    real, intent(IN),optional               :: value_p6
    real, intent(IN),optional               :: value_p7
    real, intent(IN),optional               :: value_p8
    real, intent(IN),optional               :: value_p9
    real, intent(IN),optional               :: value_p10
    real, intent(IN),optional               :: value_p11
    real, intent(IN),optional               :: value_p12
    integer, intent(IN),optional            :: etiq

          if (present(value_p1).and.int(value_p1).eq.-999) then
              call EnvoyerMsgHistorique(etiq,code_name)
              call exit(9)
          endif
          if (present(value_p2).and.int(value_p2).eq.-999) then
              call EnvoyerMsgHistorique(etiq,code_name)
              call exit(9)
          endif
          if (present(value_p3).and.int(value_p3).eq.-999) then
              call EnvoyerMsgHistorique(etiq,code_name)
              call exit(9)
          endif
          if (present(value_p4).and.int(value_p4).eq.-999) then
              call EnvoyerMsgHistorique(etiq,code_name)
              call exit(9)
          endif
          if (present(value_p5).and.int(value_p5).eq.-999) then
              call EnvoyerMsgHistorique(etiq,code_name)
              call exit(9)
          endif
          if (present(value_p6).and.int(value_p6).eq.-999) then
              call EnvoyerMsgHistorique(etiq,code_name)
              call exit(9)
          endif
          if (present(value_p7).and.int(value_p7).eq.-999) then
              call EnvoyerMsgHistorique(etiq,code_name)
              call exit(9)
          endif
          if (present(value_p8).and.int(value_p8).eq.-999) then
              call EnvoyerMsgHistorique(etiq,code_name)
              call exit(9)
          endif
          if (present(value_p9).and.int(value_p9).eq.-999) then
              call EnvoyerMsgHistorique(etiq,code_name)
              call exit(9)
          endif
          if (present(value_p10).and.int(value_p10).eq.-999) then
              call EnvoyerMsgHistorique(etiq,code_name)
              call exit(9)
          endif
          if (present(value_p11).and.int(value_p11).eq.-999) then
              call EnvoyerMsgHistorique(etiq,code_name)
              call exit(9)
          endif
          if (present(value_p12).and.int(value_p12).eq.-999) then
              call EnvoyerMsgHistorique(etiq,code_name)
              call exit(9)
          endif

end subroutine envoi_message

end module Tests
