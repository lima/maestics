subroutine get_forcing_parameters( nompar, valpar,nbpar)
    USE SticsFiles
    USE Messages
    USE iso_varying_string
    
    implicit none
    
    integer,parameter :: nb_parameters_max = 300
    
    real, intent(OUT)    :: valpar(nb_parameters_max)
    type(varying_string), intent(OUT) :: nompar (nb_parameters_max)
    integer, intent(OUT)           :: nbpar
    type(File_), pointer :: optim_file
    logical :: file_open,exists
    integer :: foptim
    integer :: ret
    integer :: i
    integer :: par_nb
    character(len=30) :: parname


    ! Getting param.sti infos
    !call open_file(stics_files,'optim',optim_file, file_open)
    call open_file('optim',optim_file, file_open)
    
    !print *, char(optim_file%path), len(optim_file%path)
      
      exists = existFile(optim_file)
    if (.NOT.exists) then
        call EnvoyerMsgHistorique("Forcing parameters file doesn't exist : ",char(optim_file%path))
        call EnvoyerMsgErreur( "Forcing parameters file doesn't exist : ",char(optim_file%path) )
    end if
    
    if ( .NOT.file_open ) then
        call EnvoyerMsgHistorique("Error opening Forcing parameters file: ", char(optim_file%path))
        call EnvoyerMsgErreur( "Error opening Forcing parameters file: ", char(optim_file%path) )
    end if
 
    if ( any((/.NOT.exists,.NOT.file_open/)) ) then
       call exit(9)
    end if

    foptim = optim_file%unit
      
    read(foptim,*) par_nb ! le nb de parametres a lire
        
    do i = 1, par_nb
        ! call get(foptim,nompar(i))
        ! read (foptim,*) nompar(i)
        read (foptim,*, end = 80, err = 250) parname 
        nompar(i) = parname
        read (foptim,*, end = 80,  err = 250) valpar(i)
    enddo
        
    nbpar = par_nb
    
    ret = closeFile(optim_file)
    return

80    call EnvoyerMsgHistorique('Error in forcing parameters file, missing lines: ', char(optim_file%path))
      call EnvoyerMsgErreur('Error in forcing parameters file, missing lines: ', char(optim_file%path))
      call exit(9)
      
250   call EnvoyerMsgHistorique('Error reading forcing parameters file: ', char(optim_file%path))
      call EnvoyerMsgErreur('Error reading forcing parameters file: ', char(optim_file%path))
      call exit(9)

end subroutine get_forcing_parameters
