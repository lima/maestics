! subroutine of reading initialization parameters/
! - reading of the initializations parameters in the file ficini.txt
subroutine lecinitialisations(sc,p,soil,sta)

USE Stics
USE Plante
USE Sol

USE Messages
USE Station

implicit none

    type(Stics_Communs_), intent(INOUT) :: sc  
    type(Plante_),        intent(INOUT) :: p(sc%P_nbplantes)  
    type(Sol_),           intent(INOUT) :: soil  

    !!!!!!!!!!! pour init snow !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(Station_), intent(INOUT) :: sta  

    character(len=30) :: nomVar
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
! Variables locales
    integer :: nbp  !  
    integer :: i  !  
    integer :: k  


    type(File_), pointer :: ini_file
    logical :: file_open
    integer :: fini
    integer :: ret
    logical :: error
    
    error = .FALSE.



      ! Getting ini file infos
      ! TODO : set file_open as an output of lecinitialisations
      !call open_file(stics_files,'ini',ini_file, file_open)
      call open_file('ini',ini_file, file_open)

      fini = ini_file%unit

      read (fini,*,err = 250, end = 90) nomVar
      read (fini,*,err = 250, end = 90) nbp

      if (nbp /= sc%P_nbplantes) then
         call EnvoyerMsgHistorique(5162, sc%P_nbplantes)
        !stop
        call exit(9)
      endif

      do i = 1, sc%P_nbplantes
        read (fini,*,err = 250, end = 90) nomVar
        read (fini,*,err = 250, end = 90) p(i)%P_stade0
        read (fini,*,err = 250, end = 90) p(i)%P_lai0
        read (fini,*,err = 250, end = 90) p(i)%P_magrain0
        read (fini,*,err = 250, end = 90) p(i)%P_zrac0
        read (fini,*,err = 250, end = 90) nomVar
        read (fini,*,err = 250, end = 90) p(i)%P_code_acti_reserve_ini
 
! Loic fevrier 2021
! code_acti_reserve == 1
        read (fini,*,err = 250, end = 90) p(i)%P_maperenne0
        read (fini,*,err = 250, end = 90) p(i)%P_QNperenne0
        read (fini,*,err = 250, end = 90) p(i)%P_masecnp0
        read (fini,*,err = 250, end = 90) p(i)%P_QNplantenp0
! code_acti_reserve == 2
        read (fini,*,err = 250, end = 90) p(i)%P_masec0
        read (fini,*,err = 250, end = 90) p(i)%P_QNplante0
        read (fini,*,err = 250, end = 90) p(i)%P_restemp0
        read (fini,*)
        read (fini,*,err = 250, end = 90) (p(i)%P_densinitial(k),k = 1,nblayers_max)   !5)
      end do
! Loic Fevrier 20201 : ces lignes de code servent � sauter des lignes de lecture quand il n'y a qu'une seule plante
      if (sc%P_nbplantes == 1) then
        do k = 1,16
          read (fini,*)
        enddo
      endif
      
      read (fini,*) nomVar
      read (fini,*,err = 250, end = 90) (sc%P_Hinitf(k),k = 1,nblayers_max)   !5)
      read (fini,*) nomVar
      read (fini,*,err = 250, end = 90) (soil%P_NO3initf(k),k = 1,nblayers_max)   !5)
      read (fini,*) nomVar
      read (fini,*,err = 250, end = 90) (sc%P_NH4initf(k),k = 1,nblayers_max)   !5)

      ! snow initializations detection 
      ! 2022/04/14, PL: changes according to other init variables
      ! used ini initialisation files.
      read (fini,*,end=80,err=250) nomVar
      read (fini,*,end=80,err=250) nomVar
      if (nomVar == 'Sdepth0') then
        read (fini,*,end=90,err=250) sta%P_Sdepth0
        ! print *, 'Sdepth0: ',sta%P_Sdepth0
      endif
      read (fini,*,end=80,err=250) nomVar
      if (nomVar == 'Sdry0') then
        read (fini,*,end=90,err=250) sta%P_Sdry0
        !print *, 'Sdry0: ',sta%P_Sdry0
      endif
      read (fini,*,end=80,err=250) nomVar
      if (nomVar == 'Swet0') then
        read (fini,*,end=90,err=250) sta%P_Swet0
        !print *, 'Swet0: ',sta%P_Swet0
      endif
      read (fini,*,end=80,err=250) nomVar
      if (nomVar == 'ps0') then
        read (fini,*,end=90,err=250) sta%P_ps0
        !print *, 'ps0: ',sta%P_ps0
      endif
      
      if (error) then
         call exit(9)
      end if 
      
      return

80    ret = closeFile(ini_file)
      !print *, "ret : ", ret
      return
      
90    call EnvoyerMsgHistorique('End of ini file :  missing value for ',nomVar)
      call EnvoyerMsgErreur('End of ini file :  missing value for ',nomVar)
      !return
      error = .TRUE.
          
250   call EnvoyerMsgHistorique('Error reading ini file: ',char(ini_file%path))
      call EnvoyerMsgErreur('Error reading ini file: ',char(ini_file%path))
      !call exit(9)
      error = .TRUE.
      

end
 
 
