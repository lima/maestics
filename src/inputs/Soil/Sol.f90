! Soil Module
! DR 14/02/2020 prise en compte de la participation de Nicolas Beaudoin sur les algos concerant
!  la prise en compte des cailloux dans Stics
module Sol


USE Messages

USE Stics ,only: nbCouchesSol_max, nbLayers_max

type Sol_

  integer :: itrav1  
  integer :: itrav2  
  integer :: nstoc0  
  integer :: profcalc  

  real :: Ndenit      ! // OUTPUT // Daily denitrification of nitrogen from fertiliser or soil (if option  denitrification  is activated)" // kg.ha-1.j-1
  real :: Norgeng      ! // OUTPUT // Daily organisation of nitrogen from fertiliser // kgN.ha-1.j-1
  real :: Nvolorg  
  real :: Nvoleng      ! // OUTPUT // Daily volatilisation of nitrogen from fertiliser // kgN.ha-1.j-1
  real :: QNdenit      ! // OUTPUT // "Cumulated denitrification of nitrogen from fertiliser or soil (if option  denitrification  is activated)" // kgN.ha-1
  real :: QNorgeng      ! // OUTPUT // Cumulated organisation of nitrogen from fertiliser // kgN.ha-1
  real :: QNvolorg      ! // OUTPUT // Cumulated volatilisation of nitrogen from organic inputs // kgN.ha-1
  real :: Nvolatorg
  real :: QNgaz
  real :: Qminrcult  
  real :: cumvminr      ! // OUTPUT // daily mineral nitrogen arising from humus // kgN.ha-1.j-1
  real :: qdraincum      ! // OUTPUT // Cumulated quantity of water evacuated towards drains // mm
  real :: qdrain      ! // OUTPUT // Flow rate towards drains // mm j-1
  real :: remontee      ! // OUTPUT // Capillary uptake in the base of the soil profile // mm j-1
  real :: hmax      ! // OUTPUT // Maximum height of water table between drains // cm
  real :: hnappe      ! // OUTPUT // Height of water table with active effects on the plant // cm
  real :: hph      ! // OUTPUT // Maximum depth of perched water table // cm
  real :: hpb      ! // OUTPUT // Minimum depth of perched water table // cm
  real :: qlesd      ! // OUTPUT // Cumulated N-NO3 leached into drains // kgN.ha-1
  real :: Ldrains  
  real :: nitrifj      ! // OUTPUT // Daily nitrification of nitrogen (if option  nitrification  is activated) // kg.ha-1
  real :: profnappe      ! // OUTPUT // Depth of water table // cm
  real :: condenit      ! // OUTPUT // Denitrifying condition rate regard to the potential // 0-1
  real :: concno3les      ! // OUTPUT // Nitrate concentration in drainage water // mg NO3 l-1
  real :: concN_W_drained ! // OUTPUT // daily nitrate concentration in drainage water // mg NO3 l-1
  real :: azlesd      ! // OUTPUT // Nitric nitrogen flow in drains // kgN.ha-1 j-1
  real :: AZnit(5)      ! // OUTPUT // Amount of nitric nitrogen in the horizon 3 (table)  // kgN.ha-1
  real :: QNvoleng      ! // OUTPUT // Cumulated volatilisation of nitrogen from fertiliser // kgN.ha-1
  real :: sumes00  
  real :: sumes10  
  real :: sumes20  
  real :: supres0  
  real :: ses2j00  
  real :: sesj00  
  real :: smes020  
  real :: stoc0  
  real :: minoffrN  
  real :: NO3init(5)  

  real :: vminr  
  real :: aevap  
  real :: amm(nbCouchesSol_max)
  real :: nit(nbCouchesSol_max)  ! pourquoi demarrer a 0 ?

  real :: P_profdenit  ! // PARAMETER // soil depth on which denitrification is active (with the appropriate option) // cm // PARSOL // 1
  integer :: P_codedenit  ! // PARAMETER // option to allow the calculation of denitrification :yes (1), no(2) // code 1/2 // PARSOL // 0
  real :: P_zesx  ! // PARAMETER // maximal depth of soil affected by soil evaporation // cm // PARSOL // 1
  real :: P_cfes  ! // PARAMETER // parameter defining the soil contribution to evaporation as a function of depth  // SD // PARSOL // 1
  real :: P_vpotdenit  ! // PARAMETER // potential rate of denitrification (per 1 cm soil layer) // kg ha-1 j-1 cm-1 // PARSOL // 1
  real :: P_z0solnu  ! // PARAMETER // roughness length of bare soil // m // PARSOL // 1

! ** parametres sol
  character(len=12) :: P_typsol  ! // PARAMETER // Soil type // * // PARSOL // 1

  integer :: P_numsol      ! // PARAMETER // Soil number in the file PARAM.SOL // * // PARSOL // 1
  integer :: P_typecailloux(5)     ! // PARAMETER // Pebbles type defined by a volumetric mass value (masvolx) and a field capacity moisture value (HCCCX) only used  if codecailloux=1 . (typecailloux= 1:calcaire B1,  2:calcaire B2,  3:calcaire L,  4:caillasse L,  5:gravier m,  6:silex, 7:granite, 8:calcaire J, 9-10:others) // SD // PARSOL // 1
  integer :: P_epd(5)     ! // PARAMETER //  mixing cells thickness (=2 x dispersivity) // cm // PARSOL // 1
  integer :: P_codecailloux  ! // PARAMETER // option of accounting of pebbles in the soil balances (1 = yes; 2 = no)  // code1/2 // PARSOL // 0
  integer :: P_codemacropor  ! // PARAMETER // "Simulation option of water flux in the macroporosity of soils to estimate water excess and drip by  overflowing : yes(1), no (0)" // code 1/2 // PARSOL // 0
  integer :: P_codefente  ! // PARAMETER // option allowing an additional water compartment for the swelling soils (1 = yes; 2 = no) // code1/2 // PARSOL // 0
  integer :: P_codrainage  ! // PARAMETER // artificial drainage (1 = yes; 2 = no)  // code1/2 // PARSOL // 0
  integer :: P_codenitrif  ! // PARAMETER // option to activate nitrification calculation (1 = yes; 2 = no) // code 1/2 // PARSOL // 0
  integer :: P_coderemontcap  ! // PARAMETER // option to activate capillary rise (1 = yes; 2 = no)  // code1/2 // PARSOL // 0
  integer :: izcel(5)  
  integer :: izc(5)  
  !integer :: iz_base_horizon(5)
  integer :: ncel  
  integer :: icel(0:nbCouchesSol_max)

  real :: P_NO3initf(5)     ! // PARAMETER // initial nitric nitrogen in the soil per horizon  // kg N ha-1 // INIT // 1
  real :: P_profhum  ! // PARAMETER // Humification depth  (max.60 cm) // cm // PARSOL // 1
  integer :: jhum     ! Depth of biologically active layer (cm)
  real :: P_pH0  ! // PARAMETER // Initial soil pH  // SD // PARSOL // 1
  real :: P_q0  ! // PARAMETER // Parameter of the end of the maximum evaporation stage  // mm // PARSOL // 1
  real :: P_ruisolnu  ! // PARAMETER // fraction of drip rainfall (by ratio at the total rainfall) on a bare soil  // between 0 and 1 // PARSOL // 1
  real :: P_obstarac  ! // PARAMETER // Soil depth which will block the root growth  // cm // PARSOL // 1
  real :: P_profimper  ! // PARAMETER // Upper depth of the impermeable layer (from the soil surface). May be greater than the soil depth // cm // PARSOL // 1
  real :: P_ecartdrain  ! // PARAMETER // inbetween drains distance // cm // PARSOL // 1
  real :: P_Ksol  ! // PARAMETER // hydraulic conductivity in the soil above and below the drains // SD // PARSOL // 1
  real :: P_profdrain  ! // PARAMETER // drain depth // cm // PARSOL // 1
  real :: P_DAF(5)     ! // PARAMETER // Table of bulk density of the fine earth fraction in each soil layer  // g cm-3 // PARSOL // 1
  real :: P_hminf(5)     ! // PARAMETER // gravimetric water content at wilting point of each soil layer (/fine earth) (table) // % w // PARSOL // 1
  real :: P_hccf(5)     ! // PARAMETER // gravimetric water content at field capacity of each soil layer (/fine earth) (table) // % w // PARSOL // 1
  real :: da(5)      ! // OUTPUT // Bulk density in the horizon 1 // g cm-3
  real :: P_epc(5)     ! // PARAMETER // thickness of each soil layer // cm   // PARSOL // 1      // OUTPUT // Thickness of the horizon (1 or 2 )// cm
  real :: hcc(5)  
  real :: hmin(5)  
  real :: P_cailloux(5)     ! // PARAMETER // stone volumetric content per horizon // m3 m-3 // PARSOL // 1
  real :: P_infil(0:5)     ! // PARAMETER // infiltrability parameter at the base of each layer (if codemacropor = 1) // mm day-1 // PARSOL // 1      // OUTPUT // Infiltrability parameter at the base of the horizon 1 // mm day-1
  real :: P_calc  ! // PARAMETER // percentage of calcium carbonate in the surface layer // % // PARSOL // 1
  real :: P_argi  ! // PARAMETER // percentage of clay in the surface layer  // % // PARSOL // 1
  real :: P_Norg  ! // PARAMETER // Organic Nitrogen  content of the tilled layer  (supposed constant on the depth  P_profhum) // % ponderal // PARSOL // 1
  real :: profsol  
  real :: P_albedo  ! // PARAMETER // P_albedo of the bare dry soil // SD // PARSOL // 1
  real :: P_humcapil  ! // PARAMETER // threshold of soil gravimetric water content under which capillary rise occurs // % w // PARSOL // 1
  real :: P_capiljour  ! // PARAMETER // capillary rise upward water flux // mm j-1 // PARSOL // 1
  real :: P_concseuil  ! // PARAMETER // Minimum Concentration of soil to NH4 // kgN ha-1 mm-1 // PARSOL // 1
  real :: da_ini(2)  
  real :: q0_ini  
  real :: zesx_ini  
  real :: cumvminh      ! // OUTPUT // daily mineral nitrogen arising from organic residues // kgN.ha-1.j-1
  real :: profhum_tass(2)  

  real :: P_pluiebat  ! // PARAMETER // minimal rain quantity for the crust occurrence // mm day-1 // PARSOL // 1
  real :: P_mulchbat  ! // PARAMETER // mulch depth from which a crust occurs // cm // PARSOL // 1
  real :: pHvol      ! // OUTPUT // soil surface pH varying after organic residue application (such as slurry) // SD
  real :: dpH  
  real :: P_CsurNsol0  ! // PARAMETER // Initial C to N ratio of soil humus // SD // PARSOL // 1 ! Bruno-declaration du parametre CN du sol
  real :: CsurNsol     ! C to N ratio of soil throughout time
  real :: P_penterui   ! // PARAMETER // runoff coefficient taking account for plant mulch // SD // PARSOL // 1 ! 27/07/2012 ajout de ce parametre pour le bresil

!DR 10/09/2012 j'ajoute le sol pour Patrice
  character(len=50) :: ficsol
  real  :: N_volatilisation  ! // OUTPUT // Cumulated volatilisation of nitrogen from organic inputs and fertiliser // kgN.ha-1
  real  :: epc_recal(5)
  real  :: infil_recal(5)
  real  :: pH_soil

  real :: remonteemax ! // capillary rise from simulated deep soil layer (mm)
! Loic Fevrier 2021 : Migration finert depuis param_gen
  real :: P_finert  ! // PARAMETER // fraction of the humus stock inactive for mineralisation  (= stable OM/ total OM) // SD // PARSOL // 1


end type Sol_

contains



subroutine Sol_Lecture(soil,nbLayers_max)

implicit none

   type(Sol_), intent(INOUT) :: soil
   integer, intent(IN) :: nbLayers_max


! Variables locales
      integer :: icou  
    type(File_), pointer :: sol_file
    logical :: file_open, exists
    integer :: fsol
    integer :: ret
    !integer  :: set_name

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


    ! ouverture du fichier sol (TODO : nom a parametrer)
    ! DR 10/09/2012 on parametre le nom du sol pour optimistics
!      open(3,file = soil%ficsol,status = 'old')
      ! open(3,file = filepluspath,status = 'old') ! enabling_record

      ! setting soil file name
      ! setting file name
      !call set_file_name(stics_files,'sol',soil%ficsol, set_name) ! to be used to set status
      !call set_file_name('sol',soil%ficsol, set_name) ! to be used to set status
      ! Getting soil file infos
      ! TODO : set file_open as an output of Sol_Lecture_V6
      !call open_file(stics_files,'sol',sol_file, file_open)
      call open_file('sol',sol_file, file_open)
      
      exists = existFile(sol_file)
    if (.NOT.exists) then
        call EnvoyerMsgHistorique("Soil file doesn't exist : ",char(sol_file%path))
        call EnvoyerMsgErreur( "Soil file doesn't exist : ",char(sol_file%path) )
    end if
    
    if ( .NOT.file_open ) then
        call EnvoyerMsgHistorique("Error opening soil file: ", char(sol_file%path))
        call EnvoyerMsgErreur( "Error opening soil file: ", char(sol_file%path) )
    end if
 
    if ( any((/.NOT.exists,.NOT.file_open/)) ) then
       call exit(9)
    end if

      fsol = sol_file%unit

 ! DR 02/06/2017 remplacement des P_pH par P_pH0 et P_csurNsol par P_CsurNsol0
      read(fsol,*,err = 120) soil%P_numsol,soil%P_typsol,soil%P_argi,soil%P_Norg,soil%P_profhum,soil%P_calc,soil%P_pH0, &
                          soil%P_concseuil,soil%P_albedo,soil%P_q0,soil%P_ruisolnu,soil%P_obstarac,soil%P_pluiebat,     &
                          soil%P_mulchbat,soil%P_zesx,soil%P_cfes,soil%P_z0solnu,soil%P_CsurNsol0,soil%P_finert,        &
                          soil%P_penterui

      read(fsol,*,err = 120) soil%P_numsol,soil%P_codecailloux,soil%P_codemacropor,soil%P_codefente,   &
                          soil%P_codrainage,soil%P_coderemontcap,soil%P_codenitrif,soil%P_codedenit

      read(fsol,*,err = 120) soil%P_numsol,soil%P_profimper,soil%P_ecartdrain,soil%P_ksol,soil%P_profdrain,     &
                          soil%P_capiljour,soil%P_humcapil,soil%P_profdenit,soil%P_vpotdenit

! Modif Bruno mars 2018 pour calculer le nb de couches elementaires (nhe) sans attendre la routine initnonsol
!      nhe = 0
      do icou = 1,nbLayers_max   !5
        read(fsol,*,err = 120) soil%P_numsol,soil%P_epc(icou),soil%P_hccf(icou),soil%P_hminf(icou),soil%P_DAF(icou), &
                            soil%P_cailloux(icou),soil%P_typecailloux(icou),soil%P_infil(icou),soil%P_epd(icou)
        soil%P_epc(icou) = nint(soil%P_epc(icou))
!        nhe = nhe + int(soil%P_epc(icou))
      end do
    ! fermeture du fichier
      !close(fsol)
      ret = closeFile(sol_file)

    ! si on est arrive jusque la, pas d'erreur, on quitte la routine
      return


120   call EnvoyerMsgHistorique(137)
      call EnvoyerMsgErreur('Error in soil file, missing lines !')
      call exit(9)


end subroutine Sol_Lecture


subroutine Sol_Tests(soil, nbCouches, P_ichsl, P_masvolcx, beta_sol, nbLayers_max)

implicit none

    type(Sol_), intent(INOUT) :: soil
    integer,    intent(IN)    :: nbCouches
    integer,    intent(IN)    :: P_ichsl          ! // PARAMETER // "soil number in the param.soil  file" // SD // P_USM // 1
    real,       intent(IN)    :: P_masvolcx(10)   ! taille fixe, a definir/parametrer ? 	  // PARAMETER // volumetric mass (bulk) of pebbles // g cm-3 // PARAM // 1 
    real,       intent(INOUT) :: beta_sol(2)
    integer,    intent(IN)    :: nbLayers_max

! Variables locales
    integer :: i  !  
    integer :: icou
    real    :: dax  

   ! Boucle pour unix et tests
    ! test sur P_typecailloux (ML le 22/03/04)
      do i = 1,nbLayers_max   !5
        ! if (soil%P_epc(i) /= 0.) then
        if (abs(soil%P_epc(i)).gt.1.0E-8) then
          if (soil%P_codecailloux == 2) then
            soil%P_cailloux(i) = 0.
            soil%P_typecailloux(i) = 1
          else
        ! domi 17/01/06 si on a coche cailloux et pas defini P_cailloux y'a un souci
        !  je rajoute un test
            ! if (soil%P_cailloux(i) /= 0 .and. soil%P_typecailloux(i) == 0) then
            if (abs(soil%P_cailloux(i)).gt.1.0E-8 .and. soil%P_typecailloux(i) == 0) then
              call EnvoyerMsgHistorique(216)
              !stop
              call exit(9)
            endif
            ! if (soil%P_cailloux(i) == 0 .and. soil%P_typecailloux(i) == 0) then
            if (abs(soil%P_cailloux(i)).lt.1.0E-8 .and. soil%P_typecailloux(i) == 0) then
              soil%P_typecailloux(i) = 1
            endif
             if (soil%P_typecailloux(i) .gt. 0.and. (soil%P_cailloux(i) .lt.0 .or.soil%P_cailloux(i) .gt.100) ) then
               call EnvoyerMsgHistorique(210)
               call exit(9)
            endif

          endif
        endif
      end do


      if (soil%P_numsol /= P_ichsl) then
        call EnvoyerMsgHistorique(136)
        !stop
        call exit(9)
      endif

      if (soil%P_profhum > nbCouches) then  ! il faudrait remplacer 1000 par nbCouches
        call EnvoyerMsgHistorique(138)
        !stop
        call exit(9)
      endif

      if (soil%P_profhum <= 0.) then
        call EnvoyerMsgHistorique(211)
         !stop
        call exit(9)
      endif
! NB le 15/07/05 activation de P_coderemontcap
      ! if (soil%P_coderemontcap == 2.) soil%P_capiljour = 0.
      if (abs(soil%P_coderemontcap-2.).lt.1.0E-8) soil%P_capiljour = 0.

    ! domi - 27/08/07
    ! on force les epaisseurs en entier
      do icou = 1,nbLayers_max   !5
        soil%P_epc(icou) = int(soil%P_epc(icou))
      enddo

    ! domi - 02/07/2002 - test pour pas avoir de pb d'unite avec hmin et P_hccf
      do icou = 1,nbLayers_max   !5
        ! if (soil%P_epc(icou) == 0.) CYCLE
        if (abs(soil%P_epc(icou)).lt.1.0E-8) CYCLE
        if (soil%P_hccf(icou) > 0 .and. soil%P_hccf(icou) < 1.) then
          call EnvoyerMsgHistorique(212)
          !stop
          call exit(9)
        endif
        if (soil%P_hminf(icou) > 0 .and. soil%P_hminf(icou) < 1.) then
          call EnvoyerMsgHistorique(213)
          !stop
          call exit(9)
        endif
      ! dr 10/01/08 on rajoute un test pour que P_hccf ne soit pas < P_hminf
        if (soil%P_hccf(icou) > 0 .and. (soil%P_hccf(icou) < soil%P_hminf(icou))) then
          call EnvoyerMsgHistorique(217)
          !stop
          call exit(9)
        endif

      end do

    ! profondeur maximale du sol = 10m
!      if (soil%P_obstarac == nbCouchesSol_max) soil%P_obstarac = nbCouchesSol_max-1
      if (abs(soil%P_obstarac - real(nbCouchesSol_max)).lt.1.0E-8) soil%P_obstarac = nbCouchesSol_max-1

    ! domi - 03/07/02
      if (soil%P_profhum <= 0.) then
        call EnvoyerMsgHistorique(214,soil%P_profhum)
        !stop
        call exit(9)
      endif

    ! DR et Julie 24/07/08 on introduit une constante sol_beta dependant
    ! de l'infiltrabilite et de la densite apparente initial entrant dans le
    ! calcul de l'infiltrabilite modifi en cas de detassement ou tassement

      do icou = 1,2

      ! DR 31/07/08 on prend en compte les P_cailloux on verra avec nadine s'il fallait le faire
      ! DA
        dax = P_masvolcx(soil%P_typecailloux(icou))
        soil%da(icou) = (soil%P_cailloux(icou) * dax + (100. - soil%P_cailloux(icou)) * soil%P_daf(icou)) / 100.
      ! DR 07/08/08 on rejoute un test pour les valeurs de P_infil a zero sinon plantade
        if (soil%P_infil(icou) <= 0) soil%P_infil(icou) = 0.001
        beta_sol(icou) = (log(soil%P_infil(icou)) + 3.9) / (soil%da(icou) - 2.7)
      ! on conserve aussi da
        soil%da_ini(icou) = soil%da(icou)

      enddo

      soil%q0_ini = soil%P_q0
      soil%zesx_ini = soil%P_zesx
      soil%profhum_tass(1) = soil%P_epc(1)
      soil%profhum_tass(2) = soil%P_profhum - soil%P_epc(1)

return
end subroutine Sol_Tests


subroutine sol_test_itk(itk,profhum)

USE Itineraire_Technique

    type(ITK_),        intent(IN) :: itk
    real,       intent(IN)    :: profhum
    integer i

    ! le 24/09/2012  test sur proftrav > P_profhum
      do i = 1,itk%P_nbjtrav
        if (itk%P_proftrav(i) > profhum) then
          call EnvoyerMsgHistorique(170)
           !stop
          call exit(9)
        endif
      enddo
end subroutine sol_test_itk



subroutine Sol_Ecriture(soil,pg,sc)

USE Parametres_Generaux
USE Stics
USE Messages

implicit none

    type(Sol_),                 intent(IN) :: soil  
    type(Parametres_Generaux_), intent(IN) :: pg  
    type(Stics_Communs_),       intent(IN) :: sc  

    character(len=250) :: tmp  
    integer :: icou  

       call EnvoyerMsgHistorique('')
       call EnvoyerMsgHistorique(135)
       call EnvoyerMsgHistorique(5005)
       call EnvoyerMsgHistorique(506)

 ! DR 02/06/2017 remplacement des P_pH par P_pH0
      write(tmp,'(a12,18f12.2)')soil%P_typsol,soil%P_argi,soil%P_Norg,soil%P_profhum,soil%P_calc,            &
                    soil%P_pH0,soil%P_concseuil,soil%P_albedo,soil%P_q0,soil%P_ruisolnu,soil%P_obstarac,     &
                    soil%P_pluiebat,soil%P_mulchbat,soil%P_zesx,soil%P_cfes,soil%P_z0solnu,soil%P_CsurNsol0, &
                    soil%P_finert,soil%P_penterui

      call EnvoyerMsgHistorique(tmp)
      call EnvoyerMsgHistorique(503)
! DR 03/11/2005 rajout message pour expliquer les codes
      call EnvoyerMsgHistorique(215)
      write(tmp,'(7i16)')soil%P_codecailloux,soil%P_codemacropor,soil%P_codefente,    &
                             soil%P_codrainage,soil%P_coderemontcap,soil%P_codenitrif,soil%P_codedenit


      call EnvoyerMsgHistorique(tmp)
      call EnvoyerMsgHistorique(504)

      write(tmp,'(8f13.2)') soil%P_profimper, soil%P_ecartdrain, soil%P_ksol, soil%P_profdrain,     &
                            soil%P_capiljour, soil%P_humcapil, soil%P_profdenit, soil%P_vpotdenit
      call EnvoyerMsgHistorique(tmp)
      call EnvoyerMsgHistorique(507)

      do icou = 1,nbLayers_max   !5
        write(tmp,703) soil%P_epc(icou),soil%P_hccf(icou),soil%P_hminf(icou),  &
                      soil%P_DAF(icou),soil%P_cailloux(icou),soil%P_typecailloux(icou),    &
                      soil%P_infil(icou),soil%P_epd(icou)
          call EnvoyerMsgHistorique(tmp)
703   format(5f14.2,i14,f14.2,i14)

      end do

      if (soil%P_codenitrif == 1) then
        call EnvoyerMsgHistorique('P_hminn',pg%P_hminn)
        call EnvoyerMsgHistorique('P_hoptn',pg%P_hoptn)
        call EnvoyerMsgHistorique('P_fnx',pg%P_fnx)
        call EnvoyerMsgHistorique('P_pHminnit',pg%P_pHminnit)
        call EnvoyerMsgHistorique('P_pHmaxnit',pg%P_pHmaxnit)
        call EnvoyerMsgHistorique('P_tnitmin',pg%P_tnitmin)
        call EnvoyerMsgHistorique('P_tnitopt',pg%P_tnitopt)
        call EnvoyerMsgHistorique('P_tnitmax',pg%P_tnitmax)
        call EnvoyerMsgHistorique('NH4initf1',sc%P_NH4initf(1))
        call EnvoyerMsgHistorique('NH4initf2',sc%P_NH4initf(2))
        call EnvoyerMsgHistorique('NH4initf3',sc%P_NH4initf(3))
        call EnvoyerMsgHistorique('NH4initf4',sc%P_NH4initf(4))
        call EnvoyerMsgHistorique('NH4initf5',sc%P_NH4initf(5))
      endif
      if (soil%P_codedenit == 1) then
        call EnvoyerMsgHistorique('P_profdenit',soil%P_profdenit)
        call EnvoyerMsgHistorique('P_vpotdenit',soil%P_vpotdenit)
        call EnvoyerMsgHistorique('P_ratiodenit',pg%P_ratiodenit)
      endif

return
end subroutine Sol_Ecriture



subroutine Sol_Zero(soil)

implicit none

  type(Sol_),intent(inout) :: soil  

      soil%itrav1 = 0
      soil%itrav2 = 0
      soil%nstoc0 = 0
      soil%profcalc = 0
      soil%P_profdenit = 0
      soil%P_codedenit = 0
      soil%P_numsol = 0
      soil%P_typecailloux(:) = 0
      soil%P_epd(:) = 0
      soil%P_codecailloux = 0
      soil%P_codemacropor = 0
      soil%P_codefente = 0
      soil%P_codrainage = 0
      soil%P_codenitrif = 0
      soil%P_coderemontcap = 0
      soil%izcel(:) = 0
      soil%izc(:) = 0
      !soil%iz_base_horizon(:)= 0
      soil%ncel = 0
      soil%icel(:) = 0
! reels
      soil%Ndenit = 0.
      soil%Norgeng = 0.
      soil%Nvolorg = 0.
      soil%Nvoleng = 0.
      soil%QNdenit = 0.
      soil%QNorgeng = 0.
      soil%QNvolorg = 0.
      soil%Nvolatorg = 0.
      soil%Qminrcult = 0.
      soil%cumvminr = 0.
      soil%qdraincum = 0.
      soil%qdrain = 0.
      soil%remontee = 0.
      soil%hmax = 0.
      soil%hnappe = 0.
      soil%hph = 0.
      soil%hpb = 0.
      soil%qlesd = 0.
      soil%Ldrains = 0.
      soil%nitrifj = 0.
      soil%profnappe = 0.
      soil%condenit = 0.
      soil%concno3les = 0.
      soil%concN_W_drained = 0.
      soil%azlesd = 0.
      soil%AZnit(:) = 0.
      soil%QNvoleng = 0.
      soil%sumes00 = 0.
      soil%sumes10 = 0.
      soil%sumes20 = 0.
      soil%supres0 = 0.
      soil%ses2j00 = 0.
      soil%sesj00 = 0.
      soil%smes020 = 0.
      soil%stoc0 = 0.
      soil%minoffrN = 0.
      soil%NO3init(:) = 0.
     ! sc%CO2sol = 0. Bruno  transferes dans STICS_Zero
     ! sc%QCO2sol = 0.
      soil%vminr = 0.
      soil%aevap = 0.
      soil%amm(:) = 0.
      soil%nit(:) = 0.
      soil%P_zesx = 0.
      soil%P_cfes = 0.
      soil%P_vpotdenit = 0.
      soil%P_z0solnu = 0.

      soil%P_NO3initf(:) = 0.
      soil%P_profhum = 0.
      soil%P_pH0 = 0.
      soil%P_q0 = 0.
      soil%P_ruisolnu = 0.
      soil%P_obstarac = 0.
      soil%P_profimper = 0.
      soil%P_ecartdrain = 0.
      soil%P_Ksol = 0.
      soil%P_profdrain = 0.
      soil%P_DAF(:) = 0.
      soil%P_hminf(:) = 0.
      soil%P_hccf(:) = 0.
      soil%da(:) = 0.
      soil%P_epc(:) = 0.
      soil%hcc(:) = 0.
      soil%hmin(:) = 0.
      soil%P_cailloux(:) = 0.
      soil%P_infil(:) = 0.
      soil%P_calc = 0.
      soil%P_argi = 0.
      soil%P_Norg = 0.
      soil%profsol = 0.
      soil%P_albedo = 0.
      soil%P_humcapil = 0.
      soil%P_capiljour = 0.
      soil%P_concseuil = 0.
      soil%da_ini(:) = 0.
      soil%q0_ini = 0.
      soil%zesx_ini = 0.
      soil%cumvminh = 0.
      soil%profhum_tass(:) = 0.
      soil%P_pluiebat = 0.
      soil%P_mulchbat = 0.
      soil%P_penterui = 0.
      soil%P_CsurNsol0 = 0.
      soil%CsurNsol = 0.
      soil%remonteemax = 0.
      soil%P_finert = 0.

! chaines de caracteres
      soil%P_typsol = ''

return
end subroutine Sol_Zero

end module Sol
 
 
