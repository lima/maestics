
! Read the selected output variables in the file var.mod
!! one wishes to have the values ​​written to the file mod_s*.sti
subroutine Lecture_VariablesSortiesJournalieres(sc)

USE Stics
USE STicsFiles
USE Messages

implicit none

    type(Stics_Communs_), intent(INOUT) :: sc  

! Variables locales
    integer :: i  !  
    integer ::  eof  
    
    type(File_), pointer :: var_file
    logical :: file_open, exists
    integer :: fvar
    integer :: ret



        
        !call open_file(stics_files,'var',var_file, file_open)
        call open_file('var',var_file, file_open)
        
        exists = existFile(var_file)
        if (.NOT.exists) then
            call EnvoyerMsgHistorique("Daily variable names file doesn't exist : ",char(var_file%path))
            call EnvoyerMsgErreur( "Daily variable names  file doesn't exist : ",char(var_file%path) )
        end if
        
        
        
        if ( exists .AND. .NOT.file_open ) then
            call EnvoyerMsgHistorique("Error opening daily variable names file: ", char(var_file%path))
            call EnvoyerMsgErreur( "Error opening daily variable names file: ", char(var_file%path) )
        end if
     
        if ( any((/.NOT.exists,.NOT.file_open/)) ) then
           ! call exit(9)
           stics_files%flag_jour = .FALSE.
           return
        end if
    
        fvar = var_file%unit

        sc%nbvarsortie = 0
        eof = 0
        i = 0
        sc%valpar(:) = ''   ! pour faire fonctionner le test sur la longueur de chaine plus bas,
                            ! sinon la chaine est remplie de caracteres de terminaison par defaut
                            ! et donc retourne une longueur de 19 (puisque valpar est un tableau de chaines de 19 car.)

        do
            i = i + 1
            !DR 30112020 merge trunk
            read(fvar,'(a40)',iostat=eof) sc%valpar(i)

            if (eof /= 0) EXIT
        end do

      ! si on a termine sur une ligne vide, on retranche 1
        if (len(trim(sc%valpar(i))) > 0) then
          sc%nbvarsortie = i
        else
          sc%nbvarsortie = i - 1
        endif

    ! on ferme le fichier
        ret = closeFile(var_file) 

return
end subroutine Lecture_VariablesSortiesJournalieres
 
 
