! reading the variables to write in the profile
!! profiles in prof.mod
subroutine Lecture_Profil(sc,p,soil,ipl)

USE Stics
USE Plante
USE Sol
USE SticsFiles

  implicit none

  type(Stics_Communs_),       intent(INOUT) :: sc  
  type(Plante_),              intent(IN)    :: p  
  type(Sol_),                 intent(IN)    :: soil  

  integer, intent(IN) :: ipl

! les VARIABLES LOCALES
!      character(len=10) :: sc%nomvarprof(82) ! DR 17/02/2010 les variables de profil.csv sont codees sur 10 caracteres
                                          ! j'ai corrige aussi profil.csv pour qu'il soit coherent avce ce dimensionnement
! Modif Bruno juillet 2018 : ajout d'un nouveau fichier (profil.sti) contenant toutes les variables de profil, a savoir :
!                            tsol, hur, amm, nit, lracz, rlf, rlg, rl, %rl, msrac, humirac, efnrac
      integer :: freqprof
      integer :: nbjParAnnee
      integer :: jourdebutprof  !
      integer :: moisdebutprof  !
      integer :: andebutprof
      integer :: datedebutprof
      integer :: jourprof  !
      integer :: moisprof  !
      integer :: anprof

      integer :: j
      integer :: i
      integer :: kk
      integer :: jour_courant
      integer :: profsol_int
      integer :: an1, iz
      real    :: rlcum, rltot, msrac, msraccum
      type(File_), pointer :: prof_file
      logical :: file_open, exists
      integer :: fprof
      integer :: ret,err
      logical :: flag_date_bad, flag_var_bad

      flag_date_bad=.TRUE.
      flag_var_bad=.TRUE.
      profsol_int=int(soil%profsol)
       !jour_courant=sc%n+sc%P_iwater+1 ! DR 01/04/2015
      jour_courant = sc%n+sc%P_iwater-1

    ! lecture des variables de profil au jour n = 1
      if (sc%n == 1) then

        !call open_file(stics_files,'prof_mod', prof_file, file_open)
        call open_file('prof_mod', prof_file, file_open)
      
        exists = existFile(prof_file)
       
        if (.NOT.exists) then
            call EnvoyerMsgHistorique("Profile variables file doesn't exist : ",char(prof_file%path))
            call EnvoyerMsgErreur( "Profile variables file doesn't exist : ",char(prof_file%path) )
        end if
        
        
        
        if ( exists .AND. .NOT.file_open ) then
            call EnvoyerMsgHistorique("Error opening profile variables file: ", char(prof_file%path))
            call EnvoyerMsgErreur( "Error opening profile variables file: ", char(prof_file%path) )
        end if
     
        if ( any((/.NOT.exists,.NOT.file_open/)) ) then
           ! call exit(9)
           stics_files%flag_profil = .FALSE.
           call exit(9)
        end if
        
    
         fprof = prof_file%unit

        
!        read(fprof,*,err=1999,end=999) sc%codeprofil
        read(fprof,*,iostat=err) sc%codeprofil
        if(err.ne.0)then
           call EnvoyerMsgHistorique(1651)
           call exit(9)
        else
      ! domi 30/09/05 y'avait un pb avec l'ecriture des profils a des frequences
        if (sc%codeprofil == 1 .or. sc%codeprofil == 2) then


          if (sc%codeprofil == 1) then            ! choix de dates fixes
            read(fprof,'(a19)',end=999) sc%valprof
            flag_var_bad=.FALSE.
            i = 1

          ! DR 27/06/2013 on lit une date (jour/mois/an) et non plus un jour dans l'annee
          ! DR 02/04/2015 je verifie que les dates sont bonnes sinon pb et je rajoute un message
888         read(fprof,*,end=777) jourprof,moisprof,anprof
            flag_date_bad=.FALSE.
            if(anprof.eq.sc%annee(sc%P_iwater))then
                call NDATE (jourprof,moisprof,anprof,sc%dateprof(ipl,i))
                i = i+1
            else
                if(anprof.eq.sc%annee(sc%P_ifwater))then
                    call NDATE (jourprof,moisprof,anprof,sc%dateprof(ipl,i))
                    an1=sc%annee(sc%P_iwater)
                    sc%dateprof(ipl,i)=sc%dateprof(ipl,i)+nbjParAnnee(an1)
                    i = i+1
                else
                    call EnvoyerMsgHistorique(1650,anprof)
                endif
            endif

            goto 888 ! TODO: remplacer ce goto par une boucle avec test sur eof
777         continue

          else         ! choix date de debut et frequence
            read(fprof,'(a19)',end=999,iostat=err) sc%valprof
            flag_var_bad=.FALSE.
            read(fprof,*,end=999,iostat=err) freqprof
            flag_date_bad=.FALSE.
          ! DR 23/12/09 on lit une date (jour/mois/an) et non plus un jour dans l'annee
            read(fprof,*,iostat=err,end=999) jourdebutprof,moisdebutprof,andebutprof
            if(err.ne.0)then
                  call EnvoyerMsgHistorique(1651)
                  call exit(9)
            endif
            call NDATE (jourdebutprof,moisdebutprof,andebutprof,datedebutprof)


        ! tests de coherence
            if (datedebutprof < jour_courant) then
              datedebutprof = jour_courant
              call EnvoyerMsgHistorique(165)
            endif
            freqprof = min(freqprof,sc%maxwth/19)

        ! calcul des dates de stockage
        ! DR 07/02/2014 j'augmenet le nb de jours de sorties pour Simtraces
            do j = 1,600
              sc%dateprof(ipl,j) = datedebutprof + ((j-1) * freqprof)
            end do
          endif

999       ret = closeFile(prof_file)
          if(flag_var_bad.or.err.ne.0.or.flag_date_bad)then
             call EnvoyerMsgHistorique(1651)
             call exit(9)
          endif

          ! fin de freq ou date
! si impossible de lire le codeprofi, on ferme le fichier et on sort
! 1999   ret = closeFile(prof_file)


          sc%numdebprof(ipl) = 1

   ! DR 07/02/2014 j'augmente le nb de jours de sorties pour Simtraces
   ! pose pb on va tester pluto sur le debut de simul
          do kk = 1,600
            if (jour_courant > sc%dateprof(ipl,kk)) then
              sc%numdateprof(ipl) = sc%numdateprof(ipl)+1
              sc%numdebprof(ipl)  = sc%numdateprof(ipl)
            else
              EXIT
            endif
          end do
        endif
        endif ! fin test erreur
      endif ! fin test n == 1


    ! stockage des profils dans le tableau tabprof
    ! domi - 14/12/00 - passage des tableaux sol de 200 a 1000

     if (jour_courant == sc%dateprof(ipl,sc%numdateprof(ipl))) then
        if(index(sc%valprof,"tsol") == 1) then
          sc%tabprof(ipl,sc%numdateprof(ipl),1:profsol_int) = sc%tsol(1:profsol_int)
          sc%numdateprof(ipl) = sc%numdateprof(ipl)+1
        else
          if(index(sc%valprof,"hur") == 1) then
            sc%tabprof(ipl,sc%numdateprof(ipl),1:profsol_int) = sc%hur(1:profsol_int)
            sc%numdateprof(ipl) = sc%numdateprof(ipl)+1
          else
            if (index(sc%valprof,"lracz") == 1) then
              sc%tabprof(ipl,sc%numdateprof(ipl),1:profsol_int) = p%lracz(1:profsol_int)
              sc%numdateprof(ipl) = sc%numdateprof(ipl)+1
            else
              if (index(sc%valprof,"rlf") == 1) then
                sc%tabprof(ipl,sc%numdateprof(ipl),1:profsol_int) = p%rlf(1:profsol_int)
                sc%numdateprof(ipl) = sc%numdateprof(ipl)+1
              else
                if (index(sc%valprof,"rlg") == 1) then
                  sc%tabprof(ipl,sc%numdateprof(ipl),1:profsol_int) = p%rlg(1:profsol_int)
                  sc%numdateprof(ipl) = sc%numdateprof(ipl)+1
                else
                  if (index(sc%valprof,"rl") == 1) then
                    do iz = 1,profsol_int
                      sc%tabprof(ipl,sc%numdateprof(ipl),iz) = p%rlf(iz) + p%rlg(iz)
                    end do
                    sc%numdateprof(ipl) = sc%numdateprof(ipl)+1
                  else
                    if (index(sc%valprof,"msrac") == 1) then
                      msraccum = 0.
                      do iz = 1,profsol_int
                        msrac = (p%rlf(iz)/p%longsperacf + p%rlg(iz)/p%longsperacf)*100.
                        msraccum = msraccum + msrac
                        sc%tabprof(ipl,sc%numdateprof(ipl),iz) = msraccum
                      end do
                      sc%numdateprof(ipl) = sc%numdateprof(ipl)+1
                    else
                      if (index(sc%valprof,"%rlf") == 1) then
                        do iz = 1,profsol_int
                          sc%tabprof(ipl,sc%numdateprof(ipl),iz) = 0.
                          rlcum = p%rlf(iz) + p%rlg(iz)
                          if(rlcum > 0.) sc%tabprof(ipl,sc%numdateprof(ipl),iz) = p%rlf(iz)/rlcum
                        end do
                        sc%numdateprof(ipl) = sc%numdateprof(ipl)+1
                      else
                        if (index(sc%valprof,"%rl") == 1) then
                          rltot = 0.
                          do iz = 1,profsol_int
                            rltot = rltot + p%rlf(iz) + p%rlg(iz)
                          end do
                          rlcum = 0.
                          do iz = 1,profsol_int
                            if(rltot > 0.) rlcum = rlcum + (p%rlf(iz) + p%rlg(iz))/rltot
                            sc%tabprof(ipl,sc%numdateprof(ipl),iz) = rlcum
                          end do
                          sc%numdateprof(ipl) = sc%numdateprof(ipl)+1
                        else
                          if (index(sc%valprof,"nit") == 1) then
                            sc%tabprof(ipl,sc%numdateprof(ipl),1:profsol_int) = soil%nit(1:profsol_int)
                            sc%numdateprof(ipl) = sc%numdateprof(ipl)+1
                          else
                            if (index(sc%valprof,"humirac") == 1) then
                              sc%tabprof(ipl,sc%numdateprof(ipl),1:profsol_int) = p%humirac_z(1:profsol_int)
                              sc%numdateprof(ipl) = sc%numdateprof(ipl)+1
                            else
                              if (index(sc%valprof,"efnrac") == 1) then
                                sc%tabprof(ipl,sc%numdateprof(ipl),1:profsol_int) = p%efnrac_z(1:profsol_int)
                                sc%numdateprof(ipl) = sc%numdateprof(ipl)+1
                              else
                                if (index(sc%valprof,"Chum") == 1) then
                                  sc%tabprof(ipl,sc%numdateprof(ipl),1:profsol_int) = &
                                                 sc%Chum(1:profsol_int)
                                  sc%numdateprof(ipl) = sc%numdateprof(ipl)+1
                                else
                                  if (index(sc%valprof,"Nhum") == 1) then
                                    sc%tabprof(ipl,sc%numdateprof(ipl),1:profsol_int) = &
                                                   sc%Nhum(1:profsol_int)
                                    sc%numdateprof(ipl) = sc%numdateprof(ipl)+1
                                  else
                                    if (index(sc%valprof,"C_allresidues") == 1) then
                                      sc%tabprof(ipl,sc%numdateprof(ipl),1:profsol_int) = &
                                                   sc%C_allresidues(1:profsol_int)
                                      sc%numdateprof(ipl) = sc%numdateprof(ipl)+1
                                    else
                                      if (index(sc%valprof,"N_allresidues") == 1) then
                                        sc%tabprof(ipl,sc%numdateprof(ipl),1:profsol_int) = &
                                                   sc%N_allresidues(1:profsol_int)
                                         sc%numdateprof(ipl) = sc%numdateprof(ipl)+1
                                      else
                                        if (index(sc%valprof,"husup_by_cm") == 1) then
                                          sc%tabprof(ipl,sc%numdateprof(ipl),1:profsol_int) = &
                                                   sc%husup_by_cm(1:profsol_int)
                                          sc%numdateprof(ipl) = sc%numdateprof(ipl)+1
                                        endif
                                      endif
                                    endif
                                  endif
                                endif
                              endif
                            endif
                          endif
                        endif
                      endif
                    endif
                  endif
                endif
              endif
            endif
          endif
        endif

! dr 07/02/2014 on augmente le nb de jour du profil a 600
        sc%numdateprof(ipl) = min(sc%numdateprof(ipl),600)

!  tsol, hur, amm, nit, lracz, rlf, rlg, rl, %rl, msrac, humirac, efnrac
!        ivar = 1
!        sc%tabprof2(ipl,sc%numdateprof(ipl),ivar,1:profsol_int) = sc%tsol(1:profsol_int)
!        ivar = 2
!        sc%tabprof2(ipl,sc%numdateprof(ipl),ivar,1:profsol_int) = sc%hur(1:profsol_int)
!        ivar = 3
!        sc%tabprof2(ipl,sc%numdateprof(ipl),ivar,1:profsol_int) = soil%amm(1:profsol_int)
!        ivar = 4
!        sc%tabprof2(ipl,sc%numdateprof(ipl),ivar,1:profsol_int) = soil%nit(1:profsol_int)
!        ivar = 5
!        sc%tabprof2(ipl,sc%numdateprof(ipl),ivar,1:profsol_int) = p%lracz(1:profsol_int)
!        ivar = 6
!        sc%tabprof2(ipl,sc%numdateprof(ipl),ivar,1:profsol_int) = p%rlf(1:profsol_int)
!        ivar = 7
!        sc%tabprof2(ipl,sc%numdateprof(ipl),ivar,1:profsol_int) = p%rlg(1:profsol_int)
!        ivar = 8
!        sc%tabprof2(ipl,sc%numdateprof(ipl),ivar,1:profsol_int) = p%rlf(1:profsol_int)

      endif     ! fin test jour_courant

! si impossible de lire le codeprofi, on ferme le fichier et on sort
       !ret = closeFile(prof_file)  
       !close(prof)


return
end subroutine Lecture_Profil
 
 
