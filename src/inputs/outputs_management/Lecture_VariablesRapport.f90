! Lecture des variables du fichier Rapport (rapport.sti)
! On va lire dans rap.mod la liste des variables dont
! on souhaite avoir les valeurs ecrites dans le fichier rapport.sti
! D'abord on recuperer les dates et/ou les stades pour lesquels
! on veut ces valeurs, puis on lit le nom des variables souhaitees.
! Read variables File Report (rapport.sti)
!! We will read rap.mod the list of variables which
!! one wishes to have the values written to the file rapport.sti
!!
!! First we get the dates and / or stages for which we want these values
!! then we read the name of the desired variables.
subroutine Lecture_VariablesRapport(sc)

    USE Stics
    USE SticsFiles
    USE Messages

    implicit none

    type(Stics_Communs_), intent(INOUT) :: sc  

    integer :: i  
    type(File_), pointer :: rap_file
    logical :: file_open, exists
    integer :: frap
    integer :: ret
    integer::codeenteterap

! 17/02/2010 on ajoute la declaration de la date sous sa nouvelle forme
    !integer :: jourdebutprof  !  
    !integer :: moisdebutprof  !  
    !integer :: andebutprof  
    
    
    !call open_file(stics_files,'rap_mod',rap_file, file_open)
    call open_file('rap_mod',rap_file, file_open)
      
      exists = existFile(rap_file)
    if (.NOT.exists) then
        call EnvoyerMsgHistorique("Report variables file doesn't exist : ",char(rap_file%path))
        call EnvoyerMsgErreur( "Report variables file doesn't exist : ",char(rap_file%path) )
    end if
    
    if ( exists .AND. .NOT.file_open ) then
        call EnvoyerMsgHistorique("Error opening report variables file: ", char(rap_file%path))
        call EnvoyerMsgErreur( "Error opening report variables file: ", char(rap_file%path) )
    end if
 
    if ( any((/.NOT.exists,.NOT.file_open/)) ) then
       ! call exit(9)
        stics_files%flag_rap = .FALSE.
        return
    end if
    

     frap = rap_file%unit


    ! on initialise les variables a FAUX
      sc%rapplt = .FALSE.
      sc%rapger = .FALSE.
      sc%raplev = .FALSE.
      sc%raplax = .FALSE.
      sc%rapflo = .FALSE.
      sc%rapdrp = .FALSE.
      sc%rapsen = .FALSE.
      sc%raprec = .FALSE.
      sc%rapamf = .FALSE.
      sc%rapfin = .FALSE.

      sc%rapdebdes = .FALSE.
      sc%rapdebdebour = .FALSE.
      sc%rapmat = .FALSE.

      sc%rapdebdorm = .FALSE.
      sc%rapfindorm = .FALSE.

    ! Lecture codeentete ou pas

      read(frap,*,end=300) sc%codeaucun
      ! PL, 24/09/2020, sc%codeenteterap is now a vector of 2
      read(frap,*,end=300) codeenteterap
      sc%codeenteterap(:) = codeenteterap
      read(frap,*,end=300) sc%codetyperap

! domi 07/10/03 pour Vianney on peut lire des dates et des stades
! si codetyperap = 1 --> dates
! si codetyperap = 2 --> stades
! si codetyperap = 3 --> dates puis stades

    ! on lit des dates
      if (sc%codetyperap == 1 .or. sc%codetyperap == 3) then

      ! on lit le nombre de dates
        read(frap,*,end=300) sc%nboccurrap
      ! on borne ce nombre par la taille max du tableau
        sc%nboccurrap = min(sc%nboccurrap,SIZE(sc%daterap))
        do i = 1,sc%nboccurrap
           read(frap,*,end=300)sc%date_calend_rap(i,3),sc%date_calend_rap(i,2),sc%date_calend_rap(i,1)

        end do

      endif

    ! on lit des stades
      if (sc%codetyperap == 2 .or. sc%codetyperap == 3) then

      ! on lit le nombre de stades
        read(frap,*,end=300) sc%nboccurstd
      ! on borne ce nombre par la taille max du tableau
        sc%nboccurstd = min(sc%nboccurstd,SIZE(sc%staderap))

      ! on lit nboccurstd stades
        do i = 1,sc%nboccurstd

          read(frap,'(a9)',end=913) sc%staderap(i)

        ! en fonction des stades lus, on met a VRAI les booleens correspondants
! DR 29/12/09 javastics donne plt et non nplt
!         if (sc%staderap(i) == 'nplt') sc%rapplt=.TRUE.
          if (sc%staderap(i) == 'plt') sc%rapplt=.TRUE.
        ! DR 08/01/07 pour Sophie on rajoute l'ecriture a la germination
          if (sc%staderap(i) == 'ger') sc%rapger=.TRUE.
          if (sc%staderap(i) == 'lev') sc%raplev=.TRUE.
          if (sc%staderap(i) == 'amf') sc%rapamf=.TRUE.
          if (sc%staderap(i) == 'lax') sc%raplax=.TRUE.
          if (sc%staderap(i) == 'flo') sc%rapflo=.TRUE.
          if (sc%staderap(i) == 'drp') sc%rapdrp=.TRUE.
          if (sc%staderap(i) == 'sen') sc%rapsen=.TRUE.
          if (sc%staderap(i) == 'rec') sc%raprec=.TRUE.
          if (sc%staderap(i) == 'fin') sc%rapfin=.TRUE.

        ! DR 30/07/08 3 stades ont ete rajoutes
          if (sc%staderap(i) == 'mat') sc%rapmat=.TRUE.
          if (sc%staderap(i) == 'debdes') sc%rapdebdes=.TRUE.
          if (sc%staderap(i) == 'debdebour') sc%rapdebdebour=.TRUE.

        ! DR 12/08/08 2 stades ajoutes
          if (sc%staderap(i) == 'findorm') sc%rapfindorm=.TRUE.
          if (sc%staderap(i) == 'debdorm') sc%rapdebdorm=.TRUE.
          if (sc%staderap(i) == 'start') sc%rapdeb=.TRUE.
        ! Bruno avril 2018 1 stade ajoute pour les fauches
          if (sc%staderap(i) == 'cut') sc%rapcut =.TRUE.
        end do
      endif

913   continue

! on lit le nom des variables dont on veut ecrire la valeur dans le fichier Rapport.
! Bruno j'augmente le nb de variables maxi du rapport  100 --> 999
! DR 23/11/2020 merge du trunk
! c'etait pass� de 100 � 300 dans le trunk mis ici c'est deja a 999 , je laisse 300 cra c'est pas coherent avce le nb d'ecriture dans le format
! qui est rest� a 200
      do i = 1,300
        read(frap,'(a29)',end=299) sc%valrap(i)
! test Bruno juin 2017 en cas de plantes associees ajout d'un 2 au nom de la variable
            if(sc%P_nbplantes == 2) then
               if(sc%valrap(i) == 'cep')         sc%valrap(i) = 'cep2'
               if(sc%valrap(i) == 'QNabso')      sc%valrap(i) = 'QNabso2'
               if(sc%valrap(i) == 'QCressuite')  sc%valrap(i) = 'QCressuite2'
               if(sc%valrap(i) == 'QNressuite')  sc%valrap(i) = 'QNressuite2'
               if(sc%valrap(i) == 'QCracmort')   sc%valrap(i) = 'QCracmort2'
               if(sc%valrap(i) == 'QNracmort')   sc%valrap(i) = 'QNracmort2'
               if(sc%valrap(i) == 'QCperennemort')  sc%valrap(i) = 'QCperennemort2'
               if(sc%valrap(i) == 'QNperennemort')  sc%valrap(i) = 'QNperennemort2'
               if(sc%valrap(i) == 'QNplantetombe')  sc%valrap(i) = 'QNplantetombe2'
               if(sc%valrap(i) == 'QNrogne')     sc%valrap(i) = 'QNrogne2'
             endif
! fin modif
      end do
    ! on a lu le fichier jusqu'a sa fin, on calcul le nombre de variables lues pour le stocker.
    
299   sc%nbvarrap = i-1

300   ret = closeFile(rap_file)
      ! close(frap)

! dr 11/03/2013 j'ajoute la variable   sc%codeenteterap_agmip
        sc%codeenteterap_agmip = codeenteterap

return
end subroutine Lecture_VariablesRapport
 
 
