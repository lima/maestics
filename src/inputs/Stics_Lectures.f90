! subroutine to read all files parameters
subroutine Stics_Lectures(sc,pg,p,itk,soil,c,sta,t,usma,ag)

USE iso_varying_string

USE Stics
USE USM
USE Plante
USE Itineraire_Technique
USE Sol
USE Climat
USE Station
USE Parametres_Generaux
USE Divers
USE Module_AgMIP
USE SticsFiles
USE Messages

  implicit none

  type(Stics_Communs_),       intent(INOUT) :: sc  
  type(Parametres_Generaux_), intent(INOUT) :: pg  
  type(Plante_),              intent(INOUT) :: p(sc%P_nbplantes)  
  type(ITK_),                 intent(INOUT) :: itk(sc%P_nbplantes)  
  type(Sol_),                 intent(INOUT) :: soil  
  type(Climat_),              intent(INOUT) :: c  
  type(Station_),             intent(INOUT) :: sta  
  type(Stics_Transit_),       intent(INOUT) :: t  
  !DR 17/07/2012 je rajoute le module usm pour les noms de fichiers pour optimistics
  type(USM_),                 intent(INOUT) :: usma  ! // PARAMETER // name of the P_USM // SD // USMXML // 0

  type(AgMIP_),               intent(INOUT) :: ag

  integer,parameter :: nb_parameters_max = 300


! Variables locales
    integer :: i  !  
    integer ::  ii  
    real    :: valpar(nb_parameters_max)
    type(varying_string) :: nompar(nb_parameters_max)
    
    integer :: nbpar
    character (len = 4) :: ntec, nplt

    integer           :: codeRetour  

    integer :: codesnowout 
    integer  :: set_name


    call EnvoyerMsgEcran("lecinitialisations")
    !! Lecture du fichier d'initialisations : ficini.txt (format JavaStics)
      if (usma%file_Method_Optimistics_Init == 1) then
           call set_file_name('ini',usma%P_ficInit , set_name) ! to be used to set status
      endif


      call lecinitialisations(sc,p,soil,sta)


    !: Domi - 21/10/2004 - on met la lecture de lecparam en premier
    ! call Lecture_Parametres_Generaux(pg, sc%path, sc%pathtempopar)
      call Lecture_Parametres_Generaux(pg)


      call Ecriture_Parametres_Generaux(pg)
      
      
      ! Setting explicitly writing flags after reading 
      ! pg%P_flagEcriture
      !call set_writing_flags(stics_files, pg%P_flagEcriture)
      call set_files_writing_flags( pg%P_flagEcriture ) 
      
    !: Lecture du fichier des parametres en attente d'etre affectes a leurs fichiers respectifs
    !call Stics_Lecture_Transit(t,sc%P_nbplantes, sc%path, sc%pathtempoparv6)
      call Stics_Lecture_Transit(t,sc%P_nbplantes)
       

    ! Pour chaque plante de la culture, on va lire un fichier technique et un fichier plante
      do i = 1, sc%P_nbplantes

      ! Ouverture des fichiers de sorties journalieres
       if ( stics_files%flag_jour ) then

          ! 23/03/2016 pour Constance on a ajoute un param pour activer ou non la lecture des eclaircissages multiples
!          if(t%P_option_thinning.eq.1) then
!              itk(i)%flag_eclairmult=.TRUE.
!          else
!              itk(i)%flag_eclairmult=.FALSE.
!          endif

!          if(t%P_option_engrais_multiple.eq.1) then
!              itk(i)%flag_plusieurs_engrais=.TRUE.
!          else
!              itk(i)%flag_plusieurs_engrais=.FALSE.
!          endif

! DR 31/05/2017 avant la sortie de la version je desactive en dur la possibilite d'activation des patures
! Bruno juillet 2018 on re-active pour l'etude 4 p mille
!           if(t%P_option_pature.eq.1) then
!              call EnvoyerMsgHistorique(2100)
!              t%P_option_pature = 2
!           endif
! fin 31/05/2017
           if(t%P_option_pature.eq.1) then
                itk(i)%flag_pature=.TRUE.
           else
                itk(i)%flag_pature=.FALSE.
           endif
        endif

      ! Ouverture des fichiers rapports
      ! Sorties specifiques pour AGMIP
        if ( stics_files%flag_agmip ) then
               call read_projects_specificities(sc) !,p,pg%P_flagEcriture)
        endif
      ! lecture des fichiers parametres de AgMIP
        call Stics_Lecture_Param_AgMIP(ag) !,sc%path, sc%pathtempoparv6)


        ii = index(sc%P_wdata1,'.')
        sc%wlieu = sc%P_wdata1(1:ii-2)


      ! getting file tag
      write (ntec,'(A3,i1)') 'tec',i
      ! Lecture du fichier technique (*.tec ou tempotec.sti)
      if (usma%file_Method_Optimistics_Tec(i) == 1)then
        call set_file_name(ntec,itk(i)%P_ftec, set_name) ! to be used to set status
      endif

      call ITK_Lecture(itk(i),ntec)

      call ITK_Ecriture_Tests(itk(i),pg,sc) !TODO: Separer tests et ecritures ?

      ! Lecture du fichier de parametres de la plante (*.plt)
      ! -----------------------------------------------------
      ! getting plant file tag
      write (nplt,'(A3,i1)') 'plt', i
      if (usma%file_Method_Optimistics_Plt(i) == 1)then
        call set_file_name(nplt,p(i)%P_fplt, set_name)
      endif
        
        call Plante_Lecture(p(i),nplt,PLANTE_METHOD_STICS_V6,codeRetour)

        call Plante_Tests(p(i),itk(i)%P_variete)

        call Plante_Ecriture(p(i),sc,sta,itk(i),pg)

        sc%plante_ori(i)=.TRUE.
      ! Par defaut, la plante lue en premier est consideree comme dominante et les suivantes comme dominees.
      ! TODO : voir si on peut mettre les affectations de estDominante ailleurs
        p(i)%estDominante = .FALSE.

      ! si sol nu pas de test/ecriture
       if (p(i)%P_codeplante == 'snu') itk(i)%P_iplt0 = sc%P_iwater
      ! si sol nu, pas de plante vivante
       if (p(i)%P_codeplante == 'snu') then
          p(i)%estVivante = .FALSE.
       else
          p(i)%estVivante = .TRUE.
       endif

! 08/11/2018 test pour l activation de hautfauchedyn pour la luzerne dans la version perenne
! solution temporaire avant migration vers fichier plante puis vers fichier tec
!      if (p(i)%P_codeplante == 'fou' .and. p(i)%P_code_acti_reserve.eq.1)then
!         t%P_code_hautfauche_dyn = 1
!      endif
!
! stockage des variables par profil
! disabling for restoring old functionning, call from Stics_jour_After
!       if ( stics_files%flag_profil ) then 
!           call Lecture_Profil(sc,p(i),soil,i)
!       end if
     enddo

     p(1)%estDominante = .TRUE.


      if (usma%file_Method_Optimistics_Station == 1)then
      ! DR 17/07/2012 on lit le nom du fichier dans newtravail.usm pour optimistics
        call set_file_name('sta',usma%P_ficStation, set_name)
      endif

    ! Lecture du fichier station
      call Station_Lecture(sta) !,sc%path,sc%pathstation)

    ! Ecriture du fichier historique pour la structure Station

    !DR 13/12/2017 deplacement du test sur codeclichange ici et besoin de alphaCO2
      if ( write_histo ) then
        ! modifying codesnow if codeabri == 1
        call Station_Ecriture(sta,itk(1)%P_codabri,p(1)%P_alphaCO2,pg%P_codesnow,codesnowout)
        pg%P_codesnow=codesnowout
      endif

    ! dr 31/10/07 pour le moment on remet le nometp qu'il faudra changer apres
      if (sta%P_codeetp == 1) c%nometp = 'pe'
      if (sta%P_codeetp == 2) c%nometp = 'pc'
      if (sta%P_codeetp == 3) c%nometp = 'sw'
      if (sta%P_codeetp == 4) c%nometp = 'pt'

    ! pour les cultures associees, il faut SW, sinon stop
    ! - P_codeetp = 1 --> Penman force
    ! - P_codeetp = 2 --> Penman calcule
    ! - P_codeetp = 3 --> Shuttleworth & Wallace
    ! - P_codeetp = 4 --> Prestley - Taylor
      if (sc%P_nbplantes > 1 .and. sta%P_codeetp /= 3) then
        call EnvoyerMsgHistorique(6)
        call EnvoyerMsgHistorique(433)
        call exit(9)
      endif

    ! On effectue un test de coherence des parametres pour le transfert radiatif
      if (sc%P_nbplantes > 1) then
        if (itk(1)%P_codetradtec == 2) then
           call EnvoyerMsgHistorique(6)
           call EnvoyerMsgHistorique(430)
           call exit(9)
        endif
        p(1)%codetransradb = p(1)%P_codetransrad
        p(1)%P_codetransrad = 2
      endif

      do i = 1, sc%P_nbplantes
        if (p(i)%P_codetransrad == 2) then
           if ( itk(i)%P_interrang >= 999    .or.   &
                itk(i)%P_interrang <= 0.     .or.   &
                itk(i)%P_orientrang >= 999.  .or.   &
                p(i)%P_ktrou(itk(i)%P_variete) >= 999.         .or.   &
                p(i)%P_forme >= 999.         .or.   &
                p(i)%P_rapforme >= 999.      .or.   &
                p(i)%P_hautbase(itk(i)%P_variete) >= 999.      .or.   &
                p(i)%dfol >= 999.) then
             call EnvoyerMsgHistorique(6)
             call EnvoyerMsgHistorique(432)
             call EnvoyerMsgHistorique(6)
             call exit(9)
           end if
        endif
      end do

! DR 11/12/2014 on verifie la coherence des parametres pour l'etp SW et le codebeso
     if(sta%P_codeetp == 3 .and. p(1)%P_codebeso==1 ) then
           call EnvoyerMsgHistorique(6)
           call EnvoyerMsgHistorique(203)
           call EnvoyerMsgHistorique(62)
           call exit(9)
     endif

! DR  et Fr 30/05/2016 on verifie la coherence des parametres pour l'etp PC ou PE et le codebeso
     if(sta%P_codeetp .ne.3 .and. p(1)%P_codebeso==2 ) then
           call EnvoyerMsgHistorique(6)
           call EnvoyerMsgHistorique(1203)
           call EnvoyerMsgHistorique(1062)
     endif
    ! Lecture des parametres de sol
     if (usma%file_Method_Optimistics_Sol == 1) then
        call set_file_name('sol',usma%P_nomsol, set_name)
      endif

      call Sol_Lecture(soil, nbLayers_max)
      
! Bruno - initialisation du nouveau parametre rapport C/N du sol
      !if(soil%P_CsurNsol0 == 0.)  then
      if(abs(soil%P_CsurNsol0).lt.1.0E-8)  then
        soil%CsurNsol = 1./pg%P_Wh
      else
        soil%CsurNsol = soil%P_CsurNsol0
      endif

      !if(soil%P_penterui == 0.) soil%P_penterui = 0.33   !DR 27/07/2012 - externalisation du parametre penterui
      if(abs(soil%P_penterui).lt.1.0E-8) soil%P_penterui = 0.33   !DR 27/07/2012 - externalisation du parametre penterui

      sc%nbCouchesSol =  nbCouchesSol_max ! je le rajoute la car il n'est pas encore connu

    ! Pour tester les parametres du sol
     call Sol_Tests(soil, sc%nbCouchesSol, sc%P_ichsl, pg%P_masvolcx, sc%beta_sol, nbLayers_max)
     do i = 1, sc%P_nbplantes
          call sol_test_itk(itk(i),soil%P_profhum)
     enddo

      if ( write_histo ) then
        call Sol_Ecriture(soil,pg,sc)
      end if

!: MODULE D'OPTIMISATION DES PARAMETRES D'UNE PLANTE
!- Dans le cas ou le modele est lance pour une optimisation, lecture des parametres de depart
!- si codoptim=1, on optimise s'il y a lieu les parametres de la Culture Principale
!- si codoptim=2  on optimise s'il y a lieu les parametres de la Culture Associee
      if (sc%codoptim /= 0) then ! si le P_codeoptim est different de zero, on optimise
        
        ! Getting names, values and number of parameters from the param.sti file
        call get_forcing_parameters( nompar, valpar , nbpar)
        
      ! DR 19/09/2012 je passe les parametres a forcer en arguments
        call Lecture_Optimisation(sc,pg,p,itk,soil,sta,t,nbpar,nompar,valpar)  ! DR 20/07/2012  c ne sert pas je vire

      endif

    ! Lecture des variables du fichier de sorties journalieres
    ! Ce sont les variables choisies par l'utilisateur que le systeme
    ! transmettra en sortie a chaque pas de temps.
    if ( stics_files%flag_jour ) then
      call Lecture_VariablesSortiesJournalieres(sc)
    end if

    ! Lecture des variables du fichier rapport.sti
    if ( stics_files%flag_rap ) then
      call Lecture_VariablesRapport(sc)
    end if

! ecritures des messages dans l'history
      if ( write_histo ) then
        !call Ecriture_Transit(t,sc%P_nbplantes,p(1)%P_codeplante,p(2)%P_codeplante )
        call Ecriture_Transit(t,p(1)%P_codeplante,p(2)%P_codeplante )
      end if


return
end subroutine Stics_Lectures
 
 
