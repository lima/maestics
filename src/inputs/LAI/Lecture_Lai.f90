!*********************************************************************
! reading of continous values of leaf area index
! - in the case of forcing lai ( file *.lai )
!*********************************************************************
subroutine Lecture_Lai(sc,p,laifile,itk)

    USE Stics
    USE Plante
    USE Messages, only: EnvoyerMsgHistorique
    USE Itineraire_Technique

    implicit none

    type(Stics_Communs_),       intent(INOUT) :: sc  
    type(Plante_),              intent(INOUT) :: p  
    character(len=50),          intent(IN) :: laifile
    type(ITK_),                 intent(INOUT) :: itk

    logical :: consec
    integer :: nblai  !  
    integer :: date(3),i,iderlai,ipremlai  
    integer :: j  !  
    integer :: premier  
    character(len=3)  :: sufflai  

! Modif Bruno nl = entier ?
!   real :: tlai(731),nl(731),sbvmin
    real :: tlai(731),sbvmin
    integer :: nl(731)
    
    ! using SticsFiles ------------------
      type(File_), pointer :: lai_file
      logical :: file_open, exists, status
      integer :: flai, file_closed, set_name
      character (len = 4) :: nlai

! Initialisation.
    p%lai(:,:) = 0.
    nblai = 0
    
!    if ( p%ipl == 1 ) then
!         tag = 'lai1'
!    end if
!    if (p%ipl == 2) then 
!         tag = 'lai2'
!    end if
    
    ! getting plant file tag
      write (nlai,'(A3,i1)') 'lai', p%ipl
          
    !call set_file_name(stics_files,tag,laifile, set_name) ! to be used to set status
    call set_file_name(nlai,laifile, set_name) ! to be used to set status
    
    !call open_file(stics_files,tag,lai_file, file_open)
    call open_file(nlai,lai_file, file_open)
    
    !call dispFile(lai_file,.TRUE.)
    
      
    exists = existFile(lai_file)
    if (.NOT.exists) then
        call EnvoyerMsgHistorique("Lai file doesn't exist : ",char(lai_file%path))
        call EnvoyerMsgErreur( "Lai file doesn't exist : ",char(lai_file%path) )
    end if
    
    if ( .NOT.file_open ) then
        call EnvoyerMsgHistorique("Error opening Lai file: ", char(lai_file%path))
        call EnvoyerMsgErreur( "Error opening Lai file: ", char(lai_file%path) )
    end if
 
    if ( any((/.NOT.exists,.NOT.file_open/)) ) then
       call exit(9)
    end if
    
    flai = lai_file%unit
    
    
! ================== debut =================================
!    laifile = p%P_flai
    !open (12,file=laifile,status='old',err=999)

! ----------------------------------------------------------
! on extrait l'extension du fichier pour determiner si c'est
! un fichier .obs ou .lai
! ici on extrait les 3 dernieres lettres du nom du fichier lai

    i = LEN_TRIM(laifile)

    sufflai = laifile(i-2:i)

    if (sufflai == 'obs' .or. sufflai == 'OBS') then

  ! les lai observees   fichier (*.obs)
      premier = 0
      do i = 1,731
        read(flai,100,end=30) (date(j),j=1,3),nl(i),tlai(i)
100     format(i4,2i3,i4,f8.2)
        if (tlai(i) > 0.0 .and. premier == 0) then
          ipremlai = i
          premier = 1
        endif
        if (tlai(i) > 0.0 .and. premier == 1) iderlai = i

      end do
30    nblai = i-1

! lecture des LAI
! *****************

    ! test sur les valeurs manquantes
      consec = .true.
      do i = ipremlai,iderlai
        ! if (tlai(i) == 0) then
        if (abs(tlai(i)).lt.1.0E-8) then
          consec = .false.
        endif
      end do

    ! le fichier lai a des donnees manquantes
      if (.not.consec) then
        call EnvoyerMsgHistorique(101)
        !stop
        call exit(9)
      endif

    ! changement de calendrier
      do i = sc%P_iwater,nblai
        ! sc%n = nl(i) - sc%P_iwater + 1
        sc%n = int(nl(i)) - sc%P_iwater + 1
        p%lai(:,sc%n) = tlai(i)
!        write(*,*)n,lai(ipl,ens,n)
      end do

    else

    ! fichier des lai estime par ajust (*.lai)
    ! changement de format des LAI ajustes + 1ligne
      do i=1,8
        read(flai,*)
      end do

      do i=1,731
        read(flai,*,end=65) j,nl(j),tlai(j)
      end do

65    nblai = i - 1

    ! changement de calendrier
      do i = 1,nblai
        ! sc%n = nl(i) - sc%P_iwater + 1
        sc%n = int(nl(i)) - sc%P_iwater + 1
        if (sc%n < 0) CYCLE
        p%lai(:,sc%n) = tlai(i)

      ! calcul de l'IF senescent
        if (p%lai(AS,sc%n) < p%lai(AS,sc%n-1)) then
          p%laisen(:,sc%n) = p%lai(AS,sc%n-1) - p%lai(AS,sc%n)
        else
          p%laisen(:,sc%n) = 0.0
        endif

        if (p%P_codeindetermin == 2) then
        ! NB - le 21/04 - recalcul d'un sbv approprie
          sbvmin = p%P_slamin / (1.0 + p%P_tigefeuil(itk%P_variete))
          p%fpv(:,sc%n) = (p%lai(AS,sc%n)-p%lai(AS,sc%n-1)) * 1e4 / sbvmin
          p%fpv(:,sc%n) = max(p%fpv(AS,sc%n),0.0)
        else
          p%fpv(:,sc%n) = 0.0
        endif
      end do

    endif
    
    file_closed = closeFile(lai_file)
    if ( file_closed > 0 ) then
         status = .FALSE.
    end if 

    !close(flai)

    sc%maxwth = sc%ifwater_courant - sc%P_iwater + 1
    
    !print * , "end of reading lai file ..."

    return

!999 call EnvoyerMsgHistorique(4000,laifile,'a50')
!    !stop
!    call exit(9)

return
end subroutine Lecture_Lai
 
 
