! Module station
! - Description of the struture Station_
! - reading of station parameters
module Station

implicit none


type Station_

  integer :: P_codecaltemp  ! // PARAMETER // option of use of crop temperature for phasic development calculation : yes (2), no (1)  // code 1/2 // STATION // 0
  integer :: P_codernet  ! // PARAMETER // option of calculation of net radiation // code 1/2/3 // STATION // 0
  integer :: P_codeclichange  ! // PARAMETER // option for climatel change : yes (2), no (1)  // code 1/2 // STATION // 0
  real :: P_zr  ! // PARAMETER // Reference height of meteorological data measurement // m // STATION // 0
  real :: P_ra  ! // PARAMETER // Aerodynamic resistance (used in volatilization  module when we use ETP approach) // s m-1 // STATION // 1 	!zarbi a voir avce marie  // OUTPUT // Aerodynamic resistance between the cover and the reference level P_zr // s.m-1
  real :: P_NH3ref  ! // PARAMETER // NH3 concentration in the atmosphere // ug.m-3 // STATION // 1
  real :: P_aangst  ! // PARAMETER // coefficient of the Angstrom's relationship for extraterrestrial radiation // SD // STATION // 1
  real :: P_bangst  ! // PARAMETER // coefficient of the Angstrom's relationship for extraterrestrial radiation // SD // STATION // 1
  real :: P_coefdevil  ! // PARAMETER // multiplier coefficient of the exterior radiation to compute PET inside of a greenhouse // SD // STATION // 1
  real :: P_albveg  ! // PARAMETER // P_albedo of the vegetation // SD // STATION // 1
  real :: P_altistation  ! // PARAMETER // altitude of the input metorological station  // m // STATION // 0
  real :: P_altisimul  ! // PARAMETER // altitude of simulation // m // STATION // 0
  real :: P_gradtn  ! // PARAMETER // thermal gradient in altitude for minimal temperatures  // degree C m-1 // STATION // 1
  real :: P_gradtx  ! // PARAMETER // thermal gradient in altitude for maximal temperatures  // degree C m-1 // STATION // 1
  real :: P_altinversion  ! // PARAMETER // altitude of inversion of the thermal gradiant // m // STATION // 1
  real :: P_gradtninv  ! // PARAMETER // thermal gradient in altitude for minimal temperatures under the inversion level // degree C m-1 // STATION // 1
  real :: P_cielclair  ! // PARAMETER // threshold for the proportion of sunny hours allowing the inversion of thermal gradiant with altitude // SD // STATION // 1
  real :: P_ombragetx  ! // PARAMETER // shadow effect to calculate the thermal modification in the northern parts of montains  // degree C // STATION // 1
  real :: P_latitude  ! // PARAMETER // Latitudinal position of the crop  // degree // STATION // 0

! deplace depuis la structure climat
  real :: P_aks  ! // PARAMETER // parameter of calculation of the energetic lost between the inside and the outside of a greenhouse  // Wm-2K-1 // STATION // 1
  real :: P_bks  ! // PARAMETER // parameter of calculation of the energetic lost between the inside and the outside of a greenhouse  // Wm-2K-1 // STATION // 1
  real :: P_cvent  ! // PARAMETER // parameter of the climate calculation under the shelter // SD // STATION // 1
  real :: P_phiv0  ! // PARAMETER // parameter allowing the calculation of the under shelter climate // * // STATION // 1
  real :: P_coefrnet  ! // PARAMETER // coefficient of calculation of the net radiation under greenhouse // * // STATION // 1
  real :: P_patm  ! // PARAMETER // atmospheric pressure // mbars // STATION // 0
  real :: P_corecTrosee  ! // PARAMETER // temperature to substract to Tmin to estimate dew point temperature (in case of missing air humidity data) // degree C // STATION // 1
  integer :: P_codeetp !: DR - 31/10/07 	  // PARAMETER // code of calculation mode of ETP [pe/pc/sw/pt] // code 1/2/3/4 // STATION // 0
  real :: P_alphapt  ! // PARAMETER // Parameter of Priestley-Taylor  // SD // STATION // 1
  integer :: P_codaltitude  ! // PARAMETER // option of calculation of the climate in altitude // code 1/2 // STATION // 0
  integer :: P_codadret  ! // PARAMETER // option of calculation of climate in montain accounting for the orientation (1 : south, 2 : north) // code 1/2 // STATION // 0
  real :: P_aclim  ! // PARAMETER // climatic component of A // mm // STATION // 1
  real :: ra_recal  ! // OUTPUT // Aerodynamic resistance (used in volatilization  module when we use ETP approach) // s m-1
  
  
  ! Params for Snow
  real :: P_tsmax ! maximum daily air temperature (tmax) below which all precipitation is assumed to be snow (degree C)
  real :: P_trmax ! tmax above which all precipitation is assumed to be rain (degree C)
  real :: P_DKmax ! difference between the maximum and the minimum melting rates (mm degree C-1 day-1)
  real :: P_Kmin ! minimum melting rate on 21 December (mm degree C-1 day-1)
  real :: P_Tmf ! threshold temperature for snow melting (degree C)
  real :: P_SWrf ! degree-day temperature index for refreezing (mm degree C-1 day-1)
  real :: P_Pns ! density of the new snow(kg m-3)
  real :: P_E ! snow compaction parameter (mm mm-1 day-1)
  real :: P_prof ! snow cover threshold for snow insulation (cm)
  real :: P_tminseuil ! minimum temperature when snow cover is higher than prof
  real :: P_tmaxseuil  ! maximum temperature when snow cover is higher than prof
  ! init vars
  ! 2022/04/14, PL: changes according to other init variables
  ! used ini initialisation files. old names SDepth,Sdry,Swet,ps
  real :: P_Sdry0 ! water in solid state in the snow cover (mm)
  real :: P_Swet0 ! water in liquid state in the snow cover (mm)
  real :: P_ps0 ! density of snow cover (kg m-3)
  real :: P_Sdepth0 ! snow cover depth (m)
  ! model
  integer :: P_codemodlsnow ! for choosing snow model to execute
! Loic Fevrier 2021 : migration param�tres pour sortie v10
  real :: P_concrr  ! // PARAMETER // inorganic N concentration (NH4+NO3-N) in the rain // kgN ha-1 mm-1 // PARAM // 1


end type Station_


 contains

subroutine zero_sta(sta)
    type(Station_), intent(OUT) :: sta  

    sta%P_codecaltemp=0  
    sta%P_codernet=0  
    sta%P_codeclichange=0  
    sta%P_zr=0. 
    sta%P_ra=0.   
    sta%P_NH3ref=0.  
    sta%P_aangst=0.  
    sta%P_bangst=0.  
    sta%P_coefdevil=0.  
    sta%P_albveg=0.  
    sta%P_altistation=0.  
    sta%P_altisimul=0.  
    sta%P_gradtn=0.  
    sta%P_gradtx=0.  
    sta%P_altinversion=0.  
    sta%P_gradtninv=0.  
    sta%P_cielclair=0.  
    sta%P_ombragetx=0.  
    sta%P_latitude=0.  
    sta%P_aks=0.  
    sta%P_bks=0.  
    sta%P_cvent=0.  
    sta%P_phiv0=0.  
    sta%P_coefrnet=0.  
    sta%P_patm=0.  
    sta%P_corecTrosee=0.  
    sta%P_codeetp=0 
    sta%P_alphapt=0.  
    sta%P_codaltitude=0  
    sta%P_codadret=0  
    sta%P_aclim=0.  
    sta%ra_recal=0.  
  
    ! snow parameters  zero
    sta%P_tsmax=0.
    sta%P_trmax=0.
    sta%P_DKmax=0.
    sta%P_Kmin=0.
    sta%P_Tmf=0.
    sta%P_SWrf=0.
    sta%P_Pns=0.
    sta%P_E=0.
    sta%P_prof=0.
    sta%P_tminseuil=0.
    sta%P_tmaxseuil=0.  
    ! setting snow init variables
    ! 2022/04/14, PL: changes according to other init variables
    ! used ini initialisation files. 
    !sta%SDepth=0.
    !sta%Sdry=0.
    !sta%Swet=0.
    !sta%ps=0.
    sta%P_Sdepth0=0.
    sta%P_Sdry0=0.
    sta%P_Swet0=0.
    sta%P_ps0=0.
    ! model code
    sta%P_codemodlsnow=0
! Loic Fevrier 2021 : migration param�tres pour sortie v10
    sta%P_concrr=0.
  
end subroutine zero_sta


subroutine Station_Lecture(sta)

USE Messages


implicit none

    type(Station_), intent(INOUT) :: sta  


! Variables locales


    
    !!!!!!!!!!! pour param snow !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    character(len=30) :: nomVar
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    type(File_), pointer :: sta_file
    logical :: file_open, exists
    integer :: fsta
    integer :: ret
    !integer  :: set_name
    logical :: error
    
    error = .FALSE.

    call open_file('sta',sta_file, file_open)
      
    exists = existFile(sta_file)
    if (.NOT.exists) then
        call EnvoyerMsgHistorique("Station file doesn't exist : ",char(sta_file%path))
        call EnvoyerMsgErreur( "Station file doesn't exist : ",char(sta_file%path) )
    end if
    
    if ( .NOT.file_open ) then
        call EnvoyerMsgHistorique("Error opening station file: ", char(sta_file%path))
        call EnvoyerMsgErreur( "Error opening station file: ", char(sta_file%path) )
    end if
 
    if ( any((/.NOT.exists,.NOT.file_open/)) ) then
       call exit(9)
    end if
    

      fsta = sta_file%unit

! weather Station
!******************
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_zr
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_NH3ref
! Loic Fevrier 2021 : migration param�tres pour sortie v10
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_concRR
! 09/01/2012 passe dans param.par
!      read (fsta,*,end=80,err=250) nomVar
!      read (fsta,*,end=90,err=250) sta%P_parsurrg
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_latitude
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_patm
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_aclim

! climate
!******************
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_codeetp
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_alphapt
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_codeclichange
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_codaltitude
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_altistation
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_altisimul
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_gradtn
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_gradtx
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_altinversion
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_gradtninv
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_cielclair
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_codadret
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_ombragetx
! microclimate
!******************
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_ra
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_albveg
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_aangst
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_bangst
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_corecTrosee
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_codecaltemp
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_codernet
! climate under a shelter
!******************
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_coefdevil
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_aks
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_bks
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_cvent
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_phiv0
      read (fsta,*,end=80,err=250) nomVar
      read (fsta,*,end=90,err=250) sta%P_coefrnet

      ! snow parameters detection !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      read (fsta,*,end=70,err=250) nomVar
      if (nomVar == 'codemodlsnow') then
        read (fsta,*,end=90,err=250) sta%P_codemodlsnow
      endif
      read (fsta,*,end=70,err=250) nomVar
      if (nomVar == 'tsmax') then
        read (fsta,*,end=90,err=250) sta%P_tsmax
      endif
      read (fsta,*,end=70,err=250) nomVar
      if (nomVar == 'trmax') then
        read (fsta,*,end=90,err=250) sta%P_trmax
      endif
      read (fsta,*,end=70,err=250) nomVar
      if (nomVar == 'DKmax') then
        read (fsta,*,end=90,err=250) sta%P_DKmax
      endif
      read (fsta,*,end=70,err=250) nomVar
      if (nomVar == 'Kmin') then
        read (fsta,*,end=90,err=250) sta%P_Kmin
      endif
      read (fsta,*,end=70,err=250) nomVar
      if (nomVar == 'Tmf') then
        read (fsta,*,end=90,err=250) sta%P_Tmf
      endif
      read (fsta,*,end=70,err=250) nomVar
      if (nomVar == 'SWrf') then
        read (fsta,*,end=90,err=250) sta%P_SWrf
      endif
      read (fsta,*,end=70,err=250) nomVar
      if (nomVar == 'Pns') then
        read (fsta,*,end=90,err=250) sta%P_Pns
      endif
      read (fsta,*,end=70,err=250) nomVar
      if (nomVar == 'E') then
        read (fsta,*,end=90,err=250) sta%P_E
      endif
      read (fsta,*,end=70,err=250) nomVar
      if (nomVar == 'prof') then
        read (fsta,*,end=90,err=250) sta%P_prof
      endif
      read (fsta,*,end=70,err=250) nomVar
      if (nomVar == 'tminseuil') then
        read (fsta,*,end=90,err=250) sta%P_tminseuil
      endif
      read (fsta,*,end=70,err=250) nomVar
      if (nomVar == 'tmaxseuil') then
        read (fsta,*,end=90,err=250) sta%P_tmaxseuil
      endif

      if (error) then
         call exit(9)
      end if 
      
      !close(fsta)
      ret = closeFile(sta_file)

      return

70   ret = closeFile(sta_file) 
     return
      ! print *,'End of station file'
      !call EnvoyerMsgHistorique('Fin du fichier station ')

80   call EnvoyerMsgErreur('End of station file :  missing lines ')
     call EnvoyerMsgHistorique('End of station file :  missing lines ')
     error = .TRUE.      
      
90   call EnvoyerMsgErreur('End of station file :  missing value for ',nomVar)
     call EnvoyerMsgHistorique('End of station file :  missing value for ',nomVar)
     !return
     error = .TRUE.
          
250  call EnvoyerMsgErreur('Error reading station file !')
      call EnvoyerMsgHistorique('Error reading station file !')
      !print *, nomVar
      !call exit(9)
      error = .TRUE.

end subroutine Station_Lecture


subroutine Station_Ecriture(sta, P_codabri,P_alphaco2, P_codesnow,P_codesnowout)

USE Messages
USE Snow

    type(Station_), intent(IN) :: sta  
    integer,        intent(IN) :: P_codabri  ! // PARAMETER // option of calculation of the climate under a shelter // code 1/2 // PARTEC // 0 
    real,           intent(IN) :: P_alphaco2  ! // PARAMETER // coefficient allowing the modification of radiation use efficiency in case of  atmospheric CO2 increase // SD // PARPLT // 1

    integer,        intent(IN) :: P_codesnow !< // PARAMETER // code to activate the calculation of snow depth , and recaclulation of tmin, tmax, rainfall : yes (1), no (2) // code 1/2 // PARAM // 0
    integer,        intent(OUT) :: P_codesnowout ! ouput value for codesnow !
    character(len=40)    :: method_etp

    ! snow !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    real :: snowpar(11)
    snowpar(:) = 0.
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      call EnvoyerMsgHistorique(261)
      call EnvoyerMsgHistorique('P_zr', sta%P_zr)
      call EnvoyerMsgHistorique('P_NH3ref', sta%P_NH3ref)
! Loic Fevrier 2021 : migration param�tres pour sortie v10
      call EnvoyerMsgHistorique('P_concRR', sta%P_concRR)
      call EnvoyerMsgHistorique('P_ra', sta%P_ra)
      call EnvoyerMsgHistorique('P_aangst', sta%P_aangst)
      call EnvoyerMsgHistorique('P_bangst', sta%P_bangst)
      call EnvoyerMsgHistorique('P_albveg', sta%P_albveg)

!      call EnvoyerMsgHistorique('P_parsurrg', sta%P_parsurrg)
      call EnvoyerMsgHistorique('P_latitude', sta%P_latitude)

      call EnvoyerMsgHistorique('P_patm', sta%P_patm)
      call EnvoyerMsgHistorique('P_corecTrosee', sta%P_corecTrosee)
      call EnvoyerMsgHistorique('P_codeclichange', sta%P_codeclichange)

! DR 30/04/2013 je rajoute la methode de calcul de l'etp choisie dans le fichier station
      if(sta%P_codeetp.eq.1)method_etp=' Penman reading'
      if(sta%P_codeetp.eq.2)method_etp=' Penman calculated'
      if(sta%P_codeetp.eq.3)method_etp=' calcul of Shuttleworth-and-Wallace'
      if(sta%P_codeetp.eq.4)method_etp=' calcul of Priestley-Taylor'


      call EnvoyerMsgHistorique('P_codeetp', sta%P_codeetp)
      call EnvoyerMsgHistorique('P_codeetp', method_etp)
      call EnvoyerMsgHistorique('P_alphapt', sta%P_alphapt)

      call EnvoyerMsgHistorique('P_codernet', sta%P_codernet)
      call EnvoyerMsgHistorique('P_codecaltemp', sta%P_codecaltemp)
      call EnvoyerMsgHistorique('P_codaltitude', sta%P_codaltitude)


    ! ecritures pour l'altitude - NB - le 05/04
      if (sta%P_codaltitude == 2) then
        call EnvoyerMsgHistorique('P_altistation', sta%P_altistation)
        call EnvoyerMsgHistorique('P_altisimul', sta%P_altisimul)
        call EnvoyerMsgHistorique('P_codadret', sta%P_codadret)
        call EnvoyerMsgHistorique('P_gradtn', sta%P_gradtn)
        call EnvoyerMsgHistorique('P_gradtx', sta%P_gradtx)
        call EnvoyerMsgHistorique('P_altinversion', sta%P_altinversion)
        call EnvoyerMsgHistorique('P_gradtninv', sta%P_gradtninv)
        call EnvoyerMsgHistorique('P_cielclair', sta%P_cielclair)
        call EnvoyerMsgHistorique('P_ombragetx', sta%P_ombragetx)
      endif

      if (P_codabri == 2) then
        call EnvoyerMsgHistorique('P_coefdevil', sta%P_coefdevil)
        call EnvoyerMsgHistorique('P_aks', sta%P_aks)
        call EnvoyerMsgHistorique('P_bks', sta%P_bks)
        call EnvoyerMsgHistorique('P_cvent', sta%P_cvent)
        call EnvoyerMsgHistorique('P_phiv0', sta%P_phiv0)
        call EnvoyerMsgHistorique('P_coefrnet', sta%P_coefrnet)
      endif
      
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! snow : checking parameters 
      !        display of parameters values, and init values
      !-------------------------------------------------------------------------------------------------------------
      ! If climate under a shelter is set, disabling snow module call
      ! setting P_snowcode to 2
      P_codesnowout=P_codesnow
      ! /!\ codabri is reversed : 2 for activation
      if (P_codesnow.eq.1.AND.P_codabri.eq.2) then
          P_codesnowout=2
          call EnvoyerMsgHistorique('--------------------------------------------------------------------------------------')
          call EnvoyerMsgHistorique(' Climate under a shelter is activated, snow module has been deactivated !') 
          call EnvoyerMsgHistorique('--------------------------------------------------------------------------------------')
      endif 
      ! Displaying informations about snow module use, parameters, variables initial values
      if (P_codesnowout == 1) then
         call EnvoyerMsgHistorique('--------------------------------------------------------------------------------------')
         call EnvoyerMsgHistorique(' Using Snow module for recalculating temperatures and rainfall')
         call EnvoyerMsgHistorique('--------------------------------------------------------------------------------------')
         
         snowpar(1:11) = (/ sta%P_tsmax,sta%P_trmax,sta%P_DKmax,sta%P_Kmin, &
                         sta%P_Tmf,sta%P_SWrf,sta%P_Pns,sta%P_E,sta%P_prof, &
                         sta%P_tminseuil,sta%P_tmaxseuil /)
         !if (all(snowpar.eq.0)) then
         if (all(snowpar.lt.1.0E-8)) then
            call EnvoyerMsgHistorique('--------------------------------------------------------------------------------------')
            call EnvoyerMsgHistorique(' Snow module parameters values have not being set (see station file) !')
            call EnvoyerMsgHistorique(' Using a default parameter values set for the simulation !')  
            call EnvoyerMsgHistorique('--------------------------------------------------------------------------------------')
            !call exit(9)
            ! getting default values from the Snow module
            call get_default_par(snowpar(1),snowpar(2),snowpar(3),snowpar(4),snowpar(5),  &
            snowpar(6),snowpar(7),snowpar(8),snowpar(9),snowpar(10),snowpar(11))
            
         endif
         call EnvoyerMsgHistorique('Parameters')
         call EnvoyerMsgHistorique('P_tsmax',snowpar(1))
         call EnvoyerMsgHistorique('P_trmax',snowpar(2))
         call EnvoyerMsgHistorique('P_DKmax',snowpar(3))
         call EnvoyerMsgHistorique('P_Kmin',snowpar(4))
         call EnvoyerMsgHistorique('P_Tmf',snowpar(5))
         call EnvoyerMsgHistorique('P_SWrf',snowpar(6))
         call EnvoyerMsgHistorique('P_Pns',snowpar(7))
         call EnvoyerMsgHistorique('P_E',snowpar(8))
         call EnvoyerMsgHistorique('P_prof',snowpar(9))
         call EnvoyerMsgHistorique('P_tminseuil',snowpar(10))
         call EnvoyerMsgHistorique('P_tmaxseuil',snowpar(11))
         call EnvoyerMsgHistorique('Initializations')
         ! 2022/04/14, PL: changes according to other init variables
         ! used ini initialisation files. 
         call EnvoyerMsgHistorique('Sdepth0',sta%P_Sdepth0)
         call EnvoyerMsgHistorique('Sdry0',sta%P_Sdry0)
         call EnvoyerMsgHistorique('Swet0',sta%P_Swet0)
         call EnvoyerMsgHistorique('ps0',sta%P_ps0)
      endif
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      if (sta%P_codeclichange == 2) then
        call EnvoyerMsgHistorique('P_alphaCO2',P_alphaCO2)
      ! test sur alpha co2
        if (P_alphaco2 >= 2.) then
          call EnvoyerMsgHistorique(361)
          !stop
          call exit(9)
        endif
      endif



return
end subroutine Station_Ecriture


end module Station
 
 
