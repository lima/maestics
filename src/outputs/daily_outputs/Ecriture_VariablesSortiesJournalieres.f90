! ------------------------------------------------
!  ecriture des variables de sortie
!  les variables de .st2 sont dans var2.mod
!  les variables de .sti sont dans var.mod
! ------------------------------------------------
! writing of the output variables
!! variables. sti are in var.mod
subroutine Ecriture_VariablesSortiesJournalieres(sc,pg,p,soil,c,sta,plt_num)

    USE Stics
    USE Parametres_Generaux
    USE Plante
    USE Sol
    USE Climat
    USE Station

    implicit none

!: Arguments
    type(Stics_Communs_),       intent(INOUT)   :: sc  
    type(Parametres_Generaux_), intent(IN)      :: pg  
    type(Plante_),              intent(INOUT)      :: p  
    type(Sol_),                 intent(IN)      :: soil  
    type(Climat_),              intent(IN)      :: c  
    type(Station_),             intent(IN)      :: sta  
    integer , intent(IN) :: plt_num

!: Variables locales
    integer           :: i
    integer           :: k
    character         :: sep  

! using SticsFiles ------------------
    type(File_), pointer :: sort_file
    integer :: sort
    character(len = 20 ) :: tag
!    write(*,*)'n dans corres ',sc%n, p%zrac
    call CorrespondanceVariablesDeSorties(sc,p,soil,c,sta, sc%nbvarsortie,   &
                                          sc%valpar(1:sc%nbvarsortie),     &
                                          sc%valsortie(1:sc%nbvarsortie))


! Ecriture de l'entete dans .sti
! ==============================
      
      if ( p%ficsort(p%ipl) == 0 ) then
      ! Getting the sorties tag, for selecting the right file
      ! default report tag
      tag = 'daily'   
      ! if inter crops
      if (plt_num == 1 .and. sc%P_nbplantes > 1) then
         tag = 'daily_p'
      end if
      if (plt_num == 2) then 
         tag = 'daily_a'
      end if
      
      
      ! Getting sorties file infos
      ! TODO : set file_open as an output of Ecriture_VariablesSortiesJournalieres
      !call get_file(stics_files,trim(tag),sort_file)
      call get_file(trim(tag),sort_file)

      p%ficsort(p%ipl) = sort_file%unit

        write(p%ficsort(p%ipl),222)
222     format('ian;mo;jo;jul',$)
        do i = 1,sc%nbvarsortie
          write(p%ficsort(p%ipl),221) trim(sc%valpar(i))
        end do
221     format(';',A,$)

      ! on retourne a la ligne
        write(p%ficsort(p%ipl),*)

      endif
! ===============================

    sep = ';'
      ! getting the right unit for outputs
      sort = p%ficsort(p%ipl)
      if (pg%P_codeoutscient == 2) then
        write(sort,852) sc%annee(sc%jjul),sep,sc%nummois,sep,sc%jour,sep,sc%jul,(sep,sc%valsortie(k),k = 1,sc%nbvarsortie)
      else
        write(sort,851) sc%annee(sc%jjul),sep,sc%nummois,sep,sc%jour,sep,sc%jul,(sep,sc%valsortie(k),k = 1,sc%nbvarsortie)
      endif

! DR 03/03/2020 Pour Sam on ajoute de la precision aux sorties
 851  format(i4,a1,i2,a1,i2,a1,i3,200(a1,e16.7),/)
 ! 851  format(i4,a1,i2,a1,i2,a1,i3,200(a1,e12.3),/)


 852  format(i4,a1,i2,a1,i2,a1,i3,200(a1,f12.5),/)

return
end subroutine Ecriture_VariablesSortiesJournalieres
 
 
