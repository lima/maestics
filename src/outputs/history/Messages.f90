! Module Messages
!! Description :
!! management messages to history
module Messages

USE iso_varying_string
USE SticsFiles

! default values : stdout
integer :: fichist = 6
integer :: ficdebug = 6
integer :: ficdebug2 = 6
integer :: ficerrors = 6

logical :: write_histo = .FALSE.  
logical :: write_ecran = .FALSE.
logical :: write_debug = .FALSE.

! DR 18/03/2015 on augmente la taille du vecteur message qui etait insuffisante
integer :: nbMessages = 6500
type(varying_string) :: messagesStr(6500)


interface init_messages
  module procedure remplirMessages
end interface init_messages


interface EnvoyerMsgHistorique
module procedure e_EnvoyerMsgHistorique,r_EnvoyerMsgHistorique,         &
                 i_EnvoyerMsgHistorique,c_EnvoyerMsgHistorique,         &
                 cc_EnvoyerMsgHistorique,cc_r_EnvoyerMsgHistorique,     &
                 cc_i_EnvoyerMsgHistorique,cc_c_EnvoyerMsgHistorique,   &
                 cc_t_r_EnvoyerMsgHistorique
end interface EnvoyerMsgHistorique


interface EnvoyerMsgEcran
module procedure cc_EnvoyerMsgEcran, cc_r_EnvoyerMsgEcran, &
                 cc_i_EnvoyerMsgEcran, cc_l_EnvoyerMsgEcran, &
                 cc_c_EnvoyerMsgEcran, cc_t_r_EnvoyerMsgEcran, &
                 cc_t_i_EnvoyerMsgEcran, r_EnvoyerMsgEcran, &
                 t_r_EnvoyerMsgEcran, i_EnvoyerMsgEcran, &
                 t_i_EnvoyerMsgEcran, t_cc_EnvoyerMsgEcran
end interface EnvoyerMsgEcran


interface EnvoyerMsgErreur
module procedure cc_EnvoyerMsgErreur, cc_r_EnvoyerMsgErreur, &
                 cc_i_EnvoyerMsgErreur, cc_l_EnvoyerMsgErreur, &
                 cc_c_EnvoyerMsgErreur, cc_t_r_EnvoyerMsgErreur, &
                 cc_t_i_EnvoyerMsgErreur, r_EnvoyerMsgErreur, &
                 t_r_EnvoyerMsgErreur, i_EnvoyerMsgErreur, &
                 t_i_EnvoyerMsgErreur, t_cc_EnvoyerMsgErreur
end interface EnvoyerMsgErreur


!interface setSorties
!module procedure isetSorties,rsetSorties
!end interface setSorties

contains


!subroutine remplirMessages(langue)

    !character(len=2), intent(IN) :: langue  

!    select case(langue)

! DR 13/08/2012 maintenant tout est en anglais
!        case('FR')
!            call remplirMessages_FR(messagesStr,nbMessages) !TODO: taille definie en dur, a passer en dynamique un jour ?
!            write(*,*)'!!!! FR'
!        case default
!            call remplirMessages_ENG(messagesStr,nbMessages)
!            write(*,*)'!!!! ang'
!    end select

!return
!end subroutine remplirMessages


subroutine remplirMessages(flag_ecriture, status_out)
  implicit none

  integer, intent(IN) :: flag_ecriture
  logical, intent(OUT) :: status_out
  ! for units
  integer :: udbg, udbg2
  ! pointer to a File_ structure
  type(File_), pointer            :: fhisto, fdbg, fdbg2
  ! status for opening files
  logical                         :: open
  integer :: uhisto, uerr 
  status_out = .TRUE.
  open = .FALSE.

  ! Fixing files units: even if files are not used after that
  ! 
  !  call setFichierHistorique(get_file_unit(stics_files, 'histo'))
  call get_file_unit('histo', uhisto)
  call setFichierHistorique(uhisto)
  
  call get_file_unit('errors', uerr)
  !call setFichierErrors(get_file_unit(stics_files, 'errors'))
  call setFichierErrors(uerr)
  
  !udbg = get_file_unit(stics_files, 'dbg1')
  !udbg2 = get_file_unit(stics_files, 'dbg2')
!  udbg = get_file_unit( 'dbg1')
!  udbg2 = get_file_unit('dbg2')
   call get_file_unit('dbg1', udbg)
   call get_file_unit('dbg2', udbg2)

  
  call setFichierDebug(udbg, udbg2)
  
  if ( flag_ecriture == 0 ) then 
    return
  end if 
  
  call set_messages_flags
  
  if ( write_histo ) then
    call remplirMessages_ENG(messagesStr,nbMessages)
    !call open_file(stics_files,'histo',fhisto,open)
    call open_file('histo',fhisto,open)
    if ( .NOT.open ) status_out = open
    call EnvoyerMsgEcran("ouverture du fichier history")
    ! print *, "ecriture histo : ", write_histo
  end if 
  
  if ( write_debug ) then
    !call open_file(stics_files,'dbg1',fdbg,open)
    call open_file('dbg1',fdbg,open)
    if ( .NOT.open ) status_out = open
    call EnvoyerMsgEcran("ouverture du fichier de debug")
    !call open_file(stics_files,'dbg2',fdbg2,open)
    call open_file('dbg2',fdbg2,open)

    if ( .NOT.open ) status_out = open
    call EnvoyerMsgEcran("ouverture du fichier de debug2")
    ! print *, "ecriture debug : ", write_debug
  end if 

return
end subroutine remplirMessages


subroutine set_messages_flags

    call setFlagEcritureHisto(stics_files%flag_histo)
    !print *, "flag_histo", write_histo

    call setFlagEcritureEcran(stics_files%flag_ecran)
    
     !print *, "flag_ecran", write_ecran
     
     call setFlagEcritureDebug(stics_files%flag_debug)
    
     !print *, "flag_debug", write_debug

end subroutine set_messages_flags


subroutine setFichierHistorique(fic)

    integer, intent(IN) :: fic  

    fichist = fic
    
    ! print *, "histo unit : ",fichist

return
end subroutine setFichierHistorique


subroutine setFichierDebug(fic, fic2)

    integer, intent(IN) :: fic, fic2

    ficdebug = fic
    
    ficdebug2 = fic2
    
    ! print *, "debug unit : ",ficdebug
    ! print *, "debug2 unit : ",ficdebug2
return
end subroutine setFichierDebug

subroutine setFichierErrors(fic)

    integer, intent(IN) :: fic

    ficerrors = fic
    
    ! print *, "error unit : ",ficerrors
    
return
end subroutine setFichierErrors

subroutine setFlagEcritureHisto(flag)

    logical, intent(IN) :: flag  

    write_histo = flag

return
end subroutine setFlagEcritureHisto


subroutine setFlagEcritureEcran(flag)

    logical, intent(IN) :: flag  

    write_ecran = flag

return
end subroutine setFlagEcritureEcran


subroutine setFlagEcritureDebug(flag)

    logical, intent(IN) :: flag  

    write_debug = flag

return
end subroutine setFlagEcritureDebug


subroutine e_EnvoyerMsgHistorique(numMsg)

implicit none

    integer, intent(IN)           :: numMsg  

    if (.not. write_histo) return

    write (fichist,*) char(messagesStr(numMsg))

end subroutine e_EnvoyerMsgHistorique


subroutine r_EnvoyerMsgHistorique(numMsg, valeurs,fmt)

implicit none

    integer, intent(IN) :: numMsg  
    real,    intent(IN) :: valeurs  
    character(len=*), intent(IN), optional   :: fmt  

    if (.not. write_histo) return

    if (present(fmt)) then
      write (fichist,fmt) char(messagesStr(numMsg)), valeurs
    else
      write (fichist,*) char(messagesStr(numMsg)), valeurs
    endif

end subroutine r_EnvoyerMsgHistorique


subroutine i_EnvoyerMsgHistorique(numMsg, valeurs,fmt)

implicit none

    integer, intent(IN) :: numMsg  
    integer, intent(IN) :: valeurs  
    character(len=*), intent(IN), optional   :: fmt  

    if (.not. write_histo) return

    if (present(fmt)) then
      write (fichist,fmt) char(messagesStr(numMsg)), valeurs
    else
      write (fichist,*) char(messagesStr(numMsg)), valeurs
    endif

end subroutine i_EnvoyerMsgHistorique


subroutine c_EnvoyerMsgHistorique(numMsg, valeurs,fmt)

implicit none

    integer,           intent(IN)   :: numMsg  
    character(len=*), intent(IN)   :: valeurs  
    character(len=*), intent(IN), optional   :: fmt  

    if (.not. write_histo) return

    if (present(fmt)) then
      write (fichist,fmt) char(messagesStr(numMsg)), trim(valeurs)
    else
      write (fichist,*) char(messagesStr(numMsg)), trim(valeurs)
    endif

end subroutine c_EnvoyerMsgHistorique


subroutine cc_EnvoyerMsgHistorique(message)

implicit none

    character(len=*), intent(IN)   :: message  

    if (.not. write_histo) return

    write (fichist,*) message

end subroutine cc_EnvoyerMsgHistorique


subroutine cc_r_EnvoyerMsgHistorique(message,valeur,fmt)

implicit none

    character(len=*), intent(IN)   :: message  
    real,             intent(IN)   :: valeur  
    character(len=*), intent(IN), optional   :: fmt  

    if (.not. write_histo) return

    if (present(fmt)) then
      write (fichist,fmt) message, valeur
    else
      write (fichist,*) message, valeur
    endif

end subroutine cc_r_EnvoyerMsgHistorique


subroutine cc_t_r_EnvoyerMsgHistorique(message,valeur,fmt)

implicit none

    character(len=*), intent(IN)   :: message  
    real,             intent(IN)   :: valeur(:)  
    character(len=*), intent(IN), optional   :: fmt  

    if (.not. write_histo) return

    if (present(fmt)) then
      write (fichist,fmt) message, valeur
    else
      write (fichist,*) message, valeur
    endif

end subroutine cc_t_r_EnvoyerMsgHistorique


subroutine cc_i_EnvoyerMsgHistorique(message,valeur,fmt)

implicit none

    character(len=*), intent(IN)   :: message  
    integer,          intent(IN)   :: valeur  
    character(len=*), intent(IN), optional   :: fmt  

    if (.not. write_histo) return

    if (present(fmt)) then
      write (fichist,fmt) message, valeur
    else
      write (fichist,*) message, valeur
    endif

end subroutine cc_i_EnvoyerMsgHistorique


subroutine cc_c_EnvoyerMsgHistorique(message,valeur,fmt)

implicit none

    character(len=*), intent(IN)   :: message  
    character(len=*), intent(IN)   :: valeur  
    character(len=*), intent(IN), optional   :: fmt  

    if (.not. write_histo) return

    if (present(fmt)) then
      write (fichist,fmt) message, trim(valeur)
    else
      write (fichist,*) message, trim(valeur)
    endif

end subroutine cc_c_EnvoyerMsgHistorique


! For sending messages to screen
! message
subroutine cc_EnvoyerMsgEcran(message)

implicit none

    character(len=*), intent(IN)   :: message  

    if (.not. write_ecran) return

    !print *, write_ecran
    write (*,*) trim(message)

end subroutine cc_EnvoyerMsgEcran


! message table
subroutine t_cc_EnvoyerMsgEcran(message_t)

implicit none

    character(len=*), intent(IN)   :: message_t(:)
    character(len=100) :: message
    integer :: i, sz
    
    sz = size( message_t )
    message = ""
    
    if ( sz == 0 ) return

    if ( .not. write_ecran ) return
    
    message = trim( message_t(1) )
    do i = 2, sz
      message = trim( message ) // "     " // trim( message_t(i) )
    end do
    
    !print *, write_ecran
    write (*,*) trim( message )
   

end subroutine t_cc_EnvoyerMsgEcran


! real
subroutine r_EnvoyerMsgEcran(valeur)

implicit none
  
    real,             intent(IN)   :: valeur  
    
    if ( .not. write_ecran ) return
   
    write (*,*) valeur

end subroutine r_EnvoyerMsgEcran


! real table
subroutine t_r_EnvoyerMsgEcran(valeur)

implicit none

    real,             intent(IN)   :: valeur(:)
    
    if ( .not. write_ecran ) return
   
    write (*,*) valeur

end subroutine t_r_EnvoyerMsgEcran


! integer
subroutine i_EnvoyerMsgEcran(valeur)

implicit none
  
    integer,             intent(IN)   :: valeur  
    
    if ( .not. write_ecran ) return
   
    write (*,*) valeur

end subroutine i_EnvoyerMsgEcran


! integer table
subroutine t_i_EnvoyerMsgEcran(valeur)

implicit none

    integer,             intent(IN)   :: valeur(:)
    
    if ( .not. write_ecran ) return
   
    write (*,*) valeur

end subroutine t_i_EnvoyerMsgEcran


! message + real
subroutine cc_r_EnvoyerMsgEcran(message,valeur)

implicit none

    character(len=*), intent(IN)   :: message  
    real,             intent(IN)   :: valeur  
    
    if ( .not. write_ecran ) return
   
    write (*,*) trim(message)," ", valeur

end subroutine cc_r_EnvoyerMsgEcran


! message + real table
subroutine cc_t_r_EnvoyerMsgEcran(message,valeur)

implicit none

    character(len=*), intent(IN)   :: message  
    real,             intent(IN)   :: valeur(:)
    
    if ( .not. write_ecran ) return
   
    write (*,*) trim(message)," ", valeur

end subroutine cc_t_r_EnvoyerMsgEcran


! message + integer
subroutine cc_i_EnvoyerMsgEcran(message,valeur)

implicit none

    character(len=*), intent(IN)   :: message  
    integer,             intent(IN)   :: valeur  
    
    if ( .not. write_ecran ) return
   
    write (*,*) trim(message)," " , valeur

end subroutine cc_i_EnvoyerMsgEcran


! message + integer table
subroutine cc_t_i_EnvoyerMsgEcran(message,valeur)

implicit none

    character(len=*), intent(IN)   :: message  
    integer,             intent(IN)   :: valeur(:)
    
    if ( .not. write_ecran ) return
   
    write (*,*) trim(message)," ", valeur

end subroutine cc_t_i_EnvoyerMsgEcran


! message + logical
subroutine cc_l_EnvoyerMsgEcran(message,valeur)

implicit none

    character(len=*), intent(IN)   :: message  
    logical,             intent(IN)   :: valeur  
   
   if ( .not. write_ecran ) return
   
    write (*,*) trim(message)," ", valeur

end subroutine cc_l_EnvoyerMsgEcran


! message + char
subroutine cc_c_EnvoyerMsgEcran(message,valeur)

implicit none

    character(len=*), intent(IN)   :: message  
    character(len=*), intent(IN)   :: valeur  
    
    if ( .not. write_ecran ) return
   
    write (*,*) trim(message)," ", trim(valeur)

end subroutine cc_c_EnvoyerMsgEcran




subroutine cc_EnvoyerMsgErreur(message)

implicit none

    character(len=*), intent(IN)   :: message  

    ! if (.not. write_histo) return

    write (ficerrors,*) trim(message)

end subroutine cc_EnvoyerMsgErreur


! message table
subroutine t_cc_EnvoyerMsgErreur(message_t)

implicit none

    character(len=*), intent(IN)   :: message_t(:)
    character(len=100) :: message
    integer :: i, sz
    
    sz = size( message_t )
    message = ""
    
    if ( sz == 0 ) return

    !if ( .not. write_Erreur ) return
    
    message = trim( message_t(1) )
    do i = 2, sz
      message = trim( message ) // "     " // trim( message_t(i) )
    end do
    
    !print *, write_Erreur
    write (ficerrors,*) trim( message )
   

end subroutine t_cc_EnvoyerMsgErreur


! real
subroutine r_EnvoyerMsgErreur(valeur)

implicit none
  
    real,             intent(IN)   :: valeur  
    
    !if ( .not. write_Erreur ) return
   
    write (ficerrors,*) valeur

end subroutine r_EnvoyerMsgErreur


! real table
subroutine t_r_EnvoyerMsgErreur(valeur)

implicit none

    real,             intent(IN)   :: valeur(:)
    
    !if ( .not. write_Erreur ) return
   
    write (ficerrors,*) valeur

end subroutine t_r_EnvoyerMsgErreur


! integer
subroutine i_EnvoyerMsgErreur(valeur)

implicit none
  
    integer,             intent(IN)   :: valeur  
    
    !if ( .not. write_Erreur ) return
   
    write (ficerrors,*) valeur

end subroutine i_EnvoyerMsgErreur


! integer table
subroutine t_i_EnvoyerMsgErreur(valeur)

implicit none

    integer,             intent(IN)   :: valeur(:)
    
    !if ( .not. write_Erreur ) return
   
    write (ficerrors,*) valeur

end subroutine t_i_EnvoyerMsgErreur


! message + real
subroutine cc_r_EnvoyerMsgErreur(message,valeur)

implicit none

    character(len=*), intent(IN)   :: message  
    real,             intent(IN)   :: valeur  
    
    !if ( .not. write_Erreur ) return
   
    write (ficerrors,*) trim(message)," ", valeur

end subroutine cc_r_EnvoyerMsgErreur


! message + real table
subroutine cc_t_r_EnvoyerMsgErreur(message,valeur)

implicit none

    character(len=*), intent(IN)   :: message  
    real,             intent(IN)   :: valeur(:)
    
    !if ( .not. write_Erreur ) return
   
    write (ficerrors,*) trim(message)," ", valeur

end subroutine cc_t_r_EnvoyerMsgErreur


! message + integer
subroutine cc_i_EnvoyerMsgErreur(message,valeur)

implicit none

    character(len=*), intent(IN)   :: message  
    integer,             intent(IN)   :: valeur  
    
    !if ( .not. write_Erreur ) return
   
    write (ficerrors,*) trim(message)," " , valeur

end subroutine cc_i_EnvoyerMsgErreur


! message + integer table
subroutine cc_t_i_EnvoyerMsgErreur(message,valeur)

implicit none

    character(len=*), intent(IN)   :: message  
    integer,             intent(IN)   :: valeur(:)
    
    !if ( .not. write_Erreur ) return
   
    write (ficerrors,*) trim(message)," ", valeur

end subroutine cc_t_i_EnvoyerMsgErreur


! message + logical
subroutine cc_l_EnvoyerMsgErreur(message,valeur)

implicit none

    character(len=*), intent(IN)   :: message  
    logical,             intent(IN)   :: valeur  
   
   !if ( .not. write_Erreur ) return
   
    write (ficerrors,*) trim(message)," ", valeur

end subroutine cc_l_EnvoyerMsgErreur


! message + char
subroutine cc_c_EnvoyerMsgErreur(message,valeur)

implicit none

    character(len=*), intent(IN)   :: message  
    character(len=*), intent(IN)   :: valeur  
    
    !if ( .not. write_Erreur ) return
   
    write (ficerrors,*) trim(message)," ", trim(valeur)

end subroutine cc_c_EnvoyerMsgErreur

!=========================
! subroutine iSetSorties( varname, varval)

! character(len=*) :: varname  
! integer :: varval  

! ! a faire

! end subroutine

! subroutine rSetSorties( varname, varval)

! character(len=*) :: varname  
! real :: varval  

! ! a faire

! end subroutine

end module Messages
 
 
