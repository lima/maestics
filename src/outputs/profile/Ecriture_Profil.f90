! ************************************
! *  ecriture du fichier profil.sti  *
! ************************************
! File writing profil.sti
subroutine Ecriture_Profil(sc,profsol)

USE Stics
USE SticsFiles

    implicit none

    type(Stics_Communs_), intent(INOUT) :: sc  
    integer,intent(IN)           :: profsol
    integer :: nbjParAnnee

! les VARIABLES LOCALES
    integer :: i
    integer :: j
    integer :: k  
    integer :: jour,nummois,jjul,an1,an2
    character(len=3) :: mois
    character(len=13) :: nomvar(16)

! DR 05/06/2019 j'ajoute les varaibles Chum et Nhum
    data nomvar/'tsol', 'hur', 'amm', 'nit', 'lracz', 'rlf', 'rlg', 'rl', '%rl', 'msrac', 'humirac', 'efnrac',  &
                 'Chum', 'Nhum','C_allresidues','N_allresidues'/

    ! using SticsFiles ------------------
    type(File_), pointer :: prof_file
    logical :: file_open, status
    integer :: prof, file_closed
  
    status = .TRUE.

    ! Opening mod_profil.sti file infos, if not already open
    ! TODO : use file_status output of Ecriture_Profil
    !call open_file(stics_files,'prof_sti',prof_file, file_open)
    call open_file('prof_sti',prof_file, file_open)
    ! TODO: return if any pb on file open
    if ( .NOT. file_open ) then 
        status = .FALSE.
        ! write a message in the errors file !!!
        ! return
    endif

    prof = prof_file%unit
    sc%numdateprof(sc%ipl) = sc%numdateprof(sc%ipl)-1
    write(prof,*) sc%valprof

    ! sinon ca plante.
    if (sc%numdebprof(sc%ipl) == 0 .or. sc%numdateprof(sc%ipl) <= 0) return

! DR 01/04/2015 pas de poissson ...
         ! on transpose n en jour julien depuis le 1er janvier de l'annee de debut de simulation
          ! sc%jjul = tCal1JAbs(sc%n,sc%P_iwater)
         ! a partir de jjul, on calcule le jour julien relatif a l'annee en cours de simulation
         ! TODO : verifier que les nouvelles dates calculees correspondent aux valeurs attendues.
         !  sc%jul = tCal1JRel(sc%jjul,sc%annee(sc%P_iwater),.false.)
         !  sc%numdate = sc%jul
           do k=sc%numdebprof(sc%ipl),sc%numdateprof(sc%ipl)
              ! jjul = tCal1JAbs(sc%dateprof(sc%ipl,k),sc%P_iwater)
               jjul = sc%dateprof(sc%ipl,k)
               ! dr 02/04/2015 verifier l'annee
               an1=sc%annee(sc%P_iwater)
               an2=sc%annee(sc%P_ifwater)
               if(sc%dateprof(sc%ipl,k).gt.nbjParAnnee(an1))jjul=jjul-nbjParAnnee(an1)
               call julien(jjul,sc%annee(sc%dateprof(sc%ipl,k)),mois,jour,nummois)
!               write(prof,*) sc%dateprof(sc%ipl,k),  sc%annee(sc%dateprof(sc%ipl,k)),nummois,jour
           enddo


    write(prof,210)(sc%dateprof(sc%ipl,j),j=sc%numdebprof(sc%ipl),sc%numdateprof(sc%ipl))

210 format(' cm ',600(3x,i5))

    do i=1,profsol
      write(prof,100)i,(sc%tabprof(sc%ipl,k,i),k=sc%numdebprof(sc%ipl),sc%numdateprof(sc%ipl))
    end do
100 format(i5,600f11.5)
    !close(prof)
       
    ! Closing the file
    ! If only one plant or 2 with plant number == 2
    if ( (sc%P_nbplantes == 1) .OR. ( (sc%P_nbplantes > 1 ).AND. & 
       (sc%ipl == 2))) then

       file_closed = closeFile(prof_file)
       if ( file_closed > 0 ) then
         status = .FALSE.
       end if 
    end if


return
end subroutine Ecriture_Profil
 
 
