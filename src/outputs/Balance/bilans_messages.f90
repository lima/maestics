! This subroutine contains the messages written to the file mod_b*.sti
subroutine bilans_messages

USE Bilans

implicit none

mes1007 = "(' 1. INPUT DATA:       INTERCROPPING',/1x,34('*'))"
mes1008 = "(' Balance of the STICS simulation ',a36,', model ',A7/1x,62('*'))"

mes1009 = "(' 1. INPUT DATA',/1x,19('*'))"
mes1010 = "(3x,'Weather file           :',A50)"
mes1011 = "(3x,'Cropping practices file:',A50)"
mes10111 ="(3x,'Initialisations file   :',A50)"
mes1012 = "(3x,'Plant file             :',A50)"
mes2014 = "(3x,'Cultivar: ',A10,6x,'variety group =',i2)"
mes1013 = "(3x,'LAI file               : ',A12)"
mes1014 = "(3x,'Initial soil values    : ',A12,/,6x,'Z (cm)',6x,'Water (%)   NO3 (kg/ha)    NH4 (kg/ha)')"
mes1020 = "(3x,'Initial plant values   :',/,3x,'stage   LAI   masec  zrac  magrain QNplante INN restemp')"
mes1021 = "(/3x,'Start of simulation   :',I3,'-',A3,'-',I4,6x,'day',I4)"
mes1022 = "( 3x,'End of simulation     :',I3,'-',A3,'-',I4,6x,'day',i4,3x,'(or',i4,')')"
mes1023 = "(4x,'Irrigation:',11x,'Number of irrigations =',i2)"
mes1025 = "(5x,'10-day period of irrigation',8x,'amount (mm)',/, 5x,'-----------------' ,8x,'---------')"
mes1024 = "(8x,'date',8x,'amount (mm)',/,5x,'---------------',   12x,'---------')"
mes1125 = "(5x,'Total quantity applied =',F7.0,' mm')"
mes1026 = "(/3x,'Fertilisation:',8x,'Number of applications =',i2,:,5x,'Fertilizer type =',i2)"
mes1027 = "(3x, '       date      amount (kg N/ha)  fertilizer type'),/,&
         &  3x, '    -----------  ----------------  ---------------')"
mes1028 = "(8x,'total',f17.0)"
mes1029 = "(/3x,'Organic residues and/or soil tillage')"
mes1030 = "(9x,'No application, no tillage')"
!mes1031 = "(5x,'Tillage ',          15x,'day',i4,4x,'on',f4.0,'cm')"
mes1031 = "(5x,'Tillage ',          15x,' made on day',i4,4x,'over',f4.0,' -',f4.0,' cm')"
!mes1032 = "(5x,'Residues of type',i3,4x,'day',i4,4x,'on',f4.0,'cm',4x,'DM=',f4.1,' t/ha',4x,'Capp=',f5.0,' kg/ha',4x,'Napp=', &
!         &  f4.0,' kg/ha',4x,'Nmin=',f4.0,' kg/ha')"
mes1032 = "(5x,'Residues of type',i3,4x,'added on day',i4,4x,'DM=',f4.1,' t/ha',4x,'Capp=',f5.0,' kg/ha',4x,'Napp=', &
         &  f4.0,' kg/ha',4x,'Nmin=',f4.0,' kg/ha')"

mes1033 = "(/,' 2. CROP DEVELOPMENT',/,1x,19('*'))"
mes1040 = "(5x,'type                   : long-day plant')"
mes1041 = "(5x,'type                   : short day plant')"
mes1042 = "(5x,'development driven by vernalo-photo-thermal units')"
mes1042b = "(5x,'development unit : chilling-photo-thermic')"
mes1043 = "(5x,'development driven by photo-thermal units')"
mes1044 = "(5x,'development driven by thermal units')"
mes1044b = "(5x,'development unit : chilling-thermic')"
mes1044c = "(5x,'development driven by vernalo-thermal units')"
mes1045 = "(5x,'temperature used : air temperature')"
mes1046 = "(5x,'temperature used : crop temperature')"
mes1050 = "(/5x,'Vernalisation or dormancy impossible: temperatures too high')"
!mes1051 = "(/8x,'stage',57x,'date',11x,'units',5x,'cumulative units',/5x,11('-'),7x,53('-'),9x,6('-'),5x,15('-'))"
mes1052 = "(5x,'initial stage   : ',a3)"
mes1053 = "('   Vegetative stages')"
mes1054 = "('   Reproductive stages')"
mes1055 = "(/,'   Length of cycle =',i3,' days')"
mes1056 = "(/,' Attention: for this simulation the date of harvest is a finishing date',/,&
    &' the sum of units from sowing till harvest can not have been reached on this date')"

mes1057 = "(/,' 3. GROWTH AND COMPONENTS OF YIELD'/1x,34('*'))"
mes1157 = "( 3x,' ',I3,'-',A3,'-',I4)"
mes1058 = "( 3x,'Aerial biomass at harvest (0% water)=',f8.2,' t/ha'/3x,       &
           &    'Grain or fruit Yield   (0% water)   =',f8.2,' t/ha'/3x,       &
           &    'Grain or fruit Yield (',f3.0,'% water)   =',f8.2,' t/ha'//3x, &
           &    'Senescent aerial biomass (0% water) =',f8.2,' t/ha')"
mes1059 = "( 3x,'Plant density                       =',f8.1,' /m2' &
           &/3x,'Grain or fruit weight (',f3.0,'% water)  = ',f9.3,' g')"
mes1060 = "(/3x,'Number of grains or fruits          =',f8.0,' /m2')"
mes1160 = "( 3x,'Number of inflorescences            =',f8.1)"
mes1161 = "( 3x,'Number of unripe fruits =',f8.0,'/m2, or a weight',' dry =',f8.2,'t/ha')"
mes1159 = "( 3x,'Growth rate (lag phase)             =', f8.1,' mg/m2/j')"
mes1360 = "( 3x,'Pruning Weight  (0% water)          =',f8.2,' t/ha')"
mes1361 = "( 3x,'Final biomass reserves  (0% water)  =',f8.2,' t/ha')"
mes2092 = "( 3x,'Number of emerged leaves            =',i6)"
mes2100 = "( 3x,'Number of hot or cold days          =',i6)"
mes1061 = "(/3x,'Quantity of N in the crop           =',f7.0,' kg/ha',  &
         &  /3x,'Quantity of N in grains or fruits   =',f7.0,' kg/ha',  &
         &  /3x,'Quantity of N in roots              =',f7.0,' kg/ha',  &
         & //3x,'N content of whole plant            =',f6.2,' % DM')"
mes1461 = "( 3x,'N concentration of grains or fruits =',f6.2,' % DM',/3x,&
             &  'Protein content of grains or fruits =',f6.1,' % DM')"
mes1258 = "(/3x,'Nitrogen fertiliser efficiency      =',f5.2)"
mes1158 = "( 3x,'Harvest index                       =',f5.2)"
mes1261 = "(/2x,'Composition of grains or fruits',/3x,&
             &  'Dry matter content                  =',f6.0,' % FW'/3x,&
             &  'Nitrogen content                    =',f6.2,' % FW')"
mes1262 = "( 3x,'Sugar content                       =',f6.1,' % FW')"
mes1263 = "( 3x,'Oil content                         =',f6.1,' % FW')"

mes1063 = "(/' 4. WATER and NITROGEN BALANCE over the crop life',/,1x,48('*'))"
mes1064 = "(5x,'Cumulative ETMax (eos+eop)     =',f6.0,' mm',/5x, &
         &     'Cumulative AET                 =',f6.0,' mm',&
         & /5x,'Cumulative Evaporation         =',f6.0,' mm',/5x, &
         &     'Cumulative Transpiration       =',f6.0,' mm',&
         & /5x,'Cumulative Rainfall+irrigation =',f6.0,' mm',/)"
mes1065 = "(5X,'Water reserve used by plant    =',f6.0,' mm',/5x,&
         &     'Maximum rooting depth          =',f6.0,' cm')"
mes1066 = "(/3x,'Mean STRESS indices :              swfac    turfac    inns   tcult-tair  exofac',&
         &     '    etr/etm   etm/etr',/,3x,'vegetative phase    (lev-drp)',7f10.2, &
         & /,3x,'reproductive phase  (drp-mat)',7f10.2)"
mes1166 = "(/4x,'Frost damage on leaves : before AMF  ',f5.0,'%',4x,'after AMF',f5.0,'%')"
mes1167 = "( 3x,'Plant frozen with no more reserves')"
mes1168 = "( 3x,'Frost damage to flowers or fruit     ',f5.0,'%')"
mes1067 = "('Nitrogen stress calculated but inactive on the plants')"
mes1068 = "('Water stress calculated but inactive on the plants')"

mes1069 = "(/,' 5. WATER, NITROGEN, CARBON BALANCE over the whole simulation period  (',i3,' days)'/,1x,79('*'))"
mes1070 = "(3x,'Normalised days at',f4.0,'degree C:',4x,'Humus =',f5.0,6x,'Residues =',f5.0)"
mes4070 = "(3x,'Potential mineralisation rate =',f5.2,' kg N/ha/day',3x,'or',f5.2,'% per year')"
mes1072 = "('WATER BALANCE  (mm)')"
mes1073 = "(&
     &  5x,'initial water content  ',f6.0,8x,'final water content    ',f6.0,&
     & /5x,'rain                   ',f6.0,8x,'evaporation            ',f6.0,&
     & /5x,'irrigation             ',f6.0,8x,'transpiration          ',f6.0,&
     & /5x,'capillary rise         ',f6.0,8x,'runoff                 ',f6.0,&
     & /42x,                                 'deep infiltration      ',f6.0,&
     & /42x,                                 'mole drainage          ',f6.0,&
     & /42x,                                 'leaf interception      ',f6.0,&
     & /42x,                                 'mulch interception     ',f6.0,&
     & /42x,                                 'ineffective irrigation ',f6.0,&
     & /5x,29('-'),8x,29('-'),&
     & /5x,'TOTAL INITIAL          ',f6.0,8x,'TOTAL FINAL            ',f6.0)"

mes1074 = "(/'PLANT NITROGEN BALANCE (kg N/ha)',                            /5x,&
           &'------ Initial pools ------',9x,'------- Final pools -------', /5x,&
           &'Aerial plant N        ',f6.0,8x,'                       ',f6.0,/5x,&
           &'Root N                ',f6.0,8x,'                       ',f6.0,/5x,&
           &'Perennial reserves N  ',f6.0,8x,'                       ',f6.0,/5x,&
           &'Total plant N         ',f6.0,8x,'                       ',f6.0,/5x,&
           &'------- Input fluxes ------',9x,'------ Output fluxes ------', /5x,&
           &'N uptake              ',f6.0,8x,'N exported             ',f6.0,/5x,&
           &'N fixation            ',f6.0,8x,'N returned to soil     ',f6.0,/5x,&
           & 28('-'),8x,28('-'),                                            /5x,&
           &'TOTAL INITIAL         ',f6.0,8x,'TOTAL FINAL            ',f6.0,/)"

mes1076 = "(/'SOIL MINERAL NITROGEN BALANCE (kg N/ha)',                     /5x,&
           &'------ Initial pools ------',9x,'------- Final pools -------', /5x,&
           &'NH4-N                 ',f6.0,8x,'                       ',f6.0,/5x,&
           &'NO3-N                 ',f6.0,8x,'                       ',f6.0,/5x,&
           &'Total mineral N       ',f6.0,8x,'                       ',f6.0,/5x,&
           &'------- Input fluxes ------',9x,'------ Output fluxes ------', /5x,&
           &'                      ',14x,    'plant N uptake         ',f6.0,/5x,&
           &'rain                  ',f6.0,8x,'leaching               ',f6.0,/5x,&
           &'irrigation            ',f6.0,8x,'leaching in mole drains',f6.0,/5x,&
           &'fertiliser            ',f6.0,8x,'fertiliser immobilisat.',f6.0,/5x,&
           &'                      ',14x,    'fertiliser volatilisat.',f6.0,/5x,&
           &'humus mineralisation  ',f6.0,8x,'manure N volatilisat.  ',f6.0,/5x,&
           &'residue mineralisation',f6.0,8x,'(N2+N2O)-N losses      ',f6.0,/5x,&
           & 28('-'),8x,28('-'),                                            /5x,&
           &'TOTAL INITIAL         ',f6.0,8x,'TOTAL FINAL            ',f6.0,/)"             

 mes1080 = "('SOIL ORGANIC NITROGEN BALANCE (kg N/ha)')"
 mes1081 = "(/'SOIL ORGANIC CARBON BALANCE   (kg C/ha)')"
 mes1082 ="(5x,&
           &'------ Initial pools ------',9x,'------- Final pools -------',/5x,&
           &'Active humified       ',f8.0,6x,'                     ',f8.0,/5x,&
           &'Inert humified        ',f8.0,6x,'                     ',f8.0,/5x,&
           &'Zymogeneous biomass   ',f8.0,6x,'                     ',f8.0,/5x,&
           &'Organic residues      ',f8.0,6x,'                     ',f8.0,/5x,&
           &'Deep root residues    ',f8.0,6x,'                     ',f8.0,/5x,&
           &'Mulch residues        ',f8.0,6x,'                     ',f8.0,/5x,&
           &'Total organic pools   ',f8.0,6x,'                     ',f8.0,/5x,&
           &'------- Input fluxes ------',9x,'------ Output fluxes ------',/5x,&
           &'fertiliser immobilis. ',f8.0,6x,                             /5x,&
           &'organic fertiliser    ',f8.0,6x,                             /5x,&
           &'crop residues         ',f8.0,6x,                             /5x,&
           &'fallen leaves         ',f8.0,6x,'Priming (PE)         ',f8.0,/5x,&
           &'trimmed leaves        ',f8.0,6x,'Mineralisation - PE  ',f8.0,/5x,&
           &'dead roots            ',f8.0,6x,                             /5x,&
           &'dead perennial organs ',f8.0,6x,                             /5x,&
           & 28('-'),8x,28('-'),                                          /5x,&
           &'TOTAL INITIAL         ',f8.0,6x,'TOTAL FINAL          ',f8.0)"

mes1083 = "(/,'Heterotrophic Respiration (kg C/ha): Residues =',f6.0,/,&
             &'                                     Humus    =',f6.0,/,&
             &'                                     Total    =',f6.0)"

mes1084 = "(/,'Cumulative N2O emissions  (kg N/ha): nitrification      =',f6.2,/,&
             &'                                     denitrification    =',f6.2,/,&
             &'                                     Total              =',f6.2)"

mes1062b = "(/,'Aboveground CROP RESIDUES returned to soil:  ',10x,   'DM =',f5.1,' t/ha',3x,'C content =',f6.0,' kg/ha',   &
         &  3x,'N content =',f6.0,' kg/ha',3x,'C/N =',f5.1)"
mes1062 =  "(  'Total CROP RESIDUES returned to soil:',a15,3x,'DM =',f5.1,' t/ha',3x,'C content =',f6.0,' kg/ha',   &
         &  3x,'N content =',f6.0,' kg/ha',3x,'C/N =',f5.1)"

mes3181  = "(//,'   &&&&&&&&&&&&&&&&&&&&&&&&&&&&&  ',i4,'   &&&&&&&&&&&&&&&&&&&&&&&&&&&&&',//)"
mes3008  = "(3x,'weather station at an altitude of    ',f6.1,' m')"
mes3009  = "(3x,'simulation at an altitude of      ',f6.1,' m')"
mes3110  = "(' adret (south) option ')"
mes3111  = "(' ubac (north) option ')"
mes3101   = "('     harvest num',i2,' on ',i2,'-',a3,'-',i4,': ',f7.2,'t/ha (',f8.1,' fruits/m2 -- ',f6.2,'% FW)' )"

mes3011    = "(/,'B / Ldrains (cm) / Ksol (cm/j) / profdrain (cm)/sensanox /distdrain:',/,f5.2,' /',f8.1,' /',f8.1,3(' /',f7.2))"
mes3015    = "('  Permeability to bottom of profile (mm) : ',f5.2)"

mes2001 = "(' Balance of the STICS simulation ',a40,', model ',A7,' forage version'/1x,67('*'))"
mes2012 = "(3x,'Plant file                : ',A15)"
mes2017 = "(5x,'End of simulation   : ',I2,'-',A3,'-',I4,6x,'day',I4)"
mes2018 = "(3x,'In this simulation the mowing dates are imposed by the calendar')"
mes2019 = "(3x,'In this simulation the mowing dates are calculated by the temperature sums')"
mes2020 = "(3x,'In this simulation the mowing dates are calculated at stage ',a3)"
mes2021 = "('   Irrigation read ')"
mes2022 = "(' ATTENTION: irrigations applied automatically when the water stress falls below ',f4.2)"
mes20221 = "(' The authorized period for calculated irrigations is : ',i3,' and ',i3)"
mes2023 = "('   Fertilisation read')"
mes2024 = "(' ATTENTION: fertilisations applied automatically when the N stress index falls below ',f4.2)"
mes2025 = "(/,5x,'SUMMARY: Production and Balances'/1x,38('*'))"
mes2026 = "(/,3x,'Irrigation')"
mes2027 = "(5x,'Number of cuts =',I2,'    Day: ',8I5)"
mes2029 = "(5x,'Number of irrigations =',I2)"
mes2030 = "(/3x,'Fertilisation')"
mes2031 = "('In this simulation the sums of cuts are calculated in photo-vernalo-thermal units')"
mes2032 = "('In this simulation the sums of dish are calculated in thermal units (t air)')"
mes2033 = "(5x,'Total quantity applied until cutting:',F9.0)"
mes2034 = "(/,4x,'Residues of previous crop : no residues')"
mes2035 = "(/,4x,'Residues of the previous crop :',5x,'quantity =',f5.1,' t/ha',9x,'C/N =',f5.0, &
          &  /4x,'incorporated on day',i4,' to a depth of',f4.0,' cm')"
mes2036 = "(/8x,'cut',15x,'date'/5x,11('-'),7x,11('-'))"
mes2037 = "(4x,'Total amount of N in harvested cuts =',f6.0,' kg/ha'/)"
mes2038 = "(4x,'Total aerial biomass harvested (0%) =',f6.2,' t/ha')"
mes2040b = "(4x,'Aboveground residues left on soil: ',4x,'Biomass =',f4.1,' t/ha',4x,'N content =',f4.0)"
mes2040 =  "(4x,'Residues left on soil: ',a20,2x,'Biomass =',f4.1,' t/ha',4x,'N content =',f4.0,4x,'C/N =',f4.0/)"

mes2041 = "('4. WATER and NITROGEN BALANCES',/,'   Start cut 1 to end of simulation',/,50('*'))"
mes2064 = "(/,' 1.CUT NUMBER ',i2, I6,'-',A3,'-',I4,'   day ',i3/,1x,40('*'))"
mes2065 = "('    cutting day : ',I2,'-',A3,'-',I4,' , day ',i3)"
mes2068 = "('    The residual biomass is calculated from the cutting height (',f4.1,' cm)')"
mes2069 = "('    The residual biomass is read in the tec file : lai ',f6.3,' ms ',f6.3)"
mes2066 = "('    the cutting date has been delayed ( aerial biomass < ',f5.2, 't/ha)')"
mes2067 = "(4x,'Total quantity applied until the cutting date =',F6.0,' mm')"
mes2076 = "(/8x,'stage',12x,'BBCH',7x,'date',9x,'units',2x,'cumulative units',2x,'lai', &
        &   /3x,12('-'),16x,11('-'),2x,5('-'),2x,16('-'),2x,3('-'))"
mes2083 = "(4x,'Aerial biomass (0%) =',f6.2,' t/ha including',f6.2,' t/ha of green and',f6.2,' t/ha of senescent biomass')"
mes2084 = "(4x,'Residual biomass coming from previous growth =',f6.2,' t/ha',8x,'Newly-formed biomass =',f6.2,' t/ha')"
mes2085 = "(4x,'Residual green biomass =',f6.2,' t/ha',15x,'Residual biomass for the next cycle =',f6.2,' t/ha')"
mes2039 = "(4x,'N in the crop at harvest =',f5.0,' kg/ha',13x,'N concentration =',f6.2,'%')"
mes2042 = "(4x,'Fraction of cut biomass exported =',f5.0,'%')"

mes2082 = "(4x,'Harvested biomass (0%) =',f6.2,' t/ha', 15x,'Cut biomass returned to soil  =',f6.2)"
mes2098 = "(4x,'N in harvested biomass =',f6.0,' kg/ha',14x,'N in biomass returned to soil =',f6.0)"
mes2087 = "(4x,'Plant density=',f8.2,' /m2'/)"
mes2088 = "(/,'4. Between-cut WATER and NITROGEN BALANCE (mowing',i2,' to mowing',i2,')',/,64('*'))"
mes2089 = "(/4x,'Mean STRESS indices:            swfac   turfac   inns   tcult-tair', &
       &    /4x,'vegetative phase  (lev-drp) ',5f8.2)"
mes2090 = "(2x,'  reproductive phase (drp-mat) ',5f8.2)"
mes2091 = "(2x,'last age classes did not reach maturity')"
mes2094 = "(5x,'Beginning of dormancy ',10x,I2,'-',A3,'-',I4)"
mes2093a = "(5x,'End of dormancy  ',16x,I2,'-',A3,'-',I4,' (Forcing)')"
mes2093 = "(5x,'End of dormancy  ',16x,I2,'-',A3,'-',I4,f9.0,'(Richardson)')"
mes2095 = "(5x,'End of dormancy  ',16x,I2,'-',A3,'-',I4,f9.0,'(Bidabe)')"
 mes4001 = "('   the application dates are fixed in upvt')"
! DR 30112020 merge trunk
!DR 13/02/2020 le message du type d'irrigation quand c'est en sumupvt n'etait pas fait
 mes10024 = "('   the irrigation dates are fixed in upvt')"

 mes2096 = "('the cut has been delayed by',i4,' days with regard to initial date')"
 mes2099 = "(i4,':', 3x,'===== Death of the plant due to lack of nitrogen ===')"
 mes2097 = "(3x,'===== Death of the plant due to lack of nitrogen =====')"

 mes410 = "('Trimming')"
 mes411 = "('Leaf thinning')"
 mes412 = "('   Harvest operation: whole plant harvested')"
 mes414 = "('   Harvest operation: fruit picking at end of cycle')"
 mes415 = "('   Harvest operation: fruit picking, once every',i5,' days')"
 mes417 = "('   Harvest objective: physiological maturity')"
 mes418 = "('   Harvest objective: according to water content')"
 mes419 = "('   Harvest objective: according to sugar content')"
 mes420 = "('   Harvest objective: according to protein content')"
 mes421 = "('   Harvest objective: according to oil content')"
 mes422 = "('   Harvest objective: multiple harvests')"
 mes48 =  "('   !Warning! harvest before maturity')"
 mes445 = "('   Sowing day was delayed of this number of days : ',i6)"
 mes447 = "('   Harvest day was delayed of this number of days : ',i6)"
 mes448 = "('   Method of PET calculation: ',a28)"
 mes449 = "(' This year is in case of rotation and chained with the previous one ')"
 mes451 = "(' The destruction date of annual plants is the same than the harvest date')"

end subroutine  bilans_messages
 
 
