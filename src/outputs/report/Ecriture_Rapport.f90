! ***************************************************** c
! * ecriture d'un rapport final en fin de simulation  * c
! * + eventuellement a 2 autres dates                 * c
! ***************************************************** c
! Writing a final report at the end of simulation (file mod_rapport.sti)

subroutine Ecriture_Rapport(sc,pg,soil,c,sta,p,t,date_force)

USE Stics
USE Plante
USE Itineraire_Technique
USE Parametres_Generaux
USE Climat
USE Station
USE Sol

  implicit none

  type(Stics_Communs_),       intent(INOUT) :: sc  
  type(Parametres_Generaux_), intent(IN)    :: pg  
  type(Sol_),                 intent(INOUT) :: soil  
  type(Climat_),              intent(INOUT) :: c  
  type(Station_),             intent(INOUT) :: sta  
  type(Plante_),              intent(INOUT) :: p  
  type(Stics_Transit_),       intent(INOUT) :: t  

  logical, intent(IN) :: date_force

    ! variables locales pour l'optimisation
    integer,parameter :: nb_parameters_max = 300
    integer           :: nbpar  !
    real    :: valpar(nb_parameters_max)
    type(varying_string) :: nompar (nb_parameters_max)

! Variables locales
      integer           :: ancours  !  
      integer           :: ifin  !  
      integer           :: k  !  
      integer           :: i  
      real              :: nstt1  !  
      real              :: QNF  !  
      real              :: rdt  
      real              :: nstt2
      character         :: sep  
      character(len=10) :: commentcelia  
      integer           :: kk
      
      ! using SticsFiles ------------------
      type(File_), pointer :: rap_file
      character(len = 20 ) :: tag
      logical :: file_open
      integer :: rap
      
      ! if the unit hasn't be stored yet
      if ( p%ficrap(p%ipl) == 0 ) then
          !print *, "valeur ficrap plante ",p%ipl, " ",p%ficrap(p%ipl)
          
          ! Getting the report tag, for selecting the right file
          ! default report tag
          tag = 'rap'   
          ! if inter crops
          if (p%ipl == 1 .and. sc%P_nbplantes > 1) then
             tag = 'rap_p'
          end if
          if (p%ipl == 2) then 
             tag = 'rap_a'
          end if
          
          ! Getting report file infos
          ! TODO : set file_open as an output of Ecriture_Rapport
          !call open_file(stics_files,trim(tag),rap_file, file_open)
          call open_file(trim(tag),rap_file, file_open)
    
          p%ficrap(p%ipl) = rap_file%unit
          !print *, "setting rap file unit ", p%ficrap(p%ipl)
      end if
      ! -----------------------------------
    
    rap = p%ficrap(p%ipl)
    
    ! DR 29/12/09 il ne faut ecrire l'entete que le premier jour de simulation
    ! PL, 24/09/2020 fix for writing header only once even for successive simulations 
    ! and associated crops
    if (sc%codeenteterap(p%ipl) == 1 .and. sc%P_codesuite == 0) then

        write(rap,223)

        do i = 1, sc%nbvarrap
           write(rap,221) trim(sc%valrap(i))
        end do

        write(rap,'(1x)')

        ! for inactivating header writing for tha next calls 
        sc%codeenteterap(p%ipl) = 0

      endif


      if (pg%P_codeseprapport == 2) then
         sep = pg%P_separateurrapport
      else
         sep = ' '
      endif

    ! actualisation des numeros de jour
      if (p%P_codeplante == 'snu') then
     ! DR 02/12/2021 je ne comprends pas pourquoi on fait ca , c'est trompeur et en sol nu y'a pas de stade physio
     !   p%nlax = sc%n
        p%nst1 = 1
        p%nst2 = 1
      endif

      nstt1 = max(p%nst1,1)
      nstt2 = max(p%nst2,1)

    ! calcul du rendement, des quantites H20 et N sur l'ensemble du profil
    ! domi - 5.0 9/11/00 - rdt calcule meme avant recolte
      rdt      = p%magrain(AOAS,sc%n)/100.
      sc%QH2Of = 0.
      QNF      = 0.

      QNF = sum(soil%AZnit(1:sc%nh))
      sc%QH2Of = sum(sc%hur(1:int(soil%profsol)))

    ! date courante: jour (ifin), annee (ancours)
      ifin = sc%n + sc%P_iwater - 1 ! TODO : remplacer par sc%jul ?
      ancours = sc%annee(ifin)
      if (ifin > sc%nbjsemis) ifin = ifin - sc%nbjsemis

      call CorrespondanceVariablesDeSorties(sc, p, soil, c, sta, sc%nbvarrap, sc%valrap(1:sc%nbvarrap),       &
                                            sc%valsortierap(1:sc%nbvarrap))


    ! Ecriture des noms des stades
      commentcelia = 'date'

! DR 07/01/2022 si c'est une date on le garde comme une date
     if(.not. date_force)then
      if (sc%n == p%nplt.and.sc%rapplt) commentcelia = 'semis'
      if (sc%codetyperap == 2 .or. sc%codetyperap == 3) then
        if (sc%n == p%nger) commentcelia = 'ger'
        if (sc%n == p%nlev) commentcelia = 'lev'
        if (sc%n == p%ndrp) commentcelia = 'drp'
        if (sc%n == p%nlax) commentcelia = 'lax'
        if (sc%n == p%nflo) commentcelia = 'flo'
        if (sc%n == p%nsen) commentcelia = 'sen'
        if (sc%n == p%namf) commentcelia = 'amf'
        if (sc%n == p%nmat) commentcelia = 'mat'
        ! DR 22/07/2013 j'ajoute le stade start pour Nicolas
        if (sc%n == 1.and.sc%rapdeb.and.sc%start_rap) commentcelia = 'start'
      endif
      if (sc%n == p%nrec) then
        if (sc%recolte1) then
          commentcelia = 'recphy'
          sc%recolte1 = .FALSE.
        else
          commentcelia = 'rectec'
        endif
      endif
      if (sc%n == p%nrec .and. p%nrec == p%nrecbutoir) commentcelia = 'recbut'
    ! DR 22/07/2013 je remplace 'fin' par 'end'
      if (sc%n == sc%maxwth .and. sc%P_datefin) commentcelia = 'end'
endif
      if (pg%P_codesensibilite /= 1) then
! **************** cas courant pas analyse de sensibilite *************
! DR pour benj ecriture speciale pour l'instant je mets un P_codesensibilite = 3
        if (pg%P_codesensibilite == 3) then
          if (sc%codoptim == 0) then
! Bruno juin 2017 ajoute la duree de cycle (n) dans le rapport
            write(rap,889) sc%P_usm,sep, adjustl(sc%wlieu),sep, sc%ansemis,sep, sc%P_iwater,sep, ancours,sep,     &
                           ifin,sep, sc%n,sep, sc%P_ichsl,sep, p%group,sep, p%P_codeplante,sep, commentcelia,sep, &
                           sc%codeversion, sep, (sc%valsortierap(k),sep,k = 1,sc%nbvarrap),                       &
                           float(t%nbapN(p%ipl)),sep, (float(t%dateN(p%ipl,k)),sep,k = 1,5),                      &
                           float(t%nbapirr(p%ipl)), (sep,float(t%dateirr(p%ipl,k)),k = 1,t%nbapirr(p%ipl))
          else


            ! Getting names, values and number of parameters from the param.sti file
            call get_forcing_parameters( nompar, valpar , nbpar)

            write(rap,889) sc%P_usm,sep,adjustl(sc%wlieu),sep,sc%ansemis,sep,sc%P_iwater,sep,ancours,sep,       &
                           ifin,sep ,sc%n,sep, sc%P_ichsl,sep,p%group,sep,p%P_codeplante,sep,commentcelia,sep,  &
                           sc%codeversion,sep,                                                                  &
                           (valpar(k),sep,k = 1,nbpar),(sc%valsortierap(k),sep,k = 1,sc%nbvarrap),              &
                           float(t%nbapN(p%ipl)),sep,                                                           &
                           (float(t%dateN(p%ipl,k)),sep,k = 1,5),float(t%nbapirr(p%ipl)),                   &
                           (sep,float(t%dateirr(p%ipl,k)),k = 1,t%nbapirr(p%ipl))
          endif
        else
          if (pg%P_codeoutscient == 1) then ! ecriture scientifique
 !           write(rap,889) sc%P_usm,sep,adjustl(sc%wlieu),sep,sc%ansemis,sep,sc%P_iwater,sep,ancours,sep,       &
 !                               ifin,sep,sc%n,sep, sc%P_ichsl,sep,p%group,sep,p%P_codeplante,sep,commentcelia,sep,    &
 !                               sc%codeversion,sep,&
 !                               (sc%valsortierap(k),sep,k = 1,sc%nbvarrap)
           write(rap,889) sc%P_usm,sep,adjustl(sc%wlieu),sep,sc%ansemis,sep,sc%P_iwater,sep,ancours,sep,       &
                                ifin,sep,sc%n,sep, sc%P_ichsl,sep,p%group,sep,p%P_codeplante,sep,commentcelia,sep,    &
                                sc%codeversion,&
                                (sep,sc%valsortierap(k),k = 1,sc%nbvarrap)

          else
 !           write(rap,888) sc%P_usm,sep,sc%P_wdata1(1:index(sc%P_wdata1,'.')-1),sep,sc%ansemis,sep,    &
 !                               sc%P_iwater,sep,ancours,sep,       &
 !                               ifin,sep, sc%n,sep, sc%P_ichsl,sep,p%group,sep,p%P_codeplante,sep,commentcelia,sep,    &
 !                               sc%codeversion,sep,&
 !                               (sc%valsortierap(k),sep,k = 1,sc%nbvarrap)
            write(rap,888) sc%P_usm,sep,sc%P_wdata1(1:index(sc%P_wdata1,'.')-1),sep,sc%ansemis,sep,    &
                                sc%P_iwater,sep,ancours,sep,       &
                                ifin,sep, sc%n,sep, sc%P_ichsl,sep,p%group,sep,p%P_codeplante,sep,commentcelia,sep,    &
                                sc%codeversion,&
                                (sep,sc%valsortierap(k),k = 1,sc%nbvarrap)

      if(p%nflo==sc%n )then
         do kk=1,sc%nbvarrap
             sc%valsortie_flo(kk)= sc%valsortierap(kk)
         enddo
      endif
      if(p%nmat==sc%n .or. (p%nrec==sc%n))then
       call EnvoyerMsgEcran( (/'  n','mat', 'rec' /))
       call EnvoyerMsgEcran((/sc%n, p%nmat, p%nrec/))
         do kk=1,sc%nbvarrap
             sc%valsortie_mat(kk)= sc%valsortierap(kk)
         enddo
      endif
! DR 08/12/2013 pour macsur , j'ajoute les valeurs au semis
      if(p%nplt==sc%n )then
         do kk=1,sc%nbvarrap
             sc%valsortie_iplt(kk)= sc%valsortierap(kk)
         enddo
      endif

! pour LI j'ajoute les valeurs a la recolte qui peuvent etre diff si la recolte n'est pas faite a maturite
      if(p%nrec==sc%n)then
      call EnvoyerMsgEcran( (/'  n','mat', 'rec' /))
      call EnvoyerMsgEcran((/sc%n, p%nmat, p%nrec/))

         do kk=1,sc%nbvarrap
             sc%valsortie_rec(kk)= sc%valsortierap(kk)
         enddo
         !write(*,*)'sc%valsortie_rec(6) nrec',sc%valsortie_rec(6)
      endif


          endif
        endif
      else
        
        ! Getting names, values and number of parameters from the param.sti file
        call get_forcing_parameters( nompar, valpar , nbpar)


        if (pg%P_codeoutscient == 1) then
!          write(rap,889) sc%P_usm,sep,adjustl(sc%wlieu),sep,sc%ansemis,sep,sc%P_iwater,sep,ancours,sep,         &
!                              ifin,sep,sc%P_ichsl,sep,p%group,sep,p%P_codeplante,sep,commentcelia,sep,      &
!                              sc%codeversion,sep,&
!                              (valpar(k),sep,k = 1,nbpar),(sc%valsortierap(k),sep,k = 1,sc%nbvarrap)
          write(rap,889) sc%P_usm,sep,adjustl(sc%wlieu),sep,sc%ansemis,sep,sc%P_iwater,sep,ancours,sep,         &
                              ifin,sep,sc%P_ichsl,sep,p%group,sep,p%P_codeplante,sep,commentcelia,sep,      &
                              sc%codeversion,&
                              (sep,valpar(k),k = 1,nbpar),(sep,sc%valsortierap(k),k = 1,sc%nbvarrap)
        else
 !         write(rap,888) sc%P_usm,sep,adjustl(sc%wlieu),sep,sc%ansemis,sep,sc%P_iwater,sep,ancours,sep,         &
 !                             ifin,sep,sc%P_ichsl,sep,p%group,sep,p%P_codeplante,sep,commentcelia,sep,      &
 !                             sc%codeversion,sep,&
 !                             (valpar(k),sep,k = 1,nbpar),(sc%valsortierap(k),sep,k = 1,sc%nbvarrap)
          write(rap,888) sc%P_usm,sep,adjustl(sc%wlieu),sep,sc%ansemis,sep,sc%P_iwater,sep,ancours,sep,         &
                              ifin,sep,sc%P_ichsl,sep,p%group,sep,p%P_codeplante,sep,commentcelia,sep,      &
                              sc%codeversion,&
                              (sep,valpar(k),k = 1,nbpar),(sep,sc%valsortierap(k),k = 1,sc%nbvarrap)
        endif
      endif


221   format(';',A,$)
223   format('P_usm;wlieu;ansemis;P_iwater;ancours;ifin;nbdays;P_ichsl;group;P_codeplante;stade;nomversion',$)
!! merge trunk 23/11/2020 Pour Sam on ajoute de la precision aux sorties
!888   format(2(a40,a1),7(i5,a1),a3,a1,a10,a1,a30,a1,300(f10.3,a1))
!889   format(2(a40,a1),7(i5,a1),a3,a1,a10,a1,a30,a1,300(e16.7,a1))

888   format((a40),(a1,a40),7(a1,i5),a1,a3,a1,a10,a1,a30,300(a1,f10.3))
889   format((a40),(a1,a40),7(a1,i5),a1,a3,a1,a10,a1,a30,300(a1,e16.7))

      sc%ecritrap = .TRUE.
      commentcelia = '   '


return
end subroutine Ecriture_Rapport
 
 
