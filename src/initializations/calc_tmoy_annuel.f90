! sous-programme de calcul de la temperature moyenne annuelle pour la prise
!! en compte du CC sur la matiere organique

subroutine calc_tmoy_annuel(tmoy_an)

USE Messages
USE Divers, only: nbjParAnnee
USE SticsFiles

implicit none

!: Arguments
    real, intent(INOUT) :: tmoy_an(2,300) ! TODO : taille a mettre en dynamique ?  

!: Variables locales

    character(len=7) :: station  
    integer          :: an  !  
    integer          :: imois  !  
    integer          :: ijour  !  
    integer          :: jul  !  
    integer          :: i  !  
    integer          :: andeb  !  
    integer          :: k  !  
    integer          :: l  
    real             ::tn  !  
    real             :: tx  !  
    real             ::  tm  

    type(File_), pointer :: clim_file
    logical :: file_open, exists
    integer :: fclim
    integer :: ret
  
    !call open_file(stics_files,'clim',clim_file, file_open)
    call open_file('clim',clim_file, file_open)
        
    exists = existFile(clim_file)
    if (.NOT.exists) then
        call EnvoyerMsgHistorique("Climate file doesn't exist : ",char(clim_file%path))
        call EnvoyerMsgErreur( "Climate file doesn't exist : ",char(clim_file%path) )
    end if
    
    if ( .NOT.file_open ) then
        call EnvoyerMsgHistorique("Error opening climate file: ", char(clim_file%path))
        call EnvoyerMsgErreur( "Error opening climate file: ", char(clim_file%path) )
    end if
 
    if ( any((/.NOT.exists,.NOT.file_open/)) ) then
       call exit(9)
    end if

    fclim = clim_file%unit

    !open(fclim, file='climat.txt')
    read(fclim,*) station, andeb
    backspace(fclim)

! calcul de la temperature moyenne historique sur la periode lue dans le paramv6.par
!  P_an_debut_serie_histo-P_an_fin_serie_histo

    l = 0
    do i = andeb,2100
      l = l+1
      tm = 0.
      do k = 1,nbjParAnnee(i)
        read(fclim,*,err=250,end=80) station,an,imois,ijour,jul,tn,tx
        tm = tm + ((tn+tx) / 2.)
      enddo
      tmoy_an(1,l)= i
      tmoy_an(2,l)= tm / nbjParAnnee(i)
    enddo

80  continue
    !close(fclim)
    ret = closeFile(clim_file)
    return

250 call EnvoyerMsgHistorique(139)
    ret = closeFile(clim_file)
    !close(fclim)

return
end
 
 
