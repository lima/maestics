! *********************************************************************************
!  sous programme general d'appel des sous_programmes d'initialisation
!  dans l'ordre
!      initsol2
!     initnonsol
!     initcycle
!      testvartech
!      lecstat
!      iniclim
! *********************************************************************************


subroutine initialisations(sc,pg,p,r,itk,soil,c,sta,t,ag,usma,GLOBALRAD) ! CBF COUPLING 2/8/24

USE Stics
USE Parametres_Generaux
USE Plante
USE Root
USE Itineraire_Technique
USE Sol
USE Climat
USE Station
USE Divers
USE USM
USE Module_AgMIP
USE maestcom ! CBF COUPLING 2/8/24

USE Snow

  implicit none

  type(Stics_Communs_),       intent(INOUT) :: sc
  type(Parametres_Generaux_), intent(IN)    :: pg
  type(Plante_),              intent(INOUT) :: p(sc%P_nbplantes)
  type(Root_),                intent(INOUT) :: r(sc%P_nbplantes)
  type(ITK_),                 intent(INOUT) :: itk(sc%P_nbplantes)
  type(Sol_),                 intent(INOUT) :: soil
  type(Climat_),              intent(INOUT) :: c
  type(Station_),             intent(INOUT) :: sta  
  type(Stics_Transit_),       intent(INOUT) :: t  
  type (USM_),               intent(IN)    :: usma
  

  type(AgMIP_) ,              intent(INOUT) :: ag

  type(snow_)  :: snow_struct

! Variable(s) locale(s)
  integer :: i  !  
  integer :: codeRetour  

  character(len=50) :: laifile
   logical :: AgMIP,Macsur
   real :: ferti_semis
   
   real  :: GLOBALRAD(MAXT,MAXDATE) ! CBF COUPLING 2/8/24

  AgMIP= .FALSE.
  Macsur= .FALSE.
  if(ag%P_type_project.eq.2) AgMIP= .TRUE.
  if(ag%P_type_project.eq.3) Macsur= .TRUE.


! DR 28/08/07
! DR 06/02/08 dans la version 6.4 P_stdrpdes devient varietal
!   do i = 1,P_nbplantes
!     if (codeforcedrpdes == 1) then
!       P_stdrpdes = p(i)%P_stdrpmat(P_variete)
!       call EnvoyerMsgHistorique(449)
!     endif
!   enddo

! DR 12/02/08 passe dans initnonsol
!      if (P_nbplantes > 1) then
!        dassoinit = densite(2)
! ** densites pour le calcul de la competition
!          densite(2) = densite(2)+densite(1)/P_bdens(1)*P_bdens(2)
!          dassoiniteqv = densite(2)
!      endif


     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Snow module initialization !!!!!!!!!!!!!!!!!!!!!!!!!!!! 
   call init(snow_struct,sta%P_codemodlsnow,sta%P_Sdry0,sta%P_Swet0,sta%P_ps0,sta%P_Sdepth0, &
        sta%P_tsmax,sta%P_trmax,sta%P_DKmax,sta%P_Kmin,sta%P_Tmf, &
        sta%P_SWrf,sta%P_Pns,sta%P_E,sta%P_prof,sta%P_tminseuil, &
        sta%P_tmaxseuil)
     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


    call EnvoyerMsgEcran('15. initnonsol')
! ***
! Initialisations des variables de cultures
! On ne passe plus qu'une fois dans initial
! pour l'ensemble de la culture
!      call initnonsol(sc,pg,p,itk,soil,c,sta,t)  ! DR 20/07/2012 c n'est pas utilise
      call initnonsol(sc,pg,p,r,itk,soil,sta,t,ag,snow_struct)



      do i = 1, sc%P_nbplantes

        call EnvoyerMsgEcran('Initialisations du cycle, plante :', i)
        call initcycle(sc,pg,p(i),itk(i),t)
        ! Pour initialiser les cumuls (notamment pour les enchainements d'annee)
        call cumAOetAS(1,sc%P_codesimul,p(i),itk(i))
      end do



! print *, 'Testvartech'
! ***
! Controle de coherence des parametres
! un seul jour de travail du sol (celui de la principale)
      call testvartech(sc%P_nbplantes,itk)

!  dr 14/09/2011 dans le cas de serie on force la fin a P_iwater-1 mais il faut qu'on soit sur 2 ans
    ! dr 14/09/2011 on calcule automatiquement le ifwater si on est en enchainement sur plusieurs annees (nbans>1), on fait en sorte de commencer tj le meme jour (P_iwater)
     if (sc%nbans > 1.and.sc%numcult==1) then
!  dr 21/01/2015 si c'est une culture annuelle qui commence au premier janvier y'a pas de raison de la passer en 2
        if (sc%P_culturean==1 .and.sc%P_iwater==1) then
            sc%P_culturean = 1
        else
            sc%P_culturean = 2
        endif
        if(isBissextile(sc%ansemis))then
           sc%P_ifwater = sc%P_iwater-1 + 366-1
        else
           sc%P_ifwater = sc%P_iwater-1 + 365-1
        endif
     endif

! Climat
     call EnvoyerMsgEcran('Lecture du fichier climatique')




      !call Climat_Lecture(c, filepluspath, CLIMAT_METHOD_STICS_V6, sc%P_culturean, codeRetour, sc%flag_record)
      ! removed file name
      call Climat_Lecture(c, CLIMAT_METHOD_STICS_V6, sc%P_culturean, codeRetour, sc%flag_record, GLOBALRAD) ! CBF COUPLING 2/8/24

      sc%ansemis = c%anneezero

    ! calcul du nombre de jours de l'annee de semis et de l'annee de recolte
      sc%nbjsemis = nbjParAnnee(c%anneezero)
      sc%annee(c%julzero:sc%nbjsemis) = sc%ansemis

     if (sc%P_culturean /= 1) then
        sc%nbjrecol = nbjParAnnee(c%anneezero+1)
        sc%annee(sc%nbjsemis+1:sc%nbjsemis+sc%nbjrecol) = sc%ansemis + 1
     else
        sc%nbjrecol = 0
     endif

    ! DR 13/01/06 dans le cas ou on a coche 2 ans et que la simulation est sur 1 an
     if (sc%P_culturean /= 1 .and. sc%P_ifwater < sc%nbjsemis) then
        call EnvoyerMsgHistorique(442)
!        stop
! dr 04/08/2014 je reactive le stop parait coherent de faire corriger ca par l'utilisateur
! dr 08/09/2014 voir comment corriger ca dans le cas de series ca a l'air de merder
!        call exit(9)
     endif

    ! affectation nbjanrec pour recup.tmp
    !14/09/2011 correction si on demarre un 01/01 on a qu'une annee
    !  if (sc%P_culturean == 1) then
! 27/12/2016 c'est etrange on peut demarrer un 01 janv et avaoir une simul sur 2 ans ...
!      if (sc%P_culturean == 1.or.sc%P_iwater==1) then
     if (sc%P_culturean == 1) then
        sc%nbjanrec = sc%nbjsemis
     else
        sc%nbjanrec = sc%nbjrecol
     endif


    ! A CHANGER : initialisations 2 X des variables climatiques si cultures ASSO: iniclim
    ! extraire ce qui releve des var clim et du reste qui est lie a la plante ! 
    ! Modif faite dans iniclim, test si plante 1
    ! initialisation des variables climatiques
      do i = 1, sc%P_nbplantes

call EnvoyerMsgEcran('Initialisations climatique, plante :', i)
        
        call iniclim(sc,pg,p(i),itk(i),c,sta,snow_struct,usma)
        
    ! DR 13/07/06 essaie d'initialisation de sioncoupe
        p(i)%sioncoupe = .FALSE.
     end do
     if (iand(pg%P_flagEcriture,sc%ECRITURE_ECRAN) >0 ) print *, '17. iniclim'

      ! DR 26012016 j'implemente les regles de decision pour Agmip, on a lu la date de semis
      ! rules=.TRUE. passe dans param_new_form
      if(AgMIP.and.ag%P_rules_sowing_AgMIP.eq.1)then
          call determinate_sowing_Agmip(c,sc%P_usm,sc%ansemis,sc%P_iwater,p(1)%nplt,sc%iplt(1),ferti_semis)
          !DR 05/02/2015 pour le moment Giacomo precnise de demarrer 10 jours avant la premiere date de semis de la fenetre
          sc%anit(p(1)%nplt) = ferti_semis
      endif


      if (sc%P_nbplantes > 1 .and. .not.sc%posibsw) then
          call EnvoyerMsgHistorique(433)
          !stop
          call exit(9)
      endif

    ! TODO : Lecture des lai
      if (lge(sc%P_codesimul,'feuille') .eqv. .TRUE.) then
        do i = 1, sc%P_nbplantes
            laifile = p(i)%P_flai
            call Lecture_Lai(sc,p(i),laifile,itk(i))
        end do
      endif


return
end subroutine initialisations
 
 
