! annual loop
! - initializations
! - Call of the loop DAILY
! - write the output files

subroutine Stics_Boucle_Annees(sc,p,pg,r,itk,c,sta,soil,t,usma,ag,GLOBALRAD)

USE Stics
USE USM
USE Plante
USE Itineraire_Technique
USE Sol
USE Climat
USE Station
USE Parametres_Generaux
USE Divers
USE Bilans
USE Root
USE Module_AgMIP
USE SticsFiles
USE Messages
USE maestcom ! CBF COUPLING 2/8/24

! DR 17/10/2013 on ajoute le module patho pour couplage avce Mila
!USE mod_patho on enleve les appel a Mila geres sur la branche Stics_Mila

  implicit none

    type(Stics_Communs_),        intent(INOUT) :: sc
    type(Parametres_Generaux_),  intent(INOUT) :: pg
    type(Plante_),               intent(INOUT) :: p(sc%P_nbplantes)
    type(Root_),                 intent(INOUT) :: r(sc%P_nbplantes)
    type(ITK_),                  intent(INOUT) :: itk(sc%P_nbplantes)
    type(Sol_),                  intent(INOUT) :: soil
    type(Climat_),               intent(INOUT) :: c
    type(Station_),              intent(INOUT) :: sta
    type(Stics_Transit_),        intent(INOUT) :: t
    type(USM_),                  intent(INOUT) :: usma
    type(AgMIP_),                intent(INOUT) :: ag

    integer :: numcult  
    integer :: i  !  
    integer ::  n  
    integer :: jour_iwater  !  
    integer :: nummois_iwater  
    character(len=3) :: mois_iwater  !  
    character(len=3) :: mois_ifwater  
    integer :: jour_ifwater  !  
    integer :: nummois_ifwater  !  
    integer :: ancours  !  
    integer :: ifin  !  
    integer :: ifwater1an  

    real :: resmes_ini,azomes_ini

    real  :: GLOBALRAD(MAXT,MAXDATE) ! CBF COUPLING 2/8/24


call EnvoyerMsgEcran("Year loop: ", sc%nbans)

!--------------------- snow ------------------------------
        ! Temporary: zero for snow cumulative vars
        c%ndays_sdepth_over_3cm(:)=0
        c%ndays_sdepth_over_10cm(:)=0
!---------------------------------------------------        
        
    ! La boucle des annees
      do numcult = 1, sc%nbans
        sc%numcult = numcult


!: On affiche le numero de la culture courante (pour info uniquement)
call EnvoyerMsgEcran("Crop number = ",numcult)

if ( write_debug ) write(ficdebug,*) 'numcult = ',numcult
write(*,*) 'numcult = ',numcult
        do i= 1, sc%P_nbplantes
          if (p(i)%P_codeplante == 'fou' .and. (p(i)%P_stade0 == 'snu' .or. p(i)%P_stade0 == 'plt')) then
            p(i)%age_prairie = numcult

          ! DR et FR 23/06/2015 dans le cas des prairies  semees avec reset il ne faut pas repartir a amf
            if (numcult > 1 .and. p(i)%codeperenne0==2 .and. pg%P_codeinitprec==2 ) then
              p(i)%P_codeperenne = 2
              p(i)%P_stade0 = 'amf'
          ! DR - 28/02/08 - Il ne faut pas faire le travail du sol si on est en annee (semis+1)
              itk(i)%P_nbjtrav = 0
            else
              p(i)%numcoupe=1
              p(i)%fauchediff=.FALSE.
            endif
          else
            p(i)%age_prairie = -999
          endif

        enddo

    ! Si on enchaine plusieurs annees climatiques => P_codesuite=1
        if (numcult > 1) sc%P_codesuite = 1
    ! Cas d'enchainement de 2 usm differentes : on force P_codeinitprec a 2
        if (sc%P_codesuite == 1 .and. sc%nbans == 1) pg%P_codeinitprec = 2

    ! Initialisation sols, climat et variables du cycle journalier

    ! Initialisation sols, climat et variables du cycle journalier
     call EnvoyerMsgEcran("Soil Initialisations")
     call initialisations_sol(sc,pg,t,soil,sta)

     call EnvoyerMsgEcran("Initialisations")
        call initialisations(sc,pg,p,r,itk,soil,c,sta,t,ag,usma,GLOBALRAD)  ! CBF COUPLING 2/8/24
        call EnvoyerMsgEcran("Soil Initialisations")


        do i = 1, sc%P_nbplantes
          if(itk(i)%P_codedateappN == 1) itk(i)%numeroappN = 1
        ! DR 09/10/09 j'ajoute la possibilite de faire des irrigations a des sommes de temp
          if (itk(i)%P_codedateappH2O == 1) itk(i)%numeroappI = 1
        enddo

! 06/01/2017 on calcule un resmes initial avant tout calcul
    ! calcul de la reserve jusqu'a profcalc
        resmes_ini = SUM(sc%hur(1:soil%profcalc)+sc%sat(1:soil%profcalc))
        azomes_ini = SUM(soil%nit(1:soil%profcalc))
        write(fichist,19) soil%profcalc, resmes_ini, azomes_ini
  19    format('Initial values over the soil depth (',i3,' cm):',5X,'SWC =',f5.0,' mm',5X,'SMN =',f5.1,' kg N/ha')

     ! writing balance headers !
     if ( stics_files%flag_bilan ) then
        do i = 1, sc%P_nbplantes
           call ecrireEnteteBilan(sc,pg,p(i),itk(i),soil,sta,t,usma)
        end do
     end if

!:***************************************************************************
!:: Boucle Journaliere                                                     !*

        do n = 1, sc%maxwth
           ! print *,'day = ',n, '       or',n+sc%P_iwater-1
           sc%n = n

         ! on transpose n en jour julien depuis le 1er janvier de l'annee de debut de simulation
           sc%jjul = tCal1JAbs(sc%n,sc%P_iwater)
           sc%jul = tCal1JRel(sc%jjul,sc%annee(sc%P_iwater),.false.)
           sc%numdate = sc%jul
           call julien(sc%jul,sc%annee(sc%jjul),sc%mois,sc%jour,sc%nummois)


! DR 10/07/02013 j'ajoute l'ecriture du stade start dans le rapport
    !::: Ecriture des rapports
      if ( stics_files%flag_rap ) then
      ! TODO : change to routine call ! 
      if ( write_debug ) write(ficdebug, *) 'Stics_Jour: reports'
      call EnvoyerMsgEcran("Stics_Jour: reports")
      
        do i = 1,sc%P_nbplantes
        ! ecriture de rapport.sti aux dates ou aux stades choisis
        ! domi 24/09/03 pb dans l'ecriture a des stades
          if (sc%codeaucun == 1) then
          ! domi - 07/10/03 - pour vianney stades+dates
          ! domi - 20/10/03 - je teste pour ne pas ecrire 2 fois le meme jour
            sc%ecritrap = .FALSE.
            if ((sc%codetyperap == 2 .or. sc%codetyperap == 3) .and. (.NOT.sc%ecritrap)) then
              call ecriture_start(sc,soil)
              if ((n == 1) .and. sc%rapdeb)then
                sc%start_rap=.TRUE.

                ! added plant num for getting the report file name in Ecriture_Rapport
                call Ecriture_Rapport(sc,pg,soil,c,sta,p(i),t, .FALSE.)
                sc%ecritrap = .FALSE.
                sc%start_rap=.FALSE.
              endif
            endif
          endif
        enddo
      endif




call EnvoyerMsgEcran("calling of the daily loop Stics_Jour",n)
call EnvoyerMsgEcran("calling of the daily loop Stics_Jour",sc%jjul)

          ! On appelle la routine journaliere de Stics
            call Stics_Jour(sc,pg,p,r,itk,soil,c,sta,t,ag)

            if ( write_debug ) write(ficdebug2, *) 'apres stics_jour',n,p(1)%chargefruit &
            ,p(2)%chargefruit
            
        end do
!:: Fin de la Boucle Journaliere                                         !*
!:***************************************************************************


call EnvoyerMsgEcran("end of annual loop iwater",sc%P_iwater)
call EnvoyerMsgEcran("ifwater",sc%P_ifwater)


    ! Recalcul des dates de debut, dates de fin de simulation en cas d'enchainement
        sc%n = sc%maxwth

      ! DR - 18/06/08 - j'essaie d'introduire un calcul coherent de P_ifwater et P_iwater en cas d'enchainement
        if (numcult == 1)then
        ! Calcul de P_iwater d'apres l'annee 1
              call julien(sc%P_iwater,sc%ansemis,mois_iwater,jour_iwater,nummois_iwater)
              ancours = sc%annee(sc%ifwater_courant)
        ! Calcul de P_ifwater d'apres l'annee 1
              ifin = sc%ifwater_courant
              if (sc%ifwater_courant > sc%nbjsemis) then
                ifwater1an = sc%ifwater_courant - sc%nbjsemis
              else
                ifwater1an = sc%ifwater_courant
              endif
              call julien(ifwater1an,ancours,mois_ifwater,jour_ifwater,nummois_ifwater)
        endif

 ! Rapport synthetique 1 ligne par annee sauf pour plantes fauchees
        do i = 1, sc%P_nbplantes
        
       !write(*,*)'codefauche',itk(i)%P_codefauche
          if(itk(i)%P_codefauche == 2) then
            if ( stics_files%flag_bilan ) then
              call EnvoyerMsgEcran("before writing default balance")
              call ecrireBilanDefaut(sc,pg,soil,p(i),itk(i),c) !,t,c)
            endif
          endif

          sc%P_datefin = .TRUE.
          ! DR 29/12/2021 pour ne pas qu'on ecrive 2 fois le meme jour si on a des dates qui correspondent a la date de fin de simul
            if (sc%rapfin .and. (sc%ecritrap.eqv..FALSE.))then
              call EnvoyerMsgEcran("Reports writing")
               if ( stics_files%flag_rap ) then
               ! added plant num for getting the report file name in Ecriture_Rapport
                  call Ecriture_Rapport(sc,pg,soil,c,sta,p(i),t, .FALSE.)
                  call EnvoyerMsgEcran("Standard report writing")
               end if 
! DR 29/08/2012 j'ajoute un code pour garder les sorties Agmip t%P_Config_Output : 1=rien , 2 sorties ecran , 3 sorties agmpi
               if ( stics_files%flag_agmip ) then
                 !print *, 'ecriture rapport agmip', sc%n,p%imats, p%irecs
                  call Ecriture_Rapport_AgMIP(sc,pg,soil,sta,p(i),ag)
                  call EnvoyerMsgEcran("Agmip report writing")
               end if
            endif
        end do
        
    ! Calculs de P_iwater et P_ifwater pour annee en cours

          
          call EnvoyerMsgEcran("numcult nbans culturean iwater",(/numcult,sc%nbans,sc%P_culturean,sc%P_iwater/))
          

    ! Recup(eration)
        call EnvoyerMsgEcran("before Ecriture_DonneesFinDeCycle")
        call Ecriture_DonneesFinDeCycle(sc, pg, p, r, itk, soil)
        call EnvoyerMsgEcran("after Ecriture_DonneesFinDeCycle")
        call writeSnowStateVariables(c,sc%P_iwater,sc%ifwater_courant)

    ! Ecriture du profil
    ! DR - 17/01/08 - On ecrit les 2 plantes dans le profil (1 a la suite de l'autre)
        do i= 1, sc%P_nbplantes
          sc%ipl = i
          if ( stics_files%flag_profil .AND. sc%codeprofil /= 0 ) then
            call EnvoyerMsgEcran("Writing profile")
            
            call Ecriture_Profil(sc,int(soil%profsol))
          endif
        end do
      end do
     ! Fin de la boucle des annees
     ! write(*,*)'finboucle annee'
    call EnvoyerMsgEcran("end of years loop")
    
    
  ! Moved to finalize_files, called in main
  ! DR 27/04/2011 on ferme le fichier climatique climat.txt
  !close(unit=ficClimat)
  
return
end subroutine Stics_Boucle_Annees
 
 
