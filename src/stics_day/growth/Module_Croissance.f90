! Module of growth
!! Description: growth, senescence, grain filling, ...)
!
! This module calculates the shoot biomass growth thanks to the radiation intercepted by foliage.
! - Stics book paragraphe 3.3, page 54-57
! ml_com !
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
! - Stics book paragraphe 3.3, page 54-57
!
!! This module calculates the shoot biomass growth thanks to the radiation intercepted by foliage.
!!
!! The linear relationship between accumulated biomass in the plant and radiation intercepted by foliage, demonstrated by Monteith (1972), defines the
!! radiation use efficiency (or RUE ) as the slope of this relationship. synthesizes the processes of photosynthesis and respiration.
!! Its calculation (ratio between above-ground biomass and absorbed radiation) implies that this parameter also takes account of a carbon allocation coefficient
!! between above-ground and below-ground parts of the plant.  Obviously, because of underlying physiological processes this ratio varies somewhat, due to stresses,
!! temperature and phenology.  To take account of these effects, Sinclair (1986) proposed that RUE should be considered as a physiological function,
!! to which stress indices should be applied.
!! In STICS the calculation of the daily production of shoot biomass (dltams) relies on the RUE concept (though the relationship between dltams and raint
!! is slightly parabolic) taking into account various factors known to influence the elementary photosynthesis and respiration processes,
!! mostly as stresses (ftemp, swfac, inns and exobiom).  Dltams accumulated day by day gives the shoot biomass of the canopy, masec.
!!
!! Influence of radiation and phasic development:
!!
!! the accumulation of shoot biomass depends on the intercepted radiation (raint), and is almost linear but slightly asymptotic at high intercepted light values.
!! It is simulated in STICS by a parabolic function involving a maximum radiation use efficiency specific to each species, ebmax.
!! The parameter coefb stands for the radiation saturating effect. This effect is the result, even buffered, of the saturation occurring within a short time step
!! on the individual leaf scale and is easily observed when daily calculations are made with instantaneous formulae of canopy photosynthesis ;
!! such calculations lead to a value of 0.0815. The efficiency, ebmax, may differ during the juvenile (ILEV-IAMF), vegetative (IAMF-IDRP) and
!! reproductive (IDRP-IMAT) phases (corresponding respectively to the parameters efcroijuv, efcroiveg and efcroirepro).
!! Classically, efcroijuv=1/2 efcroiveg is used to take account of the preferential migration of assimilates towards the roots at the beginning of the cycle.
!! The difference between efcroiveg and efcroirepro arises from the biochemical composition of storage organs: e.g. for oil or protein crops efcroirepro is less
!! than efcroiveg because the respiratory cost to make oil and protein is higher than for glucose.
!!
!! Effect of atmospheric CO2 concentration:
!!
! The CO2C parameter stands for the atmospheric CO2 concentration, which can be higher than the current value, assumed to be 350 ppm. The formalisation
!! chosen in STICS was adapted from Stockle et al. (1992): the effect of CO2C on the radiation use efficiency is expressed by an exponential relationship,
!! for which the parameter is calculated so that the curve passes through the point (600, alphaco2).
!!
!! The remobilisation of reserves:
!   - Perennial reserve available from one cycle to the next: dltaremobil is obtained by the remobilisation of winter reserves in perennial plants.
!!   Each day the maximal proportion of the reserves that can be remobilised is remobres, until perennial stocks (parameter restemp0 given as an initialisation
!!   at the beginning of the growth cycle) are exhausted.
!!   restemp0 only represents carbon reserves, and nitrogen reserves can only be added through initiation of the QNplantenp0 parameter.
!!   The nitrogen remobilisation rate of the QNplantenp0 stock is assumed to equal the nitrogen demand until it is exhausted.  These reserves are only called upon
!!   if the newly formed assimilates (dltams) fail to satisfy the sink strength (fpv and fpft), which leads to a first calculation of
!!   the source/sinks variable (sourcepuits). These remobilisations contribute to increasing the source/sink ratio the following day because they are counted
!!   in the variable dltams.
!   - Reserve built up and used during the cycle: reserves built up during the vegetative cycle (variable restemp) and reused later on simply contribute
!!   to the estimation of the source/sink ratio for indeterminate crops.  The maximum quantity which can be remobilised per day (remobilj) is calculated
!!   similarly to dltaremobil. If the plant is both perennial and indeterminate, the reserves originating from the previous cycle are first used (dltaremobil)
!!   and when exhausted the current cycles reserves (remobilj) can be used.
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c

module Module_Croissance
    implicit none

interface

subroutine biomaer(n, ipl, ens, nlev, P_codeperenne, nplt, P_codehypo, P_codegermin, P_masecplantule, P_adil, namf, P_adilmax,   &
                   nrec, P_codcueille, P_codefauche, P_codelaitr, P_codetransrad, P_efcroijuv, P_efcroiveg, ndrp, P_efcroirepro, &
                   ! DR 13/02/2019 je rajoute chargefruit pour mettre le test comme il est dans le trunk
                   chargefruit,                                                                                                  &
                   P_coefb, tcult, P_teopt, P_teoptbis, P_temin, P_temax, P_codeclichange, P_alphaco2, co2, P_resplmax, densite, &
                   P_codeh2oact, swfac, exobiom, P_codeinnact, dltaremobil, fpv, fpft, P_remobres, P_msresiduel, P_nbcueille,    &
                   rdtint, CNgrain, P_codemontaison, nmontaison, masecnp_veille, masecnp, QNplantenp_veille, QNplantenp, inns,   &
                   inn, innlai, cumdltaremobilN, ebmax, ftemp, epsib, fco2, dltams, dltamsen, dltamstombe, restemp, dltamsN,     &
                   photnet, sourcepuits, dltaremobilN, remobilj, cumdltares, magrain_veille, magrain, masecneo, surface,         &
                   surfaceSous, P_nbplantes, P_extin, cumrg, cumraint, fapar, delta, P_adfol, lairognecum, laieffcum, P_dfolbas, &
                   P_dfolhaut, dfol, rdif, parapluie, raint, P_parsurrg, P_forme, lai, laisen, eai, P_interrang, nlax, nsen,     &
                   P_codlainet, P_hautbase, P_codepalissage, P_hautmaxtec, P_largtec, originehaut, hauteur, deltahauteur,        &
                   P_hautmax, varrapforme, largeur, jul, trg, P_latitude, rombre, rsoleil, P_orientrang, P_ktrou, tauxcouv,      &
                   P_khaut, surfAO, surfAS, fpari, P_Propres, P_PropresP, P_tauxmortresP, P_Efremobil, maperenne, mafeuil,       &
                   mafeuilverte, masecveg, resperenne, maperennemort, QCperennemort, QCO2resperenne, masec, QNrestemp,           &
                   QNresperenne, QNperenne, QNveg, QNvegstruc, QNperennemort, dltarestempN, dltarestemp, innlax, dltaperennesen, &
                   dltaQNperennesen, nstopres, tmin, P_tgelveg10, P_PropresPN ,dltarespstruc, pNvegstruc, pNrestemp,             &
                   P_codeplante, QNfeuille, QNtige, P_code_acti_reserve, ndes, phoi, phoi_veille, P_phobase, P_codeindetermin,   &
                   P_restemp0, demande, P_QNperenne0, P_codemsfinal, P_codephot)

    implicit none

  integer, intent(IN)    :: ipl
  integer, intent(IN)    :: ens
  integer, intent(IN)    :: nlev
  integer, intent(IN)    :: P_codeperenne  ! // PARAMETER // option defining the annual (1) or perenial (2) character of the plant // code 1/2 // PARPLT // 0
  integer, intent(IN)    :: nplt
  integer, intent(IN)    :: n
  integer, intent(IN)    :: P_codehypo  ! // PARAMETER // option of simulation of a  phase of hypocotyl growth (1) or planting of plantlets (2) // code 1/2 // PARPLT // 0
  integer, intent(IN)    :: P_codegermin  ! // PARAMETER // option of simulation of a germination phase or a delay at the beginning of the crop (1) or  direct starting (2) // code 1/2 // PARPLT // 0
  real,    intent(IN)    :: P_masecplantule  ! // PARAMETER // initial shoot biomass of plantlet // t ha-1 // PARPLT // 1
  real,    intent(IN)    :: P_adil  ! // PARAMETER // Parameter of the critical curve of nitrogen needs [Nplante]=P_adil MS^(-P_bdil) // N% MS // PARPLT // 1
  integer, intent(IN)    :: namf
  real,    intent(IN)    :: P_adilmax  ! // PARAMETER // Parameter of the maximum curve of nitrogen needs [Nplante]=P_adilmax MS^(-P_bdilmax) // N% MS // PARPLT // 1
  integer, intent(IN)    :: nrec
  integer, intent(IN)    :: P_codcueille  ! // PARAMETER // way how to harvest // code 1/2 // PARTEC // 0
  integer, intent(IN)    :: P_codefauche  ! // PARAMETER // option of cut modes for forage crops: yes (1), no (2) // code 1/2 // PARTEC // 0
  integer, intent(IN)    :: P_codelaitr  ! // PARAMETER // choice between soil cover or LAI calculation // code 1/2 // PARPLT // 0
  integer, intent(INOUT) :: P_codetransrad  ! // PARAMETER // simulation option of radiation 'interception: law Beer (1), radiation transfers (2) // code 1/2 // PARPLT // 0
  real,    intent(IN)    :: P_efcroijuv  ! // PARAMETER // Maximum radiation use efficiency during the juvenile phase(LEV-AMF) // g MJ-1 // PARPLT // 1
  real,    intent(IN)    :: P_efcroiveg  ! // PARAMETER // Maximum radiation use efficiency during the vegetative stage (AMF-DRP) // g MJ-1 // PARPLT // 1
  integer, intent(IN)    :: ndrp
  real,    intent(IN)    :: P_efcroirepro  ! // PARAMETER // Maximum radiation use efficiency during the grain filling phase (DRP-MAT) // g MJ-1 // PARPLT // 1
! DR 13/02/2019 je rajoute chargefruit pour mettre le test comme il est dans le trunk
  real,    intent(IN)    :: chargefruit   ! // OUTPUT // Amount of filling fruits per m-2 // nb fruits.m-2
  real,    intent(IN)    :: P_coefb  ! // PARAMETER // parameter defining radiation effect on  conversion efficiency // SD // PARAM // 1
  real,    intent(IN)    :: tcult   ! // OUTPUT // Crop surface temperature (daily average) // degree C
  real,    intent(IN)    :: P_teopt  ! // PARAMETER // Optimal temperature for the biomass growth // degree C // PARPLT // 1
  real,    intent(IN)    :: P_teoptbis  ! // PARAMETER // optimal temperature for the biomass growth (if there is a plateau between P_teopt and P_teoptbis) // degree C // PARPLT // 1
  real,    intent(IN)    :: P_temin  ! // PARAMETER // Minimum threshold temperature for development // degree C // PARPLT // 1
  real,    intent(IN)    :: P_temax  ! // PARAMETER // Maximal threshold temperature for the biomass growth  // degree C // PARPLT // 1
  integer, intent(IN)    :: P_codeclichange  ! // PARAMETER // option for climatel change : yes (2), no (1)  // code 1/2 // STATION // 0
  real,    intent(IN)    :: P_alphaco2  ! // PARAMETER // coefficient allowing the modification of radiation use efficiency in case of  atmospheric CO2 increase // SD // PARPLT // 1
  real,    intent(IN)       :: co2
  real,    intent(IN)    :: P_resplmax  ! // PARAMETER // maximal reserve biomass // t ha-1 // PARAMV6 // 1
  real,    intent(IN)    :: densite   ! // OUTPUT // Actual sowing density // plants.m-2
  integer, intent(IN)    :: P_codeh2oact  ! // PARAMETER // code to activate  water stress effect on the crop: yes (1), no (2) // code 1/2 // PARAM // 0
  real,    intent(IN)    :: swfac   ! // OUTPUT // Index of stomatic water stress  // 0-1
  real,    intent(IN)    :: exobiom   ! // OUTPUT // Index of excess water active on surface growth // 0-1
  integer, intent(IN)    :: P_codeinnact  ! // PARAMETER // code activating  nitrogen stress effect on the crop: yes (1), no (2) // code 1/2 // PARAM // 0
  real,    intent(IN)    :: fpv   ! // OUTPUT // Sink strength of developing leaves // g.j-1.m-2
  real,    intent(IN)    :: fpft   ! // OUTPUT // Sink strength of fruits  // g.m-2 j-1
  real,    intent(IN)    :: P_remobres  ! // PARAMETER // proportion of daily remobilisable carbon reserve // SD // PARPLT // 1
  real,    intent(IN)    :: P_msresiduel  ! // PARAMETER // Residual dry matter after a cut // t ha-1 // PARTEC // 1
  integer, intent(IN)    :: P_nbcueille  ! // PARAMETER // number of fruit harvestings // code 1/2 // PARTEC // 0
  real,    intent(IN)    :: rdtint
  real,    intent(IN)    :: CNgrain   ! // OUTPUT // Nitrogen concentration of grains  // %
  integer, intent(IN)    :: P_codemontaison  ! // PARAMETER // code to stop the reserve limitation from the stem elongation // code 1/2 // PARAMV6 // 0
  integer, intent(IN)    :: nmontaison

! Modifs Bruno et Loic fevrier 2013
    real,    intent(IN)    :: mafeuil
    real,    intent(IN)    :: mafeuilverte
    real,    intent(IN)    :: masecveg
    real,    intent(IN)    :: P_Propres
    real,    intent(IN)    :: P_PropresP
    real,    intent(IN)    :: P_tauxmortresP
    real,    intent(IN)    :: P_Efremobil
    real,    intent(INOUT) :: masecnp_veille    ! valeur de la veille
    real,    intent(INOUT) :: masecnp           ! valeur du jour courant   // OUTPUT // Aboveground dry matter  // t.ha-1
    real,    intent(INOUT) :: QNplantenp_veille ! valeur de la veille
    real,    intent(INOUT) :: QNplantenp        ! valeur du jour courant  // OUTPUT // Amount of nitrogen accumulated in the plant  // kgN.ha-1
    real,    intent(INOUT) :: inns        ! Index of nitrogen stress active on growth in biomass // P_innmin to 1
    real,    intent(INOUT) :: inn         ! Nitrogen nutrition index (satisfaction of nitrogen needs ) // 0-1
    real,    intent(INOUT) :: innlai      ! Index of nitrogen stress active on leaf growth // P_innmin a 1
    real,    intent(INOUT) :: cumdltaremobilN
    real,    intent(INOUT) :: ebmax
    real,    intent(INOUT) :: ftemp      ! Temperature-related EPSIBMAX reduction factor // 0-1
    real,    intent(INOUT) :: epsib      ! Radiation use efficiency  // t ha-1.Mj-1. m-2
    real,    intent(INOUT) :: fco2
    real,    intent(INOUT) :: dltams     ! Growth rate of the plant  // t ha-1.j-1
    real,    intent(INOUT) :: dltamsen   ! Senescence rate // t ha-1 j-1
    real,    intent(INOUT) :: dltamstombe
    real,    intent(INOUT) :: restemp    ! C crop reserve, during the cropping season, or during the intercrop period (for perenial crops) // t ha-1
    real,    intent(INOUT) :: dltamsN
    real,    intent(INOUT) :: photnet    ! net photosynthesis // t ha-1.j-1
    real,    intent(INOUT) :: sourcepuits   ! Pool/sink ratio // 0-1
    real,    intent(INOUT) :: dltaremobil   ! Amount of perennial reserves remobilised // g.m-2.j-1
    real,    intent(INOUT) :: dltaremobilN
    real,    intent(INOUT) :: remobilj   ! Amount of biomass remobilized on a daily basis for the fruits  // g.m-2 j-1
    real,    intent(INOUT) :: cumdltares
    real,    intent(INOUT) :: magrain_veille  ! magrain_veille => valeur precedente (valeur de la veille par ex.)
    real,    intent(INOUT) :: magrain         ! magrain => valeur actuelle (valeur du jour courant par ex.)
    real,    intent(INOUT) :: masecneo   ! Newly-formed dry matter  // t.ha-1
    real,    intent(IN)    :: surface(2)
    real,    intent(OUT)   :: surfaceSous(2)
    integer, intent(IN)    :: P_nbplantes  ! // PARAMETER // number of simulated plants // SD // P_USM/USMXML // 0
    real,    intent(INOUT) :: P_extin(P_nbplantes)  ! // PARAMETER // extinction coefficient of photosynthetic active radiation canopy // SD // PARPLT // 1
    real,    intent(INOUT) :: cumrg       ! Sum of global radiation during the stage sowing-harvest   // Mj.m-2
    real,    intent(INOUT) :: cumraint    ! Sum of intercepted radiation  // Mj.m-2
    real,    intent(OUT)   :: fapar       ! Proportion of radiation intercepted // 0-1
    real,    intent(OUT)   :: delta

    ! TRANSRAD
    real,    intent(IN)    :: P_adfol  ! // PARAMETER // parameter determining the leaf density evolution within the chosen shape // m-1 // PARPLT // 1
    real,    intent(IN)    :: lairognecum
    real,    intent(IN)    :: laieffcum
    real,    intent(IN)    :: P_dfolbas  ! // PARAMETER // minimal foliar density within the considered shape // m2 leaf m-3 // PARPLT // 1
    real,    intent(IN)    :: P_dfolhaut  ! // PARAMETER // maximal foliar density within the considered shape // m2 leaf m-3 // PARPLT // 1
    real,    intent(OUT)   :: dfol   !  "Within the shape  leaf density" // m2 m-3
    real,    intent(OUT)   :: rdif   ! Ratio between diffuse radiation and global radiation  // 0-1
    integer, intent(OUT)   :: parapluie
    real,    intent(OUT)   :: raint   ! Photosynthetic active radiation intercepted by the canopy  // Mj.m-2
    real,    intent(IN)    :: P_parsurrg  ! // PARAMETER // coefficient PAR/RG for the calculation of PAR  // * // STATION // 1
    integer, intent(IN)    :: P_forme  ! // PARAMETER // Form of leaf density profile  of crop: rectangle (1), triangle (2) // code 1/2 // PARPLT // 0
    real,    intent(IN)    :: lai     ! Leaf area index (table) // m2 leafs  m-2 soil
    real,    intent(IN)    :: laisen   ! Leaf area index of senescent leaves // m2 leafs  m-2 soil
    real,    intent(IN)    :: eai
    real,    intent(IN)    :: P_interrang  ! // PARAMETER // Width of the P_interrang // m // PARTEC // 1
    integer, intent(IN)    :: nlax
    integer, intent(IN)    :: nsen
    integer, intent(IN)    :: P_codlainet  ! // PARAMETER // option of calculation of the LAI (1 : direct LAInet; 2 : LAInet = gross LAI - senescent LAI) // code 1/2 // PARPLT // 0
    real,    intent(IN)    :: P_hautbase  ! // PARAMETER // Base height of crop // m // PARPLT // 1
    integer, intent(IN)    :: P_codepalissage  ! // PARAMETER // option: no (1),  yes2) // code 1/2 // PARTEC // 0
    real,    intent(IN)    :: P_hautmaxtec  ! // PARAMETER // maximal height of the plant allowed by the management // m // PARTEC // 1
    real,    intent(IN)    :: P_largtec  ! // PARAMETER // technical width // m // PARTEC // 1
    real,    intent(IN)    :: originehaut
    real,    intent(INOUT) :: hauteur   ! Height of canopy // m
    real,    intent(INOUT) :: deltahauteur
    real,    intent(INOUT) :: P_hautmax  ! // PARAMETER // Maximum height of crop // m // PARPLT // 1
    real,    intent(INOUT) :: varrapforme
    real,    intent(OUT)   :: largeur   ! Width of the plant shape  // m
    integer, intent(IN)    :: jul
    real,    intent(IN)    :: trg   ! Active radiation (entered or calculated) // MJ.m-2
    real,    intent(IN)    :: P_latitude  ! // PARAMETER // Latitudinal position of the crop  // degree // STATION // 0
    real,    intent(OUT)   :: rombre   ! Radiation fraction in the shade // 0-1
    real,    intent(OUT)   :: rsoleil   ! Radiation fraction in the full sun // 0-1
    real,    intent(IN)    :: P_orientrang  ! // PARAMETER // Direction of ranks // rd (0=NS) // PARTEC // 1
    real,    intent(IN)    :: P_ktrou  ! // PARAMETER // Extinction Coefficient of PAR through the crop  (radiation transfer) // * // PARPLT // 1

    real,    intent(IN)    :: tauxcouv   ! Cover rate // SD
    real,    intent(IN)    :: P_khaut  ! // PARAMETER // Extinction Coefficient connecting leaf area index to height crop // * // PARAM // 1
    real,    intent(INOUT) :: surfAO
    real,    intent(INOUT) :: surfAS

    real,    intent(INOUT) :: maperenne
    real,    intent(INOUT) :: resperenne
    real,    intent(INOUT) :: maperennemort ! // Cumulative biomass of dead perennial organs  // t ha-1 /
    real,    intent(INOUT) :: QCperennemort ! // Cumulative C in dead perennial organs  // kg ha-1 /
    real,    intent(INOUT) :: QCO2resperenne
    real,    intent(INOUT) :: masec          ! Total Biomass (plant + perennial organs)  // t.ha-1
    real,    intent(INOUT) :: QNrestemp
    real,    intent(INOUT) :: QNresperenne
    real,    intent(INOUT) :: QNperennemort
    real,    intent(INOUT) :: QNperenne
    real,    intent(INOUT) :: QNveg
    real,    intent(INOUT) :: QNvegstruc
    real,    intent(INOUT) :: dltarestempN
    real,    intent(INOUT) :: dltarestemp  ! daily biomass flow between temporary reserves (restemp) and perennial reserves (maperenne) // t ha-1 d-1 /
    real,    intent(INOUT) :: innlax
    real,    intent(INOUT) :: dltaperennesen    ! // OUTPUT // daily mortality biomass flow of perennial organs  // t ha-1 d-1
    real,    intent(INOUT) :: dltaQNperennesen  ! // OUTPUT // daily N loss due to mortality of perennial organs // kg ha-1 d-1
    integer, intent(INOUT) :: nstopres          ! jour d'arret de la mise en reserve due au gel
    real,    intent(IN)    :: tmin
    real,    intent(IN)    :: P_tgelveg10
    real,    intent(OUT)   :: fpari  ! Cover rate // SD
    real,    intent(IN)    :: P_PropresPN
    real,    intent(OUT)   :: dltarespstruc     ! daily biomass flow towards structural perennial reserves // t ha-1 d-1/
    real,    intent(OUT)   :: pNvegstruc        ! proportion of N in structural biomass
    real,    intent(OUT)   :: pNrestemp         ! proportion of N in temporary reserves
    character(len=3),intent(IN) :: P_codeplante
    real,    intent(INOUT) :: QNfeuille
    real,    intent(INOUT) :: QNtige
    integer, intent(IN)    :: P_code_acti_reserve
    integer, intent(IN)    :: ndes
    real,    intent(IN)    :: phoi
    real,    intent(IN)    :: phoi_veille
    real,    intent(IN)    :: P_phobase
    integer, intent(IN)    :: P_codeindetermin
    real,    intent(IN)    :: P_restemp0
    real,    intent(IN)    :: demande
    real,    intent(IN)    :: P_QNperenne0
    integer, intent(IN)    :: P_codemsfinal
    integer, intent(IN)    :: P_codephot

end subroutine biomaer


! ----------------------------------------------------------------------------------------------------------------------------

subroutine raytrans(P_codetransrad,P_extin,cumrg,cumraint,fapar,delta,P_adfol,lairognecum,laieffcum,P_dfolbas,P_dfolhaut,     &
                    dfol,rdif,parapluie,raint,P_parsurrg,P_forme,lai,laisen,eai,P_interrang,nlax,nsen,nrec, P_codlainet,  &
                    P_hautbase, &
                    P_codepalissage,P_hautmaxtec,P_largtec,originehaut,hauteur,deltahauteur,P_hautmax,varrapforme,largeur,    &
                    jul,trg,P_latitude, rombre,rsoleil,P_orientrang,P_ktrou,P_codelaitr,P_khaut,tauxcouv,surface,surfaceSous, &
                    ipl,P_nbplantes,ens,surfAO,surfAS)

  implicit none

!: Arguments

! RAYTRANS
  integer, intent(IN)    :: P_codetransrad  ! // PARAMETER // simulation option of radiation 'interception: law Beer (1), radiation transfers (2) // code 1/2 // PARPLT // 0
  real,    intent(IN)    :: surface(2)
  real,    intent(OUT)   :: surfaceSous(2)
  integer, intent(IN)    :: ipl
  integer, intent(IN)    :: P_nbplantes  ! // PARAMETER // number of simulated plants // SD // P_USM/USMXML // 0
  integer, intent(IN)    :: ens
  real,    intent(INOUT) :: P_extin(P_nbplantes)  ! // PARAMETER // extinction coefficient of photosynthetic active radiation canopy // SD // PARPLT // 1
  real,    intent(INOUT) :: cumrg   ! // OUTPUT // Sum of global radiation during the stage sowing-harvest   // Mj.m-2
  real,    intent(INOUT) :: cumraint   ! // OUTPUT // Sum of intercepted radiation  // Mj.m-2
  real,    intent(OUT)   :: fapar   ! // OUTPUT // Proportion of radiation intercepted // 0-1
  real,    intent(OUT)   :: delta

! TRANSRAD
  real,    intent(IN)    :: P_adfol  ! // PARAMETER // parameter determining the leaf density evolution within the chosen shape // m-1 // PARPLT // 1
  real,    intent(IN)    :: lairognecum
  real,    intent(IN)    :: laieffcum
  real,    intent(IN)    :: P_dfolbas  ! // PARAMETER // minimal foliar density within the considered shape // m2 leaf m-3 // PARPLT // 1
  real,    intent(IN)    :: P_dfolhaut  ! // PARAMETER // maximal foliar density within the considered shape // m2 leaf m-3 // PARPLT // 1
  real,    intent(OUT)   :: dfol   ! // OUTPUT //  "Within the shape  leaf density" // m2 m-3
  real,    intent(OUT)   :: rdif   ! // OUTPUT // Ratio between diffuse radiation and global radiation  // 0-1
  integer, intent(OUT)   :: parapluie
  real,    intent(OUT)   :: raint   ! // OUTPUT // Photosynthetic active radiation intercepted by the canopy  // Mj.m-2
  real,    intent(IN)    :: P_parsurrg  ! // PARAMETER // coefficient PAR/RG for the calculation of PAR  // * // STATION // 1

  integer, intent(IN)    :: P_forme  ! // PARAMETER // Form of leaf density profile  of crop: rectangle (1), triangle (2) // code 1/2 // PARPLT // 0
  real,    intent(IN)    :: lai   ! // OUTPUT // Leaf area index (table) // m2 leafs  m-2 soil
  real,    intent(IN)    :: laisen   ! // OUTPUT // Leaf area index of senescent leaves // m2 leafs  m-2 soil
  real,    intent(IN)    :: eai
  real,    intent(IN)    :: P_interrang  ! // PARAMETER // Width of the P_interrang // m // PARTEC // 1
  real,    intent(IN)    :: nlax
  real,    intent(IN)    :: nsen
  real,    intent(IN)    :: nrec
  real,    intent(IN)    :: P_codlainet  ! // PARAMETER //option of calculation of the LAI (1 : direct LAInet; 2 : LAInet = gross LAI - senescent LAI) // code 1/2 // PARPLT // 0
  real,    intent(IN)    :: P_hautbase  ! // PARAMETER // Base height of crop // m // PARPLT // 1
  integer, intent(IN)    :: P_codepalissage  ! // PARAMETER // option: no (1),  yes2) // code 1/2 // PARTEC // 0
  real,    intent(IN)    :: P_hautmaxtec  ! // PARAMETER // maximal height of the plant allowed by the management // m // PARTEC // 1
  real,    intent(IN)    :: P_largtec  ! // PARAMETER // technical width // m // PARTEC // 1
  real,    intent(IN)    :: originehaut

  real,    intent(INOUT) :: hauteur   ! // OUTPUT // Height of canopy // m
  real,    intent(INOUT) :: deltahauteur
  real,    intent(INOUT) :: P_hautmax  ! // PARAMETER // Maximum height of crop // m // PARPLT // 1
  real,    intent(INOUT) :: varrapforme
  real,    intent(OUT)   :: largeur   ! // OUTPUT // Width of the plant shape  // m

  integer, intent(IN)    :: jul
  real,    intent(IN)    :: trg   ! // OUTPUT // Active radiation (entered or calculated) // MJ.m-2
  real,    intent(IN)    :: P_latitude  ! // PARAMETER // Latitudinal position of the crop  // degree // STATION // 0
  real,    intent(OUT)   :: rombre   ! // OUTPUT // Radiation fraction in the shade // 0-1
  real,    intent(OUT)   :: rsoleil   ! // OUTPUT // Radiation fraction in the full sun // 0-1
  real,    intent(IN)    :: P_orientrang  ! // PARAMETER // Direction of ranks // rd (0=NS) // PARTEC // 1
  real,    intent(IN)    :: P_ktrou  ! // PARAMETER // Extinction Coefficient of PAR through the crop  (radiation transfer) // * // PARPLT // 1

! BEER
  integer, intent(IN)    :: P_codelaitr  ! // PARAMETER // choice between soil cover or LAI calculation // code 1/2 // PARPLT // 0
  real,    intent(IN)    :: tauxcouv   ! // OUTPUT // Cover rate // SD
  real,    intent(IN)    :: P_khaut  ! // PARAMETER // Extinction Coefficient connecting leaf area index to height crop // * // PARAM // 1
  real,    intent(INOUT)   :: surfAO
  real,    intent(INOUT)   :: surfAS

end subroutine raytrans


! ------------------------------------------------------------------------------------------------------------------------------------

subroutine transrad(P_adfol,lairognecum,laieffcum,P_dfolbas,P_dfolhaut,dfol,rdif,parapluie,raint,P_parsurrg,P_forme,lai,  &
                    laisen,eai, P_interrang,nlax,nsen,P_codlainet,P_hautbase,P_codepalissage,P_hautmaxtec,P_largtec,      &
                    originehaut,hauteur,deltahauteur, P_hautmax,varrapforme,largeur,jul,trg,P_latitude,rombre,rsoleil,    &
               !     P_orientrang,P_ktrou,surfAO,surfAS,ipl,P_nbplantes)
                   P_orientrang,P_ktrou,surfAO,surfAS,ipl)

  implicit none

  real,    intent(IN)    :: P_adfol  ! // PARAMETER // parameter determining the leaf density evolution within the chosen shape // m-1 // PARPLT // 1
  real,    intent(IN)    :: lairognecum
  real,    intent(IN)    :: laieffcum
  real,    intent(IN)    :: P_dfolbas  ! // PARAMETER // minimal foliar density within the considered shape // m2 leaf m-3 // PARPLT // 1
  real,    intent(IN)    :: P_dfolhaut  ! // PARAMETER // maximal foliar density within the considered shape // m2 leaf m-3 // PARPLT // 1
  real,    intent(OUT)   :: dfol   ! // OUTPUT //  "Within the shape  leaf density" // m2 m-3
  real,    intent(OUT)   :: rdif   ! // OUTPUT // Ratio between diffuse radiation and global radiation  // 0-1
  integer, intent(OUT)   :: parapluie
  real,    intent(OUT)   :: raint   ! // OUTPUT // Photosynthetic active radiation intercepted by the canopy  // Mj.m-2
  real,    intent(IN)    :: P_parsurrg  ! // PARAMETER // coefficient PAR/RG for the calculation of PAR  // * // STATION // 1

  integer, intent(IN)    :: P_forme  ! // PARAMETER // Form of leaf density profile  of crop: rectangle (1), triangle (2) // code 1/2 // PARPLT // 0
  real,    intent(IN)    :: lai   ! // OUTPUT // Leaf area index (table) // m2 leafs  m-2 soil
  real,    intent(IN)    :: laisen   ! // OUTPUT // Leaf area index of senescent leaves // m2 leafs  m-2 soil
  real,    intent(IN)    :: eai
  real,    intent(IN)    :: P_interrang  ! // PARAMETER // Width of the P_interrang // m // PARTEC // 1
  real,    intent(IN)    :: nlax
  real,    intent(IN)    :: nsen
  real,    intent(IN)    :: P_codlainet  ! // PARAMETER //option of calculation of the LAI (1 : direct LAInet; 2 : LAInet = gross LAI - senescent LAI) // code 1/2 // PARPLT // 0
  real,    intent(IN)    :: P_hautbase  ! // PARAMETER // Base height of crop // m // PARPLT // 1
  integer, intent(IN)    :: P_codepalissage  ! // PARAMETER // option: no (1),  yes2) // code 1/2 // PARTEC // 0
  real,    intent(IN)    :: P_hautmaxtec  ! // PARAMETER // maximal height of the plant allowed by the management // m // PARTEC // 1
  real,    intent(IN)    :: P_largtec  ! // PARAMETER // technical width // m // PARTEC // 1
  real,    intent(IN)    :: originehaut

  real,    intent(INOUT) :: hauteur   ! // OUTPUT // Height of canopy // m
  real,    intent(INOUT) :: deltahauteur
  real,    intent(INOUT) :: P_hautmax  ! // PARAMETER // Maximum height of crop // m // PARPLT // 1
  real,    intent(INOUT) :: varrapforme

  real,    intent(OUT)   :: largeur   ! // OUTPUT // Width of the plant shape  // m

  integer, intent(IN)    :: jul
  real,    intent(IN)    :: trg   ! // OUTPUT // Active radiation (entered or calculated) // MJ.m-2
  real,    intent(IN)    :: P_latitude  ! // PARAMETER // Latitudinal position of the crop  // degree // STATION // 0

  real,    intent(OUT)   :: rombre   ! // OUTPUT // Radiation fraction in the shade // 0-1
  real,    intent(OUT)   :: rsoleil   ! // OUTPUT // Radiation fraction in the full sun // 0-1
  real,    intent(IN)    :: P_orientrang  ! // PARAMETER // Direction of ranks // rd (0=NS) // PARTEC // 1
  real,    intent(IN)    :: P_ktrou  ! // PARAMETER // Extinction Coefficient of PAR through the crop  (radiation transfer) // * // PARPLT // 1

  real,    intent(INOUT)   :: surfAO
  real,    intent(INOUT)   :: surfAS

  integer, intent(IN)    :: ipl
!  integer, intent(IN)    :: P_nbplantes  ! // PARAMETER // number of simulated plants // SD // P_USM/USMXML // 0

end subroutine transrad


! ---------------------------------------------------------------------------------------------------------------


subroutine beer(P_codelaitr, raint, P_parsurrg, P_extin,lai, eai, trg, tauxcouv, rombre, rsoleil, parapluie, nsen,  &
                nlax, nrec, n, P_codlainet, hauteur, deltahauteur, P_hautmax, P_hautbase, P_khaut, laisen, surface,  &
                ipl, surfaceSous)

  implicit none

  integer, intent(IN)    :: P_codelaitr  ! choice between soil cover or LAI calculation // code 1/2 // PARPLT // 0
  real,    intent(OUT)   :: raint   ! Photosynthetic active radiation intercepted by the canopy  // Mj.m-2
  real,    intent(IN)    :: P_parsurrg  ! coefficient PAR/RG for the calculation of PAR  // * // STATION // 1
  real,    intent(IN)    :: P_extin  ! extinction coefficient of photosynthetic active radiation canopy // SD // PARPLT // 1
  real,    intent(IN)    :: lai   ! Leaf area index (table) // m2 leafs  m-2 soil
  real,    intent(IN)    :: eai
  real,    intent(IN)    :: trg   ! Active radiation (entered or calculated) // MJ.m-2
  real,    intent(IN)    :: tauxcouv   ! Cover rate // SD
  real,    intent(OUT)   :: rombre   ! Radiation fraction in the shade // 0-1
  real,    intent(OUT)   :: rsoleil   ! Radiation fraction in the full sun // 0-1
  integer, intent(OUT)   :: parapluie
  integer, intent(IN)    :: nsen
  integer, intent(IN)    :: nlax
  integer, intent(IN)    :: nrec
  integer, intent(IN)    :: n
  integer, intent(IN)    :: P_codlainet  !option of calculation of the LAI (1 : direct LAInet; 2 : LAInet = gross LAI - senescent LAI) // code 1/2 // PARPLT // 0
  real,    intent(INOUT) :: hauteur   ! Height of canopy // m
  real,    intent(INOUT) :: deltahauteur
  real,    intent(IN)    :: P_hautmax  ! Maximum height of crop // m // PARPLT // 1
  real,    intent(IN)    :: P_hautbase  ! Base height of crop // m // PARPLT // 1
  real,    intent(IN)    :: P_khaut  ! Extinction Coefficient connecting leaf area index to height crop // * // PARAM // 1
  real,    intent(IN)    :: laisen   ! Leaf area index of senescent leaves // m2 leafs  m-2 soil
  real,    intent(IN)    :: surface(2)

  integer, intent(IN)    :: ipl
  real,    intent(OUT)   :: surfaceSous(2)

end subroutine beer


! -----------------------------------------------------------------------------------------------------------------------------

subroutine senescen(nlev, n, nbjmax, lai, P_codeinnact, P_codeh2oact, senfac, innsenes, P_codlainet, P_codeperenne, nbfeuille,  &
                    P_nbfgellev, P_codgellev, tcultmin, P_tletale, P_tdebgel, P_tgellev90, P_tgellev10, ipl, densitelev,        &
                    densiteassoc, P_codgeljuv, P_tgeljuv90, P_tgeljuv10, P_codgelveg, P_tgelveg90, P_tgelveg10,                 &
                    nstopfeuille, somcour, restemp, ndrp, nrec, ndes, P_QNpltminINN, numcult, P_codeinitprec, ulai, P_vlaimax,  &
                    durvieI, P_durvieF, inn, P_durviesupmax, P_codestrphot, phoi, P_phobasesen, dltams, P_msresiduel,           &
                    P_ratiosen, tdevelop, somtemp, pfeuilverte, deltai, lai_to_sen, dernier_n, nsencour, dltaisen, dltamsen,    &
                    fstressgel, fgellev, gelee, densite, laisen, nlan, P_stsenlan, nsen, P_stlaxsen, namf, nlax, P_stlevamf,    &
                    P_stamflax, nrecbutoir, mortplante, nst2, mortplanteN, durvie, strphot, msres, dltamsres, ndebsen,          &
                    somsenreste, msresjaune, QNplantenp, P_dltamsminsen, P_dltamsmaxsen, P_alphaphot,             &
                    strphotveille, cumdltaremobsen, nstopres, dltaremobsen, mafeuilverte, codeinstal, P_code_acti_reserve,      &
                    laiveille, mafeuiljaune, nmat, P_lai0, P_codeplante)

USE Divers, only: GEL
USE Messages

  implicit none

  integer, intent(INOUT) :: nlev
  integer, intent(IN)    :: n
  integer, intent(IN)    :: nbjmax
  real,    intent(INOUT) :: lai   ! // OUTPUT // Leaf area index (table) // m2 leafs  m-2 soil
  integer, intent(IN)    :: P_codeinnact  ! // PARAMETER // code activating  nitrogen stress effect on the crop: yes (1), no (2) // code 1/2 // PARAM // 0
  integer, intent(IN)    :: P_codeh2oact  ! // PARAMETER // code to activate  water stress effect on the crop: yes (1), no (2) // code 1/2 // PARAM // 0
  real,    intent(IN)    :: senfac   ! // OUTPUT // Water stress index on senescence // 0-1
  real,    intent(IN)    :: innsenes   ! // OUTPUT // Index of nitrogen stress active on leaf death // P_innmin to 1
  integer, intent(IN)    :: P_codlainet  ! // PARAMETER //option of calculation of the LAI (1 : direct LAInet; 2 : LAInet = gross LAI - senescent LAI) // code 1/2 // PARPLT // 0
  integer, intent(IN)    :: P_codeperenne  ! // PARAMETER // option defining the annual (1) or perenial (2) character of the plant // code 1/2 // PARPLT // 0
  integer, intent(IN)    :: nbfeuille   ! // OUTPUT // Number of leaves on main stem // SD
  integer, intent(IN)    :: P_nbfgellev  ! // PARAMETER // leaf number at the end of the juvenile phase (frost sensitivity)  // nb pl-1 // PARPLT // 1
  integer, intent(IN)    :: P_codgellev  ! // PARAMETER // activation of plantlet frost // code 1/2 // PARPLT // 0
  real,    intent(IN)    :: tcultmin
  real,    intent(IN)    :: P_tletale  ! // PARAMETER // lethal temperature for the plant // degree C // PARPLT // 1
  real,    intent(IN)    :: P_tdebgel  ! // PARAMETER // temperature of frost beginning // degree C // PARPLT // 1
  real,    intent(IN)    :: P_tgellev90  ! // PARAMETER // temperature corresponding to 90% of frost damage on the plantlet  // degree C // PARPLT // 1
  real,    intent(IN)    :: P_tgellev10  ! // PARAMETER // temperature corresponding to 10% of frost damage on the plantlet  // degree C // PARPLT // 1
  integer, intent(IN)    :: ipl
  real,    intent(IN)    :: densitelev
  real,    intent(IN)    :: densiteassoc
  integer, intent(IN)    :: P_codgeljuv  ! // PARAMETER // activation of LAI frost at the juvenile stadge // code 1/2 // PARPLT // 0
  real,    intent(IN)    :: P_tgeljuv90  ! // PARAMETER // temperature corresponding to 90 % of frost damage on the LAI (juvenile stage) // degree C // PARPLT // 1
  real,    intent(IN)    :: P_tgeljuv10  ! // PARAMETER // temperature corresponding to 10 % of frost damage on the LAI (juvenile stage) // degree C // PARPLT // 1
  integer, intent(IN)    :: P_codgelveg  ! // PARAMETER // activation of LAI frost at adult stage // code 1/2 // PARPLT // 0
  real,    intent(IN)    :: P_tgelveg90  ! // PARAMETER // temperature corresponding to 90 % of frost damage on the LAI (adult stage) // degree C // PARPLT // 1
  real,    intent(IN)    :: P_tgelveg10  ! // PARAMETER // temperature corresponding to 10 % of frost damage on the LAI (adult stage) // degree C // PARPLT // 1
  integer, intent(IN)    :: nstopfeuille
  real,    intent(INOUT) :: somcour   ! // OUTPUT // Cumulated units of development between two stages // degree.days
  integer, intent(IN)    :: ndrp
  integer, intent(INOUT) :: nrec
  integer, intent(INOUT) :: ndes
  integer, intent(IN)    :: nmat
  real,    intent(IN)    :: P_QNpltminINN  ! // PARAMETER // minimal amount of nitrogen in the plant allowing INN computing // kg ha-1 // PARAM // 1
  integer, intent(IN)    :: numcult
  integer, intent(IN)    :: P_codeinitprec  ! // PARAMETER // reinitializing initial status in case of chaining simulations : yes (1), no (2) // code 1/2 // PARAM // 0
  ! dans l'ideal max(n,dernier_n) ! (dernier_n)
  real,    intent(IN)    :: ulai(nbjmax)  !   // OUTPUT // Daily relative development unit for LAI // 0-3
  real,    intent(IN)    :: P_vlaimax  ! // PARAMETER // ULAI at inflection point of the function DELTAI=f(ULAI) // SD // PARPLT // 1
  real,    intent(IN)    :: durvieI
  real,    intent(IN)    :: P_durvieF  ! // PARAMETER // maximal  lifespan of an adult leaf expressed in summation of P_Q10=2 (2**(T-Tbase)) // P_Q10 // PARPLT // 1
  real,    intent(IN)    :: inn   ! // OUTPUT // Nitrogen nutrition index (satisfaction of nitrogen needs ) // 0-1
  real,    intent(IN)    :: P_durviesupmax  ! // PARAMETER // proportion of additional lifespan due to an overfertilization // SD // PARPLT // 1
  integer, intent(IN)    :: P_codestrphot  ! // PARAMETER // activation of the photoperiodic stress on lifespan : yes (1), no (2) // code 1/2 // PARPLT // 0
  real,    intent(IN)    :: phoi   ! // OUTPUT // Photoperiod // hours
  real,    intent(IN)    :: P_phobasesen  ! // PARAMETER // photoperiod under which the photoperiodic stress is activated on the leaf lifespan // heures // PARPLT // 1
  ! dans l'ideal max(n,dernier_n) ! (dernier_n) ! ndebsen a dernier_n, puis si n < ndebsen, 1 a n
  real,    intent(INOUT) :: dltams(nbjmax)  !   // OUTPUT // Growth rate of the plant  // t ha-1.j-1
  real,    intent(IN)    :: P_msresiduel  ! // PARAMETER // Residual dry matter after a cut // t ha-1 // PARTEC // 1
  real,    intent(IN)    :: P_ratiosen  ! // PARAMETER // fraction of senescent biomass (by ratio at the total biomass) // between 0 and 1 // PARPLT // 1
  real,    intent(IN)    :: tdevelop(n) ! 1 to n
  real,    intent(IN)    :: somtemp   ! // OUTPUT // Sum of temperatures // degree C.j
  ! dans l'ideal max(n,dernier_n) ! (dernier_n) ! ndebsen a dernier_n, puis si n < ndebsen, 1 a n
  real,    intent(IN)    :: pfeuilverte(nbjmax) !  // OUTPUT // Proportion of green leaves in total non-senescent biomass // 0-1
   ! dans l'ideal max(n,dernier_n) ! (dernier_n) ! ndebsen a dernier_n, puis si n < ndebsen, 1 a n
  real,    intent(IN)    :: deltai(nbjmax)  !   // OUTPUT // Daily increase of the green leaf index // m2 leafs.m-2 soil
  real,    intent(IN)    :: P_lai0  ! // PARAMETER // Initial leaf area index // m2 m-2 // INIT // 1
  character(len=3), intent(IN) :: P_codeplante ! // PARAMETER // Name code of the plant in 3 letters // * // PARPLT // 0

  integer, intent(INOUT) :: dernier_n
  integer, intent(INOUT) :: nsencour
  real,    intent(INOUT) :: dltaisen   ! // OUTPUT // Daily increase of the senescent leaf index // m2.m-2 sol.j-1
  real,    intent(INOUT) :: dltamsen   ! // OUTPUT // Senescence rate // t ha-1 j-1
  real,    intent(INOUT) :: fstressgel   ! // OUTPUT // Frost index on the LAI // 0-1
  real,    intent(INOUT) :: fgellev
  logical, intent(INOUT) :: gelee
  real,    intent(INOUT) :: densite   ! // OUTPUT // Actual sowing density // plants.m-2
  real,    intent(INOUT) :: laisen(0:1) ! veille (0), aujourd'hui (1)    // OUTPUT // Leaf area index of senescent leaves // m2 leafs  m-2 soil
  integer, intent(INOUT) :: nlan
  real,    intent(INOUT) :: P_stsenlan  ! // PARAMETER // Sum of development units between the stages SEN et LAN // degree.days // PARPLT // 1
  integer, intent(INOUT) :: nsen
  real,    intent(INOUT) :: P_stlaxsen  ! // PARAMETER // Sum of development units between the stages LAX and SEN // degree.days // PARPLT // 1
  integer, intent(INOUT) :: namf
  integer, intent(INOUT) :: nlax
  real,    intent(INOUT) :: P_stlevamf  ! // PARAMETER // Sum of development units between the stages LEV and AMF // degree.days // PARPLT // 1
  real,    intent(INOUT) :: P_stamflax  ! // PARAMETER // Sum of development units between the stages AMF and LAX // degree.days // PARPLT // 1
  integer, intent(INOUT) :: nrecbutoir
  integer, intent(INOUT) :: mortplante
  integer, intent(INOUT) :: nst2
  integer, intent(INOUT) :: mortplanteN
  ! dans l'ideal max(n,dernier_n) ! (dernier_n) ! ndebsen a dernier_n, puis si n < ndebsen, 1 a n
  real,    intent(INOUT) :: durvie(nbjmax) !   // OUTPUT // Actual life span of the leaf surface //  degree C
  real,    intent(OUT)   :: strphot
  real,    intent(INOUT) :: msres
  real,    intent(INOUT) :: dltamsres
  integer, intent(INOUT) :: ndebsen
  real,    intent(INOUT) :: somsenreste
  real,    intent(INOUT) :: msresjaune   ! // OUTPUT // Senescent residual dry matter  // t.ha-1
!  real,    intent(INOUT) :: mafeuiljaune   ! // OUTPUT // Dry matter of yellow leaves // t.ha-1
  real,    intent(INOUT) :: restemp   ! // OUTPUT // C crop reserve, during the cropping season // t ha-1

  real,    intent(IN)    :: QNplantenp   ! // OUTPUT // Amount of nitrogen taken up by the plant  // kgN.ha-1

! STRESS PHOTOPERIODIQUE
  real,    intent(IN)    :: P_dltamsminsen  ! // PARAMETER // threshold value of deltams from which the photoperiodic effect on senescence is maximal // t ha-1j-1 // PARAM // 1
  real,    intent(IN)    :: P_dltamsmaxsen  ! // PARAMETER // threshold value of deltams from which there is no more photoperiodic effect on senescence // t ha-1j-1 // PARPLT // 1
  real,    intent(IN)    :: P_alphaphot  ! // PARAMETER // parameter of photoperiodic effect on leaf lifespan // P_Q10 // PARAM // 1
  real,    intent(INOUT) :: strphotveille
! -- FIN STRESS PHOTOPERIODIQUE
! Ajout Loic Juin 2016
  real,    intent(INOUT) :: cumdltaremobsen ! // cumulative amount of leaves biomass remobilized during senescence t ha-1
  integer, intent(INOUT) :: nstopres
! Modif Loic et Bruno fevrier 2013 : mise a jour du pool restemp
  real,    intent(OUT)   :: dltaremobsen ! // Leaves biomass remobilized during senescence t ha-1 j-1
  real,    intent(IN)    :: mafeuilverte
  integer, intent(IN)    :: codeinstal
  integer, intent(IN)    :: P_code_acti_reserve
  real,    intent(INOUT) :: laiveille
  real,    intent(IN)    :: mafeuiljaune   ! // OUTPUT // Dry matter of yellow leaves // t.ha-1
  !real,    intent(IN)    :: masecveg
  real,    intent(IN)    :: lai_to_sen ! lai for senescence in case au cut crop

end subroutine senescen

! -------------------------------------------------------------------------------------------------------------------

subroutine ApportFeuillesMortes(P_codeplante, P_codemonocot, P_codedyntalle, P_code_acti_reserve, &
                               P_abscission, P_parazofmorte, fstressgel, inn, mortmasec, surf, CNresmin, &
                               CNresmax, Qmulchdec, QNplantenp, dltamsen, dltamstombe, mafeuiltombe, QCplantetombe, &
                               QNplantetombe, QCapp, QNapp, QCresorg, QNresorg, Cres1, Nres1, Cnondec, Nnondec,  &
                               awb, bwb, cwb, CroCo, akres, bkres, ahres, bhres, Wb, kres, hres, msneojaune, mafeuiljaune, &
                               lai, laisen, masecnp, QNvegstruc, mafeuiltombefauche, msresgel, QNresgel, matigestruc, &
                               QNfeuille, QNtige, mafeuil, Chum1, Nhum1, Chumi, Nhumi, CsurNsol, Chuma, Nhuma)
  implicit none

  character(len=3), intent(IN) :: P_codeplante
  integer, intent(IN)    :: P_codemonocot
  integer, intent(IN)    :: P_codedyntalle ! // PARAMETER // Activation of the module simulating tiller dynamic: yes (1), no (2) // code 1/2 // PARAMV6/PLT // 0
  integer, intent(IN)    :: P_code_acti_reserve
  real,    intent(IN)    :: P_abscission   ! // PARAMETER // proportion of senescent leaves falling onto the soil // SD // PARPLT // 1
  real,    intent(IN)    :: P_parazofmorte ! // PARAMETER // parameter of proportionality between the C/N of dead leaves and the INN // SD // PARPLT // 1
  real,    intent(IN)    :: fstressgel     ! // OUTPUT // Frost index on the LAI // 0-1
  real,    intent(IN)    :: inn            ! // OUTPUT // Nitrogen nutrition index (satisfaction of nitrogen needs ) // 0-1
  real,    intent(IN)    :: mortmasec      ! // OUTPUT // Dead tiller biomass  // t.ha-1
  real,    intent(IN)    :: surf           ! // OUTPUT // Fraction of surface in the shade // 0-1
  real,    intent(IN)    :: CNresmin       ! // PARAMETER // minimum observed value of ratio C/N of organic residue ires  // g g-1 // PARAM // 1
  real,    intent(IN)    :: CNresmax       ! // PARAMETER // maximum observed value of ratio C/N of organic residue ires // g g-1 // PARAM // 1

  real,    intent(IN)    :: Qmulchdec      ! // PARAMETER // maximal amount of decomposing mulch of residue ires// t C.ha-1 // PARAM // 1
  real,    intent(INOUT) :: QNplantenp     ! // OUTPUT // Amount of N taken up by the whole plant // kg.ha-1
  real,    intent(INOUT) :: dltamsen       ! // OUTPUT // Senescence rate // t ha-1 d-1
  real,    intent(INOUT) :: dltamstombe
  real,    intent(INOUT) :: mafeuiltombe   ! // OUTPUT // Dry matter of fallen leaves // t.ha-1
  real,    intent(INOUT) :: QCplantetombe  ! // OUTPUT // cumulative amount of C in fallen leaves // kg.ha-1
  real,    intent(INOUT) :: QNplantetombe
  real,    intent(INOUT) :: QCapp      ! // OUTPUT // cumulative amount of organic C added to soil // kg.ha-1
  real,    intent(INOUT) :: QNapp      ! // OUTPUT // cumulative amount of organic N added to soil // kg.ha-1
  real,    intent(INOUT) :: QCresorg
  real,    intent(INOUT) :: QNresorg
  real,    intent(INOUT) :: Cres1      ! premiere couche, itrav1 = 1 & itrav2 = 1
  real,    intent(INOUT) :: Nres1      ! premiere couche, itrav1 = 1 & itrav2 = 1
  real,    intent(INOUT) :: Cnondec    ! // OUTPUT // undecomposable C stock of the type 10 residues on the surface // t.ha-1
  real,    intent(INOUT) :: Nnondec    ! // OUTPUT // undecomposable N stock of the type 10 residues on the surface // kg.ha-1

  real,    intent(IN)    :: awb
  real,    intent(IN)    :: bwb
  real,    intent(IN)    :: cwb
  real,    intent(IN)    :: akres
  real,    intent(IN)    :: bkres
  real,    intent(IN)    :: ahres
  real,    intent(IN)    :: bhres
  real,    intent(IN)    :: Croco
  real,    intent(INOUT) :: Wb
  real,    intent(INOUT) :: kres
  real,    intent(INOUT) :: hres

  real,    intent(INOUT) :: msneojaune
  real,    intent(INOUT) :: mafeuiljaune
  real,    intent(IN)    :: lai
  real,    intent(IN)    :: laisen
  real,    intent(INOUT) :: masecnp
  real,    intent(INOUT) :: QNvegstruc
  real,    intent(INOUT) :: mafeuiltombefauche
  real,    intent(IN)    :: msresgel ! Strutural biomasse of stems after cutting of forages
  real,    intent(IN)    :: QNresgel ! Strutural N content after cutting of forages
  real,    intent(INOUT) :: matigestruc
  real,    intent(INOUT) :: QNfeuille
  real,    intent(INOUT) :: QNtige
  real,    intent(IN)    :: mafeuil
  real,    intent(INOUT) :: Chum1
  real,    intent(INOUT) :: Nhum1
  real,    intent(INOUT) :: Chumi
  real,    intent(INOUT) :: Nhumi
  real,    intent(INOUT) :: CsurNsol
  real,    intent(INOUT) :: Chuma
  real,    intent(INOUT) :: Nhuma

end subroutine ApportFeuillesMortes


! --------------------------------------------------------------------------------------------------------------------------

subroutine fruit(n, namf, P_codcalinflo, P_pentinflores, densite, P_restemp0, cumdltares, P_inflomax, ndrp, P_nbcueille,nrec, &
                 P_nboite, P_dureefruit, fpv, dltams, remobilj, P_spfrmin, P_spfrmax, P_afruitpot, upvt, fgelflo,             &
                 P_codazofruit, P_codeinnact, inns, nnou, P_codeclaircie, nb_eclair, neclair, P_nbinfloecl, P_codetremp,      &
                 tcultmin, tcultmax, P_tminremp, P_tmaxremp, nbrecolte, rdtint, P_allocfrmax, nmat, P_afpf, P_bfpf, P_cfpf,   &
                 P_dfpf, pfmax, P_nbinflo, nfruit, pdsfruit, fpft, sourcepuits, spfruit, nbfruit, nfruitnou, nbfrote,         &
                 devjourfr, cumdevfr, pousfruit, ftempremp, nbj0remp, dltags, frplusp, ircarb, magrain, allocfruit, masecnp,  &
                 compretarddrp, pdsfruittot, QNgrain, CNgrain, P_code_acti_reserve, dltaremobil)

  USE Messages

  implicit none

  integer, intent(IN)    :: n
  integer, intent(IN)    :: namf
  integer, intent(IN)    :: P_codcalinflo  ! // PARAMETER // option of the way of calculation of the inflorescences number  // code 1/2 // PARPLT // 0
  real,    intent(IN)    :: P_pentinflores  ! // PARAMETER // parameter of the calculation of the inflorescences number  // SD // PARPLT // 1
  real,    intent(IN)    :: densite   ! // OUTPUT // Actual sowing density // plants.m-2
  real,    intent(IN)    :: P_restemp0  ! // PARAMETER // initial reserve biomass // t ha-1 // INIT // 1
  real,    intent(IN)    :: cumdltares
  real,    intent(IN)    :: P_inflomax  ! // PARAMETER // maximal number of inflorescences per plant // nb pl-1 // PARPLT // 1
  integer, intent(IN)    :: ndrp
  integer, intent(IN)    :: P_nbcueille  ! // PARAMETER // number of fruit harvestings // code 1/2 // PARTEC // 0
  integer, intent(IN)    :: nrec
  integer, intent(IN)    :: P_nboite  ! // PARAMETER // "Number of  box  or  age class  of fruits for the fruit growth for the indeterminate crops " // SD // PARPLT // 1
  real,    intent(IN)    :: P_dureefruit  ! // PARAMETER // total growth period of a fruit at the setting stage to the physiological maturity // degree.days // PARPLT // 1
  real,    intent(IN)    :: fpv   ! // OUTPUT // Sink strength of developing leaves // g.j-1.m-2
  real,    intent(IN)    :: dltams   ! // OUTPUT // Growth rate of the plant  // t ha-1.j-1
  real,    intent(IN)    :: remobilj   ! // OUTPUT // Amount of biomass remobilized on a daily basis for the fruits  // g.m-2 j-1
  real,    intent(IN)    :: P_spfrmin  ! // PARAMETER // minimal sources/sinks value allowing the trophic stress calculation for fruit onset // SD // PARPLT // 1
  real,    intent(IN)    :: P_spfrmax  ! // PARAMETER // maximal sources/sinks value allowing the trophic stress calculation for fruit onset // SD // PARPLT // 1
  real,    intent(IN)    :: P_afruitpot  ! // PARAMETER // maximal number of set fruits per degree.day (indeterminate growth) // nbfruits degree.day-1 // PARPLT // 1
  real,    intent(IN)    :: upvt   ! // OUTPUT // Daily development unit  // degree.days
  real,    intent(IN)    :: fgelflo   ! // OUTPUT // Frost index on the number of fruits // 0-1
  integer, intent(IN)    :: P_codazofruit  ! // PARAMETER // option of activation of the direct effect of the nitrogen plant status upon the fruit/grain number // code 1/2 // PARPLT // 0
  integer, intent(IN)    :: P_codeinnact  ! // PARAMETER // code activating  nitrogen stress effect on the crop: yes (1), no (2) // code 1/2 // PARAM // 0
  real,    intent(IN)    :: inns   ! // OUTPUT // Index of nitrogen stress active on growth in biomass // P_innmin to 1
  integer, intent(IN)    :: nnou
  integer, intent(IN)    :: nb_eclair
  integer, intent(IN)    :: P_codeclaircie  ! // PARAMETER // option to activate techniques of fruit removal  // code 1/2 // PARTEC // 0
  integer, intent(IN)    :: neclair(1:nb_eclair)
  real,    intent(IN)    :: P_nbinfloecl(1:nb_eclair)  ! // PARAMETER // "number of inflorescences or fruits removed at  fruit removal  " // nb pl-1 // PARTEC // 1
  integer, intent(IN)    :: P_codetremp  ! // PARAMETER // option of heat effect on grain filling: yes (2), no (1) // code 1/2 // PARPLT // 0
  real,    intent(IN)    :: tcultmin
  real,    intent(IN)    :: tcultmax   ! // OUTPUT // Crop surface temperature (daily maximum) // degree C
  real,    intent(IN)    :: P_tminremp  ! // PARAMETER // Minimal temperature for grain filling // degree C // PARPLT // 1
  real,    intent(IN)    :: P_tmaxremp  ! // PARAMETER // maximal temperature for grain filling // degree C // PARPLT // 1
  integer, intent(IN)    :: nbrecolte
  real,    intent(IN)    :: rdtint(nbrecolte) ! 1 to nbrecolte
  real,    intent(IN)    :: P_allocfrmax  ! // PARAMETER // maximal daily allocation towards fruits // SD // PARPLT // 1
  integer, intent(IN)    :: nmat
  real,    intent(IN)    :: P_afpf  ! // PARAMETER // parameter of the  function logistic defining sink strength of fruits (indeterminate growth) : relative fruit age at which growth is maximal // SD // PARPLT // 1
  real,    intent(IN)    :: P_bfpf  ! // PARAMETER // parameter of the logistic curve defining sink strength of fruits (indeterminate growth) :  rate of maximum growth proportionately to maximum weight of fruits // * // PARPLT // 1
  real,    intent(IN)    :: P_cfpf  ! // PARAMETER // parameter of the first potential growth phase of fruit, corresponding to an exponential type function describing the cell division phase. // SD // PARPLT // 1
  real,    intent(IN)    :: P_dfpf  ! // PARAMETER // parameter of the first potential growth phase of fruit, corresponding to an exponential type function describing the cell division phase. // SD // PARPLT // 1
  real,    intent(IN)    :: pfmax

  real,    intent(INOUT) :: P_nbinflo  ! // PARAMETER // imposed number of inflorescences  // nb pl-1 // PARPLT // 1   // OUTPUT // Number of inflorescences // SD
  real,    intent(INOUT) :: nfruit(P_nboite)    ! 1 to P_nboite    // OUTPUT // Number of fruits in box 5 // nb fruits
  real,    intent(INOUT) :: pdsfruit(P_nboite)  ! 1 to P_nboite    // OUTPUT // Weight of fruits in box 3 // g m-2
  real,    intent(INOUT) :: fpft   ! // OUTPUT // Sink strength of fruits  // g.m-2 j-1
  real,    intent(INOUT) :: sourcepuits   ! // OUTPUT // Pool/sink ratio // 0-1
  real,    intent(INOUT) :: spfruit   ! // OUTPUT // Index of trophic stress applied to the number of fruits // 0 to 1
  real,    intent(INOUT) :: nbfruit
  real,    intent(INOUT) :: nfruitnou   ! // OUTPUT // Number of set fruits  // nb set fruits.m-2
  real,    intent(INOUT) :: nbfrote
  real,    intent(INOUT) :: devjourfr
  real,    intent(INOUT) :: cumdevfr
  real,    intent(INOUT) :: pousfruit   ! // OUTPUT // Number of fruits transferred from one box to the next  // nb fruits
  real,    intent(INOUT) :: ftempremp
  integer, intent(INOUT) :: nbj0remp   ! // OUTPUT // Number of shrivelling days //
  real,    intent(INOUT) :: dltags   ! // OUTPUT // Growth rate of the grains  // t ha-1.j-1
  real,    intent(INOUT) :: frplusp
  real,    intent(INOUT) :: ircarb   ! // OUTPUT // Carbon harvest index // g  grain g plant-1
  real,    intent(INOUT) :: magrain(0:1) ! n-1 & n
  real,    intent(INOUT) :: allocfruit   ! // OUTPUT // Allocation ratio of  assimilats to the  fruits 0 to 1  // 0-1
  real,    intent(INOUT) :: masecnp   ! // OUTPUT // Aboveground dry matter  // t.ha-1
  integer, intent(INOUT) :: compretarddrp
  real,    intent(INOUT) :: pdsfruittot
  real,    intent(INOUT) :: QNgrain
  real,    intent(INOUT) :: CNgrain
  ! Loic aout 2019 : traque bug vigne v9
  integer, intent(IN)    :: P_code_acti_reserve
  real,    intent(IN)    :: dltaremobil

end subroutine fruit


! --------------------------------------------------------------------------------------------------------------------


subroutine grain(n,ndrp,nrec,nlev,nrecbutoir,P_nbjgrain,dltams,P_cgrain,P_cgrainv0, & ! IN
                 P_nbgrmin,P_nbgrmax,P_codazofruit,P_codeinnact,inns,fgelflo,P_codeir,  & ! IN
                 P_vitircarb,P_irmax,P_vitircarbT,somcourdrp,nmat,masecnp,P_codetremp,  & ! IN
                 tcultmin,tcultmax,P_tminremp,P_tmaxremp,P_pgrainmaxi,              & ! IN
                 ircarb,nbgrains,pgrain,CNgrain,vitmoy,nbgraingel,pgraingel,  & ! INOUT
                 dltags,ftempremp,magrain,nbj0remp,pdsfruittot)                 ! INOUT

  USE Messages

  implicit none

!: Arguments

  integer, intent(IN)    :: n
  integer, intent(IN)    :: ndrp
  integer, intent(IN)    :: nrec
  integer, intent(IN)    :: nlev
  integer, intent(IN)    :: nrecbutoir
  integer, intent(IN)    :: P_nbjgrain  ! // PARAMETER // Period to compute NBGRAIN // days // PARPLT // 1
  real,    intent(IN)    :: dltams(ndrp) ! ndrp-P_nbjgrain+1 to ndrp    // OUTPUT // Growth rate of the plant  // t ha-1.j-1
  real,    intent(IN)    :: P_cgrain  ! // PARAMETER // slope of the relationship between grain number and growth rate  // grains gMS -1 jour // PARPLT // 1
  real,    intent(IN)    :: P_cgrainv0  ! // PARAMETER // number of grains produced when growth rate is zero // grains m-2 // PARPLT // 1
  real,    intent(IN)    :: P_nbgrmin  ! // PARAMETER // Minimum number of grain // grains m-2  // PARPLT // 1
  real,    intent(IN)    :: P_nbgrmax  ! // PARAMETER // Maximum number of grain // grains m-2 // PARPLT // 1
  integer, intent(IN)    :: P_codazofruit  ! // PARAMETER // option of activation of the direct effect of the nitrogen plant status upon the fruit/grain number // code 1/2 // PARPLT // 0
  integer, intent(IN)    :: P_codeinnact  ! // PARAMETER // code activating  nitrogen stress effect on the crop: yes (1), no (2) // code 1/2 // PARAM // 0
  real,    intent(IN)    :: inns   ! // OUTPUT // Index of nitrogen stress active on growth in biomass // P_innmin to 1
  real,    intent(IN)    :: fgelflo   ! // OUTPUT // Frost index on the number of fruits // 0-1
  integer, intent(IN)    :: P_codeir  ! // PARAMETER // option of computing the ratio grain weight/total biomass: proportional to time(1), proportional to sum temperatures (2) // code 1/2 // PARPLT // 0
  real,    intent(IN)    :: P_vitircarb  ! // PARAMETER // Rate of increase of the carbon harvest index // g grain g plant -1 day-1 // PARPLT // 1
  real,    intent(IN)    :: P_irmax  ! // PARAMETER // Maximum harvest index // SD // PARPLT // 1
  real,    intent(IN)    :: P_vitircarbT  ! // PARAMETER // Heat rate of increase of the carbon harvest index  // g grain g plant-1 degree.day-1 // PARPLT // 1
  real,    intent(IN)    :: somcourdrp
  integer, intent(IN)    :: nmat
  real,    intent(IN)    :: masecnp(0:1)  ! n-1 (0) & n (1)    // OUTPUT // Aboveground dry matter  // t.ha-1
  integer, intent(IN)    :: P_codetremp  ! // PARAMETER // option of heat effect on grain filling: yes (2), no (1) // code 1/2 // PARPLT // 0
  real,    intent(IN)    :: tcultmin
  real,    intent(IN)    :: tcultmax   ! // OUTPUT // Crop surface temperature (daily maximum) // degree C
  real,    intent(IN)    :: P_tminremp  ! // PARAMETER // Minimal temperature for grain filling // degree C // PARPLT // 1
  real,    intent(IN)    :: P_tmaxremp  ! // PARAMETER // maximal temperature for grain filling // degree C // PARPLT // 1
  real,    intent(IN)    :: P_pgrainmaxi  ! // PARAMETER // Maximum weight of one grain (at 0% water content) // g // PARPLT // 1

  real,    intent(INOUT) :: ircarb(0:1)   ! n-1 (0) & n (1)    // OUTPUT // Carbon harvest index // g  grain g plant-1
  real,    intent(INOUT) :: nbgrains
  real,    intent(INOUT) :: pgrain
  real,    intent(INOUT) :: CNgrain   ! // OUTPUT // Nitrogen concentration of grains  // %
  real,    intent(INOUT) :: vitmoy   ! // OUTPUT // mean growth rate of the canopy (dans le livre c'est en g mais ca colle pas)  // g.m-2.d-1
  real,    intent(INOUT) :: nbgraingel
  real,    intent(INOUT) :: pgraingel
  real,    intent(INOUT) :: dltags   ! // OUTPUT // Growth rate of the grains  // t ha-1.j-1
  real,    intent(INOUT) :: ftempremp
  real,    intent(INOUT) :: magrain(0:1)  ! n-1 (0) & n (1)
  integer, intent(INOUT) :: nbj0remp   ! // OUTPUT // Number of shrivelling days //
  real,    intent(INOUT) :: pdsfruittot

end subroutine grain


! ------------------------------------------------------------------------------------------------------------------------------


subroutine eauqual(n,P_deshydbase,P_tempdeshyd,tcult,tairveille,ndrp,nrec,P_codeindetermin,P_nboite,P_stdrpdes,P_dureefruit, & ! IN
                   nfruit,pousfruit,pdsfruit,P_h2ofrvert,frplusp,P_vitpropsucre,P_vitprophuile,magrain,                      &
                   somcourdrp,nmat,maenfruit,nlev,masecnp,mafeuilverte,P_h2ofeuilverte,mafeuiljaune,P_h2ofeuiljaune,           &
                   matigestruc,P_h2otigestruc,restemp,P_h2oreserve,                                                       &
                   deshyd,eaufruit,ndebdes,teaugrain,h2orec,sucre,huile,sucreder,huileder,sucrems,huilems,               & ! INOUT
                   pdsfruitfrais,mafraisrec,CNgrain,mafraisfeuille,mafraistige,mafraisres,mafrais)

USE Messages

  implicit none

!: Arguments

  integer, intent(IN)    :: n
  real,    intent(IN)    :: P_deshydbase  ! // PARAMETER // phenological rate of evolution of fruit water content (>0 or <0) // g water.g MF-1.degree C-1 // PARPLT // 1
  real,    intent(IN)    :: P_tempdeshyd  ! // PARAMETER // increase in the fruit dehydration due to the increase of crop temperature (Tcult-Tair) // % water degree C-1 // PARPLT // 1
  real,    intent(IN)    :: tcult   ! // OUTPUT // Crop surface temperature (daily average) // degree C
  real,    intent(IN)    :: tairveille   ! // OUTPUT // Mean air temperature the previous day // degree C
  integer, intent(IN)    :: ndrp
  integer, intent(IN)    :: nrec
  integer, intent(IN)    :: P_codeindetermin  ! // PARAMETER // option of  simulation of the leaf growth and fruit growth : indeterminate (2) or determinate (1) // code 1/2 // PARPLT // 0
  integer, intent(IN)    :: P_nboite  ! // PARAMETER // "Number of  box  or  age class  of fruits for the fruit growth for the indeterminate crops " // SD // PARPLT // 1
  real,    intent(IN)    :: P_stdrpdes  ! // PARAMETER // phasic duration between the DRP stage and the beginning of the water fruit dynamics  // degree.days // PARPLT // 1
  real,    intent(IN)    :: P_dureefruit  ! // PARAMETER // total growth period of a fruit at the setting stage to the physiological maturity // degree.days // PARPLT // 1
  real,    intent(IN)    :: nfruit(P_nboite)        ! 1 to P_nboite    // OUTPUT // Number of fruits in box 5 // nb fruits
  real,    intent(IN)    :: pousfruit   ! // OUTPUT // Number of fruits transferred from one box to the next  // nb fruits
  real,    intent(IN)    :: pdsfruit(P_nboite)      ! 1 to P_nboite    // OUTPUT // Weight of fruits in box 3 // g m-2
  real,    intent(IN)    :: P_h2ofrvert  ! // PARAMETER // water content of fruits before the beginning of hydrous evolution (DEBDESHYD) // g water g-1 MF // PARPLT // 1
  real,    intent(IN)    :: frplusp
  real,    intent(IN)    :: P_vitpropsucre  ! // PARAMETER // increase rate of sugar harvest index  // g sugar g MS-1  j-1 // PARPLT // 1
  real,    intent(IN)    :: P_vitprophuile  ! // PARAMETER // increase rate of oil harvest index  // g oil g MS-1 j-1 // PARPLT // 1
  real,    intent(IN)    :: magrain
  real,    intent(IN)    :: somcourdrp
  integer, intent(IN)    :: nmat
  real,    intent(IN)    :: maenfruit   ! // OUTPUT // Dry matter of harvested organ envelopes // t.ha-1
  integer, intent(IN)    :: nlev
  real,    intent(IN)    :: masecnp   ! // OUTPUT // Aboveground dry matter  // t.ha-1
  real,    intent(IN)    :: mafeuilverte   ! // OUTPUT // Dry matter of green leaves // t.ha-1
  real,    intent(IN)    :: P_h2ofeuilverte  ! // PARAMETER // water content of green leaves // g water g-1 MF // PARPLT // 1
  real,    intent(IN)    :: mafeuiljaune   ! // OUTPUT // Dry matter of yellow leaves // t.ha-1
  real,    intent(IN)    :: P_h2ofeuiljaune  ! // PARAMETER // water content of yellow leaves // g water g-1 MF // PARPLT // 1
  real,    intent(IN)    :: matigestruc   ! // OUTPUT // Dry matter of stems (only structural parts) // t.ha-1
  real,    intent(IN)    :: P_h2otigestruc  ! // PARAMETER // structural stem part water content // g eau g-1 MF // PARPLT // 1
  real,    intent(IN)    :: restemp   ! // OUTPUT // C crop reserve, during the cropping season, or during the intercrop period (for perenial crops) // t ha-1
  real,    intent(IN)    :: P_h2oreserve  ! // PARAMETER // reserve water content // g eau g-1 MF // PARPLT // 1


  real,    intent(INOUT) :: deshyd(0:P_nboite)       ! 0 to P_nboite
  real,    intent(INOUT) :: eaufruit(0:P_nboite)     ! 0 to P_nboite
  integer, intent(INOUT) :: ndebdes
  real,    intent(INOUT) :: teaugrain
  real,    intent(INOUT) :: h2orec   ! // OUTPUT // Water content of harvested organs // %
  real,    intent(INOUT) :: sucre   ! // OUTPUT // Sugar content of fresh harvested organs // % (of fresh weight)
  real,    intent(INOUT) :: huile   ! // OUTPUT // Oil content of fresh harvested organs // % (of fresh weight)
  real,    intent(INOUT) :: sucreder
  real,    intent(INOUT) :: huileder
  real,    intent(INOUT) :: sucrems
  real,    intent(INOUT) :: huilems
  real,    intent(INOUT) :: pdsfruitfrais   ! // OUTPUT // Total weight of fresh fruits // g m-2
  real,    intent(INOUT) :: mafraisrec
  real,    intent(INOUT) :: CNgrain   ! // OUTPUT // Nitrogen concentration of grains  // %
  real,    intent(INOUT) :: mafraisfeuille
  real,    intent(INOUT) :: mafraistige
  real,    intent(INOUT) :: mafraisres
  real,    intent(INOUT) :: mafrais   ! // OUTPUT // Aboveground fresh matter // t.ha-1

end subroutine eauqual


! ---------------------------------------------------------------------------------------------------------------


subroutine croissanceFrontRacinaire(n, nh, nbCouches, hur, hucc, tcult, tsol, humin, dacouche, P_codesimul, P_epc,         &
                 P_obstarac, P_daseuilbas, P_daseuilhaut, P_dacohes, P_zrac0, P_coderacine, P_codeplante, P_codeperenne,   &
                 P_codehypo, P_codegermin, P_zracplantule, P_stoprac, P_codetemprac, P_tcmin, P_tcmax, P_tgmin, P_croirac, &
                 P_contrdamax, P_codeindetermin, P_sensanox, nplt, nrec, codeinstal, nger, nsen, nlax, nflo, nmat, nlev, namf, &
                 izrac, P_profsem, P_codcueille, deltai, lai, sioncoupe, P_sensrsec, P_codedyntalle, P_tcxstop, dltams,    &
                 rlf0, rlg0, nhe, nstoprac, zrac, zracmax, znonli, rlf_veille, rlg_veille, deltaz, dtjn, cumlracz,         &
                 poussracmoy, lracsenzf, lracsenzg, tempeff, efda, humirac_mean, profsol, P_humirac, P_code_acti_reserve)

USE Divers, only: F_humirac, escalin
USE Messages

  implicit none

  integer,           intent(IN)    :: n
  integer,           intent(IN)    :: nh
  integer,           intent(IN)    :: nbCouches
  real,              intent(IN)    :: hur(nbCouches)
  real,              intent(IN)    :: hucc(nbCouches)
  real,              intent(IN)    :: tcult   ! // OUTPUT // Crop surface temperature (daily average) // degree C
  real,              intent(IN)    :: tsol(nbCouches)
  real,              intent(IN)    :: humin(nbCouches)
  real,              intent(IN)    :: dacouche(nbCouches)
  character(len=*),  intent(IN)    :: P_codesimul  ! // PARAMETER // simulation code (culture ou feuille=lai force) // SD // P_USM/USMXML // 0
  real,              intent(IN)    :: P_epc(nh)  ! // PARAMETER // thickness of each soil layer // cm   // PARSOL // 1      // OUTPUT // Thickness of the horizon (1 or 2 )// cm
  real,              intent(IN)    :: P_obstarac  ! // PARAMETER // Soil depth which will block the root growth  // cm // PARSOL // 1
  real,              intent(IN)    :: P_daseuilbas  ! // PARAMETER // Threshold of bulk density of soil below that the root growth is not limited // g cm-3 // PARAM // 1
  real,              intent(IN)    :: P_daseuilhaut  ! // PARAMETER // Threshold of bulk density of soil below that the root growth  no more possible // g cm-3 // PARAM // 1
  real,              intent(IN)    :: P_dacohes  ! // PARAMETER // bulk density under which root growth is reduced due to a lack of cohesion d // g cm-3 // PARAM // 1
  real,              intent(IN)    :: P_zrac0  ! // PARAMETER // initial depth of root front  // cm // INIT // 1
  integer,           intent(IN)    :: P_coderacine  ! // PARAMETER // Choice of estimation module of growth root in volume: standard profile (1) or by actual density (2) // code 1/2 // PARPLT // 0
  character(len=3),  intent(IN)    :: P_codeplante  ! // PARAMETER // Name code of the plant in 3 letters // * // PARPLT // 0
  integer,           intent(IN)    :: P_codeperenne  ! // PARAMETER // option defining the annual (1) or perenial (2) character of the plant // code 1/2 // PARPLT // 0
  integer,           intent(IN)    :: P_codehypo  ! // PARAMETER // option of simulation of a  phase of hypocotyl growth (1) or planting of plantlets (2) // code 1/2 // PARPLT // 0
  integer,           intent(IN)    :: P_codegermin  ! // PARAMETER // option of simulation of a germination phase or a delay at the beginning of the crop (1) or  direct starting (2) // code 1/2 // PARPLT // 0
  real,              intent(IN)    :: P_zracplantule  ! // PARAMETER // depth of the initial root front of the plantlet  // cm // PARPLT // 1
  character(len=3),  intent(IN)    :: P_stoprac  ! // PARAMETER // stage when root growth stops (LAX or SEN) // * // PARPLT // 0
  integer,           intent(IN)    :: P_codetemprac  ! // PARAMETER // option calculation mode of heat time for the root: with crop temperature (1)  or with soil temperature (2) // code 1/2 // PARPLT // 0
  real,              intent(IN)    :: P_tcmin  ! // PARAMETER // Minimum temperature of growth // degree C // PARPLT // 1
  real,              intent(IN)    :: P_tcmax  ! // PARAMETER // Maximum temperature of growth // degree C // PARPLT // 1
  real,              intent(IN)    :: P_tgmin  ! // PARAMETER // Minimum threshold temperature used in emergence stage // degree C // PARPLT // 1
  real,              intent(IN)    :: P_croirac  ! // PARAMETER // Growth rate of the root front  // cm degree.days-1 // PARPLT // 1
  real,              intent(IN)    :: P_contrdamax  ! // PARAMETER // maximal root growth reduction due to soil strenghtness (high bulk density) // SD // PARPLT // 1
  integer,           intent(IN)    :: P_codeindetermin  ! // PARAMETER // option of  simulation of the leaf growth and fruit growth : indeterminate (2) or determinate (1) // code 1/2 // PARPLT // 0
  real,              intent(IN)    :: P_sensanox  ! // PARAMETER // anoxia sensitivity (0=insensitive) // SD // PARPLT // 1
  integer,           intent(IN)    :: nplt
  integer,           intent(IN)    :: nrec
  integer,           intent(IN)    :: codeinstal
  integer,           intent(IN)    :: nger
  integer,           intent(IN)    :: nsen
  integer,           intent(IN)    :: nlax
  integer,           intent(IN)    :: nflo
  integer,           intent(IN)    :: nmat
  integer,           intent(IN)    :: nlev
  integer,           intent(IN)    :: namf
  real,              intent(IN)    :: izrac   ! // OUTPUT // Index of excess water stress on roots  // 0-1
  real,              intent(IN)    :: P_profsem  ! // PARAMETER // Sowing depth // cm // PARTEC // 1
  integer,           intent(IN)    :: P_codcueille  ! // PARAMETER // way how to harvest // code 1/2 // PARTEC // 0
  real,              intent(IN)    :: deltai   ! // OUTPUT // Daily increase of the green leaf index // m2 leafs.m-2 soil
  real,              intent(IN)    :: lai   ! // OUTPUT // Leaf area index (table) // m2 leaves  m-2 soil
  logical,           intent(IN)    :: sioncoupe
  real,              intent(IN)    :: P_sensrsec  ! // PARAMETER // root sensitivity to drought (1=insensitive) // SD // PARPLT // 1
  integer,           intent(IN)    :: P_codedyntalle  ! // PARAMETER // Activation of the module simulating tiller dynamic: yes (1), no (2) // code 1/2 // PARAMV6/PLT // 0
  real,              intent(IN)    :: P_tcxstop  ! // PARAMETER // threshold temperature beyond which the foliar growth stops // degree C // PARPLT // 1
  real,              intent(IN)    :: dltams   ! // OUTPUT // Growth rate of the plant  // t ha-1.j-1

  integer,           intent(INOUT) :: nhe
  integer,           intent(INOUT) :: nstoprac
  real,              intent(INOUT) :: zrac   ! // OUTPUT // Depth reached by root system // cm
  real,              intent(INOUT) :: znonli     ! dans le doute, INOUT
  real,              intent(INOUT) :: rlf_veille(nbCouches) ! nger-1 ou n-1
  real,              intent(INOUT) :: rlg_veille(nbCouches) ! nger-1 ou n-1
  real,              intent(INOUT) :: deltaz   ! // OUTPUT // Deepening of the root front  // cm day-1
  real,              intent(INOUT) :: dtjn     !  // OUTPUT // Efficient temperature for root growth on day n // degree C.j-1
  real,              intent(OUT)   :: cumlracz   ! // OUTPUT // Sum of the effective root lengths  // cm root.cm -2 soil
  real,              intent(OUT)   :: poussracmoy   ! // OUTPUT // "Average index of the effect of soil constraints on the rooting profile (option  true density )" // 0-1
  real,              intent(INOUT) :: lracsenzf(nbCouches)
  real,              intent(INOUT) :: lracsenzg(nbCouches)
  real,              intent(INOUT) :: tempeff   ! // OUTPUT // Efficient temperature for growth // degree C
 ! 11/06/2013 la variable efda devient une variable de sortie journaliere pour Simtraces
  real,              intent(INOUT) :: efda      ! // OUTPUT // efda defines the effect of soil compaction through bulk density // 0-1
  real,              intent(INOUT) :: humirac_mean ! // OUTPUT // soil dryness // 0-1
  real,              intent(IN)    :: rlf0(nbCouches)
  real,              intent(IN)    :: rlg0(nbCouches)
  real,              intent(INOUT) :: zracmax          ! maximal rooting depth reached
  real,              intent(IN)    :: profsol
  integer,           intent(IN)    :: P_humirac
  integer,           intent(IN)    :: P_code_acti_reserve

end subroutine croissanceFrontRacinaire


! -----------------------------------------------------------------------------------------------------------------------------

subroutine densiteRacinaire(n, nbCouches, nbCoum, anox, hur, humin, dacouche, P_daseuilbas, P_daseuilhaut, P_dacohes, P_lvopt, &
             P_coderacine, P_contrdamax, P_sensanox, P_zlabour, P_zpente, P_zprlim, P_profsem, P_codazorac, P_minazorac,    &
             P_maxazorac, P_minefnra, P_codtrophrac, P_coefracoupe, P_stamflax, P_lvfront, P_laicomp, P_adens, P_bdens,     &
             P_lvmax,  P_draclong, P_sensrsec, P_stlevamf, P_vlaimax, longsperacf, longsperacg, debsenracf, debsenracg,     &
!             P_codedyntalle, P_propracfmax, P_zramif, P_longsperac, P_msresiduel, sioncoupe, codeinstal, nger, nlev, nrec,  &
             P_codedyntalle, P_propracfmax, P_longsperac, P_msresiduel, sioncoupe, codeinstal, nger, nlev, nrec,            &
             nit, amm,profsol, lai, tauxcouv, drlsenmortalle, densite, repracmin, repracmax, kreprac, idzrac,     &
             precrac, lai_veille, dltams, nstoprac, somtemprac, msrac_veille, zrac, znonli, deltaz, maxdayg, dtj, zracmax,  &
             racnoy, flrac, lracz, msrac, cumflrac, efdensite_rac, difrac, cumlracz, poussracmoy, cumlraczmaxi, dltamsrac,  &
             nsencourpreracf, nsencourpreracg, ndebsenracf, ndebsenracg, ndecalf, ndecalg, maxdayf, lracsentotf, lracsentotg,    &
             rltotf, rltotg, rlf_veille,rlg_veille, rlf, rlg, drlf, drlg, lracsenzf, lracsenzg, drlsenf, drlseng, somtempracvie, &
!            efda, efnrac_mean, humirac_mean, humirac_z, efnrac_z, rlj, P_codemortalracine, dltmsrac_plante, dltaremobil,   &
             efda, efnrac_mean, humirac_mean, humirac_z, efnrac_z, rlj, dltaremobil,   &
             P_code_diff_root, P_code_acti_reserve, inn, turfac, P_code_stress_root, P_codemonocot, P_kdisrac, P_codedisrac, &
             P_alloperirac, P_pgrainmaxi, P_humirac, hucc, masecnp, P_codemortalracine, QNrac, P_parazorac,               &
             P_code_rootdeposition, urac, msrac0, QNrac0, P_code_ISOP)

  implicit none

  integer,  intent(IN)    :: n
  integer,  intent(IN)    :: nbCouches      ! number of soil layers containing roots
  integer,  intent(IN)    :: nbCoum         ! maximum number of soil layers = nbCouchesSol_max
  real,     intent(IN)    :: anox(nbCouches)
  real,     intent(IN)    :: hur(nbCouches)
  real,     intent(IN)    :: humin(nbCouches)
  real,     intent(IN)    :: dacouche(nbCouches)
  real,     intent(IN)    :: P_daseuilbas   ! // PARAMETER // Threshold of bulk density of soil below that the root growth is not limited // g cm-3 // PARAM // 1
  real,     intent(IN)    :: P_daseuilhaut  ! // PARAMETER // Threshold of bulk density of soil below that the root growth  no more possible // g cm-3 // PARAM // 1
  real,     intent(IN)    :: P_dacohes      ! // PARAMETER // bulk density under which root growth is reduced due to a lack of cohesion d // g cm-3 // PARAM // 1
  real,     intent(IN)    :: P_lvopt        ! // PARAMETER // Optimum root density // cm root.cm-3 soil // PARAM // 1
  integer,  intent(IN)    :: P_coderacine   ! // PARAMETER // Choice of estimation module of growth root in volume: standard profile (1) or by actual density (2) // code 1/2 // PARPLT // 0
  real,     intent(IN)    :: P_contrdamax   ! // PARAMETER // maximal root growth reduction due to soil strenghtness (high bulk density) // SD // PARPLT // 1
  real,     intent(IN)    :: P_sensanox     ! // PARAMETER // anoxia sensitivity (0=insensitive) // SD // PARPLT // 1
  real,     intent(IN)    :: P_zlabour      ! // PARAMETER // Depth of ploughing  // cm // PARPLT // 1
  real,     intent(IN)    :: P_zpente       ! // PARAMETER // Depth where the root density is 50 percent of the surface root density for the reference profile // cm  // PARPLT // 1
  real,     intent(IN)    :: P_zprlim       ! // PARAMETER // Maximum depth of the root profile for the reference profile // cm  // PARPLT // 1
  real,     intent(IN)    :: lai            ! // INPUT // Leaf area index (table) // m2 leaf.m-2 soil
  real,     intent(IN)    :: tauxcouv       ! // INPUT // Cover rate // SD
  integer,  intent(IN)    :: nrec
  integer,  intent(IN)    :: codeinstal
  integer,  intent(IN)    :: nger
  integer,  intent(IN)    :: nlev
  real,     intent(IN)    :: P_profsem      ! // PARAMETER // Sowing depth // cm // PARTEC // 1
  integer,  intent(IN)    :: P_codazorac    ! // PARAMETER // activation of the nitrogen influence on root partitionning within the soil profile  // code 1/2 // PARPLT // 0
  real,     intent(IN)    :: P_minazorac    ! // PARAMETER // parameter of the effect of soil nitrogen on root soil partitioning // kg N ha-1 mm-1 // PARPLT // 1
  real,     intent(IN)    :: P_maxazorac    ! // PARAMETER // parameter of the effect of soil nitrogen on root soil partitioning  // kg N ha-1 mm-1 // PARPLT // 1
  real,     intent(IN)    :: P_minefnra     ! // PARAMETER // parameter of the effect of soil nitrogen on root soil partitioning // SD // PARPLT // 1
  integer,  intent(IN)    :: P_codtrophrac  ! // PARAMETER // trophic effect on root partitioning within the soil // code 1/2/3 // PARPLT // 0
  real,     intent(IN)    :: P_coefracoupe  ! // PARAMETER // coefficient to define the proportion of dying roots after cut (grass) // SD // PARAMV6/PLT // 1
  real,     intent(IN)    :: P_sensrsec     ! // PARAMETER // root sensitivity to drought (1=insensitive) // SD // PARPLT // 1
  real,     intent(IN)    :: P_stlevamf     ! // PARAMETER // Sum of development units between the stages LEV and AMF // degree.days // PARPLT // 1
  real,     intent(IN)    :: P_stamflax     ! // PARAMETER // Sum of development units between the stages AMF and LAX // degree.days // PARPLT // 1
  real,     intent(IN)    :: P_lvfront      ! // PARAMETER // Root density at the root front // cm root.cm-3 soil // PARPLT // 1
  real,     intent(IN)    :: P_laicomp      ! // PARAMETER // LAI from which starts competition inbetween plants // m2 m-2 // PARPLT // 1
  real,     intent(IN)    :: P_adens        ! // PARAMETER // Interplant competition parameter // SD // PARPLT // 1
  real,     intent(IN)    :: P_bdens        ! // PARAMETER // minimal density from which interplant competition starts // plants m-2 // PARPLT // 1
  real,     intent(IN)    :: P_draclong     ! // PARAMETER // Maximum rate of root length production // cm root plant-1 degree.days-1 // PARPLT // 1
  real,     intent(IN)    :: P_vlaimax      ! // PARAMETER // ULAI  at inflection point of the function DELTAI=f(ULAI) // SD // PARPLT // 1
  integer,  intent(IN)    :: P_codedyntalle ! // PARAMETER // Activation of the module simulating tiller dynamic: yes (1), no (2) // code 1/2 // PARAMV6/PLT // 0
  real,     intent(IN)    :: nit(nbCouches)
  real,     intent(IN)    :: amm(nbCouches)
  real,     intent(IN)    :: profsol
  real,     intent(IN)    :: idzrac
  real,     intent(IN)    :: precrac(nbCouches)
  logical,  intent(IN)    :: sioncoupe
  real,     intent(IN)    :: drlsenmortalle ! // OUTPUT // Root biomass corresponding to dead tillers // t ha-1.j-1
  real,     intent(IN)    :: densite        ! // OUTPUT // Actual sowing density // plants.m-2
  real,     intent(IN)    :: P_msresiduel   ! // PARAMETER // Residual dry matter after a cut // t ha-1 // PARTEC // 1
  real,     intent(IN)    :: lai_veille     ! lai(n-1)
  real,     intent(IN)    :: dltams         ! // OUTPUT // Growth rate of the plant  // t ha-1.j-1
  real,     intent(IN)    :: repracmin
  real,     intent(IN)    :: repracmax
  real,     intent(IN)    :: kreprac
  real,     intent(IN)    :: P_longsperac
 ! 11/06/2013 la variable efda devient une variable de sortie journaliere pour Simtraces
  real,     intent(INOUT)   :: efda          ! // OUTPUT // efda defines the effect of soil compaction through bulk density // 0-1
  real,     intent(INOUT)   :: efnrac_mean   ! // OUTPUT // effect of mineral nitrogen, which contributes to the root distribution in the layers with high mineral nitrogen content. // 0-1
  real,     intent(INOUT)   :: humirac_mean  ! // OUTPUT // soil dryness // 0-1
  real,     intent(INOUT)   :: humirac_z(nbcouches)
  real,     intent(INOUT)   :: efnrac_z(nbcouches)
  real,     intent(OUT)     :: rlj

  integer,  intent(INOUT) :: nstoprac
  real,     intent(INOUT) :: somtemprac
  real,     intent(INOUT) :: msrac_veille   ! msrac(n-1)
  real,     intent(INOUT) :: msrac          ! msrac(n)
  real,     intent(INOUT) :: zrac           ! // OUTPUT // Depth reached by root system // cm
  real,     intent(INOUT) :: znonli
  real,     intent(INOUT) :: deltaz         ! // OUTPUT // Deepening of the root front  // cm day-1
  real,     intent(INOUT) :: zracmax
  real,     intent(INOUT) :: racnoy
  real,     intent(INOUT) :: flrac(nbCouches)
  real,     intent(INOUT) :: lracz(nbCouches)

  real,     intent(OUT)   :: cumflrac
  real,     intent(OUT)   :: efdensite_rac
  real,     intent(OUT)   :: difrac
  real,     intent(OUT)   :: cumlracz       ! // OUTPUT // Sum of the effective root length  // cm root.cm-2 soil
  real,     intent(OUT)   :: poussracmoy    ! // OUTPUT // "average effect of soil constraints on the root length profile (option 'true density')" // 0-1
  real,     intent(OUT)   :: cumlraczmaxi
! DR 06/05/2015 je rajoute un code pour tester la mortalite des racines
!  integer,  intent(IN)    :: P_codemortalracine ! // PARAMETER // masec servant a calculer les racines mortes a la coupe : masec (1), masectot (2) // code 1/2 //PARAMv6 // 1

!   Modifs Loic et Bruno avril 2014
!   Longueur et masse des nouvelles racines emises au jour n (dltarltot et dltamsrac)
  real,    intent(INOUT) :: P_lvmax         ! // INPUT  // Maximum root length density encountered in the top soil (option profil-type) // cm.cm-3
  real,    intent(IN)    :: rlf_veille(nbCouches)    ! length of fine roots in layer iz on day n-1
  real,    intent(IN)    :: rlg_veille(nbCouches)    ! length of coarse roots in layer iz on day n-1
  real,    intent(IN)    :: debsenracf               ! Lifespan of fine roots  (degrees C.days)
  real,    intent(IN)    :: debsenracg               ! Lifespan of coarse roots  (degrees C.days)
  real,    intent(IN)    :: longsperacf              ! Specific root length of fine roots  (cm/g DM)
  real,    intent(IN)    :: longsperacg              ! Specific root length of coarse roots  (cm/g DM)
  real,    intent(IN)    :: P_propracfmax            !
! Lo�c Janvier 2021 : zramif n'est pas utilise je le supprime
!  real,    intent(IN)    :: P_zramif
  integer, intent(IN)    :: maxdayf
  integer, intent(IN)    :: maxdayg

  real,    intent(INOUT) :: dtj(maxdayg)         ! 1 to n    // OUTPUT // Daily efficient temperature for the root growth  // degree C.d
  real,    intent(INOUT) :: dltamsrac       ! // OUTPUT // Biomass of new roots emitted on day n // t ha-1 d-1
  real,    intent(INOUT) :: rlf(nbCouches)           ! length of fine roots in layer iz on day n
  real,    intent(INOUT) :: rlg(nbCouches)           ! length of coarse roots in layer iz on day n
! real,    intent(INOUT) :: drlf(maxdayf,nbCouches)  ! length of fine roots emitted in layer iz on day n
! real,    intent(INOUT) :: drlg(maxdayg,nbCouches)  ! length of coarse roots emitted in layer iz on day n
  real,    intent(INOUT) :: drlf(nbCoum,maxdayf)     ! length of fine roots emitted in layer iz on day n
  real,    intent(INOUT) :: drlg(nbCoum,maxdayg)     ! length of coarse roots emitted in layer iz on day n
  real,    intent(INOUT) :: drlsenf(nbCouches)       ! length of dead fine roots in layer iz on day n
  real,    intent(INOUT) :: drlseng(nbCouches)       ! length of dead coarse roots in layer iz on day n
  integer, intent(INOUT) :: ndebsenracf              ! first day when fine roots start to die
  integer, intent(INOUT) :: ndebsenracg              ! first day when coarse roots start to die
  integer, intent(INOUT) :: nsencourpreracf          ! last day at which fine root mortality is calculated
  integer, intent(INOUT) :: nsencourpreracg          ! last day at which coarse root mortality is calculated
  integer, intent(INOUT) :: ndecalf                  ! number of days remaining in the previous simulation without mortality of fine roots
  integer, intent(INOUT) :: ndecalg                  ! number of days remaining in the previous simulation without mortality of coarse roots
  real,    intent(INOUT) :: lracsenzf(nbCouches)     ! length of fine roots which die in layer iz on day n
  real,    intent(INOUT) :: lracsenzg(nbCouches)     ! length of coarse roots which die in layer iz on day n
  real,    intent(INOUT) :: somtempracvie            ! cumulative thermal time during successive runs (used to calculate initiation of root mortality)

  real,    intent(OUT)   :: lracsentotf      !// OUTPUT // Total length of fine senescent roots // cm root.cm-2 soil
  real,    intent(OUT)   :: lracsentotg      !// OUTPUT // Total length of coarse senescent roots // cm root.cm-2 soil
  real,    intent(OUT)   :: rltotf           !// OUTPUT // Total length of fine roots // cm root.cm-2 soil
  real,    intent(OUT)   :: rltotg           !// OUTPUT // Total length of coarse roots // cm root.cm-2 soil
!  real,    intent(OUT)   :: dltmsrac_plante  !// OUTPUT // pour sorties ArchiSTICS: biomasse journaliere allouee aux racines en g / m2 sol / plante
  real,    intent(IN)    :: dltaremobil
  integer, intent(IN)    :: P_code_diff_root
  integer, intent(IN)    :: P_code_acti_reserve
  real,    intent(IN)    :: inn
  real,    intent(IN)    :: turfac
  integer, intent(IN)    :: P_code_stress_root
! Florent Juin 2018
  real,    intent(IN)    :: P_kdisrac
  integer, intent(IN)    :: P_codedisrac
  real,    intent(IN)    :: P_alloperirac
  real,    intent(IN)    :: P_pgrainmaxi
  integer, intent(IN)    :: P_codemonocot
  integer, intent(IN)    :: P_humirac
  real,    intent(IN)    :: hucc(nbCouches)
  real,    intent(IN)    :: masecnp
  integer, intent(IN)    :: P_codemortalracine ! // PARAMETER // masec servant a calculer les racines mortes a la coupe : masec (1), masectot (2) // code 1/2 //PARAMv6 // 1
  real,    intent(INOUT) :: QNrac
  real,    intent(IN)    :: P_parazorac
  integer, intent(IN)    :: P_code_rootdeposition

  ! PL, 7/10/2020 
  real,    intent(OUT)   :: urac   ! // OUTPUT // daily relative development unit for root growth  // 1-3
! Ajout Loic juin 2021
  real,    intent(OUT)   :: msrac0
  real,    intent(OUT)   :: QNrac0
  integer, intent(IN)    :: P_code_ISOP

end subroutine densiteRacinaire

! -------------------------------------------------------------------------------------------------------------------------------

      subroutine densirac(n, nbCouches, nbCoum, dacouche, hur, humin, anox, nstoprac, P_codazorac, P_minazorac, P_maxazorac,       &
                    P_minefnra, P_codtrophrac, P_coefracoupe, P_daseuilbas, P_daseuilhaut, P_dacohes, P_contrdamax, P_sensrsec, &
                    P_sensanox, P_stlevamf, P_stamflax, P_lvfront, P_laicomp, P_adens, P_bdens, P_draclong, P_vlaimax,          &
                    P_longsperac, P_codedyntalle, drlsenmortalle, P_profsem, P_msresiduel, P_lvopt, profsol, densite, lai_veille,&
                    maxdayf, maxdayg, nit, amm, nrec, nger, nlev, codeinstal, poussracmoy, zrac, somtemprac, dtj, deltaz,       &
                    precrac, idzrac, efdensite_rac, dltams, repracmin, repracmax , kreprac, sioncoupe, debsenracf,    &
!                    debsenracg, P_propracfmax, P_zramif, lracz, cumlracz, cumflrac, flrac, cumlraczmaxi, racnoy, ndebsenracf,   &
                    debsenracg, P_propracfmax, lracz, cumlracz, cumflrac, flrac, cumlraczmaxi, racnoy, ndebsenracf,   &
                    ndebsenracg, ndecalf, ndecalg, nsencourpreracf, nsencourpreracg, rlf, rlg, drlf, drlg, lracsenzf, lracsenzg,&
                    lracsentotf, lracsentotg, rltotf, rltotg, rlf_veille, rlg_veille, dltarltotf, dltarltotg, dltasentotf,      &
                    dltasentotg, drlsenf, drlseng, somtempracvie, efda, efnrac_mean, humirac_mean, humirac_z, efnrac_z, rlj,    &
                    dltaremobil, lai, P_code_acti_reserve, inn, turfac, P_code_stress_root, P_humirac, hucc, P_codemortalracine,   &
                    masecnp, urac)
                    

  USE Divers, only: F_humirac, escalin
  USE Messages

  implicit none

  integer, intent(IN)    :: n
  integer,  intent(IN)   :: nbCouches      ! number of soil layers containing roots
  integer,  intent(IN)   :: nbCoum         ! maximum number of soil layers = nbCouchesSol_max
  real,    intent(IN)    :: dacouche(nbCouches)
  real,    intent(IN)    :: hur(nbCouches)
  real,    intent(IN)    :: humin(nbCouches)
  real,    intent(IN)    :: anox(nbCouches)
  integer, intent(IN)    :: nstoprac
  integer, intent(IN)    :: P_codazorac  ! // PARAMETER // activation of the nitrogen influence on root partitionning within the soil profile  // code 1/2 // PARPLT // 0
  real,    intent(IN)    :: P_minazorac  ! // PARAMETER // parameter of the effect of soil nitrogen on root soil partitioning // kg N ha-1 mm-1 // PARPLT // 1
  real,    intent(IN)    :: P_maxazorac  ! // PARAMETER // parameter of the effect of soil nitrogen on root soil partitioning  // kg N ha-1 mm-1 // PARPLT // 1
  real,    intent(IN)    :: P_minefnra  ! // PARAMETER // parameter of the effect of soil nitrogen on root soil partitioning // SD // PARPLT // 1
  integer, intent(IN)    :: P_codtrophrac  ! // PARAMETER // trophic effect on root partitioning within the soil // code 1/2/3 // PARPLT // 0
  real,    intent(IN)    :: P_coefracoupe  ! // PARAMETER // coefficient to define the proportion of dying roots after cut (grass) // SD // PARAMV6/PLT // 1
  real,    intent(IN)    :: P_daseuilbas  ! // PARAMETER // Threshold of bulk density of soil below that the root growth is not limited // g cm-3 // PARAM // 1
  real,    intent(IN)    :: P_daseuilhaut  ! // PARAMETER // Threshold of bulk density of soil below that the root growth  no more possible // g cm-3 // PARAM // 1
  real,    intent(IN)    :: P_dacohes  ! // PARAMETER // bulk density under which root growth is reduced due to a lack of cohesion d // g cm-3 // PARAM // 1
  real,    intent(IN)    :: P_contrdamax ! // PARAMETER // maximal root growth reduction due to soil strenghtness (high bulk density) // SD // PARPLT // 1
  real,    intent(IN)    :: P_sensrsec  ! // PARAMETER // root sensitivity to drought (1=insensitive) // SD // PARPLT // 1
  real,    intent(IN)    :: P_sensanox  ! // PARAMETER // anoxia sensitivity (0=insensitive) // SD // PARPLT // 1
  real,    intent(IN)    :: P_stlevamf  ! // PARAMETER // Sum of development units between the stages LEV and AMF // degree.days // PARPLT // 1
  real,    intent(IN)    :: P_stamflax  ! // PARAMETER // Sum of development units between the stages AMF and LAX // degree.days // PARPLT // 1
  real,    intent(IN)    :: P_lvfront   ! // PARAMETER // Root density at the root front // cm root.cm-3 soil // PARPLT // 1
  real,    intent(IN)    :: P_laicomp   ! // PARAMETER // LAI from which starts competition inbetween plants // m2 m-2 // PARPLT // 1
  real,    intent(IN)    :: P_adens     ! // PARAMETER // Interplant competition parameter // SD // PARPLT // 1
  real,    intent(IN)    :: P_bdens     ! // PARAMETER // minimal density from which interplant competition starts // plants m-2 // PARPLT // 1
  real,    intent(IN)    :: P_draclong  ! // PARAMETER // Maximum rate of root length production // cm root plant-1 degree.days-1 // PARPLT // 1
  real,    intent(IN)    :: P_vlaimax   ! // PARAMETER // ULAI  at inflection point of the function DELTAI=f(ULAI) // SD // PARPLT // 1
  real,    intent(IN)    :: P_longsperac ! // PARAMETER // specific root length // cm g-1 // PARPLT // 1
  integer, intent(IN)    :: P_codedyntalle  ! // PARAMETER // Activation of the module simulating tiller dynamic: yes (1), no (2) // code 1/2 // PARAMV6/PLT // 0
  real,    intent(IN)    :: P_lvopt      ! // PARAMETER // Optimum root density // cm root.cm-3 soil // PARAM // 1
  real,    intent(IN)    :: P_profsem    ! // PARAMETER // Sowing depth // cm // PARTEC // 1

  real,    intent(IN)    :: nit(nbCouches)
  real,    intent(IN)    :: amm(nbCouches)
  real,    intent(IN)    :: profsol
  integer, intent(IN)    :: nrec
  real,    intent(IN)    :: precrac(nbCouches)
  integer, intent(IN)    :: nger
  integer, intent(IN)    :: nlev
  logical, intent(IN)    :: sioncoupe
  integer, intent(IN)    :: codeinstal
  real,    intent(IN)    :: deltaz   ! // OUTPUT // Deepening of the root front  // cm d-1
  real,    intent(IN)    :: idzrac

  real,    intent(IN)    :: drlsenmortalle  ! // OUTPUT // Root biomass corresponding to dead tillers // t ha-1.j-1
  real,    intent(IN)    :: densite      ! // OUTPUT // Actual sowing density // plants.m-2
  real,    intent(IN)    :: P_msresiduel ! // PARAMETER // Residual dry matter after a cut // t ha-1 // PARTEC // 1
  real,    intent(IN)    :: lai_veille   ! lai(n-1)
  real,    intent(IN)    :: dltams       ! // OUTPUT // Growth rate of the plant // t ha-1.d-1
  real,    intent(IN)    :: repracmin
  real,    intent(IN)    :: repracmax
  real,    intent(IN)    :: kreprac
  real,    intent(INOUT) :: somtemprac
  real,    intent(INOUT) :: zrac   ! // OUTPUT // Depth reached by root system // cm
  real,    intent(INOUT) :: racnoy
  real,    intent(INOUT) :: flrac(nbCouches)
  real,    intent(INOUT) :: lracz(nbCouches)
  real,    intent(INOUT) :: efda      ! // OUTPUT // effect of soil compaction through bulk density // 0-1
  real,    intent(INOUT) :: efnrac_mean    ! // OUTPUT // effect of mineral nitrogen, which contributes to the root distribution in the layers with high mineral nitrogen content. // 0-1
  real,    intent(INOUT) :: humirac_mean ! // OUTPUT // soil dryness // 0-1
  real,    intent(INOUT) :: humirac_z(nbcouches)
  real,    intent(INOUT) :: efnrac_z(nbcouches)
  real,    intent(OUT)   :: rlj ! // OUTPUT // roots length growth rate  // m.d-1

  real,    intent(OUT)   :: efdensite_rac
  real,    intent(OUT)   :: poussracmoy   ! // OUTPUT // Effect of soil constraints on the rooting profile (option true density )" // 0-1
  real,    intent(OUT)   :: cumlracz   ! // OUTPUT // Sum of the effective root length // cm root.cm-2 soil
  real,    intent(OUT)   :: cumflrac
  real,    intent(OUT)   :: cumlraczmaxi

! Modifs Loic et Bruno fevrier 2014
  real,    intent(IN)    :: debsenracf               ! Lifespan of fine roots  (degreesC.days)
  real,    intent(IN)    :: debsenracg               ! Lifespan of coarse roots  (degreesC.days)
  real,    intent(IN)    :: P_propracfmax            !
! Lo�c Janvier 2021 : zramif n'est pas utilise je le supprime
!  real,    intent(IN)    :: P_zramif
  integer, intent(IN)    :: maxdayf
  integer, intent(IN)    :: maxdayg               !
  real,    intent(IN)    :: dtj(maxdayg)    ! // OUTPUT // Daily efficient temperature for the root growing  // degree C.j-1

  real,    intent(INOUT) :: rlf(nbCouches)           ! length of fine roots in layer iz on day n
  real,    intent(INOUT) :: rlg(nbCouches)           ! length of coarse roots in layer iz on day n
  real,    intent(INOUT) :: rlf_veille(nbCouches)    ! length of fine roots in layer iz on day n-1
  real,    intent(INOUT) :: rlg_veille(nbCouches)    ! length of coarse roots in layer iz on day n-1
  real,    intent(INOUT) :: drlsenf(nbCouches)       ! length of dead fine roots in layer iz on day n
  real,    intent(INOUT) :: drlseng(nbCouches)       ! length of dead coarse roots in layer iz on day n

! real,    intent(INOUT) :: drlf(maxdayf,nbCouches)  ! length of fine roots emitted in layer iz on day n
! real,    intent(INOUT) :: drlg(maxdayg,nbCouches)  ! length of coarse roots emitted in layer iz on day n
  real,    intent(INOUT) :: drlf(nbCoum,maxdayf)     ! length of fine roots emitted in layer iz on day n
  real,    intent(INOUT) :: drlg(nbCoum,maxdayg)     ! length of coarse roots emitted in layer iz on day n
  integer, intent(INOUT) :: ndebsenracf              ! first day when fine roots start to die
  integer, intent(INOUT) :: ndebsenracg              ! first day when coarse roots start to die
  integer, intent(INOUT) :: nsencourpreracf          ! last day at which fine root mortality is calculated
  integer, intent(INOUT) :: nsencourpreracg          ! last day at which coarse root mortality is calculated
  integer, intent(INOUT) :: ndecalf                  ! number of days remaining in the previous simulation without mortality of fine roots
  integer, intent(INOUT) :: ndecalg                  ! number of days remaining in the previous simulation without mortality of coarse roots
  real,    intent(INOUT) :: dltarltotf           ! length of fine roots emitted over the root profile on day n   cm.cm-2 sol
  real,    intent(INOUT) :: dltarltotg           ! length of coarse roots emitted over the root profile on day n  cm.cm-2 sol
  real,    intent(INOUT) :: dltasentotf          ! length of fine roots which die on day n over the root profile  cm.cm-2 sol
  real,    intent(INOUT) :: dltasentotg          ! length of coarse roots which die on day n over the root profile  cm.cm-2 sol
  real,    intent(INOUT) :: lracsenzf(nbCouches)     ! length of fine roots which die in layer iz on day n
  real,    intent(INOUT) :: lracsenzg(nbCouches)     ! length of coarse roots which die in layer iz on day n
  real,    intent(INOUT) :: somtempracvie        ! cumulative thermal time during successive runs (used to calculate initiation of root mortality)

  real,    intent(OUT)   :: lracsentotf  ! // OUTPUT // Total length of fine senescent roots // cm root.cm-2 soil
  real,    intent(OUT)   :: lracsentotg  ! // OUTPUT // Total length of coarse senescent roots // cm root.cm-2 soil
  real,    intent(OUT)   :: rltotf       ! // OUTPUT // Total length of fine roots // cm root.cm-2 soil
  real,    intent(OUT)   :: rltotg       ! // OUTPUT // Total length of coarse roots // cm root.cm-2 soil

  real,    intent(IN)    :: dltaremobil
  real,    intent(IN)    :: lai
  integer, intent(IN)    :: P_code_acti_reserve
  real,    intent(IN)    :: inn
  real,    intent(IN)    :: turfac
  integer, intent(IN)    :: P_code_stress_root
  integer, intent(IN)    :: P_humirac
  real,    intent(IN)    :: hucc(nbCouches)
  integer, intent(IN)    :: P_codemortalracine
  real,    intent(IN)    :: masecnp
  ! PL, 7/10/2020 
  real,    intent(OUT)   :: urac   ! // OUTPUT // daily relative development unit for root growth  // 1-3

end subroutine densirac


! ----------------------------------------------------------------------------------------------------------------------

subroutine densirac2(n, nbCouches, nbCoum, dacouche, hur, humin, anox, nstoprac, P_codazorac, P_minazorac, P_maxazorac,       &
                    P_minefnra, P_codtrophrac, P_coefracoupe, P_daseuilbas, P_daseuilhaut, P_dacohes, P_contrdamax, P_sensrsec,  &
                    P_sensanox, P_stlevamf, P_stamflax, P_lvfront, P_laicomp, P_adens, P_bdens, P_draclong, P_vlaimax,           &
                    P_longsperac, P_codedyntalle, drlsenmortalle, P_profsem, P_msresiduel, P_lvopt, profsol, densite, lai_veille,&
                    maxdayf, maxdayg, nit, amm, nrec, nger, nlev, codeinstal, poussracmoy, zrac, somtemprac, dtj, idzrac,        &
                    efdensite_rac, dltams, repracmin, repracmax , kreprac, sioncoupe, debsenracf, debsenracg,          &
!                    P_propracfmax, P_zramif, lracz, cumlracz, cumflrac, flrac, cumlraczmaxi, racnoy, ndebsenracf, ndebsenracg,   &
                    P_propracfmax, lracz, cumlracz, cumflrac, flrac, cumlraczmaxi, racnoy, ndebsenracf, ndebsenracg,   &
                    ndecalf, ndecalg, nsencourpreracf, nsencourpreracg, rlf, rlg, drlf, drlg, lracsenzf, lracsenzg, lracsentotf, &
                    lracsentotg, rltotf, rltotg, rlf_veille, rlg_veille, dltarltotf, dltarltotg, dltasentotf, dltasentotg,       &
                    drlsenf, drlseng, somtempracvie, efda, efnrac_mean, humirac_mean, humirac_z, efnrac_z, rlj, dltaremobil, lai,&
                    P_code_acti_reserve, inn, turfac, P_code_stress_root, P_codemonocot, P_kdisrac, P_alloperirac, P_pgrainmaxi, &
                    P_humirac, hucc, urac) 
                    

  USE Divers, only: F_humirac, escalin
  USE Messages

  implicit none

  integer, intent(IN)    :: n
  integer,  intent(IN)   :: nbCouches      ! number of soil layers containing roots
  integer,  intent(IN)   :: nbCoum         ! maximum number of soil layers = nbCouchesSol_max
  real,    intent(IN)    :: dacouche(nbCouches)
  real,    intent(IN)    :: hur(nbCouches)
  real,    intent(IN)    :: humin(nbCouches)
  real,    intent(IN)    :: anox(nbCouches)
  integer, intent(IN)    :: nstoprac
  integer, intent(IN)    :: P_codazorac  ! // PARAMETER // activation of the nitrogen influence on root partitioning within the soil profile  // code 1/2 // PARPLT // 0
  real,    intent(IN)    :: P_minazorac  ! // PARAMETER // parameter of the effect of soil nitrogen on root soil partitioning // kg N ha-1 mm-1 // PARPLT // 1
  real,    intent(IN)    :: P_maxazorac  ! // PARAMETER // parameter of the effect of soil nitrogen on root soil partitioning  // kg N ha-1 mm-1 // PARPLT // 1
  real,    intent(IN)    :: P_minefnra   ! // PARAMETER // parameter of the effect of soil nitrogen on root soil partitioning // SD // PARPLT // 1
  integer, intent(IN)    :: P_codtrophrac  ! // PARAMETER // trophic effect on root partitioning within the soil // code 1/2/3 // PARPLT // 0
  real,    intent(IN)    :: P_coefracoupe  ! // PARAMETER // coefficient to define the proportion of dying roots after cut (grass) // SD // PARAMV6/PLT // 1
  real,    intent(IN)    :: P_daseuilbas   ! // PARAMETER // Threshold of bulk density of soil below that the root growth is not limited // g cm-3 // PARAM // 1
  real,    intent(IN)    :: P_daseuilhaut  ! // PARAMETER // Threshold of bulk density of soil below that the root growth  no more possible // g cm-3 // PARAM // 1
  real,    intent(IN)    :: P_dacohes  ! // PARAMETER // bulk density under which root growth is reduced due to a lack of cohesion d // g cm-3 // PARAM // 1
  real,    intent(IN)    :: P_contrdamax ! // PARAMETER // maximal root growth reduction due to soil strenghtness (high bulk density) // SD // PARPLT // 1
  real,    intent(IN)    :: P_sensrsec  ! // PARAMETER // root sensitivity to drought (1=insensitive) // SD // PARPLT // 1
  real,    intent(IN)    :: P_sensanox  ! // PARAMETER // anoxia sensitivity (0=insensitive) // SD // PARPLT // 1
  real,    intent(IN)    :: P_stlevamf  ! // PARAMETER // Sum of development units between the stages LEV and AMF // degree.days // PARPLT // 1
  real,    intent(IN)    :: P_stamflax  ! // PARAMETER // Sum of development units between the stages AMF and LAX // degree.days // PARPLT // 1
  real,    intent(IN)    :: P_lvfront   ! // PARAMETER // Root density at the root front // cm root.cm-3 soil // PARPLT // 1
  real,    intent(IN)    :: P_laicomp   ! // PARAMETER // LAI from which starts competition inbetween plants // m2 m-2 // PARPLT // 1
  real,    intent(IN)    :: P_adens     ! // PARAMETER // Interplant competition parameter // SD // PARPLT // 1
  real,    intent(IN)    :: P_bdens     ! // PARAMETER // minimal density from which interplant competition starts // plants m-2 // PARPLT // 1
  real,    intent(IN)    :: P_draclong  ! // PARAMETER // Maximum rate of root length production // cm root plant-1 degree.days-1 // PARPLT // 1
  real,    intent(IN)    :: P_vlaimax   ! // PARAMETER // ULAI  at inflection point of the function DELTAI=f(ULAI) // SD // PARPLT // 1
  real,    intent(IN)    :: P_longsperac ! // PARAMETER // specific root length // cm g-1 // PARPLT // 1
  integer, intent(IN)    :: P_codedyntalle  ! // PARAMETER // Activation of the module simulating tiller dynamic: yes (1), no (2) // code 1/2 // PARAMV6/PLT // 0
  real,    intent(IN)    :: P_lvopt      ! // PARAMETER // Optimum root density // cm root.cm-3 soil // PARAM // 1
  real,    intent(IN)    :: P_profsem    ! // PARAMETER // Sowing depth // cm // PARTEC // 1

  real,    intent(IN)    :: nit(nbCouches)
  real,    intent(IN)    :: amm(nbCouches)
  real,    intent(IN)    :: profsol
  integer, intent(IN)    :: nrec
  integer, intent(IN)    :: nger
  integer, intent(IN)    :: nlev
  logical, intent(IN)    :: sioncoupe
  integer, intent(IN)    :: codeinstal
  real,    intent(IN)    :: idzrac

  real,    intent(IN)    :: drlsenmortalle  ! // OUTPUT // Root biomass corresponding to dead tillers // t ha-1.j-1
  real,    intent(IN)    :: densite      ! // OUTPUT // Actual sowing density // plants.m-2
  real,    intent(IN)    :: P_msresiduel ! // PARAMETER // Residual dry matter after a cut // t ha-1 // PARTEC // 1
  real,    intent(IN)    :: lai_veille                  ! lai(n-1)
  real,    intent(IN)    :: dltams       ! // OUTPUT // Growth rate of the plant // t ha-1.d-1
  real,    intent(IN)    :: repracmin
  real,    intent(IN)    :: repracmax
  real,    intent(IN)    :: kreprac
  real,    intent(INOUT) :: somtemprac
  real,    intent(INOUT) :: zrac   ! // OUTPUT // Depth reached by root system // cm
  real,    intent(INOUT) :: racnoy
  real,    intent(INOUT) :: flrac(nbCouches)
  real,    intent(INOUT) :: lracz(nbCouches)
  real,    intent(INOUT) :: efda      ! // OUTPUT // effect of soil compaction through bulk density // 0-1
  real,    intent(INOUT) :: efnrac_mean    ! // OUTPUT // effect of mineral nitrogen, which contributes to the root distribution in the layers with high mineral nitrogen content. // 0-1
  real,    intent(INOUT) :: humirac_mean ! // OUTPUT // soil dryness // 0-1
  real,    intent(INOUT) :: humirac_z(nbcouches)
  real,    intent(INOUT) :: efnrac_z(nbcouches)
  real,    intent(OUT)   :: rlj ! // OUTPUT // roots length growth rate  // m.d-1

  real,    intent(OUT)   :: efdensite_rac
  real,    intent(OUT)   :: poussracmoy   ! // OUTPUT // Effect of soil constraints on the rooting profile (option true density )" // 0-1
  real,    intent(OUT)   :: cumlracz      ! // OUTPUT // Sum of the effective root length // cm root.cm-2 soil
  real,    intent(OUT)   :: cumflrac
  real,    intent(OUT)   :: cumlraczmaxi

! Modifs Loic et Bruno fevrier 2014
  real,    intent(IN)    :: debsenracf               ! Lifespan of fine roots  (degreesC.days)
  real,    intent(IN)    :: debsenracg               ! Lifespan of coarse roots  (degreesC.days)
  real,    intent(IN)    :: P_propracfmax            !
! Lo�c Janvier 2021 : zramif n'est pas utilise je le supprime
!  real,    intent(IN)    :: P_zramif
  integer, intent(IN)    :: maxdayf
  integer, intent(IN)    :: maxdayg               !
  real,    intent(IN)    :: dtj(maxdayg)    ! // OUTPUT // Daily efficient temperature for the root growing  // degree C.j-1

  real,    intent(INOUT) :: rlf(nbCouches)           ! length of fine roots in layer iz on day n
  real,    intent(INOUT) :: rlg(nbCouches)           ! length of coarse roots in layer iz on day n
  real,    intent(INOUT) :: rlf_veille(nbCouches)    ! length of fine roots in layer iz on day n-1
  real,    intent(INOUT) :: rlg_veille(nbCouches)    ! length of coarse roots in layer iz on day n-1
  real,    intent(INOUT) :: drlsenf(nbCouches)       ! length of dead fine roots in layer iz on day n
  real,    intent(INOUT) :: drlseng(nbCouches)       ! length of dead coarse roots in layer iz on day n

! real,    intent(INOUT) :: drlf(maxdayf,nbCouches)  ! length of fine roots emitted in layer iz on day n
! real,    intent(INOUT) :: drlg(maxdayg,nbCouches)  ! length of coarse roots emitted in layer iz on day n
  real,    intent(INOUT) :: drlf(nbCoum,maxdayf)     ! length of fine roots emitted in layer iz on day n
  real,    intent(INOUT) :: drlg(nbCoum,maxdayg)     ! length of coarse roots emitted in layer iz on day n
  integer, intent(INOUT) :: ndebsenracf              ! first day when fine roots start to die
  integer, intent(INOUT) :: ndebsenracg              ! first day when coarse roots start to die
  integer, intent(INOUT) :: nsencourpreracf          ! last day at which fine root mortality is calculated
  integer, intent(INOUT) :: nsencourpreracg          ! last day at which coarse root mortality is calculated
  integer, intent(INOUT) :: ndecalf                  ! number of days remaining in the previous simulation without mortality of fine roots
  integer, intent(INOUT) :: ndecalg                  ! number of days remaining in the previous simulation without mortality of coarse roots
  real,    intent(INOUT) :: dltarltotf           ! length of fine roots emitted over the root profile on day n   cm.cm-2 sol
  real,    intent(INOUT) :: dltarltotg           ! length of coarse roots emitted over the root profile on day n  cm.cm-2 sol
  real,    intent(INOUT) :: dltasentotf          ! length of fine roots which die on day n over the root profile  cm.cm-2 sol
  real,    intent(INOUT) :: dltasentotg          ! length of coarse roots which die on day n over the root profile  cm.cm-2 sol
  real,    intent(INOUT) :: lracsenzf(nbCouches)     ! length of fine roots which die in layer iz on day n
  real,    intent(INOUT) :: lracsenzg(nbCouches)     ! length of coarse roots which die in layer iz on day n
  real,    intent(INOUT) :: somtempracvie        ! cumulative thermal time during successive runs (used to calculate initiation of root mortality)

  real,    intent(OUT)   :: lracsentotf  ! // OUTPUT // Total length of fine senescent roots // cm root.cm-2 soil
  real,    intent(OUT)   :: lracsentotg  ! // OUTPUT // Total length of coarse senescent roots // cm root.cm-2 soil
  real,    intent(OUT)   :: rltotf       ! // OUTPUT // Total length of fine roots // cm root.cm-2 soil
  real,    intent(OUT)   :: rltotg       ! // OUTPUT // Total length of coarse roots // cm root.cm-2 soil

  real,    intent(IN)    :: dltaremobil
  real,    intent(IN)    :: lai
  integer, intent(IN)    :: P_code_acti_reserve
  real,    intent(IN)    :: inn
  real,    intent(IN)    :: turfac
  integer, intent(IN)    :: P_code_stress_root

  ! Florent Juin 2018
  real,    intent(IN)    :: P_kdisrac
  real,    intent(IN)    :: P_alloperirac
  real,    intent(IN)    :: P_pgrainmaxi
  integer, intent(IN)    :: P_codemonocot
  integer, intent(IN)    :: P_humirac
  real,    intent(IN)    :: hucc(nbCouches)

  ! PL, 7/10/2020 
  real,    intent(OUT)   :: urac   ! // OUTPUT // daily relative development unit for root growth  // 1-3

  end subroutine densirac2

!
! ----------------------------------------------------------------------------------------------------------------------


subroutine repartir(n, nrec, P_codcueille, P_codeperenne, nlev, nlax, P_nbcueille, tustress, P_slamin, P_slamax,   &
                    P_codlainet, P_codemonocot, P_codesimul, dltaisen, P_envfruit, chargefruit, ndrp, &
                    ndebdes, P_sea, ntaille, P_codetaille, dltams, lai_veille, restemp, masecveg,  &
                    pdsfruittot, tursla, sla, mafeuilverte, mafeuil, mafeuilp, lai, deltai, maenfruit, eai,        &
                    mareserve, deltares, mabois, masec, msresjaune, mafeuiljaune, msneojaune,          &
                    matigestruc, pfeuil, pfeuilverte, pfeuiljaune, ptigestruc, penfruit, preserve, masecnp,        &
                    maperenne, QNveg, QNvegstruc, QNrestemp, matigestrucp, P_codelaitr, P_hautmax, P_hautbase,     &
                    P_khaut, hauteur, laisen, nstopres, dltamsen, dltaremobsen, remobilj, P_inilai ,P_adilmax,     &
                    QNplantenp, QNfeuille, QNperenne, P_code_acti_reserve, codeinstal, dltags, msresgel,           &
                    P_codefauche, ratioTF, fstressgel, P_restemp0, P_codeplante, numcult, P_codeinitprec, densite)

  USE Messages

  implicit none

  integer, intent(IN)    :: n
  integer, intent(IN)    :: nrec
  integer, intent(IN)    :: P_codcueille  ! // PARAMETER // way how to harvest // code 1/2 // PARTEC // 0
  integer, intent(IN)    :: P_codeperenne  ! // PARAMETER // option defining the annual (1) or perenial (2) character of the plant // code 1/2 // PARPLT // 0
  integer, intent(IN)    :: P_codefauche  ! // PARAMETER // option of cut modes for forage crops: yes (1), no (2) // code 1/2 // PARTEC // 0
  integer, intent(IN)    :: nlev
  integer, intent(IN)    :: nlax
  integer, intent(IN)    :: P_nbcueille  ! // PARAMETER // number of fruit harvestings // code 1/2 // PARTEC // 0
  real,    intent(INOUT) :: tustress   ! // OUTPUT // Stress index active on leaf growth (= minimum(turfac,innlai))  // 0-1
  real,    intent(IN)    :: P_slamin  ! // PARAMETER // minimal SLA of green leaves // cm2 g-1 // PARPLT // 1
  real,    intent(IN)    :: P_slamax  ! // PARAMETER // maximal SLA of green leaves // cm2 g-1 // PARPLT // 1
  integer, intent(IN)    :: P_codlainet  ! // PARAMETER //option of calculation of the LAI (1 : direct LAInet; 2 : LAInet = gross LAI - senescent LAI) // code 1/2 // PARPLT // 0
  integer, intent(IN)    :: P_codemonocot  ! // PARAMETER // option plant monocot(1) or dicot(2) // code 1/2 // PARPLT // 0
  character(len=12), intent(IN) :: P_codesimul  ! // PARAMETER // simulation code (culture ou feuille=lai force) // SD // P_USM/USMXML // 0
  real,    intent(INOUT) :: dltaisen   ! // OUTPUT // Daily increase of the senescent leaf index // m2.m-2 sol.j-1
  !real,    intent(IN)    :: P_tigefeuil  ! // PARAMETER // stem (structural part)/leaf proportion // SD // PARPLT // 1
  real,    intent(IN)    :: P_envfruit  ! // PARAMETER // proportion envelop/P_pgrainmaxi in weight  // SD // PARPLT // 1
  real,    intent(IN)    :: chargefruit   ! // OUTPUT // Amount of filling fruits per m-2 // nb fruits.m-2
  integer, intent(IN)    :: ndrp
  integer, intent(IN)    :: ndebdes
  real,    intent(IN)    :: P_sea  ! // PARAMETER // specifique surface of fruit envelops // cm2 g-1 // PARPLT // 1
  integer, intent(IN)    :: ntaille
  integer, intent(IN)    :: P_codetaille  ! // PARAMETER // option of pruning // code 1/2 // PARTEC // 0
  real,    intent(INOUT) :: dltams   ! // OUTPUT // Growth rate of the plant  // t ha-1.j-1
  real,    intent(IN)    :: lai_veille              ! n-1

  real,    intent(INOUT) :: restemp   ! // OUTPUT // C crop reserve, during the cropping season, or during the intercrop period (for perenial crops) // t ha-1
  real,    intent(INOUT) :: masecveg   ! // OUTPUT // Vegetative dry matter // t.ha-1
  real,    intent(INOUT) :: pdsfruittot
  real,    intent(INOUT) :: tursla
  real,    intent(INOUT) :: sla   ! // OUTPUT // Specific surface area // cm2 g-1
  real,    intent(INOUT) :: mafeuilverte   ! // OUTPUT // Dry matter of green leaves // t.ha-1
  real,    intent(INOUT) :: mafeuil   ! // OUTPUT // Dry matter of leaves // t.ha-1
  real,    intent(INOUT) :: mafeuilp
  real,    intent(INOUT) :: matigestruc   ! // OUTPUT // Dry matter of stems (only structural parts) // t.ha-1
  real,    intent(INOUT) :: lai             ! n    // OUTPUT // Leaf area index (table) // m2 leafs  m-2 soil
  real,    intent(INOUT) :: deltai   ! // OUTPUT // Daily increase of the green leaf index // m2 leafs.m-2 soil
  real,    intent(INOUT) :: maenfruit   ! // OUTPUT // Dry matter of harvested organ envelopes // t.ha-1
  real,    intent(INOUT) :: eai
  real,    intent(INOUT) :: mareserve
  real,    intent(INOUT) :: deltares
  real,    intent(INOUT) :: mabois   ! // OUTPUT // Prunning dry weight // t.ha-1
  real,    intent(INOUT) :: masec   ! // OUTPUT // Aboveground dry matter  // t.ha-1
  real,    intent(INOUT) :: msresjaune   ! // OUTPUT // Senescent residual dry matter  // t.ha-1
  real,    intent(INOUT) :: mafeuiljaune   ! // OUTPUT // Dry matter of yellow leaves // t.ha-1
  real,    intent(INOUT) :: msneojaune   ! // OUTPUT // Newly-formed senescent dry matter  // t.ha-1
  real,    intent(INOUT) :: pfeuil   ! // OUTPUT // Proportion of leaves in total biomass // 0-1
  real,    intent(INOUT) :: pfeuilverte   ! // OUTPUT // Proportion of green leaves in total non-senescent biomass // 0-1
  real,    intent(INOUT) :: pfeuiljaune   ! // OUTPUT // Proportion of yellow leaves in total biomass // 0-1
  real,    intent(INOUT) :: ptigestruc   ! // OUTPUT // Proportion of structural stems in total biomass // 0-1
  real,    intent(INOUT) :: penfruit   ! // OUTPUT // Proportion of fruit envelopes in total biomass // 0-1
  real,    intent(INOUT) :: preserve   ! // OUTPUT // Proportion of reserve in the total biomass // 0-1
  real,    intent(INOUT) :: masecnp    ! // OUTPUT // Biomass of non perennial organs // t ha-1
! Modifs Loic juin 2013
  real,    intent(INOUT) :: maperenne
  real,    intent(INOUT) :: QNveg
  real,    intent(INOUT) :: QNvegstruc
  real,    intent(INOUT) :: QNrestemp
! Ajout Loic Mai 2016
  real,    intent(INOUT) :: matigestrucp
  integer, intent(IN)    :: P_codelaitr
  real,    intent(IN)    :: P_hautmax
  real,    intent(IN)    :: P_hautbase
  real,    intent(IN)    :: P_Khaut
  real,    intent(INOUT) :: hauteur
  real,    intent(IN)    :: laisen
  integer, intent(IN)    :: nstopres
  real,    intent(IN)    :: dltamsen
  real,    intent(IN)    :: dltaremobsen
  real,    intent(IN)    :: remobilj
  real,    intent(IN)    :: P_inilai
  real,    intent(IN)    :: P_adilmax
  real,    intent(INOUT) :: QNplantenp
  real,    intent(INOUT) :: QNfeuille
  real,    intent(IN)    :: QNperenne
  integer, intent(IN)    :: P_code_acti_reserve
  integer, intent(IN)    :: codeinstal
  real,    intent(IN)    :: dltags
  real,    intent(IN)    :: msresgel
  real,    intent(IN)    :: ratioTF
  real,    intent(IN)    :: fstressgel
! DR 05/07/2019 je remets en coherence avce la vigne
  real,    intent(INOUT) :: P_restemp0  ! // PARAMETER // initial reserve biomass // t ha-1 // INIT // 1
  character(len=3),    intent(IN) :: P_codeplante  ! // PARAMETER // Name code of the plant in 3 letters // * // PARPLT // 0
  integer, intent(IN)    :: numcult
  integer, intent(IN)    :: P_codeinitprec
!! merge trunk 23/11/2020
  real,    intent(IN)    :: densite

end subroutine repartir



end interface
end module Module_Croissance
 
 
