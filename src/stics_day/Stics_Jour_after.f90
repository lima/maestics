
subroutine Stics_Jour_after(sc,pg,c,sta,soil,p,itk,t)

USE Stics
USE Plante
USE Itineraire_Technique
USE Parametres_Generaux
USE Climat
USE Station
USE Sol
USE Messages
USE SticsFiles

  implicit none

  type(Stics_Communs_),       intent(INOUT) :: sc  
  type(Parametres_Generaux_), intent(IN)    :: pg  
  type(Climat_),              intent(INOUT) :: c  
  type(Station_),             intent(INOUT) :: sta  
  type(Plante_),              intent(INOUT) :: p(sc%P_nbplantes)  
  type(ITK_),                 intent(INOUT) :: itk(sc%P_nbplantes)  
  type(Stics_Transit_),       intent(INOUT) :: t  
  type(Sol_),                 intent(INOUT) :: soil  

   integer :: i
   integer :: n

call EnvoyerMsgEcran('beginning stics_jour_after ')
     n = sc%n

    ! preparation des variables pour les ecritures. Par defaut, on utilise les valeurs de la plante principale

if ( write_debug ) then 
  write(ficdebug2,*) 'ns',n,p(1)%masec(0,n),p(1)%nbgrains(0),p(1)%magrain(0,n),p(1)%chargefruit
end if
     call Stics_Calcul_Sorties(sc,c,soil,p(1),itk(1),pg,sta)

if ( write_debug ) then 
  write(ficdebug2,*) 'ns',n,p(1)%masec(0,n),p(1)%nbgrains(0),p(1)%magrain(0,n),p(1)%chargefruit
end if

    ! preparations des variables plantes pour les ecritures.
      do i= 1, sc%P_nbplantes

          call stadeversbbch(sc%n,p(i))

if ( write_debug ) then 
  write(ficdebug2,*) 'np',n,p(i)%masec(0,n),p(i)%nbgrains(0),p(i)%magrain(0,n),p(i)%chargefruit
end if

          call Stics_Calcul_Sorties_Plante(sc,pg,c,p(i),itk(i),soil,t)
if ( write_debug ) then 
  write(ficdebug2,*) 'np',n,p(i)%masec(0,n),p(i)%nbgrains(0),p(i)%magrain(0,n),p(i)%chargefruit
end if

      end do

      do i= 1, sc%P_nbplantes
         if (p(1)%P_codelaitr /= 1) p(1)%lai(AOAS,n) = sc%tauxcouv(n)
! DR 23/09/2016 pour Efese prairie on garde 3 variables correspondants aux maxi atteint le jour de la coupe
          p(i)%lai_mx_av_cut = p(i)%lai(AOAS,sc%n)
          p(i)%masec_mx_av_cut = p(i)%masecnp(AOAS,sc%n)
          p(i)%QNplante_mx_av_cut = p(i)%QNplantenp(AOAS,sc%n)


    ! stockage des variables par profil
    if ( stics_files%flag_profil ) call Lecture_Profil(sc,p(i),soil,i)

    ! on enregistre certaines valeurs de la plante pour des jours particuliers
    ! ca permet de supprimer certains tableaux temporels qui stockent toute la simulation et de ne conserver que les
    ! valeurs utiles
          if (sc%n == p(i)%ntaille - 1) p(i)%QNgrain_ntailleveille = p(i)%QNgrain(AOAS)
      end do

call EnvoyerMsgEcran('end stics_jour_after ')

return
end subroutine Stics_Jour_after
 
 
