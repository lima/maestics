! Module lixiviation : Module des fonctions et routines liees au phenomene de lixiviation
!! - Description :
!! Dans ce module sont decrites toutes les interfaces des routines et fonctions attachees a la routine
!!   de simulation du phenomene de lixiviation.
!!   Ainsi, une routine externe appelant une (ou plusieurs) des unites de programme du module Lixivation peut inclure
!!   la ou les interfaces correspondantes. La verification et la validation des arguments necessaires en est alors
!!   grandement facilite.
!
module Lixivation

! - LES INTERFACES -
interface


subroutine lixiv(n, nhe, nh, ncel, P_codefente, P_codemacropor, P_codrainage, P_codhnappe, profcalc, P_infil, P_concseuil, &
                 P_humcapil, P_capiljour, P_profdrain, P_bformnappe, P_distdrain, P_rdrain, P_profimper, P_ksol, profsol,  &
                 ldrains, precip, hurlim, ruisselsurf, zrac, icel, P_epc, da, hr, hcc, hmin, izcel, izc, exces, hucc, humin, &
                 epz, esz, irrigprof, hur, sat, anox, nit, bouchon, pluieruissel, saturation, RU, RsurRU, concno3les, hnappe,&
                 hmax, hph, hpb, profnappe, qdrain, azlesd, azsup, DRAT, QLES, drain, ruissel, remontee, qlesd, qdraincum,   &
                 P_profmesW, SoilAvW, SoilWatM, lessiv, som_HUR, som_sat, P_obstarac, remonteemax, & ! ajout entrees et des sorties Macsur
              !   P_profmes, iz_base_horizon,
                 P_profmes,                                                     &
                 husup_by_horizon, azsup_by_horizon, husup_under_profmes, azsup_under_profmes, husup_by_cm)
!DR 01122020 merge trunk r2382
  implicit none

  integer, intent(IN)    :: n              ! simulation day
  integer, intent(IN)    :: nhe            ! number of elementary soil layers
  integer, intent(IN)    :: nh             ! number of macro soil layers
  integer, intent(IN)    :: ncel           ! number of mixing cells
  integer, intent(IN)    :: P_codefente    ! // PARAMETER // option allowing an additional water compartment for the swelling soils: yes (1), no (0) // code 0/1 // PARSOL // 0
  integer, intent(IN)    :: P_codemacropor ! // PARAMETER // Simulation option of water flux in the macroporosity of soils to estimate water excess and drip by  overflowing : yes(1), no (0) // code 1/2 // PARSOL // 0
  integer, intent(IN)    :: P_codrainage   ! // PARAMETER // artificial drainage (yes or no) // code 0/1 // PARSOL // 0
  integer, intent(IN)    :: P_codhnappe    ! // PARAMETER // mode of calculation of watertable level // code 1/2 // PARAM // 0
  integer, intent(IN)    :: profcalc
  real,    intent(IN)    :: P_infil(0:nh)  ! // PARAMETER // infiltrability parameter at the base of each horizon (if codemacropor = 1)// mm d-1
  real,    intent(IN)    :: P_concseuil    ! // PARAMETER // Minimum concentration of NH4-N in soil // kg ha-1 mm-1 // PARSOL // 1
  real,    intent(IN)    :: P_humcapil     ! // PARAMETER // maximal water content to activate capillary rise // % weight // PARSOL // 1
  real,    intent(IN)    :: P_capiljour    ! // PARAMETER // capillary rise upward water flux // mm d-1 // PARSOL // 1
  real,    intent(IN)    :: P_profdrain    ! // PARAMETER // drain depth // cm // PARSOL // 1
  real,    intent(IN)    :: P_bformnappe   ! // PARAMETER // coefficient of water table shape (artificial drained soil) // SD // PARAM // 1
  real,    intent(IN)    :: P_distdrain    ! // PARAMETER // distance to the drain to calculate watertable height // cm // PARAM // 1
  real,    intent(IN)    :: P_rdrain       ! // PARAMETER // drain radius // cm // PARAM // 1
  real,    intent(IN)    :: P_profimper    ! // PARAMETER // Upper depth of the impermeable layer (from the soil surface). May be greater than the soil depth // cm // PARSOL // 1
  real,    intent(IN)    :: P_ksol         ! // PARAMETER // hydraulic conductivity in the soil above and below the drains // SD // PARSOL // 1
  real,    intent(IN)    :: profsol
  real,    intent(IN)    :: ldrains
  real,    intent(IN)    :: precip         ! // INPUT // Daily amount of water (precipitation + irrigation)   // mm d-1
  real,    intent(IN)    :: hurlim
  real,    intent(IN)    :: ruisselsurf    ! // INPUT // Surface run-off // mm d-1
  real,    intent(IN)    :: zrac           ! // INPUT // Depth reached by root system // cm
  integer, intent(IN)    :: icel(0:ncel)
  real,    intent(IN)    :: P_epc(nh)      ! // PARAMETER // thickness of macro layer i // cm   // PARSOL // 1      // OUTPUT // Thickness of the horizon (1 or 2 )// cm
  real,    intent(IN)    :: da(nh)         ! // OUTPUT // Bulk density of macro layer i   // g cm-3
  real,    intent(IN)    :: hr(nh)         ! // OUTPUT // Water content of macro layer i  // % weight
  real,    intent(IN)    :: hcc(nh)        ! water content at field capacity of macro layer i  // % weight
  real,    intent(IN)    :: hmin(nh)       ! water content at permanent wilting point of macro layer i // % weight
  integer, intent(IN)    :: izcel(nh)      !
  integer, intent(IN)    :: izc(nh)        !
  real,    intent(OUT)   :: exces(0:nh)    ! // OUTPUT // Amount of water present in the macroporosity of the layer i  // mm
  real,    intent(IN)    :: hucc(nhe)      ! water content at field capacity of layer i // mm
  real,    intent(IN)    :: humin(nhe)     ! water content at permanent wilting point of layer i // mm
  real,    intent(IN)    :: epz(nhe)  
  real,    intent(IN)    :: esz(nhe)  
  real,    intent(IN)    :: irrigprof(nhe)
  real,    intent(INOUT) :: hur(nhe)
  real,    intent(OUT)   :: sat(nhe)
  real,    intent(OUT)   :: anox(nhe)
  real,    intent(INOUT) :: nit(nhe)

  integer, intent(INOUT) :: bouchon     ! // OUTPUT // Index showing if the shrinkage slots are opened (0) or closed (1)  // 0-1

! variables calculees et utilisees par lixiv et ses sous-fonctions et globalisees (pour les fonctions de sortie lecrap, lecsorti)
  real,    intent(OUT)   :: pluieruissel  
  real,    intent(OUT)   :: saturation  ! // OUTPUT // Amount of water remaining in the soil macroporosity // mm
  real,    intent(OUT)   :: RU          ! // OUTPUT // maximum available water reserve over the entire profile // mm
  real,    intent(OUT)   :: RsurRU      ! // OUTPUT // Fraction of available water reserve (R/RU) over the entire profile // 0 to 1
  real,    intent(OUT)   :: concno3les  ! // OUTPUT // Nitrate concentration in drained water // mg NO3 l-1
  real,    intent(OUT)   :: hnappe      ! // OUTPUT // Height of water table with active effects on the plant // cm
  real,    intent(OUT)   :: hmax        ! // OUTPUT // Maximum height of water table between drains // cm
  real,    intent(OUT)   :: hph         ! // OUTPUT // Maximum depth of perched water table // cm
  real,    intent(OUT)   :: hpb         ! // OUTPUT // Minimum depth of perched water table // cm
  real,    intent(OUT)   :: profnappe   ! // OUTPUT // Depth of water table // cm
  real,    intent(OUT)   :: qdrain      ! // OUTPUT // Flow rate towards drains // mm d-1
  real,    intent(OUT)   :: azlesd      ! // OUTPUT // Daily NO3-N leached into drains // kg.ha-1 d-1
  real,    intent(OUT)   :: azsup
  real,    intent(INOUT) :: DRAT        ! // OUTPUT // Water flux drained at the base of the soil profile integrated over the simulation period out of the soil   // mm d-1
  real,    intent(INOUT) :: QLES        ! // OUTPUT // Cumulative NO3-N leached at the base of the soil profile // kg.ha-1
  real,    intent(OUT)   :: drain       ! // OUTPUT // Water flux drained at the base of the soil profile // mm d-1
  real,    intent(OUT)   :: ruissel     ! // OUTPUT // Daily run-off // mm d-1
  real,    intent(OUT)   :: remontee    ! // OUTPUT // Capillary uptake in the base of the soil profile // mm d-1
  real,    intent(OUT)   :: qlesd       ! // OUTPUT // Cumulative NO3-N leached into drains // kg.ha-1
  real,    intent(INOUT) :: qdraincum   ! // OUTPUT // Cumulative quantity of water evacuated towards drains // mm

! dr 16/12/2013 ajout pour Macsur
  integer, intent(IN)    :: P_profmesW
  real,    intent(INOUT) :: SoilAvW     ! // OUTPUT // Amount of soil available water in the measurement depth // mm
  real,    intent(INOUT) :: SoilWatM    ! // OUTPUT // Amount of soil available water in the observed measurement (macsur) // mm
  real,    intent(INOUT) :: lessiv      ! // OUTPUT // daily NO3-N leached at the base of the soil profile // kg.ha-1
  real,    intent(INOUT) :: som_HUR
  real,    intent(INOUT) :: som_sat
  real,    intent(IN)    :: P_obstarac
  real,    intent(INOUT) :: remonteemax ! // OUTPUT // capillary rise from simulated deep soil layer (mm)

!DR 01122020 merge trunk r2382
! DR 29/05/2019 i add some variables to have the dranaige and the lixiviation under each soil layer and under the profmes value
  real,    intent(IN)    :: P_profmes
! integer, intent(IN)    :: iz_base_horizon(1:5)
  real,    intent(OUT)   :: husup_by_horizon(5)
  real,    intent(OUT)   :: azsup_by_horizon(5)
  real,    intent(OUT)   :: husup_under_profmes
  real,    intent(OUT)   :: azsup_under_profmes

! DR 01122020 merge du trunk
! DR 25/06/2019 pour le module chloride il faut sortir les veluers de drainage de chaque couche de 1 cm
  real,    intent(OUT)   :: husup_by_cm(nhe)

end subroutine lixiv


!------------------------------------------------------------------------------------------------------------------------------------------------------------* c

subroutine TransfertsDescendants(nh, nhe, ncel, P_concseuil, P_humcapil, P_capiljour, precip, hurlim,  &
                                 icel, P_infil, P_epc, da, hr, izc, izcel, etn, hucc, hur, nit, exces, azlesm, &
                                 !azsup, husup, remontee, remonteemax, P_profmes, iz_base_horizon,
                                 azsup, husup, remontee, P_profmes,                               &
                                 husup_by_horizon, azsup_by_horizon, husup_under_profmes, azsup_under_profmes, husup_by_cm)
  implicit none

  integer, intent(IN)    :: nh
  integer, intent(IN)    :: nhe
  integer, intent(IN)    :: ncel
  real,    intent(IN)    :: P_concseuil  ! // PARAMETER // Minimum Concentration of soil to NH4 // kgN ha-1 mm-1 // PARSOL // 1
  real,    intent(IN)    :: P_humcapil   ! // PARAMETER // maximal water content to activate capillary rise // % ponderal // PARSOL // 1
  real,    intent(IN)    :: P_capiljour  ! // PARAMETER // capillary rise upward water flux // mm j-1 // PARSOL // 1
  !real,    intent(IN)    :: P_obstarac
  real,    intent(IN)    :: precip       ! // OUTPUT // Daily amount of water (precipitation + irrigation)   // mm d-1
  real,    intent(IN)    :: hurlim
  !real,    intent(IN)    :: zrac
  integer, intent(IN)    :: icel(0:ncel)

  real,    intent(IN)    :: P_infil(0:nh)! // PARAMETER // infiltrability parameter at the base of each horizon (if codemacropor=1)// mm day-1 // PARSOL // 1   // OUTPUT // Infiltrability parameter at the base of the horizon 1 // mm day-1
  real,    intent(IN)    :: P_epc(nh)    ! // PARAMETER // thickness of each soil layer // cm   // PARSOL // 1      // OUTPUT // Thickness of the horizon (1 or 2 )// cm
  real,    intent(IN)    :: da(nh)       ! (nh)    // OUTPUT // Bulk density in the different soil layers // g cm-3
  real,    intent(IN)    :: hr(nh)       ! (nh)    // OUTPUT // Water content of each soil layer (table)    // % pond.
  integer, intent(IN)    :: izc(nh)       !
  integer, intent(IN)    :: izcel(nh)    !
  real,    intent(IN)    :: etn(nhe)     ! 1 to icel(ncel), 1 to nhe. Il faut prendre le max, a priori nhe >= icel(ncel)
  real,    intent(IN)    :: hucc(nhe)    ! 1 to icel(ncel), 1 to nhe. Il faut prendre le max, a priori nhe >= icel(ncel)

  real,    intent(INOUT) :: hur(nhe)     ! 1 to icel(ncel), 1 to nhe. Il faut prendre le max, a priori nhe >= icel(ncel)
  real,    intent(INOUT) :: nit(nhe)     ! 1 to icel(ncel), 1 to nhe. Il faut prendre le max, a priori nhe >= icel(ncel)
  real,    intent(INOUT) :: exces(0:nh) ! // OUTPUT // Amount of water present in the macroporosity of the layer i  // mm
  real,    intent(OUT)   :: azlesm  
  real,    intent(OUT)   :: azsup
  real,    intent(OUT)   :: husup
  real,    intent(OUT)   :: remontee    ! // OUTPUT // Capillary uptake in the base of the soil profile // mm d-1
  !real,    intent(INOUT) :: remonteemax ! // INOUT // capillary rise from simulated deep soil layer (mm)

! DR 01122020 merge trunk R2382
  ! DR 29/05/2019 i add some variables to have the dranaige and the lixiviation under each soil layer and under the profmes value
  real,    intent(IN)    :: P_profmes
!  integer, intent(IN)    :: iz_base_horizon(1:5)
  real,    intent(OUT)   :: husup_by_horizon(1:5)
  real,    intent(OUT)   :: azsup_by_horizon(1:5)
  real,    intent(OUT)   :: husup_under_profmes
  real,    intent(OUT)   :: azsup_under_profmes

 ! DR 25/06/2019 pour le module chloride il faut sortir les valeurs de drainage de chaque couche de 1 cm
  real,    intent(OUT)   :: husup_by_cm(nhe)


end subroutine TransfertsDescendants


subroutine nappe(nh, nhe, macropor, P_epc, hnappe, hnapperch, hmax, P_profdrain, profsol, P_bformnappe,  &
                 exces, hph, hpb, de, P_distdrain, profnappe, P_codhnappe, ldrains)

!call nappe(nh, nhe, macropor(1:nh), P_epc(1:nh), hnappe, hnapperch, hmax, P_profdrain, profsol, P_bformnappe, &
!                 exces(0:nh), hph, hpb, de, P_distdrain, profnappe, P_codhnappe, ldrains)

  integer, intent(IN)   ::  nh  !
  integer, intent(IN)   ::  nhe  !
  real,    intent(IN)   ::  macropor(nh)
  real,    intent(IN)   ::  P_epc(nh)    ! // PARAMETER // thickness of soil layer i // cm
  real,    intent(IN)   ::  exces(0:nh)  ! // OUTPUT // Amount of water  present in the macroporosity of the layer i  // mm
  real,    intent(IN)   ::  P_profdrain  !  // PARAMETER // drain depth // cm // PARSOL // 1
  real,    intent(IN)   ::  profsol
  real,    intent(IN)   ::  P_bformnappe !  // PARAMETER // coefficient of water table shape (artificial drained soil) // SD // PARAM // 1
  real,    intent(IN)   ::  de  !
  real,    intent(IN)   ::  P_distdrain  !  // PARAMETER // distance to the drain to calculate watertable height // cm // PARAM // 1
  integer, intent(IN)   ::  P_codhnappe  !  // PARAMETER // mode of calculation of watertable level // code 1/2 // PARAM // 0
  real,    intent(IN)   ::  ldrains

!! arguments sortants
  real,    intent(OUT)  ::  Hmax        !    // OUTPUT // Maximum height of water table between drains // cm
  real,    intent(OUT)  ::  Hnappe      !    // OUTPUT // Height of water table with active effects on the plant // cm
  real,    intent(OUT)  ::  Hnapperch   !
  real,    intent(OUT)  ::  Hpb         !    // OUTPUT // Minimum depth of perched water table // cm
  real,    intent(OUT)  ::  Hph         !    // OUTPUT // Maximum depth of perched water table // cm
  real,    intent(OUT)  ::  profnappe   ! // OUTPUT // Depth of water table // cm

end subroutine nappe

!------------------------------------------------------------------------------------------------------------------------------------------------------------* c
subroutine excesEau(nh, nhe, P_codemacropor, P_codefente, pluiefente, zrac, macropor, P_epc, hur, hucc, bouchon, exces)

  integer, intent(IN)    :: nh
  integer, intent(IN)    :: nhe
  integer, intent(IN)    :: P_codemacropor  ! // PARAMETER // "Simulation option of water flux in the macroporosity of soils to estimate water excess and drip by  overflowing : yes(1), no (0)" // code 1/2 // PARSOL // 0
  integer, intent(IN)    :: P_codefente  ! // PARAMETER // option allowing an additional water compartment for the swelling soils: yes (1), no (0) // code 0/1 // PARSOL // 0
  real,    intent(IN)    :: pluiefente
  real,    intent(IN)    :: zrac   ! // OUTPUT // Depth reached by root system // cm
  real,    intent(IN)    :: P_epc(nh)  ! // PARAMETER // thickness of each soil layer // cm   // PARSOL // 1      // OUTPUT // Thickness of the horizon (1 or 2 )// cm
  real,    intent(IN)    :: hucc(nhe)
  real,    intent(IN)    :: macropor(nh)

  integer, intent(OUT)   :: bouchon   ! // OUTPUT // Index showing if the shrinkage slots are opened (0) or closed (1)  // 0-1
  real,    intent(OUT)   :: hur(nhe)
  real,    intent(OUT)   :: exces(0:nh)   ! // OUTPUT // Amount of water  present in the macroporosity of the horizon 1  // mm

  end subroutine excesEau


!------------------------------------------------------------------------------------------------------------------------------------------------------------* c
subroutine drainageAgricole(n, nh, nhe, P_infil, hur, macropor, ldrains, P_profdrain, profsol, P_ksol, P_concseuil, de, &
                            exces, nit, hmax, qdrain, azlesd, qlesd, hnappe)

  integer, intent(IN)    ::  n
  integer, intent(IN)    ::  nh  
  integer, intent(IN)    ::  nhe
  real,    intent(IN)    ::  P_infil(0:nh)  ! // PARAMETER // infiltrability parameter at the base of each horizon (if codemacropor = 1)// mm day-1 // PARSOL // 1   // OUTPUT // Infiltrability parameter at the base of the horizon 1 // mm day-1
! real,    intent(IN)    ::  hur(int(profsol))
  real,    intent(IN)    ::  hur(nhe)
  real,    intent(IN)    ::  macropor(nh)  
  real,    intent(IN)    ::  ldrains  
  real,    intent(IN)    ::  P_profdrain  ! // PARAMETER // drain depth // cm // PARSOL // 1 
  real,    intent(IN)    ::  profsol  
  real,    intent(IN)    ::  P_ksol  ! // PARAMETER // hydraulic conductivity in the soil above and below the drains // SD // PARSOL // 1 
  real,    intent(IN)    ::  P_concseuil  ! // PARAMETER // Minimum Concentration of soil to NH4 // kgN ha-1 mm-1 // PARSOL // 1 
  real,    intent(IN)    ::  de  
  real,    intent(INOUT) ::  exces(0:nh)   ! // OUTPUT // Amount of water  present in the macroporosity of the horizon 1  // mm
! real,    intent(INOUT) ::  nit(int(profsol))
  real,    intent(INOUT) ::  nit(nhe)
  real,    intent(INOUT) ::  hmax   ! // OUTPUT // Maximum height of water table between drains // cm
  real,    intent(INOUT) ::  qdrain   ! // OUTPUT // Flow rate towards drains // mm j-1
  real,    intent(INOUT) ::  azlesd   ! // OUTPUT // Nitric nitrogen flow in drains // kgN.ha-1 j-1
  real,    intent(INOUT) ::  qlesd   ! // OUTPUT // Cumulated N-NO3 leached into drains // kgN.ha-1
  real,    intent(INOUT) ::  hnappe   ! // OUTPUT // Height of water table with active effects on the plant // cm

end subroutine drainageAgricole

! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!


subroutine transf(nh, nhe, hucc, hur, nit, etn, izc, hurlim, exces, P_infil, P_concseuil, precip, husup, azsup, remontee, &
                  P_capiljour, P_humcapil, hrtest, da, P_epc, P_profmes,  &
                  husup_by_horizon, azsup_by_horizon, husup_under_profmes, azsup_under_profmes, husup_by_cm)

    integer, intent(IN)    :: nhe
    integer, intent(IN)    :: nh
    real,    intent(IN)    :: hucc(nhe)
    real,    intent(INOUT) :: hur(nhe)
    real,    intent(OUT)   :: nit(nhe)
    real,    intent(IN)    :: etn(nhe)
    integer, intent(IN)    :: izc(nh)
    real,    intent(IN)    :: hurlim
    real,    intent(INOUT) :: exces(0:nh)   ! // OUTPUT // Amount of water  present in the macroporosity of the horizon 1  // mm

    real,    intent(IN)    :: P_infil(0:nh) ! // PARAMETER // infiltrability parameter at the base of each horizon (if codemacropor=1)// mm day-1 // PARSOL // 1   // OUTPUT // Infiltrability parameter at the base of the horizon 1 // mm day-1
    real,    intent(IN)    :: P_concseuil   ! // PARAMETER // Minimum Concentration of soil to NH4 // kgN ha-1 mm-1 // PARSOL // 1
    real,    intent(IN)    :: precip        ! // OUTPUT // Daily amount of water (precipitation + irrigation)   // mm d-1
    real,    intent(OUT)   :: husup
    real,    intent(OUT)   :: azsup
    real,    intent(OUT)   :: remontee      ! // OUTPUT // Capillary uptake in the base of the soil profile // mm j-1
    real,    intent(IN)    :: P_capiljour   ! // PARAMETER // capillary rise upward water flux // mm j-1 // PARSOL // 1
    real,    intent(IN)    :: P_humcapil    ! // PARAMETER // maximal water content to activate capillary rise // % ponderal // PARSOL // 1
    real,    intent(IN)    :: hrtest        ! // INPUT // Water content by horizon (table)    // % pond.
    real,    intent(IN)    :: da(nh)
    real,    intent(IN)    :: P_epc(nh)     ! // PARAMETER // thickness of each soil layer // cm   // PARSOL // 1      // OUTPUT // Thickness of the horizon (1 or 2 )// cm
    !real,    intent(IN)    :: zrac
    !real,    intent(IN)    :: P_obstarac
    !real,    intent(INOUT) :: remonteemax   ! // OUTPUT // capillary rise from simulated deep soil layer (mm)
!DR 01122020 merge trunk r2382
! DR 29/05/2019 i add some variables to have the dranaige and the lixiviation under each soil layer and under the profmes value
  real,    intent(IN)    :: P_profmes
!  integer, intent(IN)    :: iz_base_horizon(1:5)
  real,    intent(OUT)   :: husup_by_horizon(1:5)
  real,    intent(OUT)   :: azsup_by_horizon(1:5)
  real,    intent(OUT)   :: husup_under_profmes
  real,    intent(OUT)   :: azsup_under_profmes
    ! DR 25/06/2019 pour le module chloride il faut sortir les valeurs de drainage de chaque couche de 1 cm
  real,    intent(OUT)   :: husup_by_cm(nhe)

  end subroutine transf

! -----------------------------------------------------------------------------------------------------------------------------
!
  function sommeCouchesParCellule(nbCellules, iCellules, tabCouches)
    integer, intent(in) :: nbCellules  
    integer, intent(in) :: iCellules(0:nbCellules)  
    real,    intent(in) :: tabCouches(iCellules(nbCellules))  
    real, dimension(nbCellules) :: sommeCouchesParCellule  
  end function sommeCouchesParCellule

!------------------------------------------------------------------------------------------------------------------------------------------------------------* c
  real function calculRsurRU(RU, nhe, hur, sat, humin)

  real, intent(IN)  :: RU   ! // OUTPUT // maximum available water reserve over the entire profile // mm
!  real, intent(IN)  :: profsol
  integer, intent(IN) :: nhe
  real, intent(IN)  :: hur(nhe)  ! hur(int(profsol))
  real, intent(IN)  :: humin(nhe)! humin(int(profsol)
  real, intent(IN)  :: sat(nhe)  ! sat(int(profsol))

  end function calculRsurRU

!------------------------------------------------------------------------------------------------------------------------------------------------------------* c
  real function calculRU(nhe, hucc, humin)

  implicit none

! real, intent(IN)  :: profsol
  integer, intent(IN) :: nhe
  real, intent(IN)  :: hucc(nhe)  ! hucc(int(profsol))
  real, intent(IN)  :: humin(nhe) ! humin(int(profsol))

  end function calculRU


  real function concNO3_EauxDrainage(husup, QLES, DRAT)
    real, intent(IN)  :: husup  !  
    real, intent(IN)  ::  QLES  !    // OUTPUT // Cumulated N-NO3 leached at the base of the soil profile // kgN.ha-1
    real, intent(IN)  ::  DRAT   ! // OUTPUT // Water flux drained at the base of the soil profile integrated over the simulation periodout of the soil   // mm

  end function concNO3_EauxDrainage

end interface

end module Lixivation
