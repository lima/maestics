
module Module_Apports
    implicit none

interface

subroutine apports(sc,pg,t,c,soil,p,itk,sta)
USE Stics
USE Plante
USE Itineraire_Technique
USE Sol
USE Climat
USE Parametres_Generaux
USE Station

  implicit none

  type(Stics_Communs_),       intent(INOUT) :: sc
  type(Parametres_Generaux_), intent(IN)    :: pg
  type(Plante_),              intent(INOUT) :: p(sc%P_nbplantes)    ! Toutes les plantes du systeme, pour somme des LAI et des ABSO
  type(ITK_),                 intent(INOUT) :: itk(sc%P_nbplantes)
  type(Sol_),                 intent(INOUT) :: soil
  type(Climat_),              intent(IN)    :: c
  type(Stics_Transit_),       intent(INOUT) :: t
  type(Station_),             intent(INOUT) :: sta

end subroutine Apports

! -------------------------------------------------------------------------------------------------------------
subroutine apportsNparEngraisMineraux(P_orgeng, P_voleng, P_deneng, anit, n, Vabso5, P_Vabs2, P_codlocferti, P_locferti, &
                                      P_pHminvol, P_pHmaxvol, P_pH, P_Xorgmax, P_codedenit, P_engamm, Nvoleng, Ndenit,   &
                                      Norgeng, QNvoleng, QNorgeng, QNdenit, Nhum, Nhuma, amm, nit, totapN)
  implicit none

  real,    intent(IN)    :: P_orgeng      !  maximal quantity of mineral fertilizer that can be organized in the soil (fraction for type 8) // kg.ha-1
  real,    intent(IN)    :: P_voleng      !  maximal fraction of mineral fertilizer that can be volatilized
  real,    intent(IN)    :: P_deneng      !  proportion of the mineral fertilizer that can be denitrified (useful if codenit not active)
  real,    intent(IN)    :: anit          !  Amount of fertilizer added on day n  // kg.ha-1.d-1
  integer, intent(IN)    :: n
  real,    intent(IN)    :: Vabso5(5)     ! Maximal N uptake during the last 5 days
  real,    intent(IN)    :: P_Vabs2       ! N uptake rate for which fertilizer losses are divided by 2 // kg.ha-1.d-1
  integer, intent(IN)    :: P_codlocferti ! code of fertilisation localisation:  1: at soil surface, 2 = in the soil // code 1/2 // PARTEC // 0
  integer, intent(IN)    :: P_locferti    ! Depth of nitrogen injection (when fertiliser is applied at depth) // cm
  real,    intent(IN)    :: P_pHminvol    ! pH above which the fertilizer volatilisation is nil
  real,    intent(IN)    :: P_pHmaxvol    ! pH beyond which the fertilizer volatilisation is maximum
  real,    intent(IN)    :: P_pH          ! pH of mixing soil + organic amendments
  real,    intent(IN)    :: P_Xorgmax     ! maximal amount of immobilised N coming from the mineral fertilizer  // kg.ha-1
  integer, intent(IN)    :: P_codedenit   ! option to allow the calculation of denitrification :yes (1), no(2) // code 1/2
  real,    intent(IN)    :: P_engamm      ! proportion of ammonium in the fertilizer

  real,    intent(INOUT) :: Nvoleng      ! Daily volatilisation of NH3-N from fertiliser // kg.ha-1.d-1
  real,    intent(INOUT) :: Ndenit       ! Daily denitrification of N from fertiliser or soil (if option  denitrification  is activated) // kg.ha-1.d-1
  real,    intent(INOUT) :: Norgeng      ! Daily organisation of N from fertiliser // kg.ha-1.d-1
  real,    intent(INOUT) :: QNvoleng     ! Cumulative volatilisation of NH3-N  from fertiliser // kg.ha-1
  real,    intent(INOUT) :: QNorgeng     ! Cumulative organisation of N from fertiliser // kg.ha-1
  real,    intent(INOUT) :: QNdenit      ! Cumulative denitrification of N from fertiliser or soil (if option  denitrification  is activated) // kg.ha-1
  real,    intent(INOUT) :: Nhum(1:10)   ! N immobilised from fertilizers by soil microbial biomass in the 10 first cm  // kg.ha-1
  real,    intent(INOUT) :: Nhuma        ! Amount of active N in the humus pool  // kg.ha-1
  real,    intent(INOUT) :: amm(1:max(1,P_locferti))  ! soil ammonium N  // kg.ha-1
  real,    intent(INOUT) :: nit(1:max(1,P_locferti))  ! soil nitrate N   // kg.ha-1
  real,    intent(INOUT) :: totapN       ! Cumulative amount of mineral N inputs coming from fertilizer and organic residues // kg.ha-1

end subroutine apportsNparEngraisMineraux

! -------------------------------------------------------------------------------------------------------------


subroutine ResidusApportSurfaceSol(qresidu, Crespc, eauresidu, CNresmin, CNresmax, Qmulchdec, CNresid, &
                                    Cnondec, Nnondec, Cres1, Nres1, QCapp, QNapp, QCresorg, QNresorg, CroCo, &
                                    Chum1, Nhum1, Chumi, Nhumi, CsurNsol, Chuma, Nhuma)
  implicit none

  real,    intent(IN)    :: qresidu    ! Fresh matter (FM) added from residue ires  t.ha-1
  real,    intent(IN)    :: Crespc     ! C content of residue ires     // %FM
  real,    intent(IN)    :: eauresidu  ! Water content of residue ires // %FM
  real,    intent(IN)    :: CNresmin   ! minimum observed value of ratio C/N of residue ires // g g-1
  real,    intent(IN)    :: CNresmax   ! maximum observed value of ratio C/N of residue ires // g g-1
  real,    intent(IN)    :: Qmulchdec  ! maximal amount of decomposing mulch of residue ires // t.ha-1
  real,    intent(INOUT) :: CNresid    ! C:N ratio of the added residue
  real,    intent(INOUT) :: Cnondec    ! undecomposable C stock of the residue ires at soil surface //  t.ha-1
  real,    intent(INOUT) :: Nnondec    ! undecomposable N stock of the residue ires at soil surface // kg.ha-1
  real,    intent(INOUT) :: Cres1      ! Amount of C in pool Cres at depth iz=1 and residue ires
  real,    intent(INOUT) :: Nres1      ! Amount of N in pool Cres at depth iz=1 and residue ires
  real,    intent(INOUT) :: QCapp      ! cumulative amount of organic C added to soil // kg.ha-1
  real,    intent(INOUT) :: QNapp      ! cumulative amount of organic N added to soil // kg.ha-1
  real,    intent(INOUT) :: QCresorg   ! cumulative amount of exogenous C added to soil // kg.ha-1
  real,    intent(INOUT) :: QNresorg   ! cumulative amount of exogenous N added to soil // kg.ha-1
  real,    intent(INOUT) :: CroCo      ! decomposition parameter of residue ires
  real,    intent(INOUT) :: Chum1      ! Amount of C in active humus pool at depth iz=1 // kg.ha-1
  real,    intent(INOUT) :: Nhum1      ! Amount of N in active humus pool at depth iz=1 // kg.ha-1
  real,    intent(INOUT) :: Chumi      ! Amount of C in inert humus pool // kg.ha-1
  real,    intent(INOUT) :: Nhumi      ! Amount of N in inert humus pool // kg.ha-1
  real,    intent(INOUT) :: CsurNsol
  real,    intent(INOUT) :: Chuma
  real,    intent(INOUT) :: Nhuma

  end subroutine ResidusApportSurfaceSol

! -------------------------------------------------------------------------------------------------------------


subroutine ResiduParamDec(awb, bwb, cwb, akres, bkres, ahres, bhres, Wr, CNresmin, CNresmax, Wb, kres, hres)

  implicit none

  real,    intent(IN)    :: awb       ! decomposition parameter : CsurNbio = awb+bwb.Wr // SD // PARAM // 1
  real,    intent(IN)    :: bwb       ! decomposition parameter : CsurNbio = awb+bwb.Wr // g g-1 // PARAM // 1
  real,    intent(IN)    :: cwb       ! decomposition parameter : CsurNbio = awb+bwb.Wr // g g-1 // PARAM // 1
  real,    intent(IN)    :: akres     ! decomposition parameter : kres = akres+bkres.Wr // d-1 // PARAM // 1
  real,    intent(IN)    :: bkres     ! decomposition parameter : kres = akres+bkres.Wr // g g-1 // PARAM // 1
  real,    intent(IN)    :: ahres     ! decomposition parameter : hres = 1-ahres/(bhres.Wr+1) // g g-1 // PARAM // 1
  real,    intent(IN)    :: bhres     ! decomposition parameter : hres = 1-ahres/(bhres.Wr+1) // g g-1 // PARAM // 1
  real,    intent(IN)    :: Wr        ! N/C of the residue    g.g-1
  real,    intent(IN)    :: CNresmin  ! minimum value of C/N ratio of residue // g g-1 // PARAM // 1
  real,    intent(IN)    :: CNresmax  ! maximum value of C/N ratio of residue // g g-1 // PARAM // 1
  real,    intent(INOUT) :: Wb        ! N/C ratio of the microbial biomass    g.g-1
  real,    intent(INOUT) :: kres      ! decomposition rate of the organic residue  d-1
  real,    intent(INOUT) :: hres      ! humification rate of the organic residue   g.g-1

end subroutine ResiduParamDec

! -------------------------------------------------------------------------------------------------------------

subroutine ResidusMelangeSol(itrav1, itrav2, nbResidus, ihum, Cres, Nres, Cbio, Nbio, Chum, Nhum, &
                              Cnondec, Nnondec, Cmulchnd, Nmulchnd) 
                              ! PL, 12/04/2022: option inutile maintenant
                              !,P_code_depth_mixed_humus)
implicit none

  integer, intent(IN)    :: itrav1                  ! cote superieure de l'operation de travail j     (cm)
  integer, intent(IN)    :: itrav2                  ! cote inferieure de l'operation de travail j     (cm)
  integer, intent(IN)    :: nbResidus               ! Total number of residues
  integer, intent(IN)    :: ihum
  real,    intent(INOUT) :: Cres(ihum,nbResidus)  ! stock C du pool residu j de la couche i       (kg/ha)
  real,    intent(INOUT) :: Nres(ihum,nbResidus)  ! stock N du pool residu j de la couche i       (kg/ha)
  real,    intent(INOUT) :: Cbio(ihum,nbResidus)  ! stock C du pool biomasse j de la couche i     (kg/ha)
  real,    intent(INOUT) :: Nbio(ihum,nbResidus)  ! stock N du pool biomasse j de la couche i     (kg/ha)
  real,    intent(INOUT) :: Chum(ihum)            ! stock C du pool humus actif de la couche i    (kg/ha)
  real,    intent(INOUT) :: Nhum(ihum)            ! stock N du pool humus actif de la couche i    (kg/ha)
  real,    intent(INOUT) :: Cnondec(nbResidus)      ! undecomposable C stock of residue i on the surface // kg.ha-1
  real,    intent(INOUT) :: Nnondec(nbResidus)      ! undecomposable N stock of residue i on the surface // kg.ha-1
  real,    intent(INOUT) :: Cmulchnd
  real,    intent(INOUT) :: Nmulchnd
  ! PL, 12/04/2022: option inutile maintenant
  !integer, intent(IN)    :: P_code_depth_mixed_humus ! option to mixed the humus on the depth itrav1-itrav2 (yes=1) or on the depth 1-itrav2 (no=2) // 1,2

end subroutine ResidusMelangeSol

! -------------------------------------------------------------------------------------------------------------
subroutine ResidusParamDecMelange(itrav2, ihum, nbResidus, awb, bwb, cwb, akres, bkres, ahres, bhres, Cres, Nres,  &
                                   CNresmin, CNresmax, Wb, kres, hres)
  implicit none

  integer, intent(IN)    :: itrav2              ! depth of soil tillage = proftrav (cm)
  integer, intent(IN)    :: ihum
  integer, intent(IN)    :: nbResidus           ! total number of residues
  real,    intent(IN)    :: awb(nbResidus)      ! decomposition parameter : CsurNbio=awb+bwb.Wr // SD // PARAM // 1
  real,    intent(IN)    :: bwb(nbResidus)      ! decomposition parameter : CsurNbio=awb+bwb.Wr // SD // PARAM // 1
  real,    intent(IN)    :: cwb(nbResidus)      ! decomposition parameter : CsurNbio=awb+bwb.Wr // SD // PARAM // 1
  real,    intent(IN)    :: akres(nbResidus)    ! decomposition parameter : kres=akres+bkres.Wr // d-1 // PARAM // 1
  real,    intent(IN)    :: bkres(nbResidus)    ! decomposition parameter : kres=akres+bkres.Wr // d-1 // PARAM // 1
  real,    intent(IN)    :: ahres(nbResidus)    ! decomposition parameter : hres=1-ahres/(bhres.Wr+1) // g g-1 // PARAM // 1
  real,    intent(IN)    :: bhres(nbResidus)    ! decomposition parameter : hres=1-ahres/(bhres.Wr+1) // g g-1 // PARAM // 1
  real,    intent(IN)    :: Cres(ihum,nbResidus)
  real,    intent(IN)    :: Nres(ihum,nbResidus)
  real,    intent(IN)    :: CNresmin(nbResidus)  ! minimum observed value of ratio C/N of all residues // g g-1 // PARAM // 1
  real,    intent(IN)    :: CNresmax(nbResidus)  ! maximum observed value of ratio C/N of all residues // g g-1 // PARAM // 1
  real,    intent(INOUT) :: Wb(nbResidus)        ! N/C of the biomass
  real,    intent(INOUT) :: kres(nbResidus)      ! decomposition rate of the organic residues
  real,    intent(INOUT) :: hres(nbResidus)      ! humification rate of the organic residues

end subroutine ResidusParamDecMelange

! -------------------------------------------------------------------------------------------------------------

 subroutine Rootdeposition(nbCouches, drlsenf, drlseng, longsperacf, longsperacg, awb, bwb, cwb, &
                           akres, bkres, ahres, bhres, CNresmin, CNresmax, QCrac, QNrac,  &
                           Wb, kres, hres, Cres, Nres, msrac, msracmort, QCracmort, QNracmort, &
                        ! DR 19/02/2019 substitution de parazorac en tant que code par un vrai code P_code_rootdeposition
                        !   masecnp, magrain, QNplantenp, QNgrain, P_parazorac, P_profhum, &
                           masecnp, magrain, QNplantenp, QNgrain, P_code_rootdeposition, P_profhum, &
                           P_coderacine)
  implicit none

  integer, intent(IN)    :: nbCouches     ! Number of layers containing dead roots
  real,    intent(IN)    :: drlsenf(1:nbCouches)
  real,    intent(IN)    :: drlseng(1:nbCouches)
  real,    intent(IN)    :: longsperacf   ! specific root length of fine roots   // cm g-1
  real,    intent(IN)    :: longsperacg   ! specific root length of coarse roots // cm g-1
  real,    intent(IN)    :: awb           ! parameter of organic residue decomposition: CsurNbio=awb+bwb/CsurNres // SD // PARAM // 1
  real,    intent(IN)    :: bwb           ! parameter of organic residue decomposition: CsurNbio=awb+bwb/CsurNres // g g-1 // PARAM // 1
  real,    intent(IN)    :: cwb           ! Minimum ratio C/N of microbial biomass  // g g-1 // PARAM // 1
  real,    intent(IN)    :: akres         ! parameter of organic residues decomposition: kres=akres+bkres/CsurNres // day-1 // PARAM // 1
  real,    intent(IN)    :: bkres         ! parameter of organic residues decomposition: kres=akres+bkres/CsurNres // g g-1 // PARAM // 1
  real,    intent(IN)    :: ahres         ! parameter of organic residues humification: hres=1-ahres*CsurNres/(bhres+CsurNres) // g g-1 // PARAM // 1
  real,    intent(IN)    :: bhres         ! parameter of organic residues humification: hres=1-ahres*CsurNres/(bhres+CsurNres) // g g-1 // PARAM // 1
  real,    intent(IN)    :: CNresmin      ! minimum observed value of ratio C/N of organic residues  // g g-1 // PARAM // 1
  real,    intent(IN)    :: CNresmax      ! maximum observed value of ratio C/N of organic residues // g g-1 // PARAM // 1
  real,    intent(IN)    :: msrac         ! biomasse racinaire vivante (t.ha-1)

  real,    intent(INOUT) :: QCrac         ! amount of C in living roots //  kg.ha-1
  real,    intent(INOUT) :: QNrac         ! amount of N in living roots //  kg.ha-1
  real,    intent(INOUT) :: Wb
  real,    intent(INOUT) :: kres
  real,    intent(INOUT) :: hres
  real,    intent(INOUT) :: Cres(1:nbCouches)
  real,    intent(INOUT) :: Nres(1:nbCouches)
  real,    intent(INOUT) :: msracmort     ! dead root biomass (t.ha-1)
  real,    intent(INOUT) :: QCracmort
  real,    intent(INOUT) :: QNracmort
! Ajout Loic 16/11/2017 : en attendant que toutes les plantes soient bien parametrees :
  real,    intent(IN)    :: masecnp
  real,    intent(IN)    :: magrain
  real,    intent(IN)    :: QNplantenp
  real,    intent(IN)    :: QNgrain
!  real,    intent(IN)    :: P_parazorac
  integer, intent(IN)    :: P_code_rootdeposition
  real,    intent(IN)    :: P_profhum
  real,    intent(IN)    :: P_coderacine

  end subroutine Rootdeposition

! -------------------------------------------------------------------------------------------------------------

  subroutine Rhizomedeposition(irhiz, dltaperennesen, dltaQNperennesen, awb, bwb, cwb, akres, bkres, ahres, bhres,  &
                             CNresmin, CNresmax, Wb, kres, hres, Cres, Nres)
  implicit none

  integer, intent(IN)    :: irhiz
  real,    intent(IN)    :: dltaperennesen
  real,    intent(IN)    :: dltaQNperennesen
  real,    intent(IN)    :: awb         ! // PARAMETER // parameter  of organic residues decomposition: CsurNbio=awb+bwb/CsurNres // SD // PARAM // 1
  real,    intent(IN)    :: bwb         ! // PARAMETER // parameter of organic residues decomposition: CsurNbio=awb+bwb/CsurNres // g g-1 // PARAM // 1
  real,    intent(IN)    :: cwb         ! // PARAMETER // Minimum ratio C/N of microbial biomass in the relationship: CsurNbio=awb+bwb/CsurNres // g g-1 // PARAM // 1
  real,    intent(IN)    :: akres       ! // PARAMETER // parameter of organic residues decomposition: kres=akres+bkres/CsurNres // day-1 // PARAM // 1
  real,    intent(IN)    :: bkres       ! // PARAMETER // parameter of organic residues decomposition: kres=akres+bkres/CsurNres // g g-1 // PARAM // 1
  real,    intent(IN)    :: ahres       ! // PARAMETER // parameter of organic residues humification: hres=1-ahres*CsurNres/(bhres+CsurNres) // g g-1 // PARAM // 1
  real,    intent(IN)    :: bhres       ! // PARAMETER // parameter of organic residues humification: hres=1-ahres*CsurNres/(bhres+CsurNres) // g g-1 // PARAM // 1
  real,    intent(IN)    :: CNresmin    ! // PARAMETER // minimum observed value of ratio C/N of organic residues  // g g-1 // PARAM // 1
  real,    intent(IN)    :: CNresmax    ! // PARAMETER // maximum observed value of ratio C/N of organic residues // g g-1 // PARAM // 1

  real,    intent(INOUT) :: Wb
  real,    intent(INOUT) :: kres
  real,    intent(INOUT) :: hres
  real,    intent(INOUT) :: Cres(1:irhiz)
  real,    intent(INOUT) :: Nres(1:irhiz)

 end subroutine Rhizomedeposition


end interface
end module Module_Apports
