! Module of mixed in the soil when tillage
!
module Module_mixedSoil
    implicit none

interface

subroutine H2OandNmixedSoil(proftrav, ihum, tmix, hur, sat, amm, nit)
!call H2OandNmixedSoil(nint(P_proftrav(is)), nbCouches , P_tmix, hur, sat, amm, nit)
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c
!  Mixing of soil  mineral N and water in the soil after soil tillage
!  Each tillage operation is assumed to mix water and SMN within the tilled layer defined by
!     the surface and the deeper depth (proftrav)
!  The mixing intensity (tmix) varies from 0 (no mixing) to 1 (complete mixing)
!  It is (temporarily) forced to 0.0
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c
implicit none
  real, intent(IN)       :: proftrav
  integer, intent(IN)    :: ihum
  real,    intent(IN)    :: tmix    ! taux de melange  (0 = pas de melange; 1 = melange complet)
  real,    intent(INOUT) :: amm(ihum)  ! stock NH4 du pool biomasse j de la couche i     (kg/ha)
  real,    intent(INOUT) :: nit(ihum)  ! stock NO3 du pool biomasse j de la couche i     (kg/ha)
  real,    intent(INOUT) :: hur(ihum)  ! water content in the layer of 1 cm in the microporosity // mm
  real,    intent(INOUT) :: sat(ihum)  ! water content in the layer of 1 cm in the macroporosity // mm
end subroutine H2OandNmixedSoil

subroutine NMixedSoil( itrav, ihum, tmix,  &  ! IN
                          nit, amm)               ! INOUT
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c
!  Mixing of soil  mineral N after soil tillage
!  Each tillage operation is assumed to mix water and SMN within the tilled layer defined by
!     the surface and the deeper depth (proftrav)
!  The mixing intensity (tmix) varies from 0 (no mixing) to 1 (complete mixing)
!  It is (temporarily) forced to 0.0
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c
implicit none
  integer, intent(IN)    :: itrav      ! Depth of soil tillage  // cm
  integer, intent(IN)    :: ihum       ! Humification depth  (max.60 cm) // cm
  real,    intent(IN)    :: tmix       ! mixed rate  (0 = no mixed; 1 = total mixed)
  real,    intent(INOUT) :: amm(ihum)  !  NH4 in the layer i     (kg/ha)
  real,    intent(INOUT) :: nit(ihum)  !  NO3 in the layer i     (kg/ha)
end subroutine NMixedSoil

subroutine H2OMixedSoil( itrav, ihum, tmix, &  ! IN
                          hur, sat)               ! INOUT

! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c
!  Mixing of soil H2O after soil tillage
!  Each tillage operation is assumed to mix water and SMN within the tilled layer defined by
!     the surface and the deeper depth (proftrav)
!  The mixing intensity (tmix) varies from 0 (no mixing) to 1 (complete mixing)
!  It is (temporarily) forced to 0.0
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c
implicit none
  integer, intent(IN)    :: itrav      ! Depth of soil tillage  // cm
  integer, intent(IN)    :: ihum       ! Humification depth  (max.60 cm) // cm
  real,    intent(IN)    :: tmix       ! mixed rate  (0 = no mixed; 1 = total mixed)
  real,    intent(INOUT) :: hur(ihum)  ! water content in the microporosity // mm
  real,    intent(INOUT) :: sat(ihum)  ! water content in the macroporosity // mm

 end subroutine H2OMixedSoil

 end interface
end module Module_mixedSoil
