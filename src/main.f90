! ***** main program of Stics_modulo  *******
! - Programmation: N. Brisson et al
! call readings / initializations and annual loop
!The aims of STICS (Simulateur mulTIdisciplinaire pour les Cultures Standard) are similar to those of a large number of existing models (Whisler et al., 1986).
! It is a crop model with a daily time-step and input variables relating to climate, soil and the crop system.
! Its output variables relate to yield in terms of quantity and quality and to the environment in terms of drainage and nitrate leaching.
! The simulated object is the crop situation for which a physical medium and a crop management schedule can be determined.
! The main simulated processes are crop growth and development as well as the water and nitrogen balances.
! A full description of crop models with their fundamental concepts is available in Brisson et al. (2006).
!
! STICS has been developed since 1996 at INRA (French National Institute for Agronomic Research) in collaboration with other research
! (CIRAD , CEMAGREF , Ecole des Mines de Paris, ESA , LSCE ) or professional (ARVALIS , CETIOM , CTIFL , ITV , ITB , Agrotransferts , etc.) and teaching institutes.
! For more than 20 years STICS has been used and regularly improved thanks to a close link between development and application, involving scientists and technicians from various disciplines.

! CBF removed program Stics_Programme 1/8/24
subroutine Stics_Programme(GLOBALRAD) ! CBF COUPLING 2/8/24

!: LES MODULES
USE Stics
USE USM
USE Plante
USE Root
USE Itineraire_Technique
USE Sol
USE Climat
USE Station
USE Parametres_Generaux
USE Module_AgMIP
USE Files
USE SticsFiles
USE SticsSystem
USE Messages
USE maestcom ! CBF COUPLING 2/8/24
implicit none


    type(USM_)                      :: usma  
    type(Stics_Communs_)            :: sc  
    type(Plante_),dimension(nb_plant_max) :: p
    type(Root_),dimension(nb_plant_max)   :: r
    type(Parametres_Generaux_)      :: pg  
    type(ITK_),dimension(nb_plant_max)    :: itk
    type(Climat_)                   :: c  
    type(Station_)                  :: sta  
    type(Sol_)                      :: soil  
    type(Stics_Transit_)            :: t  
    type(AgMIP_)                    :: ag
    ! status for opening files
    logical                         :: status

    integer :: i,n_args
    character (len=100) arg_content
    real    :: t0,t1,d
    character (len=1) z

    
    real  :: GLOBALRAD(MAXT,MAXDATE) ! CBF COUPLING 2/8/24


    z=" "
    call CPU_TIME(t0)

! =============== DEBUT DU PROGRAMME ================


! On fout des zeros partout dans la structure Sol
! (pas forcement necessaire si le compilateur force l'initialisation a zero, mais on sait jamais)

      call Stics_Zero(sc,usma)
      call Sol_Zero(soil)
      call Stics_Transit_Zero(t)
      call Parametres_AgMIP_Zero(ag)
      call Parametres_Generaux_Zero(pg)
      
      ! voir pour zero(c), zero(itk), zero(p) !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      call zero_sta(sta)


! DR 03/09/2012 on a pas encore lu le flagecriture donc je l'affecte a sorties historique
      ! modification ensuite lors lecture valeur dans les parametres generaux
      ! de Stics_Lectures -> Lecture_Parametres_Generaux
      pg%P_flagEcriture = 31

   ! Setting model version
    call call_num_version(sc%codeversion)
    
    ! managing command line arguments for getting version number
    n_args=iargc()
    if  (n_args>0) then
       call getarg(1,arg_content)
       if (index(trim(arg_content),'version').gt.0) then
         print *, 'Modulostics version : ',sc%codeversion
       else
         print *, 'Unkown argument : ', arg_content
       endif
       call exit(0)
    endif

    
    ! Loading Stics files structure (SticsFiles.f90)
    ! SticsFiles initialization, with specific paths for some files
    call init_files(pg%P_flagEcriture, sc%datapath,sc%path,sc%pathstation, &
                   sc%pathclimat, sc%pathplt, sc%pathplt2, sc%pathsol, sc%pathtec, &
                   sc%pathtec2, sc%pathusm, sc%pathtempopar, sc%pathtempoparv6, & 
                   sc%pathinit, sc%pathrecup, sc%sticsid, sc%flag_record )
    
      ! Suppression Stics_initialisation car appel des 2 routines dans le main:
      ! -  call_num_version (plus haut) 
      ! -  init_messages plus bas
      ! init_messages appelle remplirMessages
      ! TODO : ajout d'un test et arret si pb !!!! 
      call init_messages(pg%P_flagEcriture,status)

    ! Lecture du fichier usm : new_travail.usm (format JavaStics)
     
      call EnvoyerMsgEcran("on lit le fichier usm")
      
      ! TODO init usma
      call USM_Lire(usma)
      
      ! TODO : init init_stics(sc,usma)
      sc%P_codesimul = usma%P_codesimul
      sc%codoptim  = usma%codoptim
      sc%P_codesuite = usma%P_codesuite
      sc%P_iwater    = usma%P_iwater
      sc%P_ifwater   = usma%P_ifwater
      sc%P_nbplantes = usma%P_nbplantes
      sc%P_ichsl     = usma%P_ichsl
      sc%P_wdata1    = usma%P_wdata1
      sc%P_wdata2    = usma%P_wdata2
      sc%nbans     = usma%nbans
      sc%P_culturean = usma%P_culturean
      sc%P_usm       = usma%P_nomSimulation



      if (sc%P_nbplantes.gt.nb_plant_max) then
        call EnvoyerMsgHistorique(5001,sc%P_nbplantes)
        call EnvoyerMsgErreur("Too many plants ... ",sc%P_nbplantes)
        call exit(9)
      endif

    ! on zero-ifie les structures plante et itk
      do i = 1, sc%P_nbplantes
      ! instance plante
      ! TODO : Plante_Zero()
        p(i)%ipl = i
      ! instance itk
        call ITK_Zero(itk(i))
        itk(i)%ipl = i
      ! fichiers relatifs a la plante (plante, ITK, LAIs)
        p(i)%P_fplt = usma%P_fplt(i)
        itk(i)%P_ftec = usma%P_ftec(i)
        p(i)%P_flai = usma%P_flai(i)
      end do


    ! lectures (lectures*, lecsol, lecoptim, lecturesSt2, lectureVariablesRapport, ..)
! ------------------------------------------------------------------------------

      call EnvoyerMsgEcran("before sticslectures")

      call Stics_Lectures(sc,pg,p,itk,soil,c,sta,t,usma,ag)
      
      call set_files_names(usma%P_nomSimulation,usma%P_nbplantes)
      
      ! Updating explicitly writing flags for messages
      call set_messages_flags


      call EnvoyerMsgEcran("after sticslectures")
! -----------------------------------------------------------------------------------

        sc%ifwater_courant =sc%P_ifwater


      do i = 1, sc%P_nbplantes
      ! DR 03/03/08 pour la prairie perenne
      ! DR 17/06/08 pour la prairie climator je mets en option a voir si on gardera apres
      ! DR 08/10/09 on remplace codeoutsti par un test sur P_stade0
        if (p(i)%P_codeplante == 'fou'         &
              .and. itk(i)%P_codefauche == 1     &
              .and. itk(i)%lecfauche           &
              .and. (itk(i)%P_codemodfauche == 3 .or. itk(i)%P_codemodfauche == 2)) then
           ! DR et Fr 11/02/2015 on ne saute les coupes de la premiere annee que si on est en annee de semis
!                     on veut y passer dans tous les cas pour initialiser les sommes ou dates de coupes
!                     on garde les valeurs initiales pour la prairie dans le cas ou elle meurt on va repartir sur les valeurs initiales
            call Initialisation_PrairiePerenne(sc,itk(i),i)
          !endif
          endif
      end do

! c DR 02/02/2010 je regarde d'abord si ca lit bien les parametres
      call EnvoyerMsgEcran("parameters are readed")

    ! Preparation a la boucle des annees : Adaptation des MO au CC
      call Stics_Initialisation_Boucle_Annees(sc,p,pg,itk,t) 
! DR 18/07/2012 on ajoute usm pour avoir les noms de fichier dans le bilan
      call EnvoyerMsgEcran("calling of the annual loop")
      !print *,'nb couches sol, duree racines fines, duree grosses racines = ',sc%nbCouchesSol, maxdayf,maxdayg
      call Stics_Boucle_Annees(sc,p,pg,r,itk,c,sta,soil,t,usma,ag,GLOBALRAD) ! CBF COUPLING 2/8/24


call EnvoyerMsgEcran("End")


 
call CPU_TIME(t1)
d = t1-t0
if (d < 1. ) z="0"
!call EnvoyerMsgHistorique('Duration of the simulation (s) = ',t1-t0)
write(*,*)'The execution was successful.'
write(*,'(A,A,F3.2,A)') ' Duration = ',z,d,' s'

call EnvoyerMsgHistorique("Program end ... ")

! Writing history, debug, error files footer and closing files

call finalize_files()

return ! CBF COUPLING 1/8/24
end ! CBF COUPLING 1/8/24

!end program Stics_Programme
! CBF removed 1/8/24
 
 
