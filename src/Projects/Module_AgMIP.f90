! Module of AgMIP parameters
! - Description of the structure AgMIP_
! - reading of the AgMIP parameters
module Module_AgMIP

!USE Stics , only: nb_residus_max
USE Messages
USE SticsFiles

! Les codes symboliques du module Plante
!integer, parameter :: METHOD_STICS_V6 = 6         ! Code symbolique. define the use herited reading method of Stics V6
!integer, parameter :: METHOD_XML_V7 = 7           ! Code symbolique.  define the use herited reading method XML / Javastics
!integer, parameter :: PG_LECTURE_OK = 1       ! Code de retour. the reading of the plant file made without error.
!integer, parameter :: PG_LECTURE_ERREUR_NO_METHOD = -1      ! Code retour. Error : the method choose is unknown.
! 17/09/2012 DR pour record j'enleve l'allocatable et je mets une taille en dur "parametrable"
! DR 15/11/2013 integer, parameter :: nb_residus_max = 21 remplace par le use stics plus haut pour Record
implicit none

type AgMIP_

  integer :: P_type_project
  integer :: P_rules_sowing_AgMIP
  integer :: P_Flag_Agmip_rap

end type AgMIP_


CONTAINS


!*********************************************************************
!     lecture et initialisation des parametres nouvelle version
!     en attente d'etre redirige dans leurs fichiers respectifs
!     fichier paramv6.par
!     23/11/07 ajout des parametres d'adaptation au CC de la MO
!********************************************************************
! subroutine of general parameters v6
! - reading of the generals parameters of the file tempoparv6.sti
SUBROUTINE Stics_Lecture_Param_AgMIP(ag) !, path, pathagmip)
! 31/05/2018 pour le moment on laisse les parametres dans param_newform a la fin
! donc pathagmip=pathtempoparv6
!USE Stics
USE Messages

implicit none

    type(AgMIP_), intent(INOUT) :: ag

   ! enabling_record :le chemin pour acceder a la config
   !character(len=255), intent(IN) :: path ! enabling_record
   ! enabling_record :le chemin pour acceder directement a la config
   !character(len=255), intent(IN) :: pathagmip ! enabling_record


! Variables locales
      !character(len=20) :: para
      !integer ib0                                                 ! enabling_record
      !integer ib1                                                 ! enabling_record
      !character(len=300) :: filepluspath                          ! enabling_record
      character(len=20) :: cara
      
      type(File_), pointer :: par_file
      logical :: file_open, existe
      integer :: fpar
      integer :: ret

  ! TODO : if using paths, set them in init_files call !!!!!!!!!!!!!!!!!!!!
!!DR 19/11/2013 pour Record
!     ! to get the full path
!      ib0 = len_trim(pathagmip)                             ! enabling_record
!      if (ib0 .ne. 0 ) then                                         ! enabling_record
!         filepluspath =  pathagmip                          ! enabling_record
!      else
!         ib1 = len_trim(path)                                     ! enabling_record
!         if (ib1 .eq. 0 ) then                                       ! enabling_record
!!            filepluspath = "tempopar_agmip.sti"                          ! enabling_record
!            filepluspath = "tempoparv6.sti"                          ! enabling_record
!         else                                                        ! enabling_record
!!            filepluspath = path(1:ib1) // '/' // "tempopar_agmip.sti" ! enabling_record
!            filepluspath = path(1:ib1) // '/' // "tempoparv6.sti" ! enabling_record
!         endif                                                       ! enabling_record
!      endif
!! fin record

    ! ouverture du fichier de parametres AgMIP
    !open (136,file = filepluspath,status = 'old') ! enabling_record
    
        
    !call open_file(stics_files,'parnew',par_file, file_open)
    call open_file('parnew',par_file, file_open)
      
      existe = existFile(par_file)
    if (.NOT.existFile(par_file)) then
        call EnvoyerMsgHistorique("General parameters file doesn't exist : ",char(par_file%path))
        call EnvoyerMsgErreur( "General parameters  file doesn't exist : ",char(par_file%path) )
    end if
    
    if ( .NOT.file_open ) then
        call EnvoyerMsgHistorique("Error opening general parameters file: ", char(par_file%path))
        call EnvoyerMsgErreur( "Error opening general parameters file: ", char(par_file%path) )
    end if
 
    if ( any((/.NOT.existe,.NOT.file_open/)) ) then
       call exit(9)
    end if
    

    fpar = par_file%unit
  
    ! lecture des parametres
! DR 05/02/2016 pour regles de semis agmipWheat
      cara = ''
      do  while(index (cara,'type_project').eq.0)
             read(fpar,*, end=260) cara
      end do
      
!      read (fpar,*)
      read (fpar,*,err = 250) Ag%P_type_project
      read (fpar,*)
      read (fpar,*,err = 250) Ag%P_rules_sowing_AgMIP
      read (fpar,*)
      read (fpar,*,err = 250) Ag%P_Flag_Agmip_rap
      
      !print *, "type project : ", Ag%P_type_project

260   ret = closeFile(par_file)
      !close (fpar)
      return


250   continue
      call EnvoyerMsgHistorique(5005)
      call EnvoyerMsgHistorique(405)
      !stop
      call exit(9)


return
end subroutine Stics_Lecture_Param_AgMIP


subroutine AgMIP_Tests(Ag)

USE Messages
!USE Module_AgMIP

    implicit none

    type(AgMIP_), intent(INOUT) :: Ag

      if (Ag%P_type_project.eq.2) then
        call EnvoyerMsgHistorique(1150)
        if (Ag%P_Flag_Agmip_rap.eq.1) call EnvoyerMsgHistorique(1153)
        if (Ag%P_Flag_Agmip_rap.eq.2) call EnvoyerMsgHistorique(1154)
        if (Ag%P_Flag_Agmip_rap.eq.3) call EnvoyerMsgHistorique(1155)
        if (Ag%P_Flag_Agmip_rap.eq.4) call EnvoyerMsgHistorique(1156)
        if (Ag%P_Flag_Agmip_rap.eq.5) call EnvoyerMsgHistorique(1157)
        if (Ag%P_Flag_Agmip_rap.eq.6) call EnvoyerMsgHistorique(1158)
      endif

      if (Ag%P_type_project.eq.3) call EnvoyerMsgHistorique(1151)
      if (Ag%P_rules_sowing_AgMIP.eq.2) call EnvoyerMsgHistorique(1152)

return
end subroutine AgMIP_Tests



!Warning messages for the file history.sti
!!
!! general parameters
subroutine remplirMessages_AgMIP(messages,nbMessages)

USE iso_varying_string

    implicit none

    integer,              intent(IN)    :: nbMessages
    type(varying_string), intent(INOUT) :: messages(nbMessages)

! pour agMIP
   messages (1150) = 'AgMIP Outputs activated'
   messages (1151) = 'Macsur Outputs activated'
   messages (1152) = 'sowing rules wheat3 AgMIP activation'

   messages (1153) = 'AgMIP Outputs activated, pilot AgMIP Wheat'
   messages (1154) = 'AgMIP Outputs activated, pilot AgMIP Wheat Giacomo (HSC)'
   messages (1155) = 'AgMIP Outputs activated, pilot wheat Canopy temp'
   messages (1156) = 'AgMIP Outputs activated, pilot face_maize'
   messages (1157) = 'AgMIP Outputs activated, pilot new wheat3'
   messages (1158) = 'AgMIP Outputs activated, pilot new wheat4'
return
end subroutine remplirMessages_AgMIP




subroutine Parametres_AgMIP_Zero(Ag)

    implicit none

type(AgMIP_), intent(OUT) :: Ag

      Ag%P_type_project = 0
      Ag%P_rules_sowing_AgMIP = 0
      Ag%P_Flag_Agmip_rap = 0
return
end subroutine Parametres_AgMIP_Zero


subroutine read_projects_specificities(sc)!,p,flagEcriture)

USE iso_varying_string

USE Stics
USE Plante

  implicit none

  type(Stics_Communs_),       intent(INOUT) :: sc

 ! type(Plante_),              intent(INOUT) :: p(sc%P_nbplantes)

!  type(Stics_Transit_),       intent(INOUT) :: t

   !integer, intent(IN) :: flagEcriture !

! Variables locales
    !integer :: i , ii !
    !character(len=50) :: moui
! pour Macsur et ses profondeurs de mesures specifiques
    character(len=4) :: treat
    integer :: num_treat
    real    :: profmesW, profmesN
    logical :: filexists
    
    type(File_), pointer :: depth_file
    integer :: depth, file_open, file_closed
    

! Done now in init_files (SticsFiles)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!      do i = 1, sc%P_nbplantes
!! ouverture du fichier de sorties journalieres st3
!        ! i = 1 pour la culture principale
!        ! i = 2 pour la culture associee
!          p(i)%ficsort3 = 125 + i
!          if (sc%P_nbplantes == 1) then
!            ii=min(40,len_trim(sc%P_usm))
!            moui = 'mod_s'//sc%P_usm(1:ii)//'.st3'
!            open(p(i)%ficsort3,file='./'//moui,status='unknown')
!            !if (iand(flagEcriture,sc%ECRITURE_ECRAN) >0 )  &
!            !         write(*,*)'stics_lectures',sc%P_usm(1:ii),'output file',moui
                     
!            call EnvoyerMsgEcran('stics_lectures',sc%P_usm(1:ii))
!            call EnvoyerMsgEcran('output file',moui)
!          else
!            ii=min(40,len_trim(sc%P_usm))
!            if (i == 1) moui = 'mod_sp'//sc%P_usm(1:ii)//'.st3'
!            if (i == 2) moui = 'mod_sa'//sc%P_usm(1:ii)//'.st3'
!            open(p(i)%ficsort3,file='./'//moui,status='unknown')
!          endif

!          print *, "etiq sorties agmip :",p(i)%ficsort3
!          stop
!! ouverture du fichier de rapport AgMIP
!        !if (iand(flagEcriture,sc%ECRITURE_AGMIP) > 0) then
!        if ( stics_files%flag_agmip ) then
!           p(i)%ficrap_AgMIP = 225 + i
!      !14/09/2011 ouverture rapport special pour AgMIP
!          if (i == 1 .and. sc%P_nbplantes == 1) moui = 'mod_rapport_AgMIP.sti'
!          if (i == 1 .and. sc%P_nbplantes > 1)  moui = 'mod_rapportP_AgMIP.sti'
!          if (i == 2) moui = 'mod_rapportA_AgMIP.sti'
!          open (p(i)%ficrap_AgMIP,file=moui,position='append',recl=2000)
!          print *, p(i)%ficrap_AgMIP
!        endif
!      enddo


! DR 16/12/2013 j'ajoute des profmes specifiques pour Macsur
! une pour l'eau et une pour l'azote qu'on lit dans un fichier du workspace
   !INQUIRE(file='depths_paramv6.txt',EXIST=filexists)
   !call get_file(stics_files,'depthv6',depth_file)
   call get_file('depthv6',depth_file)
   
   filexists = existFile(depth_file)
   if (.NOT.filexists) then
      return
      
   end if
   
      !open(9936,file='depths_paramv6.txt')
      file_open = openFile(depth_file)
      depth = depth_file%unit
      do num_treat=1,100
        read (depth,*, end=9999)treat,profmesW,profmesN
        if(treat==trim(sc%P_usm(8:11)))then
        sc%P_profmesW = int(profmesW)
        sc%P_profmesN = int(profmesN)
        endif
      enddo
 9999 continue
      file_closed = closeFile(depth_file) 
      !close(9936)
   !endif
return
end subroutine read_projects_specificities


! ------------------------------------------------
!  ecriture des variables de sortie dans le fichier st3
!  les variables de .st3 sont dans var2.mod
!  les variables de .sti sont dans var.mod
!  domi maj du 30/09/2004
! ------------------------------------------------

subroutine Ecriture_VariablesSortiesJournalieres_st3(sc,pg,p,soil,c,sta,ag) ! DR 19/07/2012 on supprime itk qui ne sert pas 20/09/2012 t aussi

    USE Stics
    USE Parametres_Generaux
    USE Plante
    USE Sol
    USE Climat
    USE Station
!    USE Module_AgMIP

    implicit none

!: Arguments
    type(Stics_Communs_),       intent(INOUT)   :: sc
    type(Parametres_Generaux_), intent(IN)      :: pg
    type(Plante_),              intent(INOUT)      :: p
!    type(ITK_),                 intent(IN)      :: itk
    type(Sol_),                 intent(IN)      :: soil
    type(Climat_),              intent(IN)      :: c
    type(Station_),             intent(IN)      :: sta
    !type(Stics_Transit_),       intent(IN)      :: t
    type(AgMIP_), intent(IN) :: Ag

!: Variables locales
    integer           :: i  !
    integer           ::  k
    character         :: sep
    !character(len=20)      :: pays
    character(len=3) :: rmon
    integer   :: nday,nummois !,nan
    character(len=850) :: ligne
    integer    ::j,nbligne_entete
    character(len=2) :: nom_mois(12)
    logical :: isLeapYear
    integer :: a,jjul2
    character(len=2) :: nom_jour(31)
    data nom_mois/'01','02','03','04','05','06','07','08','09','10','11','12'/
    data nom_jour/'01','02','03','04','05','06','07','08','09','10',   &
                       '11','12','13','14','15','16','17','18','19','20',   &
                       '21','22','23','24','25','26','27','28','29','30','31'/
    !    character(len=30)      :: chaine_debut_ligne
    integer       :: long_usm,ii,y,z
    character(len=5)  :: nom_plante
    character(len=20)  :: nom_lieu
    logical       :: macsur,AgMIP
    character(len=2) :: nom_treat
    character(len=20) :: identifiant
    
    ! using SticsFiles ------------------
    type(File_), pointer :: sort_file
    integer :: sort
    character(len = 20 ) :: tag
    
    integer :: funit
     

    AgMIP= .FALSE.
    Macsur= .FALSE.
    if(ag%P_type_project.eq.2) AgMIP= .TRUE.
    !print *, "type project : ", ag%P_type_project
    if(ag%P_type_project.eq.3) Macsur= .TRUE.

! write(*,*)sc%n, sc%nbvarsortie

      call CorrespondanceVariablesDeSorties(sc,p,soil,c,sta,         &  ! DR 19/07/2012 on supprime itk qui ne sert pas20/09/2012 t aussi
                                            sc%nbvarsortie,                 &
                                            sc%valpar(1:sc%nbvarsortie),     &
                                            sc%valsortie(1:sc%nbvarsortie))

! Ecriture de l'entete dans .sti
! ==============================

      
      ! if the unit hasn't be stored yet
      if ( p%ficsort3(p%ipl) == 0 ) then
       ! Getting the sorties tag, for selecting the right file
      ! default report tag
        tag = 'daily_agmip'
      ! if inter crops
        if (p%ipl == 1 .and. sc%P_nbplantes > 1) then
         tag = 'daily_agmip_p'
        end if
        if (p%ipl == 2) then
         tag = 'daily_agmip_a'
        end if
      
      
      ! Getting sorties file infos
      ! TODO : set file_open as an output of Ecriture_VariablesSortiesJournalieres
        !call get_file(stics_files,trim(tag),sort_file)
        call get_file(trim(tag),sort_file)
      
      !print *, "ecriture en tete sorties agmip: ",char(sort_file%path)

        p%ficsort3(p%ipl) = sort_file%unit
      

      
! dr 20/12/2013 ecriture der la ligne d'entete on l'enleve
!        write(p%ficsort3,223)
!223     format('Stics ',$)
!        write(p%ficsort3,221)trim(sc%P_usm)
!        write(p%ficsort3,222)
!222     format(';ian;jul',$)
        do i = 1,sc%nbvarsortie
!          write(p%ficsort3,221) trim(sc%valpar(i))
        end do
!221     format(';',A,$)
      ! on retourne a la ligne
!        write(p%ficsort3,*)
!       sc%chaine_debut_ligne='Stics; low '//';'//trim(pays)//';'
        sc%chaine_debut_ligne='ST;date;'//trim(sc%P_usm(5:6))//';'
        
        ! getting a free unit
        funit = getFreeFileUnit()
        
        if (sc%codeenteterap(p%ipl) == 1) then
            if (macsur)then
         !open(1,file='Daily_template_CropM_Rotationeffect.txt')
               open(funit,file='Daily_template_CropM_Rotationeffect.txt')
               nbligne_entete=0
               do j=1,10
                  !read(1,'(a300)',end=999)ligne
                  read(funit,'(a300)',end=999)ligne
                  if (j.eq.6.or.j.eq.7)then
                      write(p%ficsort3(p%ipl),'(a300)')ligne
                      write(*,'(a225)')ligne
                  endif
                  nbligne_entete=nbligne_entete+1
               enddo
   999 continue
               close(funit)
        !close(999)
            endif
            if(AgMIP)then
            !print *, "writing entete_daily ", AgMIP
                !open(1,file='entete_daily.txt')
                !read(1,*,end=9999)nbligne_entete
                open(funit,file='entete_daily.txt')
                read(funit,*,end=9999)nbligne_entete
                do j=1,nbligne_entete
                  !read(1,'(a850)',end=9999)ligne
                  read(funit,'(a850)',end=9999)ligne
                  write(p%ficsort3(p%ipl),'(a850)')ligne
                  write(*,'(a850)')ligne
                enddo
               !read(1,'(a20)',end=9999)sc%model_name
               !read(1,'(a20)',end=9999)sc%info_level
               read(funit,'(a20)',end=9999)sc%model_name
               read(funit,'(a20)',end=9999)sc%info_level
   9999 continue
               close(funit)
               !close(999)
               !close(1)
            endif
        else
             sc%model_name='ST'
             sc%info_level=''
        endif
      endif
      
      sort = p%ficsort3(p%ipl)
! =========================================================================================
      if (pg%P_codeseprapport == 2) then
        sep = pg%P_separateurrapport
      else
        sep = ' '
      endif

      long_usm=len_trim(sc%P_usm)
      nom_plante=trim(sc%P_usm(long_usm-2:long_usm+1))

      ii=index(sc%P_usm,'_')
      nom_lieu=sc%P_usm(1:ii-1)
      nom_plante= sc%P_usm(ii+1:ii+5)
      nom_treat= sc%P_usm(11:12)

! DR 13/03/2018 on recupere le nom du traitement
!           if(index(sc%P_usm,trim('CALIB')).ne.0)then
!             identifiant=sc%P_usm(8:11)//trim(sc%P_usm(27:30))
!           endif
             sc%site_name=sc%P_usm(8:11)
!DR 06/09/2018 je calule aussi l'identifiant pour la sensi
                 if(index(sc%P_usm,trim('CALIB')).ne.0)&
                      identifiant=sc%P_usm(8:11)//trim(sc%P_usm(27:30))
                 if(index(sc%P_usm,trim('SENSI')).ne.0)then
                      y=index(sc%P_usm,trim('_SENSI_'))
                      z=index(sc%P_usm,trim('_N'))
                      identifiant=sc%P_usm(8:11)//'_'//trim(sc%P_usm(y+7:z-1))
                 endif
                 if(index(sc%P_usm,trim('_BASELI_')).ne.0)then
                      identifiant=sc%P_usm(8:11)//'_'//trim('baseline')
                 endif
!print* ,'site name ',sc%site_name
!print*, 'identifiant ',identifiant
    !  Bruno - 29/04/03 - en attente de verif dans winstics
    ! DR 171006 si on a choisit le code separateur pour rapport on
    ! DR 21/07/08 pour la version standard on a plus que le sti
      if (pg%P_codeoutscient == 2) then
      ! dr 19/09/2014 je teste en elevant ca qui pose pb
!        write(2013,852)trim(sc%chaine_debut_ligne), sc%annee(sc%jjul),sep,sc%jul,   &
!        (sep,sc%valsortie(k),k = 1,sc%nbvarsortie),sep,trim(sc%P_usm(12:14))

         ! dr 09/12/2013 pour macsur il faut ecrire les cumuls a partir du semis
!        call julien(sc%jjul,sc%annee(sc%jjul),rmon,nday,nummois)
        a=sc%annee(sc%jjul)-1
        isLeapYear = ((mod(a,4) == 0) .and. (mod(a,100) /= 0)) .or. (mod(a,400) == 0)
        !isLeapYear = isBissextile(a)
        jjul2=sc%jjul
        !if (isBissextile.and.sc%jjul.gt.366)then
        if (isLeapYear.and.sc%jjul.gt.366)then
        jjul2=sc%jjul-366
        endif
        !if (.not.isBissextile.and.sc%jjul.gt.365)then
        if (.not.isLeapYear.and.sc%jjul.gt.365)then
        jjul2=sc%jjul-365
        endif
        call julien(jjul2,sc%annee(sc%jjul),rmon,nday,nummois)

        !pour agmip qminr en positif
!        sc%valsortie(15) = -sc%valsortie(15)
        if(macsur)then
            write(sort,853)'ST',sep,sc%annee(sc%jjul),nom_mois(nummois),nom_jour(nday),sep,trim(sc%P_usm(5:6)),&
            (sep,sc%valsortie(k),k = 1,sc%nbvarsortie),sep,nom_plante
 853        format(A2,a1,i4,2a2,a1,a3,22(a1,f12.5),a1,a3)
        endif
        if(AgMIP)then
! pour agmpip_face
! on converti les rendts en g/m2 on multiplie par 100
!        write(sort,855)'STICS;LOW;Braunschweig',sep,sc%annee(sc%jjul),sep,sc%jul,sep,trim(sc%P_usm(1:3)),&
!         sep,trim(sc%P_usm(5:8)),(sep,sc%valsortie(k),k = 1,sc%nbvarsortie)
! DR 03/02/2018 je commente ca , je fais le wheat4
         !   write(sort,855)trim(sc%model_name),sep,trim(sc%info_level),sep, trim(nom_lieu),   &
         !  sep, trim(nom_plante), sep,sc%annee(sc%jjul),sep,sc%jul, (sep,sc%valsortie(k),k = 1,sc%nbvarsortie)
! pour wheat4
           if(ag%P_Flag_Agmip_rap.eq.8)then
! 31/08/2018 pour les sites n'ayant pas de prof de mesures de N on mets na
              if(sc%n.ge.p%nplt.and.(p%nmat.eq.0.or.sc%n.eq.p%nmat))then
                if(sc%P_profmesN.ne.0)then
                  write(sort,856)trim(sc%model_name),sep,trim(sc%site_name),sep,trim(identifiant),sep,  &
                  sc%annee(sc%jjul),sep, sc%annee(sc%jjul),'-',nom_mois(nummois),'-',nom_jour(nday), &
                  (sep,sc%valsortie(k),k = 1,6),sep,'na',(sep,sc%valsortie(k),k = 7,sc%nbvarsortie)
                else
                  write(sort,857)trim(sc%model_name),sep,trim(sc%site_name),sep,trim(identifiant),sep,  &
                  sc%annee(sc%jjul),sep, sc%annee(sc%jjul),'-',nom_mois(nummois),'-',nom_jour(nday), &
                  (sep,sc%valsortie(k),k = 1,6),sep,'na',(sep,sc%valsortie(k),k = 7,9), sep ,       &
                  'na',(sep,sc%valsortie(k),k = 11,sc%nbvarsortie)
                endif
           endif
        else
! DR 16/03/2018 ils ne veulent les sorties journalieres qu'a partir du semis et jusqu'a maturite
            if(sc%n.ge.p%nplt.and.(p%nmat.eq.0.or.sc%n.eq.p%nmat))then
                write(sort,855)trim(sc%model_name),sep, sc%annee(sc%jjul),sep, &
                sc%annee(sc%jjul),'-',nom_mois(nummois),'-',nom_jour(nday),sep, &
                trim(nom_treat), (sep,sc%valsortie(k),k = 1,sc%nbvarsortie)
            endif
       endif
 !855  format(A22,a1,i4,a1,i4,a1,a3,a1,a4,22(a1,f12.5),a1,a3)
! 855        format(A5,a1,a14,a1,a10,a1,a5,a1,i4,a1,i3,40(a1,f12.5))
  855   format(A2,a1,i4,a1,i4,a1,a2,a1,a2,a1,a2,80(a1,f12.5))
  856   format(A2,a1,a4,a1,a14,a1,i4,a1,i4,a1,a2,a1,a2,6(a1,f12.5),a1,a2,80(a1,f12.5))
  857   format(A2,a1,a4,a1,a14,a1,i4,a1,i4,a1,a2,a1,a2,6(a1,f12.5),a1,a2,3(a1,f12.5),a1,a2,80(a1,f12.5))

        endif
    else
        write(sort,851)trim(sc%chaine_debut_ligne), sc%annee(sc%jjul),sep,sc%jul,   &
        (sep,sc%valsortie(k),k = 1,sc%nbvarsortie),sep,trim(sc%P_usm(12:14))
    endif


!-- 851      format(i4,i3,i3,i4,100(1x,e12.3))
 851  format(A,i4,a1,i3,100(a1,e12.3))
!-- 852      format(i4,i3,i3,i4, 100f8.2)
! 852  format(A,i4,a1,i3,100(a1,f12.5))
! 852  format(A,i4,a1,i3,22(a1,f12.5),a1,a3)



return
end subroutine Ecriture_VariablesSortiesJournalieres_st3

! ***************************************************** c
! * modif Bruno  20/04/99                              * c
! * ecriture d'un rapport final en fin de simulation  * c
! * + eventuellement a 2 autres dates                 * c
! ***************************************************** c
! ***************************************************** c
! Writing a final report at the end of simulation for agmip project (file mod_rapport_AGMIP.sti)
!subroutine Ecriture_Rapport_agmip(sc,pg,soil,c,sta,p,itk,t)  !DR 19/07/2012 c, itk et t pas utile
subroutine Ecriture_Rapport_agmip(sc,pg,soil,sta,p,ag)


USE Stics
USE Plante
USE Itineraire_Technique
USE Parametres_Generaux
USE Climat
USE Station
USE Sol
!USE Module_AgMIP

  implicit none

  type(Stics_Communs_),       intent(INOUT) :: sc

  type(Parametres_Generaux_), intent(IN)    :: pg

  type(Sol_),                 intent(INOUT) :: soil

  type(Station_),             intent(INOUT) :: sta

  type(Plante_),              intent(INOUT) :: p

  !type(Stics_Transit_),       intent(INOUT) :: t

  type(AgMIP_),               intent(INOUT) :: Ag


      integer           :: ancours  !
      integer           :: ifin  !
      integer           :: i
      real              :: nstt1  !
      real              :: QNF  !
      real              :: rdt
      ! PB - je le rajoute en tant que variable locale
      real              :: nstt2

      character         :: sep
      character(len=10) :: commentcelia
      character(len=10) :: commentCC


     character(len=500) :: mes3000
    ! character(len=9)   :: treatment
     integer       :: j, nbligne_entete
     character(len=329) :: ligne
     integer       :: long_usm
     character(len=3)   :: nom_plante
     logical :: AgMIP,Macsur
     real :: tcultmoy_cycle, tcultmax_cycle
     integer :: an_iplt,jour_iplt,nummois_iplt,an_iflo,jour_iflo,nummois_iflo,an_imat,jour_imat,nummois_imat
     integer :: an_ilev,jour_ilev,nummois_ilev
     integer :: jour
!     integer :: Flag_Agmpip_rap
     character(len=3) :: mois
     character(len=2) :: charmois_iplt,charjour_iplt
     character(len=2) :: charmois_ilev,charjour_ilev
     character(len=2) :: charmois_iflo,charjour_iflo
     character(len=2) :: charmois_imat,charjour_imat
     !character(len=2) :: chaine

     integer      ::  ii
     ! ,code_ecrit_nom_usm
     character(len=10) ::  scenario
     character(len=3)  ::  co2
     character(len=30) ::  treatment !,usm
     character(len=1) :: tiret
     integer :: y,z
     character(len=20) :: identifiant

     character(len=2) :: chiffre(31)
     

  ! using SticsFiles ------------------
      type(File_), pointer :: rap_file
      character(len = 20 ) :: tag
      logical :: file_open
      integer :: rap !,ret
      
      integer :: funit
      
      ! if the unit hasn't be stored yet
      if ( p%ficrap_AgMIP(p%ipl) == 0 ) then
        !print *, "valeur ficrap_AgMIP plante ",p%ipl, " ",p%ficrap_AgMIP(p%ipl)
          
          ! Getting the report tag, for selecting the right file
          ! default report tag
          tag = 'rap_agmip'   
          ! if inter crops
          if (p%ipl == 1 .and. sc%P_nbplantes > 1) then
             tag = 'rap_agmip_p'
          end if
          if (p%ipl == 2) then 
             tag = 'rap_agmip_a'
          end if
          
          !print *, "tag rap agmip ", tag
          
          ! Getting report file infos
          ! TODO : set file_open as an output of Ecriture_Rapport_agmip
          !call open_file(stics_files,trim(tag),rap_file, file_open)
          call open_file(trim(tag),rap_file, file_open)
    
          p%ficrap_AgMIP(p%ipl) = rap_file%unit
          !print *, "setting rap file unit ", p%ficrap_AgMIP(p%ipl)
      end if
      ! -----------------------------------
    
    rap = p%ficrap_AgMIP(p%ipl)
    
    
    chiffre = (/'01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19', &
                & '20','21','22','23','24','25','26','27','28','29','30','31'/)
    AgMIP= .FALSE.
    Macsur= .FALSE.
    if(ag%P_type_project.eq.2) AgMIP= .TRUE.
    if(ag%P_type_project.eq.3) Macsur= .TRUE.
    
    

! DR 26/01/2016 je mets un code Flag_Agmpip_rap pour generaliser les ecritures car maintenant les protocoles
!  sont repris entre les differents pilots
! 1 : agMIP Wheat
! 2 : AGmip Wheat Giacomo (HSC)
! 3 : DR 29/12/2014 pour wheat Canopy temp
! 4 : 21/09/2014pour face_maize
! 5 : nouveau wheat 3
!
!code_ecrit_nom_usm=1
! ecriture des entetes
if(.not.macsur)then
        ! getting a free unit
        funit = getFreeFileUnit()
! DR 27/08/2018 pour avoir des fichiers a pouvoir triter simplement je lis une entete simplifie sur une ligne que je mets dans le rapport si on ne demande pas d'entete
    if(sc%codeenteterap_agmip == 3)then
       open(funit,file='entete_summary_simple.txt')
       read(funit,'(a300)')mes3000
       write(*,'(a300)')mes3000
       close(funit)
    endif

    if(ag%P_Flag_Agmip_rap.eq.1)then
! DR pour agMIP Wheat
    mes3000 = "('P_usm;wlieu;co2;ansemis;P_iwater;ancours;ifin;P_ichsl;group;P_codeplante;' &
          &'stade;mafruit;masec(flo);masec(mat);laimax;iflos;imats;QLES;drat;Qnplante(flo);Qnplante(mat);' &
          &'QNgrain;chargefruit;cet;N_mineralisation;N_volatilisation;cum_immob;QNdenit;resmes;azomes;ruisselt;cep')"
    else if(ag%P_Flag_Agmip_rap.eq.2)then
! 28/01/2013 POur AGmip Wheat Giacomo (HSC)
    mes3000 = "('P_usm;wlieu;co2;ansemis;P_iwater;ancours;ifin;P_ichsl;group;P_codeplante;stade;' &
          &'mafruit;iflos;imats;chargefruit;masec(flo);masec(mat);laimax;drat;cet;resmes;ruisselt;cep;' &
          &'Qnplante(flo);Qnplante(mat);QLES;QNgrain;N_mineralisation;N_volatilisation;cum_immob;azomes;QNdenit;' &
          &'nlev;nbfeuille;ceo')"
    else  if(ag%P_Flag_Agmip_rap.eq.3)then
    ! DR 29/12/2014 pour wheat Canopy temp
    mes3000 = "('P_usm;' &
          &'mafruit;iflos;imats;chargefruit;masec(flo);masec(mat);laimax;drat;cet;SoilAvW;ruisselt;cep;' &
          &'Qnplante(flo);Qnplante(mat);Qles;QNgrain;N_mineralisation;N_volatilisation;cum_immob;azomes;QNdenit;' &
          &'nlev;nbfeuille;ceo;tcultmoy,tcultmax')"
    else if(ag%P_Flag_Agmip_rap.eq.4)then
! 21/09/2014pour face_maize
    mes3000 = "('STICS;treatment;ancours;'    &
          &'mafruit;masecflo; masecmat;laimax;cumraint;iflos;imats;cetm;ces;cep;ruisselt;drat;'            &
          &'QNplanteflo;QNplantemat;QNgrain;chargefruit')"
    else if(ag%P_Flag_Agmip_rap.eq.5)then
!!    28/01/2015 pour le wheat phase 3  !!  code=5
    mes3000 = "('ST;P_usm;ansemis;' &
          &'mafruit;iflos;imats;chargefruit;masec(flo);masec(mat);laimax;drat;cet;resmes;ruisselt;cep;' &
          &'Qnplante(flo);Qnplante(mat);QLES;QNgrain;N_mineralisation;N_volatilisation;cum_immob;azomes;QNdenit;' &
          &'nlev;nbfeuille;ceo;iplts;ircarb')"
    else if(ag%P_Flag_Agmip_rap.eq.7)then

! DR 13/03/2018 on fait des sorties specifiques pour wheat phase4
    mes3000 = "('ST; sowingdate ; treat ; ' &          
          &'mafruit ; emergencedate ; floweringdate ; maturitydate ; nbfeuille;  chargefruit; masec_flo ;' &
          &' masec_mat ;laimax; drain_from_plt ; cet_from_plt ; SoilAvW; runoff_from_plt ; '  &
          &'cep; QNplante_flo; QNplante_mat ;   leaching_from_plt  ; QNgrain; Nmineral_from_plt;'&
          &' Nvolat_from_plt; cum_immob ;  SoilN ;  QNdenit_from_plt;    cumrg')"
   endif
else
    mes3000 = "('ST;ancours;irecs;treatment;'    &
          &'mafruit;iflos;imats;chargefruit;masecflo; masecmat;laimax;'            &
          &'drat;cet;SoilAvW;ruissel;cep;QNplanteflo;QNplantemat;QLES;QNgrain;N_mineralisation;'&
          &'N_volatilisation;Qminr;SoilN;QNdenit;ilevs;haun=a;cetm;Corg=na;Crop')"
endif

      if(sta%P_codeclichange==2)then
        commentCC='CO2'
      else
        commentCC='noCO2'
      endif
!!!======================== on demarre la lecture/ecriture des entetes =============================================!!!

! getting a free unit
funit = getFreeFileUnit()

if (macsur)then ! Macsur !   1
!*** DR 11/08/2011 on ecrit les entetes une seule fois
!   et on met des , entre noms des varaibles de la ligne d'entete , faire plus propre avec separateurrapport plus tard
    ! DR 29/12/09 il ne faut ecrire l'entete que le premier jour de simulation
    if (sc%codeenteterap_agmip == 1 ) then
        ! open(1,file='Summary_template_CropM_Rotationeffect.txt')
        open(funit,file='Summary_template_CropM_Rotationeffect.txt')
        nbligne_entete=0
        do j=1,10
            !read(1,'(a205)',end=999)ligne
            read(funit,'(a205)',end=999)ligne
            if (j.eq.6.or.j.eq.7) then
               write(rap,'(a205)')ligne
               write(*,'(a205)')ligne
            endif
            nbligne_entete=nbligne_entete+1
        enddo
   999  continue
        close(funit)
        !close(999)
    endif
else  ! AgMIP !   1
     if (sc%codeenteterap_agmip == 1 ) then !2
       if (ag%P_Flag_Agmip_rap.eq.5)then  !3
           !open(1,file='Template-Summary.txt')
           open(funit,file='Template-Summary.txt')
           
           do j=1,3
             !read(1,'(a300)',end=998)ligne
             read(funit,'(a300)',end=998)ligne
             write(rap,'(a300)')ligne
           enddo
           ! ligne 4 ajouter le simulation
           ii=index(sc%P_usm,'-')
           scenario= sc%P_usm(ii+1:ii+2)
           co2 = sc%P_usm(ii+4:ii+6)
           treatment=sc%P_usm(1:ii-1)
           !read(1,'(a30)',end=998)ligne
           read(funit,'(a30)',end=998)ligne
           write(rap,'(a10,a10,a3)')trim(ligne),trim(scenario),trim(co2)
           ! ligne 5 ajouter le treatment
           !read(1,'(a30)',end=998)ligne
           read(funit,'(a30)',end=998)ligne
           write(rap,'(a10,a30)')trim(ligne),trim(treatment)
           do j=6,7
             !read(1,'(a300)',end=998)ligne
             read(funit,'(a300)',end=998)ligne
             write(rap,'(a300)')ligne
           enddo
           !read(1,*)
           !read(1,*)sc%code_ecrit_nom_usm
           !close(1)
           read(funit,*)
           read(funit,*)sc%code_ecrit_nom_usm
           close(funit)
       else    !3
         if (ag%P_Flag_Agmip_rap.eq.6)then   !4
            !open(1,file='entete_summary.txt')
            open(funit,file='entete_summary.txt')
            do j=1,8
              !read(1,'(a300)',end=998)ligne
              read(funit,'(a300)',end=998)ligne
              write(rap,'(a300)')ligne
            enddo
         else    !4
         !open(1,file='Stics_Phase1_final.csv')
            !open(1,file='entete_summary.txt')
            open(funit,file='entete_summary.txt')
            do j=1,20
              !read(1,'(a300)',end=998)ligne
              read(funit,'(a300)',end=998)ligne
              write(rap,'(a300)')ligne
            enddo
         endif    !4
         close(funit)
       endif   !3
 !      close(1)
     else    !2
        !if(sc%numcult.eq.1)write(rap,mes3000)
     endif   !2
998 continue
! 20/12/2013 dr on enleve la ligne d'entete a nous pour ne laisser que celle des rapports
!        write(rap,mes3000)
!*** DR 11/08/2011 on ecrit les entetes une seule fois
    sc%codeenteterap_agmip=0
endif !1
! fin des histoires d'entete
      if (pg%P_codeseprapport == 2) then
        sep = pg%P_separateurrapport
      else
        sep = ' '
      endif

    ! actualisation des numeros de jour
      if (p%P_codeplante == 'snu') then
        !--p%nrec = n
        p%nlax = sc%n
        p%nst1 = 1
        p%nst2 = 1
      endif

      nstt1 = max(p%nst1,1)
      nstt2 = max(p%nst2,1)

    ! calcul du rendement, des quantites H20 et N sur l'ensemble du profil
    ! domi - 5.0 9/11/00 - rdt calcule meme avant recolte
      rdt      = p%magrain(AOAS,sc%n)/100.
      sc%QH2Of = 0.
      QNF      = 0.
      QNF = sum(soil%AZnit(1:5))
      sc%QH2Of = sum(sc%hur(1:int(soil%profsol)))

    ! date courante: jour (ifin), annee (ancours)
      ifin = sc%n + sc%P_iwater - 1 ! TODO : remplacer par sc%jul ?
      ancours = sc%annee(ifin)
      if (ifin > sc%nbjsemis) ifin = ifin - sc%nbjsemis

    ! Ecriture des noms des stades
    ! DR 08/09/06 j'initialise commentcelia
      commentcelia = 'date'
    ! DR 11/05/06 pour celia ecriture des stades
      if (sc%n == p%nplt) commentcelia = 'semis'
!  if (n == maxwth.and.P_datefin)commentcelia = 'fin'
!     DR 08/09/06 je rajoute les noms des stades dans le cas ou on
!     ecrit dans le rapport a des stades
      if (sc%codetyperap == 2 .or. sc%codetyperap == 3) then
        if (sc%n == p%nger) commentcelia = 'ger'
        if (sc%n == p%nlev) commentcelia = 'lev'
        if (sc%n == p%ndrp) commentcelia = 'drp'
        if (sc%n == p%nlax) commentcelia = 'lax'
        if (sc%n == p%nflo) commentcelia = 'flo'
        if (sc%n == p%nsen) commentcelia = 'sen'
        if (sc%n == p%namf) commentcelia = 'amf'
! dr brazil oubli de nmat 05/08/2011
        if (sc%n == p%nmat) commentcelia = 'mat'
      endif

      if (sc%n == p%nrec) then
        if (sc%recolte1) then
          commentcelia = 'recphy'
          sc%recolte1 = .FALSE.
        else
          commentcelia = 'rectec'
        endif
      endif

      if (p%nrec == p%nrecbutoir) commentcelia = 'recbut'

    ! DR 30/04/08 on le met ici pour que ce soit la fin qui soit identifiee sinon
    ! il met 2 fois recbut
      if (sc%n == sc%maxwth .and. sc%P_datefin) commentcelia = 'fin'
! DR pour AGMIP wheat pilot et maize pilot activer plus haut
!    mes3000 = "('P_usm;wlieu;co2;ansemis;P_iwater;ancours;ifin;P_ichsl;group;P_codeplante;' &
!          &'stade;mafruit;masec(flo);masec(mat);laimax;iflos;imats;QLES;drat;Qnplante(flo);Qnplante(mat);' &
!          &'QNgrain;chargefruit;cet;N_mineralisation;N_volatilisation;cum_immob;QNdenit;resmes;azomes;ruisselt;cep')"


!888   format('ST ; ',1(i5,a1),f10.3,a1,a3,a1,22(f10.3,a1),'na; ',f10.3,a1,'na;',a3)

! dr 29/12/2014 j'ajoute les temp cult moy pour Giacomo
    tcultmoy_cycle= p%ctcult/(p%nrec - p%nplt+1)
    tcultmax_cycle= p%ctcultmax/(p%nrec - p%nplt+1)
!!!================ on attaque les ecriture en ligne ===========================================!!!
!! DR pour AGMIP wheat pilot et maize pilot
    if(ag%P_Flag_Agmip_rap.eq.2 .or. ag%P_Flag_Agmip_rap.eq.7.or. ag%P_Flag_Agmip_rap.eq.8 )then ! regarder a quoi ca correspond
! dr le 12/04/2013 je rajoute les 3 variables a Giacomo
! dr 29/12/2014 je recalcule les dates
        an_iplt=sc%annee(p%iplts)
        call julien(p%iplts,sc%annee(p%iplts),mois,jour_iplt,nummois_iplt)
        charmois_iplt=chiffre(nummois_iplt)
        charjour_iplt=chiffre(jour_iplt)
        an_ilev=sc%annee(p%ilevs)
        jour=p%ilevs
        if (p%ilevs > sc%nbjsemis)jour = p%ilevs - sc%nbjsemis
        call julien(jour,sc%annee(p%ilevs),mois,jour_ilev,nummois_ilev)
        charmois_ilev=chiffre(nummois_ilev)
        charjour_ilev=chiffre(jour_ilev)
        an_iflo=sc%annee(p%iflos)
        jour=p%iflos
        if (p%iflos > sc%nbjsemis) jour = p%iflos - sc%nbjsemis
        call julien(jour,sc%annee(p%iflos),mois,jour_iflo,nummois_iflo)
        charmois_iflo=chiffre(nummois_iflo)
        charjour_iflo=chiffre(jour_iflo)
        an_imat=sc%annee(p%imats)
        jour=p%imats
        if (p%imats > sc%nbjsemis) jour = p%imats - sc%nbjsemis
        call julien(jour,sc%annee(p%imats),mois,jour_imat,nummois_imat)
        charmois_imat=chiffre(nummois_imat)
        charjour_imat=chiffre(jour_imat)
endif


if(macsur)then

    sc%valsortie_mat(1)= p%irecs
    long_usm=len_trim(sc%P_usm)
    nom_plante=trim(sc%P_usm(long_usm-2:long_usm+1))
!DR 07/05/2014 je rajoute le recalcul des dates pour que les dates de stades ne soient pas superieures a 365 ou 366
! mat
    if (sc%valsortie_mat(1).gt.sc%nbjsemis )sc%valsortie_mat(1)= sc%valsortie_mat(1) - sc%nbjsemis
    if (sc%valsortie_mat(3).gt.sc%nbjsemis )sc%valsortie_mat(3)= sc%valsortie_mat(3) - sc%nbjsemis
    if (sc%valsortie_mat(4).gt.sc%nbjsemis )sc%valsortie_mat(4)= sc%valsortie_mat(4) - sc%nbjsemis


! si c'est une bettearve ou un raygras on met pas les dates de flo
    if (nom_plante.eq.'GRV'.or.nom_plante.eq.'SBT') sc%valsortie_mat(3) = 0


            write(rap,888)             &
!                                commentCC,sep,sc%ansemis,sep,ancours,sep,                  &
                                ancours,sep,sc%valsortie_mat(1),sep,sc%P_usm(5:6),sep,               &! irec
                                sc%valsortie_mat(2),sep,                                             &! mafruit
                                sc%valsortie_mat(3),sep,sc%valsortie_mat(4),sep,                     &! flo,mat
                                sc%valsortie_mat(5),sep,                                             &! chargefruit
                                sc%valsortie_flo(6),sep,sc%valsortie_mat(6),sep,                     &! masec a flo,mat
                                sc%valsortie_mat(7),sep,                                             &! laimax
                                sc%valsortie_mat(8)-sc%valsortie_iplt(8),sep,                                              &! drat
                                sc%valsortie_mat(9)-sc%valsortie_iplt(9),sep,                                              &! cet
                                sc%valsortie_mat(10)-sc%valsortie_iplt(10),sep,                                            &! resmes
                                sc%valsortie_mat(11)-sc%valsortie_iplt(11),sep,                                            &! ruissel
                                sc%valsortie_mat(12)-sc%valsortie_iplt(12),sep,                                            &! cep
                                sc%valsortie_flo(13),sep,sc%valsortie_mat(13),sep,                   &! qnplante
                                sc%valsortie_mat(14)-sc%valsortie_iplt(14),sep,                                            &! qles
                                sc%valsortie_mat(15),sep,                                            &! QNgrain
!! ATTENTION DR 04/09/2012 N_mineralisation est calcule directement par qminh+qminr, N_volatilisation est calcule directement par qnvolorg+qnvoleng
                                sc%valsortie_mat(16)-sc%valsortie_iplt(16),sep,                                            &! N_mineralisation
                                sc%valsortie_mat(17)-sc%valsortie_iplt(17),sep,                                            &! N_volatilisation
                                -sc%valsortie_mat(18)+sc%valsortie_iplt(18),sep,                                           &! Nimmo=qminr
                                sc%valsortie_mat(19)-sc%valsortie_iplt(19),sep,                                            &! SoilN=ammomes+azomes
                                sc%valsortie_mat(20)-sc%valsortie_iplt(20),sep,                                            &! Qndenit
                                sc%valsortie_mat(21),sep,                                            &! ilev
                                sc%valsortie_mat(23)-sc%valsortie_iplt(23) ,sep,                     &! cetm
!                                trim(sc%P_usm(12:14))
                                nom_plante

!888   format('ST ; ',1(i5,a1),f10.3,a1,a3,a1,22(f10.3,a1),'na; ',f10.3,a1,'na;',a3)

! fin ecriture macsur
else
! DR 26/01/2016 je mets un code Flag_Agmpip_rap pour generaliser les ecritures car maintenant les protocoles
!  sont repris entre les differents pilots
! 1 : agMIP Wheat
! 2 : AGmip Wheat Giacomo (HSC)
! 3 : DR 29/12/2014 pour wheat Canopy temp
! 4 : 21/09/2014pour face_maize

   if(ag%P_Flag_Agmip_rap.eq.4)then
! DR 21/09/2014 pour maize_face
!             write(rap,888)             &
             write(rap,*)'STICS ;',            &
                                sc%P_usm(1:9),sep,ancours,sep,               &!
                                sc%valsortie_mat(1),sep,                                             &! mafruit
                                sc%valsortie_flo(2),sep,sc%valsortie_mat(2),sep,                     &! masec a flo,mat
                                sc%valsortie_mat(3),sep,                                             &! laimax
                                sc%valsortie_mat(4),sep,                                             &! cumraint
                                sc%valsortie_mat(5),sep,sc%valsortie_mat(6),sep,                     &! flo,mat
                                sc%valsortie_mat(7),sep,                                             &! cetm
                                sc%valsortie_mat(8)-sc%valsortie_iplt(8),sep,                        &! ces
                                sc%valsortie_mat(9),sep,                                             &! cep
                                sc%valsortie_mat(10)-sc%valsortie_iplt(10),sep,                      &! ruissel
                                sc%valsortie_mat(11)-sc%valsortie_iplt(11),sep,                      &! drat
                                sc%valsortie_flo(12),sep,sc%valsortie_mat(12),sep,                   &! qnplante a flo,mat
                                sc%valsortie_mat(13),sep,                                            &! QNgrain
                                sc%valsortie_mat(14),sep,                                            &! chargefruit
                                (sc%valsortie_mat(i),sep,i=15,sc%nbvarrap)                          ! les autres
    endif
    if(ag%P_Flag_Agmip_rap.eq.2)then ! regarder a quoi ca correspond
! dr le 12/04/2013 je rajoute les 3 variables a Giacomo
! dr 29/12/2014 je recalcule les dates
! DR 13/03/2018 mis en commun plus haut
         write(rap,888)'ST',sep,sc%P_usm,sep,                                           &
                                an_iplt,charmois_iplt,charjour_iplt,sep,                                     &! semis
                                sc%valsortie_mat(1),sep,                                             &! mafruit
                                an_iflo,charmois_iflo,charjour_iflo,sep,                                     &! flo
                                an_imat,charmois_imat,charjour_imat,sep,                                     &! mat
                                sc%valsortie_mat(2),sep,                                            &! chargefruit,
                                sc%valsortie_flo(3),sep,sc%valsortie_mat(3),sep,                     &! masec a flo et mat
                                sc%valsortie_mat(4),sep,                                             &! laimax
                                sc%valsortie_mat(5),sep,                                             &!  drat
                                sc%valsortie_mat(6),sep,                                            &! cet,
                                sc%valsortie_mat(7),sep,                                            &! resmes
                                sc%valsortie_mat(8),sep,sc%valsortie_mat(9),sep,                   &! ruisselt,cep
                                sc%valsortie_flo(10),sep,sc%valsortie_mat(10),sep,                     &! qnplante a flo et mat
                                sc%valsortie_mat(11),sep,                                             &! qles
                                sc%valsortie_mat(12),sep,                                             &! qngrain
                                sc%valsortie_mat(13),sep,sc%valsortie_mat(14),sep,                   &! N_mineralisation,N_volatilisation
                                sc%valsortie_mat(15),sep,                                            &! cum_immob
                                sc%valsortie_mat(16),sep,                                            &! azomes
                                sc%valsortie_mat(17),sep,                                            &!  QNdenit
                                an_ilev,charmois_ilev,charjour_ilev,sep,                                  &!  nlev
                                sc%valsortie_mat(18),sep,                                            &!  nbfeuille
                                sc%valsortie_mat(19),sep,                                            &!  eo
                                !'na',sep,                                                             &!  eo
                                tcultmoy_cycle,sep,                                                  &!  tcultmoy_cycle
                                tcultmax_cycle                                                       !  tcultmax_cycle
!888   format(a2,a1,a5,a1,(i4,a2,a2),a1,f10.3,a1,(i4,a2,a2),a1,(i4,a2,a2),a1,18(f10.3,a1),(i4,a2,a2),a1,18(f10.3,a1))
    endif
    if(ag%P_Flag_Agmip_rap.eq.5)then
   ! DR 26/01/2016 je recalule les dates en numero de jour dans l'annee
        an_iplt=sc%annee(p%iplts)
        an_imat=sc%annee(p%imats)
        if (sc%valsortie_mat(20).gt.sc%nbjsemis )sc%valsortie_mat(20)= sc%valsortie_mat(20) - sc%nbjsemis     !lev
        if (sc%valsortie_mat(2).gt.sc%nbjsemis )sc%valsortie_mat(2)= sc%valsortie_mat(2) - sc%nbjsemis        ! flo
        if (sc%valsortie_mat(3).gt.sc%nbjsemis )sc%valsortie_mat(3)= sc%valsortie_mat(3) - sc%nbjsemis        ! mat
! DR 03/07/2017 il faut faire pareil pour les dates de semis
        if (sc%valsortie_mat(23).gt.sc%nbjsemis )sc%valsortie_mat(23)= sc%valsortie_mat(23) - sc%nbjsemis

          if(sc%code_ecrit_nom_usm.eq.0)then
               write(rap,888)sc%P_usm,sep,'ST',sep,                                           &
                                an_imat,sep,                                          &! an recolte
                                sc%valsortie_mat(1),sep,                                             &! mafruit
                                int(sc%valsortie_mat(2)),sep,                                  &! flo
                                int(sc%valsortie_mat(3)),sep,                                     &! mat
                                sc%valsortie_mat(4),sep,                                            &! chargefruit,
                                sc%valsortie_flo(5),sep,sc%valsortie_mat(5),sep,                     &! masec a flo et mat
                                sc%valsortie_mat(6),sep,                                             &! laimax
                                sc%valsortie_mat(7)-sc%valsortie_iplt(7),sep,                            &!  drat
                                sc%valsortie_mat(8),sep,                                            &! cet,
                                sc%valsortie_mat(9),sep,                                            &! resmes
                                sc%valsortie_mat(10)-sc%valsortie_iplt(10),sep,                  &! ruisselt
                                sc%valsortie_mat(11),sep,                   &! cep
                                sc%valsortie_flo(12),sep,sc%valsortie_mat(12),sep,                     &! qnplante a flo et mat
                                sc%valsortie_mat(13),sep,                                             &! qles
                                sc%valsortie_mat(14),sep,                                             &! qngrain
                                sc%valsortie_mat(15),sep,sc%valsortie_mat(16),sep,                   &! N_mineralisation,N_volatilisation
                                sc%valsortie_mat(17),sep,                                            &! cum_immob
                                sc%valsortie_mat(18),sep,                                            &! azomes
                                sc%valsortie_mat(19),sep,                                            &!  QNdenit
                                int(sc%valsortie_mat(20)),sep,                                  &!  nlev
                                sc%valsortie_mat(21),sep,                                            &!  nbfeuille
                                sc%valsortie_mat(22),sep,                                        &!  eop+eos
                                int(sc%valsortie_mat(23)), &                                     ! date de semis
                                (sep,sc%valsortie_mat(i),i=24,sc%nbvarrap)
888   format(a25,a1,a2,a1,(i4),a1,f10.3,a1,(i4),a1,(i4),a1,18(f10.3,a1),(i4),a1,2(f10.3,a1),(i4),5(a1,f10.3))
          else
             write(rap,777)'ST',sep,                                           &
                                an_imat,sep,                                          &! an recolte
                                sc%valsortie_mat(1),sep,                                             &! mafruit
                                int(sc%valsortie_mat(2)),sep,                                  &! flo
                                int(sc%valsortie_mat(3)),sep,                                     &! mat
                                sc%valsortie_mat(4),sep,                                            &! chargefruit,
                                sc%valsortie_flo(5),sep,sc%valsortie_mat(5),sep,                     &! masec a flo et mat
                                sc%valsortie_mat(6),sep,                                             &! laimax
                                sc%valsortie_mat(7)-sc%valsortie_iplt(7),sep,                            &!  drat
                                sc%valsortie_mat(8),sep,                                            &! cet,
                                sc%valsortie_mat(9),sep,                                            &! resmes
                                sc%valsortie_mat(10)-sc%valsortie_iplt(10),sep,                  &! ruisselt
                                sc%valsortie_mat(11),sep,                   &! cep
                                sc%valsortie_flo(12),sep,sc%valsortie_mat(12),sep,                     &! qnplante a flo et mat
                                sc%valsortie_mat(13),sep,                                             &! qles
                                sc%valsortie_mat(14),sep,                                             &! qngrain
                                sc%valsortie_mat(15),sep,sc%valsortie_mat(16),sep,                   &! N_mineralisation,N_volatilisation
                                sc%valsortie_mat(17),sep,                                            &! cum_immob
                                sc%valsortie_mat(18),sep,                                            &! azomes
                                sc%valsortie_mat(19),sep,                                            &!  QNdenit
                                int(sc%valsortie_mat(20)),sep,                                  &!  nlev
                                sc%valsortie_mat(21),sep,                                            &!  nbfeuille
                                sc%valsortie_mat(22),sep,                                                      &!  eop+eos
                                int(sc%valsortie_mat(23)), &                                           ! date de semis
                                (sep,sc%valsortie_mat(i),i=24,sc%nbvarrap)
777   format(a2,a1,(i4),a1,f10.3,a1,(i4),a1,(i4),a1,18(f10.3,a1),(i4),a1,2(f10.3,a1),(i4),5(a1,f10.3))
        endif
     endif
! DR 19/02/2016 pour ET
     if(ag%P_Flag_Agmip_rap.eq.6)then
        an_imat=sc%annee(p%imats)
             write(rap,979)trim(sc%model_name),sep,trim(sc%info_level),sep ,                         &
                                sc%P_usm,sep,'Maize',sep,                                      &
                                an_imat,sep,                                                   &! an recolte
                                sc%valsortie_mat(1),sep,                                       &! mafruit
                                'na',sep,                                                      &
                                sc%valsortie_flo(2),sep,sc%valsortie_mat(2),sep,               &! masec a flo et mat
                                sc%valsortie_mat(3),sep,                                       &! laimax
                                sc%valsortie_mat(4),sep,                                       &! cumraint,
                                int(sc%valsortie_mat(5)),sep,                                  &! flo
                                int(sc%valsortie_mat(6)),sep,                                  &! mat
                                sc%valsortie_mat(7),sep,                                       &! cetp
                                sc%valsortie_mat(8),sep,                                       &! cum_et0
                                sc%valsortie_mat(9),sep,                                       &! cep
                                sc%valsortie_mat(10),sep,                                      &! ces
                                sc%valsortie_mat(11),sep,                                      &! cet
                                sc%valsortie_mat(12),sep,                                      &! runoff_from_plt
                                sc%valsortie_mat(13),sep,                                      &! drain_from_plt
                                'na',sep,                                                      &
                                sc%valsortie_flo(14),sep,sc%valsortie_mat(14),sep,             &! qnplante a flo et mat
                                sc%valsortie_mat(15),sep,                                      &! qngrain
                                sc%valsortie_mat(16)                                            ! chargefruit,

!979   format(a4,a1,a13,a1,a15,a1,a5,a1,i4,1(a1,f10.3),a1,a3,4(a1,f10.3),2(a1,i4),&
!             & 7(a1,f10.3),a1,a3,18(a1,f10.3))

     endif

! DR 19/02/2016 pour ET
     if(ag%P_Flag_Agmip_rap.eq.7)then
      tiret ='-'
                  write(rap,979)trim(sc%model_name),sep ,                             &
                                an_iplt,tiret,charmois_iplt,tiret,charjour_iplt,sep,            &! an semis, mois semis, jour semis
                                sc%P_usm(11:12),sep,                                        &! treat
                                sc%valsortie_mat(1),sep,                                    &! mafruit
                                an_ilev,tiret,charmois_ilev,tiret,charjour_ilev,sep,            &! lev
                                an_iflo,tiret,charmois_iflo,tiret,charjour_iflo,sep,            &! flo
                                an_imat,tiret,charmois_imat,tiret,charjour_imat,sep,            &! mat
                                sc%valsortie_mat(2), sep,                                       &! nbfeuille,
                                sc%valsortie_mat(3), sep,                                       &! chargefruit,
                                sc%valsortie_flo(4),sep,sc%valsortie_mat(4),sep,            &! masec a flo et mat
                                sc%valsortie_mat(5),sep,                                    &! laimax
                                sc%valsortie_mat(6),sep,                                    &! drain_from_plt
                                sc%valsortie_mat(7),sep,                                    &! cum_et0
                                sc%valsortie_mat(8),sep,                                    &! soil_AvW
                                sc%valsortie_mat(9),sep,                                    &! runoff_from_plt
                                sc%valsortie_mat(10),sep,                                    &! cep
                                sc%valsortie_flo(11),sep,sc%valsortie_mat(11),sep,          &! qnplante a flo et mat
                                sc%valsortie_mat(12),sep,                                   &! leaching_from_plt
                                sc%valsortie_mat(13),sep,                                   &! qngrain
                                sc%valsortie_mat(14),sep,                                   &! N_mineralisation
                                sc%valsortie_mat(15),sep,                                   &! N_volatilisation
                                sc%valsortie_mat(16),sep,                                   &! cum_immob
                                sc%valsortie_mat(17),sep,                                   &! soilN
                                sc%valsortie_mat(18),sep,                                   &! N_denitrification
                                sc%valsortie_mat(19)                                          ! cumrg,

 !979   format(a4,a1,a13,a1,a15,a1,a5,a1,i4,5(a1,f10.3),2(a1,i4),18(a1,f10.3))
979   format(a2,a1,(i4,a1,a2,a1,a2),a1,a2,a1, &
             f10.3,a1,                        &
             3((i4,a1,a2,a1,a2),a1),          &
             f10.0,20(a1,f10.3))

     endif
! DR 19/02/2016 pour ET
     if(ag%P_Flag_Agmip_rap.eq.8)then
      tiret ='-'
! DR 24/08/2018 suivant si c'est la calibration , l'analyse de sensi on a pas les memes premieres colonnes
! je lis ca dans le nom de l'usm
!   si calibration  =      Model; Site.name;   Experiment.name
!   si analyse de sensi avec climat changeant  = Model;  Site.code;   Weather.file
                 if(index(sc%P_usm,trim('CALIB')).ne.0)&
                      identifiant=sc%P_usm(8:11)//trim(sc%P_usm(27:30))
                 if(index(sc%P_usm,trim('SENSI')).ne.0)then
                      y=index(sc%P_usm,trim('_SENSI_'))
                      z=index(sc%P_usm,trim('_N'))
                      identifiant=sc%P_usm(8:11)//'_'//trim(sc%P_usm(y+7:z-1))
                 endif
                 if(index(sc%P_usm,trim('_BASELI_')).ne.0)then
                      identifiant=sc%P_usm(8:11)//'_'//trim('baseline')
                 endif
                 an_imat=sc%annee(p%imats)

!write(*,*)'identifiant ',identifiant
!write(*,*)'file_sortie ',p%ficrap_AgMIP

                  if(index(sc%P_usm,trim('CALIB')).ne.0)then
                  write(rap,945)trim(sc%model_name),sep , trim(sc%site_name),sep,  &
                            !    trim(sc%experiment_name), sep,                                &
                                trim(identifiant), sep,                                &
                                an_iplt,tiret,charmois_iplt,tiret,charjour_iplt,sep,          &! an semis, mois semis, jour semis
                                sc%valsortie_mat(1),sep,                                      &! mafruit
                                an_ilev,tiret,charmois_ilev,tiret,charjour_ilev,sep,          &! lev
                                an_iflo,tiret,charmois_iflo,tiret,charjour_iflo,sep,          &! flo
                                an_imat,tiret,charmois_imat,tiret,charjour_imat,sep,          &! mat
                                sc%valsortie_mat(2), sep,                                     &! chargefruit,
                                sc%valsortie_mat(3), sep,                                     &! pgrain
                                sc%valsortie_flo(4),sep,sc%valsortie_mat(4),sep,              &! masec a flo et mat
                                sc%valsortie_mat(5),sep,                                      &! laimax
                                sc%valsortie_rec(6),sep,                                      &! ces
                                sc%valsortie_mat(7),sep,                                      &! cet
                                sc%valsortie_mat(8),sep,                                      &! qnplante a mat
                                'na',sep,                                                   &! QPlante phosphore
                                sc%valsortie_mat(9),sep,                                      &! qngrain
                                sc%valsortie_mat(10),sep,                                     &! leaching_from_plt
                                sc%valsortie_mat(11),sep,                                     &! N_mineralisation
                                sc%valsortie_mat(12),sep,                                     &! N_volatilisation
                                sc%valsortie_mat(13),sep,                                     &! cum_immob
                                sc%valsortie_mat(14),sep,                                     &! N_denitrification
                                sc%valsortie_mat(15),sep,                                          & ! soilNM
                                ! la je mets les variables supplementaires qu'on enlevera apres pour le delivery
                                (sc%valsortie_mat(i),sep,i=17,sc%nbvarrap)

                  else
                   write(rap,946)trim(sc%model_name),sep , trim(sc%site_name),sep,  &
                            !    trim(sc%experiment_name), sep,                                &
                                trim(identifiant), sep,                                        &
                                sc%valsortie_mat(1),sep,                                       &! Ninput
                                an_imat,sep,                                                   &! annee
                                an_iplt,tiret,charmois_iplt,tiret,charjour_iplt,sep,          &! an semis, mois semis, jour semis
                                sc%valsortie_mat(2),sep,                                      &! mafruit
                                an_ilev,tiret,charmois_ilev,tiret,charjour_ilev,sep,          &! lev
                                an_iflo,tiret,charmois_iflo,tiret,charjour_iflo,sep,          &! flo
                                an_imat,tiret,charmois_imat,tiret,charjour_imat,sep,          &! mat
                                sc%valsortie_mat(3), sep,                                     &! chargefruit,
                                sc%valsortie_rec(4), sep,                                     &! pgrain
                                sc%valsortie_flo(5),sep,sc%valsortie_mat(5),sep,              &! masec a flo et mat
                                sc%valsortie_mat(6),sep,                                      &! laimax
                                sc%valsortie_rec(7),sep,                                      &! ces
                                sc%valsortie_mat(8),sep,                                      &! cet
                                sc%valsortie_mat(9),sep,                                      &! qnplante a mat
                                sc%valsortie_mat(10),sep,                                      &! qngrain
                                sc%valsortie_mat(11),sep,                                     &! leaching_from_plt
                                sc%valsortie_mat(12),sep,                                     &! N_mineralisation
                                sc%valsortie_mat(13),sep,                                     &! N_volatilisation
                                sc%valsortie_mat(14),sep,                                     &! cum_immob
                                sc%valsortie_mat(15),sep,                                     &! N_denitrification
                                sc%valsortie_mat(16),sep,                                    &! soilNM
                                ! la je mets les variables supplementaires qu'on enlevera apres pour le delivery
                                (sc%valsortie_mat(i),sep,i=18,sc%nbvarrap)
                   endif
 945   format(a2,a1,a4,a1,a10,a1,(i4,a1,a2,a1,a2),1(a1,f10.3),a1, 3((i4,a1,a2,a1,a2),a1), 8(f10.3,a1),a2,a1,30(f10.3,a1))
 946   format(a2,a1,a4,a1,a14,a1,f10.3,a1,i4,a1,(i4,a1,a2,a1,a2),1(a1,f10.3),a1, 3((i4,a1,a2,a1,a2),a1),30(f10.3,a1))

     endif
     

endif

return
end subroutine Ecriture_Rapport_agmip


subroutine determinate_sowing_Agmip(c,P_usm,ansemis,iwater,nplt,iplt,anit)
!c,sc%P_usm,sc%ansemis,sc%P_iwater,p(1)%P_iplt0,p(1)%nplt,sc%iplt(1)

! 26/01/2016 subroutine de calcul des regles de semis selon agmip wheat
! lecture des precinisation : de fenetre de semis , date de debut

USE Climat
  implicit none
  type(Climat_),              intent(INOUT) :: c

  character(len=20) , intent(IN) :: P_usm
  integer, intent(INOUT) :: ansemis

  integer, intent(INOUT) :: nplt
  integer, intent(INOUT) :: iplt
  integer, intent(INOUT) :: iwater
  real, intent(OUT) :: anit

integer i,nbjansemis,jj !,date_ini,date_fin
integer nplt_deb,j_deb, m_deb,j_fin,m_fin,nplt_fin
integer k_win,k !,nplt0
character(len=2) num_site
character(len=25) nom_site,pays
character(len=50) entete
real cum_rr,ferti_semis
integer  nbjParAnnee
integer :: funit

! getting a free unit
funit = getFreeFileUnit()

open(funit,file='sowing_windows.txt')
read(funit,*)entete
do i=1,31
   read(funit,*,end=100)num_site, nom_site, pays, j_deb, m_deb,j_fin,m_fin, ferti_semis
   write(*,*)num_site,nom_site
   jj=index(P_usm,trim(nom_site))
   if(jj.ne.0)goto 10
enddo
100 write(*,*)'site non trouve'
10 read(funit,*)
close(funit)
! pour le moment on ne lit que ca , je n'ai pas besin des infos annuelles ???
!on calcule le jour de deb dans le calendrier stics
  call NDATE (j_deb,m_deb,ansemis,nplt_deb)
  call NDATE (j_fin,m_fin,ansemis,nplt_fin)
  nbjansemis=nbjParAnnee(ansemis)
  if(nplt_fin.lt.nplt_deb)nplt_fin=nplt_fin+nbjansemis
! on doit faire un cumul glissant sur les 5 jours precedant le semis potentiel
do k_win=nplt_deb,nplt_fin
cum_rr=0.
do k=k_win-5,k_win-1
     cum_rr=cum_rr+c%ttrr(k)
     write(1502,*)P_usm,ansemis,'deb', j_deb,m_deb,'fin',j_fin,m_fin,'deb cumul',k_win,k,cum_rr
enddo
! on a trouve la date dans la windows, on seme a cette date
if(cum_rr.ge.10)then
    iplt=k_win
    goto 200
endif
enddo
! on a pas trouve de date dans la windows, on seme a la date de fin
iplt=k_win-1

200 continue

!  sc%P_iwater est a recalculer , 10 jours avant le smeis preconise
!DR 05/02/2015 pour le moment Giacomo precnise de demarrer 10 jours avant la premiere date de semis de la fenetre
!iwater=iplt-10
nplt=iplt-iwater+1
anit=ferti_semis
write(1502,*)'semis realise',iplt,nplt,cum_rr

return
end subroutine determinate_sowing_Agmip




end module Module_AgMIP
