

module Files_test
  use fruit
  use Files

  implicit none

  contains
  
  
  subroutine test_assert_file_debug_activation
     implicit none
     integer :: status

     ! testing default value
     call assert_equals (getDebug(), .FALSE.)
     ! testing debug enabled
     call setDebugOn()
     call assert_equals (getDebug(), .TRUE.)
     ! testing debug disabled
     call setDebugOff()
     call assert_equals (getDebug(), .FALSE.)
  end subroutine test_assert_file_debug_activation
  
  
  
  subroutine test_assert_file_constructor
     ! different cases with different lists of arguments
     ! USE named arguments !!!
     !
  end subroutine test_assert_file_constructor
  
  
  
  subroutine test_assert_file_exists
    implicit none
    type(File_) :: init_file,test_file
    
    ! empty init file
    init_file=File()
    call assert_equals (existFile(init_file), .FALSE.)
    
    ! non existing file
    test_file=File(a_file='nofile.txt')
    call assert_equals (existFile(test_file), .FALSE.)
    
    ! existing file
    test_file=File('./data/test.txt')
    call assert_equals (existFile(test_file), .TRUE.)
  
  end subroutine test_assert_file_exists
    
    
    
  subroutine test_assert_empty_file_size
    implicit none
    type(File_) :: test_file
    character(len=files_path_length) :: file_path
    integer :: fileisopen,fileisclosed,filesize
    file_path='./data/test.txt'
    ! testing for file
    test_file=File(file_path)
    ! open file
    fileisopen=openFile(test_file)
    ! file size
    call assert_equals (getFileSize(test_file),0)
    !print *, filesize
    
    ! closed file
    fileisclosed=closeFile(test_file)
    ! file size
    call assert_equals (getFileSize(test_file),0)
    !print *, filesize
    
    ! testing for file path
    ! open file
    fileisopen=openFile(file_path)
    ! file size
    call assert_equals (getFileSize(file_path),0)
    !print *, filesize
    
    ! closed file
    fileisclosed=closeFile(file_path)
    ! file size
    call assert_equals (getFileSize(file_path),0)
    !print *, filesize
  
  end subroutine test_assert_empty_file_size
  
  
  
  subroutine test_assert_not_empty_file_size
    implicit none
    type(File_) :: test_file
    character(len=files_path_length) :: file_path
    integer :: fileisopen,fileisclosed,filesize
    file_path='./data/test_not_empty.txt'
    ! testing for file
    test_file=File(file_path)
    ! open file
    fileisopen=openFile(test_file)
    ! file size
    call assert_equals (getFileSize(test_file),2)
    !print *, filesize
    
    ! closed file
    fileisclosed=closeFile(test_file)
    ! file size
    call assert_equals (getFileSize(test_file),2)
    !print *, filesize
    
    ! testing for file path
    ! open file
    fileisopen=openFile(file_path)
    ! file size
    call assert_equals (getFileSize(file_path),2)
    !print *, filesize
    
    ! closed file
    fileisclosed=closeFile(file_path)
    ! file size
    call assert_equals (getFileSize(file_path),2)
    !print *, filesize
  
  end subroutine test_assert_not_empty_file_size



  subroutine test_assert_file_isempty
  
    implicit none
    type(File_) :: test_file
    test_file=File('./data/test.txt')
    !call dispFile(test_file)
    call assert_equals (isemptyFile(test_file), .TRUE.)
    test_file=File('./data/test_not_empty.txt')
    !call dispFile(test_file)
    call assert_equals (isemptyFile(test_file), .FALSE.)
  end subroutine test_assert_file_isempty
  
  
  
  subroutine test_assert_existing_file_open
    !character(len=30) :: char_file
    type(File_) :: test_file
    logical :: file_open
    integer :: is_open
    
    ! NO fileunit set : -1
    test_file=File('./data/test.txt')
    call assert_equals (isOpen(test_file), .FALSE.)
    ! open status 0 (file is open), but no fileunit, status != 0
    call assert_not_equals (openFile(test_file), 0)
    ! testing if is open
    call assert_equals (isOpen(test_file), .FALSE.)
    
    ! closing file
    call assert_equals (closeFile(test_file), 0)
    ! testing if is open
    call assert_equals (isOpen(test_file), .FALSE.)
    
    ! setting fileunit to 100
    !test_file=File('./data/test.txt',100,'O')
    test_file=File('./data/test.txt',100)
    !test_file=File(char_file)

    call assert_equals (isOpen(test_file), .FALSE.)
    ! open status 0 
    call assert_equals (openFile(test_file), 0)
    ! testing if is open
    call assert_equals (isOpen(test_file), .TRUE.)
    
    ! closing file
    call assert_equals (closeFile(test_file), 0)
    ! testing if is open
    call assert_equals (isOpen(test_file), .FALSE.)
    
    ! for displaying test_file content
    !files_debug=.TRUE.
    !call dispFile(test_file)
  
  end subroutine test_assert_existing_file_open
  
  
  
  !   
  ! set functions --------------------------------------------------- !
  ! const
  subroutine test_assert_set_file_const
    implicit none
    type(File_) :: init_file,test_file
    ! empty init file
    init_file=File()
    call assert_equals (init_file%const,.TRUE.)
    init_file=setconst(init_file,.FALSE.)
    call assert_equals (init_file%const,.FALSE.)
    init_file=setconst(init_file,.TRUE.)
    call assert_equals (init_file%const,.TRUE.)
    
    ! ADD SAME TESTS WITH getconst
    
    
    ! other file
    
  end subroutine test_assert_set_file_const
  
  
  
  ! tag
  subroutine test_assert_set_file_tag
    implicit none
    type(File_) :: init_file,test_file
    type(varying_string) :: tag
    character(len=files_path_length) :: loc_tag,res_tag
    ! empty init file
    init_file=File()
    tag=''
    loc_tag=init_file%tag
    res_tag=tag
    call assert_equals (loc_tag,res_tag)
    tag='test_tag'
    init_file=settag(init_file,tag)
    loc_tag=init_file%tag
    res_tag=tag
    call assert_equals (loc_tag,res_tag)
    ! ADD SAME TESTS WITH gettag
    
    ! other file: tag not empty
    
  end subroutine test_assert_set_file_tag
  
  
  
  ! prefix
  subroutine test_assert_set_file_prefix
    implicit none
    type(File_) :: init_file,test_file
    type(varying_string) :: prefix
    character(len=files_path_length) :: loc_prefix,res_prefix
    ! empty init file
    init_file=File()
    prefix=''
    loc_prefix=init_file%prefix
    res_prefix=prefix
    call assert_equals (loc_prefix,res_prefix)
    ! verif const
    call assert_equals (init_file%const,.TRUE.)
    ! prefix non vide
    prefix='test_prefix'
    init_file=setprefix(init_file,prefix)
    loc_prefix=init_file%prefix
    res_prefix=prefix
    call assert_equals (loc_prefix,res_prefix)
    ! verif const
    call assert_equals (init_file%const,.FALSE.)
    
    ! ADD SAME TESTS WITH getprefix
    
    ! other file: prefix not empty
    
  end subroutine test_assert_set_file_prefix
  
  
  
   ! ext
  subroutine test_assert_set_file_ext
    implicit none
    type(File_) :: init_file,test_file
    type(varying_string) :: ext
    character(len=files_path_length) :: loc_ext,res_ext
    ! empty init file
    init_file=File()
    ! empty ext
    ext=''
    loc_ext=init_file%ext
    res_ext=ext
    call assert_equals (loc_ext,res_ext)
    ! without .
    ext='ext'
    init_file=setext(init_file,ext)
    loc_ext=init_file%ext
    res_ext='.'//ext
    call assert_equals (loc_ext,res_ext)
    ! with .
    ext='.ext'
    init_file=setext(init_file,ext)
    loc_ext=init_file%ext
    res_ext=ext
    call assert_equals (loc_ext,res_ext)
    
    ! ADD SAME TESTS WITH getext
    
    ! other file: ext not empty
    
  end subroutine test_assert_set_file_ext 
  
  
  
  ! varname
  
  
  
  ! path
  
  
  
  ! desc
  subroutine test_assert_set_file_desc
    implicit none
    type(File_) :: init_file,test_file
    type(varying_string) :: desc
    character(len=files_path_length) :: loc_desc,res_desc
    ! empty init file
    init_file=File()
    desc='No available description'
    loc_desc=init_file%desc
    res_desc=desc
    call assert_equals (loc_desc,res_desc)
    desc='test_desc'
    init_file=setdesc(init_file,desc)
    loc_desc=init_file%desc
    res_desc=desc
    call assert_equals (loc_desc,res_desc)
    ! ADD SAME TESTS WITH getdesc
    
    ! other file: desc not empty
    
  end subroutine test_assert_set_file_desc
  
  
  
  ! unit 
  subroutine test_assert_set_file_unit
    implicit none
    type(File_) :: init_file,test_file
   
    ! empty init file
    init_file=File()
    call assert_equals (init_file%unit,-1)
    init_file=setunit(init_file,50)
    call assert_equals (init_file%unit,50)
    
    ! ADD SAME TESTS WITH getunit
    
    
    ! other file
    
    
  end subroutine test_assert_set_file_unit
  
  
  
  ! status    
  
  
  
  ! header
  
  
  
  ! sep
  
  
  
  ! action
  
  
  
  ! position
  
  
  
  ! format
  subroutine test_assert_set_file_format
    implicit none
    type(File_) :: init_file,test_file
    type(varying_string) :: format
    character(len=files_path_length) :: loc_format,res_format
    ! empty init file
    init_file=File()
    format='*'
    loc_format=init_file%format
    res_format=format
    call assert_equals (loc_format,res_format)
    format='test_format'
    init_file=setformat(init_file,format)
    loc_format=init_file%format
    res_format=format
    call assert_equals (loc_format,res_format)
    ! ADD SAME TESTS WITH getformat
    
    ! other file: format not empty
    
  end subroutine test_assert_set_file_format
  
  
  
  ! end set functions ----------------------------------------------- !
  
  
  
  
  ! get functions --------------------------------------------------- !
  subroutine test_assert_get_file_name
    implicit none
    type(File_) :: init_file,test_file
    character(len=30) :: a_file
    
    ! empty init file
    init_file=File()
    call assert_equals (getFileName(init_file), '')
    
    call dispFile(init_file)
    
    ! constructor, no dir
    a_file='test.txt'
    test_file=File(a_file)
    call assert_equals (getFileName(test_file), 'test.txt')
    print *, getFileName(test_file)
    
    ! constructor,  dir name, unix like
    a_file='./toto/test.txt'
    test_file=File(a_file)
    call assert_equals (getFileName(test_file), 'test.txt')
    print *, getFileName(test_file)
    ! constructor,  dir name, win
    a_file='.\toto\test.txt'
    test_file=File(a_file)
    call assert_equals (getFileName(test_file), 'test.txt')
    print *, getFileName(test_file)
    
    ! constructor, no dir, no ext
    a_file='test'
    test_file=File(a_file)
    call assert_equals (getFileName(test_file), 'test')
    print *, getFileName(test_file)
    ! constructor,  dir name, unix like
    a_file='./toto/test'
    test_file=File(a_file)
    call assert_equals (getFileName(test_file), 'test')
    print *, getFileName(test_file)
    
    ! constructor,  dir name, win
    a_file='.\toto\test'
    test_file=File(a_file)
    call assert_equals (getFileName(test_file), 'test')
    print *, getFileName(test_file)
 
  end subroutine test_assert_get_file_name
 
 
 
  subroutine test_assert_get_file_dir_name
  
  end subroutine test_assert_get_file_dir_name



  subroutine test_assert_get_dir_name
  
  end subroutine test_assert_get_dir_name


  
  ! end get functions ----------------------------------------------- !
  
  
  

end module Files_test
