
program Files_test_driver

  use fruit
  use SticsSystem
  use Files_test
  use Files
  integer :: failed_counts
  
    ! init cmd System structure
    call init(sys_cmd)
    !print *, 'command copy from system struct: ',sys_cmd%copy

    call init_fruit

    ! debug mode activation/desactivation
    ! call test_assert_file_debug_activation
    
    ! testing if file exists
    ! call test_assert_file_exists
    
    ! testing if file is empty
    ! call test_assert_file_isempty
    
    ! testing empty file stat
    !call test_assert_empty_file_size
    
    ! not empty file
    !call test_assert_not_empty_file_size
    
    ! testing empty file structure
    ! call test_assert_get_file_name
    
    ! testing opening/open file
    ! call test_assert_existing_file_open

    ! ---------------------------------------------------------------- !
    ! testing set functions
    ! ---------------------------------------------------------------- !
    ! const
    ! call test_assert_set_file_const
    ! tag
    ! call test_assert_set_file_tag
    ! prefix
    ! call test_assert_set_file_prefix
    ! ext
    !call test_assert_set_file_ext
    
    ! varname
    
    
    !path
    
    !desc
    !call test_assert_set_file_desc
    
    ! unit
    !call test_assert_set_file_unit

    ! status    
  
    ! header
  
  
    ! sep
  
  
    ! action
  
  
    ! position
      
    ! format
    ! call test_assert_set_file_format
    
    ! ---------------------------------------------------------------- !
    ! testing get functions
    ! ---------------------------------------------------------------- !
    
    
    

    call fruit_summary

    call fruit_finalize

  call get_failed_count (failed_counts)  
  
  print *,''
  print *, 'Failed ', failed_counts

  if (failed_counts.GT.0) call exit(2)

end program Files_test_driver
