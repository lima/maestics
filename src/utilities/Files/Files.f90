module Files

  USE iso_varying_string
  USE SticsSystem

  implicit none
  
  ! file path length
  integer, parameter :: files_path_length = 255
  
  ! file position
  character(len=10) :: append='append'
  character(len=10) :: asis='asis'

  
  ! file actions
  character(len=10) :: write='write'
  character(len=10) :: readwrite='readwrite'
  character(len=10) :: read='read'
  
  ! file status
  character(len=10) :: old='O'
  character(len=10) :: unknown='U'
  character(len=10) :: new='N'
  character(len=10) :: replac='R'
  character(len=10) :: scratch='S'
  
  ! set it to .TRUE. for displaying messages, file content
  ! using setDebugOn()
  ! set it to .FALSE. otherwise with setDebugOff()
  logical, save :: files_debug = .FALSE.


  ! Structure for storing informations about a file
  ! Getting informations dynamically through
  ! a function using *stat fortran fonctions.
  type File_Stat_
      integer :: size
      integer :: last_access
      integer :: last_modification
      integer :: last_change
  end type File_Stat_


  ! Informations structure about a file
  ! setting/getting fields values through
  ! functions/subroutines included in this module
  type File_
      
      ! fixed: logical for telling if the file
      ! has a fixed name (.TRUE.) or variable name (.FALSE.) 
      logical :: fixed
      
      ! tag: tag for identifying the file
      type(varying_string) :: tag
      
      !prefix: file name begins with it
      type(varying_string) :: prefix
      
      ! ext: file extension (.ext)
      type(varying_string) :: ext
      
      ! name 
      type(varying_string) :: name

      ! variable part of name
      ! name = prefix + 
      type(varying_string) :: varname
      
      ! file path string
      type(varying_string) :: path
      
      ! file description string
      type(varying_string) :: desc
      
      ! file descriptor
      integer :: unit
      
      ! status : 'old', 'unknown','new','replace','scratch'
      character(len=30) :: status
      !
      ! file header
      type(varying_string) :: header
      
      ! file sep
      character(len=1) :: sep
      
      ! 'read', 'write', 'rewrite'
      character(len=30) :: action
      
      ! 'append','rewind'
      character(len=30) :: position
      
      ! file format for reading,writing
      type(varying_string) :: format

      ! open file status
      logical :: open

      ! file locking status, for preventing modifying fields !
      logical :: lock

      ! ??? usefull ???
      ! file stat (size, last_access, last_modification, last_change)
      ! type(File_Stat_) :: stat
      ! 
      ! TODO: to be added only if File_ structure is returned by functions !!!
      ! add open status, close status
      ! file error / message ????
  end type File_

  
  ! structure initialization
  interface init
     module procedure init_File
  end interface init


  ! Interface for File_ structure instanciation 
  ! TODO : see solution with optional args!!!!
  interface File
     module procedure init_genFile,file_genFile,usapf_genFile!,path_genFile
  end interface File

  
  ! Interface for testing if file exists  
  interface existFile
     module procedure file_existFile,c_path_existFile,vs_path_existFile
  end interface existFile

  ! Interface for testing if dir exists  
  !interface existDir
  !   module procedure file_existFile,c_path_existFile
  !end interface existDir
  
  
  ! Interface for testing if file is empty
  interface isemptyFile
     module procedure file_isFileEmpty,path_isFileEmpty!,pathvar_isFileEmpty
  end interface isemptyFile

  
  ! Interface for getting file stat
  interface getFileStat
     module procedure file_getFileStat,path_getFileStat!,pathvar_getFileStat
  end interface getFileStat


  ! Interface for getting file size
  interface getFileSize
     module procedure file_getFileSize,path_getFileSize!,pathvar_getFileSize
  end interface getFileSize
  
  
  ! Interface for extracting file name from path or File_ type
  interface getname
      module procedure  file_getFileName,c_path_getFileName,vs_path_getFileName
  end interface getname
  
  interface getFileTag
      module procedure  file_getFileTag
  end interface getFileTag
  
  ! Interface for extracting dir name from path or File_ type
  interface getDirName
      module procedure  file_getDirName,c_path_getDirName,vs_path_getDirName
  end interface getDirName  
  
  ! interface for setname
  interface setname
      module procedure  c_setname,vs_setname
  end interface setname

  ! interface for setdir
  interface setdir
      module procedure  c_setdir,vs_setdir
  end interface setdir

  ! interfaces for setdesc
  interface setdesc
      module procedure  c_setdesc,vs_setdesc
  end interface setdesc
  
  ! interfaces for setpath
  interface setpath
      module procedure  c_setpath,vs_setpath
  end interface setpath


  !setext, setformat, setprefix, setvarname
  
  ! interfaces for getdesc, getext, getformat, getprefix, getvarname
  
  
 ! interface for opening file
 interface openFile
      module procedure file_openFile, path_openFile
  end interface openFile


 ! interface for closing file
 interface closeFile
      module procedure file_closeFile, path_closeFile, unit_closeFile
  end interface closeFile


  ! interface for testing if file is opened
  ! pathvar_isOpen ?
  interface isOpen
     module procedure file_isOpen, path_isOpen, unit_isOpen
  end interface isOpen
   
   
  interface deleteFile
     module procedure file_deleteFile, vs_path_deleteFile, c_path_deleteFile
  end interface deleteFile
  
  
  
  !interface file_moveFile
  ! module procedure ...
  !end interface file_moveFile
  
  
  
  contains


  ! Activation of debug mode
  subroutine setDebugOn()
     files_debug = .TRUE.
  end subroutine
  
  
  ! Deactivation of debug mode
  subroutine setDebugOff()
     files_debug = .FALSE.
  end subroutine
  
  
  ! Getting files_debug content
  function getDebug()
     implicit none
     logical :: getDebug
     getDebug = files_debug
  end function getDebug
  
  
  ! Displaying file infos
  subroutine dispFile(a_file, in_force, in_stop)
    implicit none
    type(File_), intent(IN) :: a_file
    logical, intent(IN), optional :: in_force
    logical, intent(IN), optional :: in_stop
    type(File_Stat_) :: statstruct
    character(len=files_path_length) :: path
    logical :: file_open,file_exists
    logical :: force, loc_stop

    character(len=4) unit_str
    character(len=1) is_open,exists,fixed, locked
    is_open='F'
    exists='F'
    fixed='F'
    locked = 'F'
    force = .FALSE.
    loc_stop = .FALSE.

    ! setting force with input arg
    if (present(in_force)) force = in_force
    if (present(in_stop)) loc_stop = in_stop
    
    file_open=isOpen(a_file)
    file_exists=existFile(a_file)
    
    if (file_open) is_open='T'
    if (file_exists) exists='T'
    if (a_file%fixed) fixed='T'
    if (a_file%lock) locked = 'T'
    
    path=a_file%path

    ! if debug mode activated or forced with input 
    if (files_debug.OR.force) then

      ! displaying file infos
        write(*,*) '----------------------------------------'
        write(*,*) 'File infos: '
        write(*,*) 'File description: '//trim(char(a_file%desc))
        write(*,*) '----------------------------------------'
        write(*,*) 'name: '//trim(getname(a_file))
        !write(*,*) 'name: '//trim(char(a_file%name))
        write(*,*) 'tag: '//trim(char(a_file%tag))
        write(*,*) 'prefix: '//trim(char(a_file%prefix))
        write(*,*) 'var name: '//trim(char(a_file%varname))
        write(*,*) 'ext: '//trim(char(a_file%ext))
        write(*,*) 'path: '//trim(path)
        write(*,*) 'status: '//trim(a_file%status)
        write(*,*) 'action: '//trim(a_file%action)
        write(*,*) 'position: '//trim(a_file%position)
        write(*,*) 'format: '//trim(char(a_file%format))
        write(*,*) 'header: '//trim(char(a_file%header))
        write(*,*) 'sep: '//'"'//a_file%sep//'"'
        !
        write(unit_str,'(I4)') a_file%unit
        write(*,*) 'unit: '//trim(unit_str)
        write(*,*) 'exists : '//exists
        write(*,*) 'is open: '//is_open
        write(*,*) 'is constant file name: '//fixed
        write(*,*) 'is locked: '//locked
        write(*,*) '----------------------------------------'
    
        ! File stat is being displayed only if exists and 
        ! if is not a directory
        if ((file_exists).AND.(.NOT.isdir(path))) then
          ! displaying file stats
          statstruct=getFileStat(a_file)
          call dispFileStat(statstruct)
        end if 

        if ( loc_stop ) then
          print *, "Forcing program stop ..."
          stop
        end if
    
    endif
    
    return
  end subroutine dispFile


  subroutine dispFileStat(statstruct)
    implicit none
     type(File_Stat_), intent(OUT) :: statstruct
     !print *, statstruct%size
     !print *, statstruct%last_access
     !print *, statstruct%last_modification
     !print *, statstruct%last_change
     WRITE (*, FMT="('File size:',               T30, I19)") statstruct%size
     WRITE (*, FMT="('Last access time:',        T30, A19)") ctime(statstruct%last_access)
     WRITE (*, FMT="('Last modification time',   T30, A19)") ctime(statstruct%last_modification)
     WRITE (*, FMT="('Last status change time:', T30, A19)") ctime(statstruct%last_change)
  end subroutine dispFileStat
  
    
  ! File structure initialization
  subroutine init_File(filestruct)
    implicit none
    type(File_), intent(INOUT) :: filestruct
    filestruct%fixed=.TRUE.
    filestruct%tag=''
    filestruct%prefix=''
    filestruct%ext=''
    filestruct%name=''
    filestruct%varname=''
    filestruct%path=''
    filestruct%desc='No available description'
    ! default 
    filestruct%unit=-1
    ! file status
    filestruct%status='unknown'
    filestruct%header=''
    filestruct%sep=' '
    filestruct%action='readwrite'
    filestruct%position='asis'
    filestruct%format='*'
    filestruct%open = .FALSE.
    filestruct%lock = .FALSE.
    
    ! init cmd System structure
     call init(sys_cmd)
   
    return
  end subroutine init_File
  
  
  subroutine init_File_Stat(statstruct)
    implicit none
    type(File_Stat_), intent(INOUT) :: statstruct
        statstruct%size=-1
        statstruct%last_access=-1
        statstruct%last_modification=-1
        statstruct%last_change=-1
    return
  end subroutine init_File_Stat
  
  
  ! File: File as empty initialized File
  function init_genFile()
    implicit none
    type(File_) :: init_genFile
    call init(init_genFile)
    return
  end function init_genFile
  
  
  ! File: File from existing File
  function file_genFile(a_file)
    implicit none
    type(File_) :: file_genFile,a_file
    file_genFile=a_file
    return
  end function file_genFile
  
  
  ! File: path
  !function path_genFile(a_file)
  !  implicit none
  !  type(File_) :: path_genFile,loc_file
  !  character(len=*) , intent(IN) :: a_file

   ! call init(loc_file)
    
   ! loc_file%path=trim(a_file)
   ! path_genFile=loc_file
   ! return
  !end function path_genFile

  ! File: path,file unit, status,action, position,format
  !function u_genFile(a_file,in_unit)
   ! implicit none
    !type(File_) :: u_genFile
    !character(len=*) , intent(IN) :: a_file
    !integer, intent(IN) :: in_unit
    
    !u_genFile=usapf_genFile(a_file,in_unit)
  !end function u_genFile
    
    
  ! File: path,file unit, status,action, position,format
  function usapf_genFile(a_file,in_unit,in_status,in_action,in_position, &
    in_format,in_tag,in_prefix, in_ext, in_lock)
    implicit none
    type(File_) :: usapf_genFile,loc_file
    character(len=*) , intent(IN) :: a_file
    integer, intent(IN), optional :: in_unit
    character(len=1), optional, intent(IN)  :: in_status
    character(len=10), optional, intent(IN)   :: in_action
    character(len=10), optional, intent(IN)   :: in_position
    type(varying_string) , optional, intent(IN)  :: in_format
    !type(varying_string) , optional, intent(IN)   :: in_tag
    character(len=*), optional, intent(IN)   :: in_tag
    character(len=*) , optional, intent(IN)  :: in_prefix
    character(len=*) , optional, intent(IN)  :: in_ext
    logical , optional, intent(IN)  :: in_lock

    character(len=50) :: status, action,position,tag
    type(varying_string) :: prefix,ext
    integer :: point_idx, set_name
    
    ! init and path
    call init(loc_file)
    loc_file%path=trim(a_file)

    ! managing locking status (default : .FALSE.)
    if (present(in_lock)) then
      loc_file%lock = in_lock
    end if

    if ((present(in_unit)).AND.(.NOT.isUnitUsed(in_unit))) then
       loc_file%unit=in_unit
    else
       loc_file%unit=getFreeFileUnit()
       ! print *, loc_file%unit
    endif
    
    !print *," file unit for ",trim(in_tag)," ", loc_file%unit

    ! TODO: see if a function calc_ext and set_ext may be usefull !
    ! searching backward!
    point_idx=index(a_file,'.',.TRUE.)
    if(point_idx>1) then
     ! print *, 'extension found ...',point_idx
     loc_file%ext=a_file(point_idx:len_trim(a_file))
    endif
    ! -----------------------------------------------------
    
    ! default status
    status='unknown'
    ! selecting file status
    if (present(in_status)) then 
       select case (in_status)
          case('O')
             status='old'
          case('N')
             status='new'
          case('U')
             status='unknown'
          case('R')
             status='replace'
          case ('S')
             status='scratch'
          case default
             status='unknown'
       end select
    endif
    
    ! default action 
    action='read'
    ! selecting file action
    if (present(in_action)) then
        select case (in_action)
           case('read')
              action='read'
           case('write')
              action='write'
           case('readwrite')
              action='readwrite'
           case default 
              action='readwrite'
        end select
    endif
    
    ! default position
    position='asis'
    ! selecting file position
    if (present(in_position)) then
        select case (in_position)
           case('append')
              position='append'
           case('asis')
              position='asis'
           case ('rewind')
              position='rewind'
     case default 
              position='asis'
     end select     
     endif 
     
     
     if (present(in_format)) then
        loc_file%format=in_format
     endif
     
    loc_file%status=trim(status)
    loc_file%action=trim(action)
    loc_file%position=trim(position)

    tag = 'na'
    if (present(in_tag)) then
      tag = in_tag
    end if
    loc_file%tag = trim(tag)

    ! TODO: set fixed if no ext in path !!!!
    ! or if isdir(path) !!!
    if (present(in_prefix)) then
      prefix = trim(in_prefix)
      loc_file%prefix = prefix
      loc_file%fixed = .FALSE.
    end if
    
    if (present(in_ext)) then
      ext = trim(in_ext)
      loc_file = setext(loc_file,ext)
      !loc_file%ext = in_ext
    end if

    ! setting file name
    ! TODO: using set_name return value
    call setname(loc_file,getname(loc_file),set_name)

    ! adding setting open status
    if (existFile(loc_file).AND.isOpen(loc_file)) then
      loc_file = setopen(loc_file, .TRUE.)
    end if

    usapf_genFile=loc_file
    ! call dispFile(loc_file,.TRUE.) 
    !stop
    return
  end function usapf_genFile
  
  
  ! set functions --------------------------------------------------- !
  ! set field for varying string type
  function vs_setFile(a_file,field,value)
     implicit none
     type(File_), intent(IN) :: a_file
     type(varying_string), intent(IN) :: value,field
     type(File_) :: vs_setFile,loc_file
     character(len=10) :: loc_field
     integer :: idx
     loc_file=a_file
     loc_field=field

     if ( a_file%lock ) then
      print *, "Changing properties in File structure is locked for : "//char(a_file%path)
      vs_setFile = loc_file
      return
     end if
     
     select case (trim(loc_field))
           case('name')
              loc_file%name = value
           case('tag')
              loc_file%tag = value
              !print *, "tag : ", char(loc_file%tag)
           case('prefix')
              loc_file%fixed=.FALSE.
              loc_file%prefix = value
              ! si prefix == '', fixed = .TRUE.
              if ( len(value) == 0 ) then
                 loc_file%fixed=.TRUE.
              endif
           case ('ext')
              idx = index(value,'.')
              ! si extension non vide et sans .
              if ( idx == 0 .AND. len_trim(value) > 0) then
                 loc_file%ext = '.'//value
              ! si extension comporte un .
              else if ( idx == 1 ) then
                 loc_file%ext = value
              ! last case > 1, erreur !
              endif
              ! case if length loc_ext > 4 erreur !
           case ('varname')
              loc_file%fixed=.FALSE.
              loc_file%varname = trim(value)
              if ( len(value) == 0 ) then
                 loc_file%fixed=.TRUE.
              endif
           case ('path')
              loc_file%path = value
              ! to be added : check file dir / parent dir ???
           case ('desc')
              loc_file%desc = value
           case ('format')
              loc_file%format = value
              ! add checking if value is a file format
              !
           case ('header')
              loc_file%header = value
           case ('sep')
              loc_file%sep = value
           case ('status')
              loc_file%status = value
           case ('action')
              loc_file%action = value
           case ('position')
              loc_file%position = value
           
     end select     
     
     
     vs_setFile=loc_file
     return
  end function vs_setFile
  
     
  ! set field for integer type    
  function int_setFile(a_file,field,value)
     implicit none
     type(File_), intent(IN) :: a_file
     type(varying_string), intent(IN) :: field
     integer ,intent(IN) :: value
     type(File_) :: int_setFile,loc_file
     character(len=10) :: loc_field
     loc_file=a_file
     loc_field=field
     
     select case (trim(loc_field))
         case ('unit')
              loc_file%unit = value
     end select
     int_setFile=loc_file
     return
  end function int_setFile
  
  
  ! set field for logical type   
  function logic_setFile(a_file,field,value)
     implicit none
     type(File_), intent(IN) :: a_file
     type(varying_string), intent(IN) :: field
     logical ,intent(IN) :: value
     type(File_) :: logic_setFile,loc_file
     character(len=10) :: loc_field
     loc_file=a_file
     loc_field=field
     
     select case (trim(loc_field))
         case ('fixed')
            loc_file%fixed = value
         case ('open')
            loc_file%open = value
     end select
     logic_setFile=loc_file
     return
  end function logic_setFile
  
  
  ! setter functions
  !
  ! fixed
  function setfixed(a_file,value)
     implicit none
     type(File_), intent(IN) :: a_file
     logical ,intent(IN) :: value
     type(File_) :: setfixed
     type(varying_string) :: field
     field = 'fixed'
     setfixed=logic_setFile(a_file,field,value)
     return
  end function setfixed
  
  
  ! tag
  function settag(a_file,file_tag)
     implicit none
     type(File_), intent(IN) :: a_file
     type(varying_string), intent(IN) :: file_tag
     type(File_) :: settag!, loc_file
     type(varying_string) :: field
     field = 'tag'
     settag=vs_setFile(a_file,field,file_tag)
     return
  end function settag
  
  
  ! prefix
  function setprefix(a_file,file_prefix)
     implicit none
     type(File_), intent(IN) :: a_file
     type(varying_string), intent(IN)  :: file_prefix
     type(File_) :: setprefix!, loc_file
     type(varying_string) :: field
     field = 'prefix'
     setprefix=vs_setFile(a_file,field,file_prefix)
     return
  end function setprefix 
  
  
  ! ext
  function setext(a_file,file_ext)
     implicit none
     type(File_), intent(IN) :: a_file
     type(varying_string) :: file_ext
     type(File_) :: setext
     type(varying_string) :: field
     integer :: idx
     field = 'ext'
     idx=index(file_ext,'.',.TRUE.)
     if (idx == 0) file_ext = '.'//file_ext
     setext=vs_setFile(a_file,field,file_ext)
     return
  end function setext 
  
  
  ! varname
  function setvarname(a_file,file_varname)
     implicit none
     type(File_), intent(IN) :: a_file
     type(varying_string), intent(IN) :: file_varname
     type(File_) :: setvarname
     type(varying_string) :: field
     field = 'varname'
     setvarname=vs_setFile(a_file,field,file_varname)
     return
  end function setvarname 
  
  ! name
  subroutine c_setname(a_file,file_name, set_name)
    implicit none
    type(File_), intent(INOUT) :: a_file
    character(len=*), intent(IN) :: file_name
    integer, intent(OUT) :: set_name
    !type(File_) :: c_setname
    type(varying_string) :: loc_name
    loc_name = var_str(file_name)
    !c_setname = vs_setname(a_file,loc_name)
    call vs_setname(a_file,loc_name, set_name)
  end subroutine c_setname

  subroutine vs_setname(a_file,file_name, set_name)
    implicit none
    type(File_), intent(INOUT) :: a_file
    type(varying_string), intent(IN) :: file_name
    integer, intent(OUT) :: set_name
    type(varying_string) :: field, loc_name, loc_path

    set_name = 1

    field = 'name'
    if (a_file%fixed) then
      a_file=vs_setFile(a_file,field,file_name)
      ! update path
      loc_path = trim(getDirName(a_file))//filesep()//trim(file_name)
      call setpath(a_file,loc_path)
      set_name = 0
      return
    end if 
    ! treating a variable file name
    ! assumption that a file name is composed of a name and an
    ! extension
    if (index(file_name,".") > 0) then
      print *, "Error when setting the variable part of the file name: ",char(file_name)
      print *, "Containing an extension, only the trailer part must be given"
      print *, "to be added after the prefix: ", char(a_file%prefix) 
      ! call exit(9)
      return
    end if
   
    a_file = setvarname(a_file,trim(file_name))
    loc_name = trim(a_file%prefix)//trim(a_file%varname)//trim(a_file%ext)
    !print *, "a_file: loc_name => ",char(loc_name)
    a_file = vs_setFile(a_file,field,trim(loc_name))

    ! setting path 
    loc_path = trim(getDirName(a_file))//filesep()//trim(loc_name)
    !print *, "vs_setname: loc_path ",char(loc_path)

    call setpath(a_file,loc_path)

    call dispFile(a_file)
    set_name = 0

  end subroutine vs_setname
  

  ! setting path for char
  subroutine c_setpath(a_file,file_path)
    implicit none
    type(File_), intent(INOUT) :: a_file
    character(len=*), intent(IN) :: file_path
    type(varying_string) :: loc_path
    loc_path = var_str(file_path)
    call vs_setpath(a_file,loc_path)!, set_desc)

    return
  end subroutine c_setpath 

  ! setting path for vs
  subroutine vs_setpath(a_file,file_path)
     implicit none
     type(File_), intent(INOUT) :: a_file
     type(varying_string), intent(IN)  :: file_path
     type(varying_string) :: field
     field = 'path'
     a_file=vs_setFile(a_file,field,trim(file_path))
     !print *, "Dans set path: ",char(a_file%path)
     !call dispFile(a_file)
     return
  end subroutine vs_setpath 
  
  
  ! setting description for char
  subroutine c_setdesc(a_file,file_desc)!, set_desc)
    implicit none
    type(File_), intent(INOUT) :: a_file
    character(len=*), intent(IN) :: file_desc
    !integer, intent(OUT) :: set_desc
    type(varying_string) :: loc_desc
    loc_desc = var_str(file_desc)
    call vs_setdesc(a_file,loc_desc)!, set_desc)
  end subroutine c_setdesc


  ! setting description for vs
  subroutine vs_setdesc(a_file,file_desc)!, set_desc)
     implicit none
     type(File_), intent(INOUT) :: a_file
     type(varying_string) , intent(IN) :: file_desc
     !integer, intent(OUT) :: set_desc
     type(varying_string) :: field
     !set_desc = 1
     field = 'desc'
     a_file=vs_setFile(a_file,field,trim(file_desc))
     !set_desc = 0
     return
  end subroutine vs_setdesc 
  
  
  ! setting file unit
  function setunit(a_file,value)
     implicit none
     type(File_), intent(INOUT) :: a_file
     integer ,intent(IN) :: value
     type(varying_string) :: field
     type(File_) :: setunit         
     field = 'unit'
     setunit=int_setFile(a_file,field,value)
     return
  end function setunit
  
  
  ! status    
  
  
  
  ! header
  
  
  
  ! sep
  
  
  
  ! action
  
  
  
  ! position
  ! function setposition(a_file,value)
  !   implicit none
  !   type(File_), intent(IN) :: a_file
  !   type(varying_string) ,intent(IN) :: value
  !   type(varying_string) :: field
  !   type(File_) :: setposition         
  !   field = 'position'
  !   setunit=int_setFile(a_file,field,value)
  !   return
  ! end function setposition

  
    
  ! setting format
  function setformat(a_file,file_format)
     implicit none
     type(File_), intent(INOUT) :: a_file
     type(varying_string), intent(IN) :: file_format
     type(File_) :: setformat
     type(varying_string) :: field
     field = 'format'
     setformat=vs_setFile(a_file,field,trim(file_format))
     return
  end function setformat 
  
  ! setting open
  function setopen(a_file,is_file_open)
    implicit none
    type(File_), intent(IN) :: a_file
    logical, intent(IN) :: is_file_open
    type(File_) :: setopen
    type(varying_string) :: field
    field = 'open'
    setopen=logic_setFile(a_file,field,is_file_open)
    return
 end function setopen 


 ! setting dir for char
 subroutine c_setdir(a_file,file_dir,set_dir)
  implicit none
  type(File_), intent(INOUT) :: a_file
  character(len=*), intent(IN) :: file_dir
  integer, intent(OUT)  :: set_dir
  type(varying_string) :: loc_dir
  set_dir = 1
  loc_dir = var_str(file_dir)
  call vs_setdir(a_file,trim(loc_dir), set_dir)
 end subroutine c_setdir


! setting dir for vs
subroutine vs_setdir(a_file,file_dir,set_dir)
  implicit none
  type(File_), intent(INOUT) :: a_file
  type(varying_string), intent(IN) :: file_dir
  integer, intent(OUT)  :: set_dir
  type(varying_string) :: new_path
  ! character(len=files_path_length) :: cur_path, loc_name, cmd
  type(varying_string) :: cur_path, loc_name, cmd
  integer :: i_closed, i_moved, i_open
  logical :: file_exists, dir_exists, file_isopen

  set_dir = 1
  i_moved = 1
  i_closed = 1
  i_open = 1

  file_exists = .FALSE.
  dir_exists = .FALSE.
  file_isopen = .FALSE.


  ! current dir
  cur_path = a_file%path
  loc_name = var_str(getname(a_file))
  file_exists = existFile(cur_path)

  ! setting path 
  dir_exists = isdir(file_dir)
  new_path = trim(file_dir)//filesep()//trim(loc_name)


  if ((file_exists).AND.dir_exists) then  
    file_isOpen = isOpen(a_file)
    ! closing file
    i_closed = closeFile(a_file)
    call setpath(a_file,new_path)  
  end if

  if (i_closed == 0) then
    !print *, "Moving file from ", trim(cur_path), " to ", char(new_path)
    ! cmd = trim(sys_cmd%move)//" "//trim(cur_path)//" "//char(new_path)
    cmd = trim(sys_cmd%move)//" "//char(cur_path)//" "//char(new_path)
    !print *,"cmd :", trim(cmd)
    print *,"cmd :", char(cmd)
    !i_moved = execute_command(trim(cmd)) 
    i_moved = execute_command(char(cmd))
  end if

  if (.NOT.file_exists) then
    !print *, "The file doesn't exist: ", trim(loc_name )
    print *, "The file doesn't exist: ", char(loc_name )
  end if

  if (.NOT.dir_exists) then
    print *, "The destination directory doesn't exist: ", char(file_dir) 
  end if

  if (i_moved > 0) then
    !print *, "Error: the file could not be renamed or moved: ", loc_name 
    print *, "Error: the file could not be renamed or moved: ", char(loc_name)
    !call exit(9)
  end if

  ! reopening file if it was open prior to move
  if (file_isOpen) then
    i_open = openFile(a_file)
  end if

  set_dir = i_moved
 end subroutine vs_setdir

 ! end set functions ----------------------------------------------- !
  
  
  
  

  



  
  ! get functions ----------------------------------------------- !
  
  ! getFileTag
  ! for File_ type
  function file_getFileTag(a_file)
    implicit none
    type(File_), intent(IN) :: a_file
    type(varying_string) :: file_getFileTag
    file_getFileTag=a_file%tag
    return
  end function file_getFileTag
  
  
  ! getname
  ! for File_ type
  function file_getFileName(a_file)
    implicit none
    type(File_), intent(IN) :: a_file
    character(len=files_path_length) :: file_getFileName

    ! character(len=files_path_length) :: path
    type(varying_string)  :: path
    !file_getFileName = ""
    ! the default: file path
    path=a_file%path
    ! if file name is variable: prefix, varname, ext
    if (.NOT.a_file%fixed) then
    ! check if prefix, varname, prefix are set
    if ((len(a_file%prefix) > 0).AND. &
    & (len(a_file%varname) > 0).AND. &
    (len(a_file%ext) > 0)) then
      file_getFileName = char(a_file%prefix)// &
      char(a_file%varname)//char(a_file%ext)
      !print *, "calculating file name !", file_getFileName
    end if
    else
      file_getFileName=vs_path_getFileName(path)
    end if
  return
  end function file_getFileName
  
  
  ! getname
  ! for vs path
  function vs_path_getFileName(a_file)
    implicit none
    type(varying_string) , intent(IN)  :: a_file
    character(len=files_path_length) :: vs_path_getFileName, name
    !character(len=files_path_length) :: path
    integer :: idx
    !path=trim(a_file)
    !vs_path_getFileName=c_path_getFileName(path)

    name = char(trim(a_file))
    idx=index(name,filesep(),.TRUE.)
    if(idx>0) name=name((idx+1):len(name))

    vs_path_getFileName = trim(name)

    return
  end function vs_path_getFileName
  
  
 ! getname
 ! for character
 function c_path_getFileName(a_file)
    implicit none
    character(len=files_path_length), intent(IN)  :: a_file
    character(len=files_path_length) :: c_path_getFileName
    type(varying_string)  :: path

    path = var_str(trim(a_file))
    c_path_getFileName = vs_path_getFileName(path)

    !print *, "dans c_path_getFileName: ",c_path_getFileName
    
    return
  end function c_path_getFileName
  
  
  ! getDirName
  ! for File_ type
  function file_getDirName(a_file)
    implicit none
    type(File_), intent(IN) :: a_file
    character(len=files_path_length) :: file_getDirName
    character(len=files_path_length) :: path
    path=char(a_file%path)
    file_getDirName=c_path_getDirName(path)
    return
  end function file_getDirName
  
  
  ! getDirName
  ! for pathvar
  function vs_path_getDirName(a_file)
    implicit none
    type(varying_string) , intent(IN)  :: a_file
    character(len=files_path_length) :: vs_path_getDirName
    character(len=files_path_length) :: name
    integer :: idx
    name = "."//filesep()
    idx=index(a_file,filesep(),.TRUE.)
    !if(idx>0) name=char(a_file(1:(idx-1)))
    if(idx>0) then
      name = char(extract(a_file,1,(idx-1)))
    end if

    vs_path_getDirName = trim(name)

    return
  end function vs_path_getDirName
  
  
 ! getDirName
 ! for character
 function c_path_getDirName(a_file)
    implicit none
    character(len=files_path_length), intent(IN)  :: a_file
    character(len=files_path_length) :: c_path_getDirName
    type(varying_string)  :: path

    path = var_str(trim(a_file))
    c_path_getDirName = vs_path_getDirName(path)
    
    !print *, c_path_getDirName
    return
  end function c_path_getDirName


  ! TODO getAbspath  ???
  ! character(len=files_path_length) :: absPath
  ! USING GETCWD() 

  ! getting file stats for File_
  function file_getFileStat(a_file)
    implicit none
    type(File_), intent(IN) :: a_file
    type(File_Stat_) :: file_getFileStat
    character(len=files_path_length) :: file_path
    integer, dimension(13) :: buff
    integer :: status, file_unit
    
    file_path=a_file%path
    file_unit=a_file%unit
    call init_File_Stat(file_getFileStat)
        
    if (isOpen(a_file)) then
        call fstat(file_unit,buff,status)
    else
        call stat(trim(file_path),buff,status)
    end if 
    
    ! if no errors calling stat/fstat
    ! getting infos
    if (status == 0) THEN
        ! file stat for size, access, modif.,change
        file_getFileStat%size=buff(8)
        file_getFileStat%last_access=buff(9)
        file_getFileStat%last_modification=buff(10)
        file_getFileStat%last_change=buff(11)
    end if 
    return
  end function file_getFileStat


  ! getting file stats for char
  function path_getFileStat(file_path)
    implicit none
    character(len=files_path_length),intent(IN) :: file_path
    type(File_) :: a_file
    type(File_Stat_) :: path_getFileStat
    a_file=File(file_path)
    path_getFileStat=file_getFileStat(a_file)
    return
  end function path_getFileStat

  ! getting file size for File_
  function file_getFileSize(a_file)
    implicit none
    type(File_), intent(IN) :: a_file
    type(File_Stat_) :: statstruct
    integer :: file_getFileSize
    statstruct=file_getFileStat(a_file)
    !print *, statstruct
    file_getFileSize=statstruct%size
    return
  end function file_getFileSize

  
  ! getting file size for char
  function path_getFileSize(file_path)
    implicit none
    character(len=files_path_length),intent(IN) :: file_path
    type(File_) :: a_file
    integer :: path_getFileSize
    
    a_file=File(file_path)
    path_getFileSize=file_getFileSize(a_file)
       
  end function path_getFileSize

  
  !function getFileDate
    ! TODO ?
  !end function getFileDate


   ! TODO : complete getters 
   ! functions for getdesc, getext, getformat, getprefix, getvarname, ....... ??????
   ! file
   ! function file_getDesc
   ! end function getDesc
   
   ! file, path
   ! function file_getext
   ! end file_getext
   
   ! file
   ! function file_getFormat
   ! end function file_getFormat
   
   ! file
   ! function file_getPrefix
   ! end function file_getPrefix
   
   ! etc ....

 ! end get functions ----------------------------------------------- !


  ! existFile
  ! for File_ type
  function file_existFile(a_file)
    implicit none
    type(File_), intent(IN) :: a_file
    logical :: file_existFile
    !character(len=files_path_length) :: path
    type(varying_string) :: path
    path=a_file%path
    file_existFile=vs_path_existFile(path)
    !file_existFile=vs_path_existFile(a_file%path)
    return
  end function file_existFile
  
  
  ! existFile
  ! for char type
  function c_path_existFile(a_file)
    implicit none
    character(len=*), intent(IN) :: a_file
    type(varying_string) :: path
    logical :: c_path_existFile
    path=var_str(trim(a_file))
    c_path_existFile=vs_path_existFile(path)
    return
  end function c_path_existFile
  
  
  ! existFile
  ! for vs
  function vs_path_existFile(a_file)
    implicit none
    type(varying_string), intent(IN) :: a_file
    logical :: vs_path_existFile, disp,loc_exist
    

    ! using system func
    loc_exist=isfile(a_file)
    
    disp=.TRUE.
    if (disp .and. files_debug) then
       if (loc_exist) then
         write(*,*) char(a_file)//' exists'
       else
         write(*,*) char(a_file)//" doesn't exist "
       endif
    endif
    vs_path_existFile=loc_exist
    return
  end function vs_path_existFile
  

  ! is empty for File_
  function file_isFileEmpty(a_file)
    implicit none
    type(File_), intent(IN) :: a_file
    integer :: fsize
    logical :: file_isFileEmpty
    file_isFileEmpty=.TRUE.
    fsize=file_getFileSize(a_file)
    !print *,'file size', fsize
    if (fsize > 0) file_isFileEmpty=.FALSE.
    return
  end function file_isFileEmpty


  ! is empty for char
  function path_isFileEmpty(file_path)
       implicit none
       character(len=*),intent(IN) :: file_path
       type(File_) :: a_file
       logical :: path_isFileEmpty       
       a_file=File(file_path)
       path_isFileEmpty=file_isFileEmpty(a_file)
       return
  end function path_isFileEmpty


  ! testing if a file is open
  function file_isOpen(a_file)
    implicit none
    type(File_), intent(in) :: a_file
    logical :: file_isOpen
    file_isOpen = unit_isOpen(a_file%unit)
    !print *, " file_isOpen:", file_isOpen
    !print *, " file unit", a_file%unit
    return
  end function file_isOpen


  ! testing if a unit is open
  function unit_isOpen(file_unit)
    implicit none
    integer, intent(IN) :: file_unit
    logical :: unit_isOpen
    character(len=files_path_length) :: file_name
    inquire(unit = file_unit, opened=unit_isOpen, name = file_name)
    ! print *,'unit',file_unit, 'open=',unit_isOpen
    if (unit_isOpen .and. files_debug) print *, file_name,': File already open !'
    return
  end function unit_isOpen


  ! testing if a path is open
  function path_isOpen(a_path)
    implicit none
    character(len=*), intent(IN) :: a_path
    logical :: path_isOpen
    inquire(file = a_path, opened=path_isOpen)
    if (path_isOpen .and. files_debug) print *, a_path,': File already open !'
    return
  end function path_isOpen


  ! TODO : cases new file, file to append, existing file, etc ....
  ! + see actions : 'read', 'write' 'readwrite' if needed ???
  ! + see position string: 'rewind', 'append'
  !
  ! TODO : why with status='new' an open file is detected as closed ????
  !  replacing 'new' with 'replace' or 'unknown' is OK !!!!!!!!!!!!!!!!!!!!!!!!!!!
  function file_openFile(a_file)
    implicit none
    ! possibly returns a_file itself modified with code/error ???
    ! plus : testing file existence if status='old', 'replace'
    ! in this case
    type(File_), intent(INOUT) :: a_file
    integer :: open_status,file_openFile ! open status of the file
    logical:: file_exists,file_open
    character(len=files_path_length):: path
    
    file_open=.FALSE.
    file_openFile=0

    ! testing if file exists: TODO managing case 'new' error ??
    file_exists=existFile(a_file)

    ! if file exists ; testing if file is open
    if (file_exists) then
      file_open=isOpen(a_file)
      if (files_debug) print *, 'File exists ,file_open = ',file_open
    endif

    if (.NOT.file_open) then
      ! TODO calling a function giving path

      path=char(a_file%path)
      !print *, "OPENING FILE :", char(a_file%name), " unit: ", a_file%unit
      open(a_file%unit,file=trim(path),status=a_file%status, &
      action=a_file%action,position=a_file%position,iostat=open_status)
      if (open_status /= 0 ) then
      print *, 'Error: could not open ',trim(getname(a_file)), &
          ' unit= ',a_file%unit    
      endif
      file_openFile=open_status
      ! print *, 'File open status :',open_status
    endif

    a_file%open = file_open

    return
  end function file_openFile

  
  ! ajouter autre args pour status, action, position iostat ...
  function path_openFile(file_path)
     implicit none
     character(len=*) :: file_path
     type(File_) :: a_file
     integer :: file_unit,path_openFile
     
     ! getting file unit
     file_unit=getFreeFileUnit()
     
     !print *, 'file unit: ', file_unit
     
     a_file=File(file_path,file_unit)
     
     path_openFile=file_openFile(a_file)
     
     return
  end function path_openFile


  !function unit_openFile(a_file)
!
 ! end function unit_openFile



    ! TODO : isClosed ??????
    
  ! close for File_
  function file_closeFile(a_file)
    implicit none
    ! possibly returns a_file itself modified with code/error ???
    ! TODO  : testing file existence if status='old', 'replace'
    ! 
    type(File_), intent(INOUT) :: a_file
    integer :: close_status,file_closeFile ! close status of the file
    logical:: file_exists,file_open
    
    file_open=.FALSE.

    ! testing if file exists: TODO managing case if doesn't exist??
    file_exists=existFile(a_file)
    
    ! if file exists ; testing if file is open
    if (file_exists) then
      file_open=isOpen(a_file)
      if  (files_debug) print *, 'File exists, retour file_open = ',file_open
    else
      ! error : closing non existing file 
      file_closeFile=99
      if  (files_debug) then
        print *, 'File doesn t exist, retour file_open = ',file_open
        print *, "Can't close !"
      end if
      return
    endif
    
    if (file_open) then
      ! closing file
      !print *, "CLOSING FILE :", char(a_file%name), " unit: ", a_file%unit
      close(a_file%unit,iostat=close_status)
      file_closeFile=close_status
      !print *, "close status", close_status
      if (files_debug) print *, 'Closing file !'
    else
      ! error for closing a unopened file / or closing success ?
      file_closeFile=0
      if (files_debug) print *, 'File is already closed !'
    endif

    a_file%open = file_open

    return
  end function file_closeFile


  ! close for char
  function path_closeFile(file_path)
    implicit none
    character(len=*), intent(IN):: file_path
    integer :: file_unit,path_closeFile
    type(File_) :: a_file
    inquire(file = file_path,number = file_unit)
    !if (error==0) then
      a_file=File(file_path,file_unit)
      path_closeFile=closeFile(a_file)
    !end if 
    return
  end function path_closeFile


  ! close for unit (int)
  function unit_closeFile(a_file)
    implicit none
    integer, intent(IN) :: a_file
    logical :: open
    integer :: io_err, unit_closeFile
    inquire(unit = a_file,opened = open)
    if (open) then
      close(unit = a_file, iostat = io_err)
    end if
    unit_closeFile = io_err
  end function unit_closeFile
 
 
 
 ! SEE: UNLINK fortran func/subr. ???
 !
 ! deleteFile
 ! for vs path, base function
function vs_path_deleteFile(a_file)
  implicit none
  !character(len=files_path_length), intent(IN)  :: a_file
  type(varying_string), intent(IN) :: a_file
  character(len=system_path_length)  :: cmd
  integer :: vs_path_deleteFile

  vs_path_deleteFile = 0

  if (existFile(a_file)) then
    cmd = sys_cmd%delete_file//char(a_file)
      call check_system
      vs_path_deleteFile=execute_command(cmd)
    endif
  return
end function vs_path_deleteFile
  
 
 ! deleteFile
 ! for File_
function file_deleteFile(a_file)
  implicit none
  type(File_), intent(IN) :: a_file
  integer :: file_deleteFile

  file_deleteFile=vs_path_deleteFile(a_file%path)
  return
end function file_deleteFile

  
! deleteFile
! for char
function c_path_deleteFile(a_file)
  implicit none
  character(len=*), intent(IN) :: a_file
  type(varying_string) :: path
  integer :: c_path_deleteFile

  path=var_str(a_file)
  c_path_deleteFile=vs_path_deleteFile(path)
  return
end function c_path_deleteFile
  
  ! TODO : see create file ?
  ! if not exist, touch, see DOS command in SYSTEM
  
  
  ! SEE : RENAME fortran sub/function
  !function file_moveFile(a_file)
  
  !end function file_moveFile

  !function path_moveFile(a_file)
  
  !end function path_moveFile
  
   
  
 ! A VOIR
 ! GET_FILE_UNIT !!!!!
 
  ! getting a unused file unit
  function getFreeFileUnit (max_num)
    !   get_file_unit returns a unit number that is not in use
    implicit none
    integer, optional, intent(IN) :: max_num
    integer :: getFreeFileUnit
    integer :: num, m, iostat
    logical :: opened

    if (present(max_num)) then
        m = max_num
    else
        m = 99
    end if
    
    if (m < 1) m = 99
    do num = m,1,-1
        inquire (unit=num, opened=opened, iostat=iostat)
        if (iostat.ne.0) cycle
        if (.not.opened) exit
    end do
!
    getFreeFileUnit = num
    
    !print *, 'in get_file_unit, unit: ',lu
    return
  end function getFreeFileUnit
  
  ! checking if a unit is in use
  function isUnitUsed(file_unit)
    implicit none
    integer, intent(IN) :: file_unit
    logical :: isUnitUsed
    integer :: iostat
    logical :: opened
    isUnitUsed = .FALSE.
    ! testing if unit used
    inquire (unit=file_unit, opened=opened, iostat=iostat)
    if (opened.OR.iostat.ne.0) isUnitUsed = .TRUE.
    return
  end function isUnitUsed

  end module Files

  
