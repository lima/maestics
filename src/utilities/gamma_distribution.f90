real function gamma_distribution(shape_,seuilsurscale)
    implicit none


!DR 26/02/2019
! returns the incomplete gamma function P(a,x)

!: Arguments
  real, intent(IN) :: shape_
  real, intent(IN) :: seuilsurscale

!: Function(s) anciennes fonctions
!  real :: gammcf  !
!  real :: gamser  !
!  real :: gln
  real :: gamma_ln


!: Arguments
!  real, intent(OUT) :: gamser
!  real, intent(IN)  :: a devient shape
!  real, intent(IN)  :: x devient seuilsurscale
!  real, intent(OUT) :: gln

!: Variables locales
  real    :: gamser
  real    :: gammcf
  real    :: gln

  integer :: ITMAX
  real    :: EPS
  real    :: FPMIN

!  PARAMETER (ITMAX = 100,EPS = 3.e-7)
  PARAMETER (ITMAX = 100,EPS = 3.e-7,FPMIN = 1.e-30)
  integer :: n
  real    :: ap  !
  real    :: del  !
  real    :: somme
  integer :: i
  real    :: an  !
  real    :: b  !
  real    :: c  !
  real    :: d  !
  real    :: h


!!! fin declaration
      ! TODO : fichier historique ? if (x < 0. .or. a <= 0.) pause 'bad arguments in gammp'
     ! plus utile  gln = gamma_logln(shape_) car est egal a log_gamma

      gln = gamma_ln(shape_)
!      write(5555,*)'gln',gln
!      gln = log_gamma(shape_)
!      write(5555,*)'gln_new',gln

      if (seuilsurscale < shape_+1.) then

 !       call gser(gamser,a,x,gln)
 !       call gser(gamser,shape_,seuilsurscale,gln)
! recup function gser

!      gln = gammln(shape_)
      if (seuilsurscale <= 0.) then
        ! TODO : fichier historique ? : -- if (x < 0.) pause 'x < 0 in gser'
        gamser = 0.
        gamma_distribution=gamser
        return
      endif

      ap = shape_
      somme = 1. / shape_
      del = somme
      do n = 1,ITMAX
        ap = ap + 1.
        del = del * seuilsurscale / ap
        somme = somme + del
        if (abs(del) < abs(somme)*EPS) EXIT
      end do

      ! TODO : fichier historique ? : -- if (n >= ITMAX)pause 'a too large, ITMAX too small in gser'

        gamser = somme * exp(-seuilsurscale + (shape_ * log(seuilsurscale)) - gln)
        gamma_distribution = gamser
        write(5555,*)'gammser',gamser
      else

!        call gcf(gammcf,a,x,gln)
!        call gcf(gammcf,shape_,seuilsurscale,gln)
! recup function gcf
!      gln = gammln(shape_)
      b = seuilsurscale + 1. - shape_
      c = 1. / FPMIN
      d = 1. / b
      h = d
      do i = 1,ITMAX
        an = -i * (i - shape_)
        b = b + 2.
        d = an * d + b
        if (abs(d) < FPMIN) d = FPMIN
        c = b + an / c
        if (abs(c) < FPMIN) c = FPMIN
        d = 1. / d
        del = d * c
        h = h * del
        if (abs(del - 1.) < EPS) EXIT
      end do

      ! TODO : fichier historique ? : -- if (i >= ITMAX) pause 'a too large, ITMAX too small in gcf'

        gammcf = exp(-seuilsurscale + (shape_ * log(seuilsurscale)) - gln) * h
        gamma_distribution = 1.-gammcf
                write(5555,*)'gammcf',gammcf

      endif

return
end function gamma_distribution


real function gamma_ln(xx)
! DR 26/02/2019
! returns the value ln(gamma(xx)) pour xx>0
! internal arithmetic will be done in double precision, a nicety that you can amit if five-figure a accuracy is good enough

  implicit none

!: Arguments
  real, intent(IN) :: xx

!: Variables locales

  integer :: j
  double precision :: ser  !
  double precision :: stp  !
  PARAMETER (stp=2.5066282746310005d0)
  double precision :: tmp  !
  double precision :: x  !
  double precision :: y  !
  double precision :: cof(6)
  SAVE cof
  DATA cof/76.18009172947146d0,-86.50532032941677d0,24.01409824083091d0,-1.231739572450155d0, &
               0.1208650973866179d-2,-0.5395239384953d-5/

      x = xx
      y = x
      tmp = x + 5.5d0
      tmp = (x + 0.5d0) * log(tmp) - tmp
      ser = 1.000000000190015d0
      do j = 1,6
        y = y + 1.d0
        ser = ser + cof(j) / y
      end do
      gamma_ln = real(tmp + log(stp * ser / x))

return
end function gamma_ln

