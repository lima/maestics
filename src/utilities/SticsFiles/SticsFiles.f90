! Module SticsFiles
! - Description : Contains files informations
! in this module are described the files structure
! and output management flags

module SticsFiles

  USE iso_varying_string

  USE Files
  
  USE SticsSystem
  
  implicit none
  
  ! DEFINING PARAMETERS
  integer, parameter :: files_nb=50
  character(2), parameter :: na_tag = "na"
  integer, parameter :: min_file_unit = 35
  character(len = 50) :: codeversion

  
  
  ! DEFINING TYPES
  type Stics_Files_
     
     ! FOR INPUT FILES DESCRIPTION
     !
     ! Files list
     type(File_), dimension(files_nb) :: files
     ! Files tags list
     type(varying_string), dimension(files_nb) :: tags
     ! Files status
     logical, dimension(files_nb) :: init_status

     
     ! FOR RECORD MANAGEMENT
     
     type(varying_string) :: datapath
    
     type(varying_string) :: path
    
     type(varying_string) :: pathstation
    
     type(varying_string) :: pathclimat
    
     type(varying_string) :: pathplt

     type(varying_string) :: pathplt2
    
     type(varying_string) :: pathsol
    
     type(varying_string) :: pathtec

     type(varying_string) :: pathtec2
    
     type(varying_string) :: pathusm
    
     type(varying_string) :: pathtempopar
    
     type(varying_string) :: pathtempoparv6
    
     type(varying_string) :: pathinit

     type(varying_string) :: pathrecup
    
     type(varying_string) :: sticsid

     logical :: flag_record
     
     
     ! FOR OUPUT FILES MANAGEMENT
     !
     ! option for writing the output files
     ! (1 = mod_history.sti, 2=daily outputs,4= report outut, 
     ! 8=balance outputs,16 = profil outputs,
     ! 32= debug outputs, 64 = screen outputs, 128 = agmip outputs) 
     ! sum codes to have several types of outputs.
     integer :: flagEcriture  
     !
     integer :: ECRITURE_HISTORIQUE 
     integer :: ECRITURE_SORTIESJOUR
     integer :: ECRITURE_RAPPORTS
     integer :: ECRITURE_BILAN
     integer :: ECRITURE_PROFIL 
     integer :: ECRITURE_DEBUG
     integer :: ECRITURE_ECRAN
     integer :: ECRITURE_AGMIP

     ! flags to be used in messages (files, screen)
     ! and for writing output files
     logical :: flag_histo
     logical :: flag_jour
     logical :: flag_rap
     logical :: flag_bilan
     logical :: flag_profil
     logical :: flag_debug
     logical :: flag_ecran
     logical :: flag_agmip
     ! for knowing if set_writing_flags
     ! already done
     logical :: flags_done

     logical :: init_done
     integer :: files_number

     ! asso flags
     logical :: asso_jour
     logical :: asso_jour_agmip
     logical :: asso_rap
     logical :: asso_rap_agmip
     logical :: asso_bilan
     logical :: asso_bilan_agmip

  end type Stics_Files_


  ! The shared files structure
  type(Stics_Files_),target, save :: stics_files


! files structure zero
  ! interface zero
  !    module procedure zero_files
  ! end interface zero

! files structure initialization
  interface init_files
     module procedure init_stics_files
  end interface init_files

  interface add
     module procedure add_file
  end interface add

  interface get_file
    module procedure c_get_file, vs_get_file
  end interface get_file

  interface disp_file
    module procedure c_disp_file, vs_disp_file
  end interface disp_file

  interface get_file_index
    module procedure c_get_file_index, vs_get_file_index
  end interface get_file_index

  interface exist_file
    module procedure c_exist_file, vs_exist_file
  end interface exist_file

  interface open_file
  module procedure c_open_file, vs_open_file
  end interface open_file

  interface close_file
  module procedure c_close_file, vs_close_file
  end interface close_file

  !interface finalize
  !  module procedure finalize_files
  !end interface finalize

contains

! TODO : saying if usefull to do this !!!!
! because they will be replaced calling init ! 
!subroutine zero_files(sf)
subroutine zero_files()
  implicit none
  !type(Stics_Files_),intent(INOUT) :: sf
  integer :: i
  ! Pointer to shared stics_files structure
  type(Stics_Files_), pointer :: sf
  !type(Stics_Files_) :: sf

  ! Pointers initialization
  sf => null()

  ! pointer association to stics_files structure
  sf => stics_files


  do i = 1, files_nb
    call init(sf%files(i))
  end do

  ! zero tags
  sf%tags(:)= na_tag

  ! init status
  sf%init_status(:) = .FALSE.

  sf%init_done = .FALSE.
  sf%files_number = 0

  
  ! For forcing files path
  sf%datapath = ""
    
  sf%path = ""

  sf%pathstation = ""

  sf%pathclimat = ""

  sf%pathplt = ""

  sf%pathplt2 = ""

  sf%pathsol = ""

  sf%pathtec = ""

  sf%pathtec2 = ""

  sf%pathusm = ""

  sf%pathtempopar = ""

  sf%pathtempoparv6 = ""

  sf%pathinit = ""

  sf%pathrecup = ""
  
  sf%sticsid = ""

  sf%flag_record = .FALSE.
  
  sf%flagEcriture = 0 
  sf%ECRITURE_HISTORIQUE = 0  
  sf%ECRITURE_SORTIESJOUR = 0   
  sf%ECRITURE_RAPPORTS = 0   
  sf%ECRITURE_BILAN = 0   
  sf%ECRITURE_PROFIL = 0   
  sf%ECRITURE_DEBUG = 0 
  sf%ECRITURE_ECRAN = 0 
  sf%ECRITURE_AGMIP = 0 

  sf%flag_histo = .FALSE.
  sf%flag_jour = .FALSE.
  sf%flag_rap = .FALSE.
  sf%flag_bilan = .FALSE.
  sf%flag_profil = .FALSE.
  sf%flag_debug = .FALSE.
  sf%flag_ecran = .FALSE.
  sf%flag_agmip = .FALSE.
  sf%flags_done = .FALSE.

  ! asso flags
  sf%asso_jour = .FALSE.
  sf%asso_jour_agmip = .FALSE.
  sf%asso_rap = .FALSE.
  sf%asso_rap_agmip = .FALSE.
  sf%asso_bilan = .FALSE.
  sf%asso_bilan_agmip = .FALSE.

end subroutine zero_files


  subroutine init_stics_files(global_write_flag,datapath, &
             path,pathstation, pathclimat, pathplt, pathplt2,pathsol, &
             pathtec, pathtec2, pathusm, pathtempopar, pathtempoparv6, &
             pathinit, pathrecup, sticsid, flag_record )
             
  implicit none
  integer , intent(IN) :: global_write_flag
  character(len=255), intent(IN) :: datapath

  character(len=255), intent(IN) :: path

  character(len=255), intent(IN) :: pathstation

  character(len=255), intent(IN) :: pathclimat

  character(len=255), intent(IN) :: pathplt

  character(len=255), intent(IN) :: pathplt2

  character(len=255), intent(IN) :: pathsol

  character(len=255), intent(IN) :: pathtec

  character(len=255), intent(IN) :: pathtec2

  character(len=255), intent(IN) :: pathusm

  character(len=255), intent(IN) :: pathtempopar

  character(len=255), intent(IN) :: pathtempoparv6

  character(len=255), intent(IN) :: pathinit

  character(len=255), intent(IN) :: pathrecup

  character(len=255), intent(IN) :: sticsid

  logical, intent(IN) :: flag_record

  type(varying_string) :: in_dir, out_dir 
  type(varying_string) :: file_path
  !
  character(1) :: sep

  ! for preventing file name modification
  logical :: file_lock

  ! Pointer to shared stics_files structure
  type(Stics_Files_), pointer :: sf
  !type(Stics_Files_) :: sf

  ! default value for locking files modifications after init
  file_lock = .FALSE.

  ! Pointers initialization
  sf => null()

  ! pointer association to stics_files structure
  sf => stics_files

  ! Getting model version info 
  call call_num_version(codeversion)

  ! Check if sf struct already initialized
  !call check_init_done(sf, .FALSE.)
  call check_init_done(.FALSE.)

  ! file separator
  sep=filesep()


  !call zero(sf)
  call zero_files()

  ! initializing writing flags
  sf%flagEcriture = 0 
  sf%ECRITURE_HISTORIQUE = 1
  sf%ECRITURE_SORTIESJOUR = 2   
  sf%ECRITURE_RAPPORTS = 4   
  sf%ECRITURE_BILAN = 8   
  sf%ECRITURE_PROFIL = 16   
  sf%ECRITURE_DEBUG = 32 
  sf%ECRITURE_ECRAN = 64 
  sf%ECRITURE_AGMIP = 128
  
  sf%datapath = datapath
    
  sf%path = path

  sf%pathstation = pathstation

  sf%pathclimat = pathclimat

  sf%pathplt = pathplt

  sf%pathplt2 = pathplt2

  sf%pathsol = pathsol

  sf%pathtec = pathtec

  sf%pathtec2 = pathtec2

  sf%pathusm = pathusm

  sf%pathtempopar = pathtempopar

  sf%pathtempoparv6 = pathtempoparv6

  sf%pathinit = pathinit

  sf%pathrecup = pathrecup
  
  sf%sticsid = sticsid

  sf%flag_record = flag_record

  if ( flag_record ) then
    file_lock = .TRUE.
  end if

  ! ADDING INPUT FILES-------------------------------------------------------
  !
  in_dir = "."
  out_dir = "."
  
  !print *,"path ", char(sf%path),".", len(trim(sf%path))
  !print *, "pathusm ", char(sf%pathusm) ,".",len(trim(sf%pathusm))

  call def_file_path(file_path,sf%path , sf%pathusm,"new_travail.usm")
  call add(File(char(file_path), get_free_file_unit(), &
  old, read,in_tag = "usm", in_lock = file_lock ))


  call def_file_path(file_path,sf%path , sf%pathinit,"ficini.txt")
  call add(File(char(file_path),get_free_file_unit(), &
  old,read,in_tag = "ini", in_lock = file_lock))


  call def_file_path(file_path,sf%path , sf%pathtempopar,"tempopar.sti")
  call add(File(char(file_path),get_free_file_unit(), &
  old,read,in_tag = "par", in_lock = file_lock))
  
  
  call def_file_path(file_path,sf%path , sf%pathtempoparv6,"tempoparv6.sti")
  call add(File(char(file_path),get_free_file_unit(), &
  old,read,in_tag = "parnew", in_lock = file_lock))

  
  call def_file_path(file_path,sf%path , sf%pathsol,"param.sol")
  call add(File(char(file_path),get_free_file_unit(), &
  old,read,in_tag = "sol", in_lock = file_lock))
  

  call def_file_path(file_path,sf%path , sf%pathtec,"fictec1.txt")
  call add(File(char(file_path),get_free_file_unit(), &
  old,read,in_tag = "tec1", in_lock = file_lock))

  
  call def_file_path(file_path,sf%path , sf%pathtec2,"fictec2.txt")
  call add(File(char(file_path),get_free_file_unit(), &
  old,read,in_tag = "tec2", in_lock = file_lock))
  

  call def_file_path(file_path,sf%path , sf%pathplt,"ficplt1.txt")
  call add(File(char(file_path),get_free_file_unit(), &
  old,read,in_tag = "plt1", in_lock = file_lock))
  
  
  call def_file_path(file_path,sf%path , sf%pathplt2,"ficplt2.txt")
  call add(File(char(file_path),get_free_file_unit(), &
  old,read,in_tag = "plt2", in_lock = file_lock))
  
  
  call def_file_path(file_path,sf%path , sf%pathstation,"station.txt")
  call add(File(char(file_path),get_free_file_unit(), &
  old,read,in_tag = "sta", in_lock = file_lock))

  
  call def_file_path(file_path,sf%path , sf%pathclimat,"climat.txt")
  call add(File(char(file_path),get_free_file_unit(), &
  old,read,in_tag = "clim", in_lock = file_lock))
  
  
  call add(File(char(in_dir)//sep//"var.mod",get_free_file_unit(), &
  old,read,in_tag = "var"))
  
  
  call add(File(char(in_dir)//sep//"rap.mod",get_free_file_unit(), &
  old,read,in_tag = "rap_mod"))
  
  
  call add(File(char(in_dir)//sep//"prof.mod",get_free_file_unit(), &
  old,read,in_tag = "prof_mod"))
  
  
  call add(File(char(in_dir)//sep//"param.sti",get_free_file_unit(), &
  old,read,in_tag = "optim"))
  
  
  call add(File(char(in_dir),get_free_file_unit(), &
  old,read,in_tag = "lai1"))
  
  
  call add(File(char(in_dir),get_free_file_unit(),old, &
  read,in_tag = "lai2"))


  ! linked to AGMIP / or MacSUR projects ...
  call add(File(char(in_dir)//sep//"depths_paramv6.txt",get_free_file_unit(), &
  old,read,in_tag = "depthv6"))

  
  ! OUPUT FILES-------------------------------------------------------
  !
  ! 
  ! + DAILY OUTPUTS
  ! PURE
  call add(File(char(out_dir),get_free_file_unit(), &
  replac,write,in_tag = "daily", in_prefix = "mod_s", in_ext = ".sti"))
  
  ! ASSOCIATION
  ! culture principale
  call add(File(char(out_dir),get_free_file_unit(), &
  replac,write,in_tag = "daily_p", in_prefix = "mod_sp", in_ext = ".sti"))
  
  ! culture associee
  call add(File(char(out_dir),get_free_file_unit(), &
  replac,write,in_tag = "daily_a", in_prefix = "mod_sa", in_ext = ".sti"))
  

  ! + AGMIP DAILY OUTPUTS
  ! PURE
  call add(File(char(out_dir),get_free_file_unit(), &
  replac,write,in_tag = "daily_agmip", in_prefix = "mod_s", in_ext = ".st3"))
  
  ! ASSOCIATION
  ! culture principale
  call add(File(char(out_dir),get_free_file_unit(), &
  replac,write,in_tag = "daily_agmip_p", in_prefix = "mod_sp", in_ext = ".st3"))

  ! culture associee
  call add(File(char(out_dir),get_free_file_unit(), &
  replac,write,in_tag = "daily_agmip_a", in_prefix = "mod_sa", in_ext = ".st3"))
  


  ! + BALANCES OUTPUTS
  !
  ! PURE
  call add(File(char(out_dir),get_free_file_unit(), &
  replac,write,in_tag = "bilan", in_prefix = "mod_b", in_ext = ".sti"))
  ! setting description
  call set_file_desc("bilan","Balance file for pure crop ....")
  
  ! ASSOCIATION
  ! culture principale
  call add(File(char(out_dir),get_free_file_unit(), &
  replac,write,in_tag = "bilan_p", in_prefix = "mod_bp", in_ext = ".sti"))
  ! setting description
  call set_file_desc("bilan_p","Balance file for associated, main crop ....")
  
  ! culture associee
  call add(File(char(out_dir),get_free_file_unit(), &
  replac,write,in_tag = "bilan_a", in_prefix = "mod_ba", in_ext = ".sti"))
  ! setting description
  call set_file_desc("bilan_a","Balance file for associated second crop ....")
  

  ! + SPECIFIC REPORT OUTPUTS
  !
  ! PURE
  call add(File(char(out_dir)//sep//"mod_rapport.sti",get_free_file_unit(), &
  unknown,write,append,in_tag = "rap"))
  
  ! ASSOCIATION
  ! culture principale
  call add(File(char(out_dir)//sep//"mod_rapportP.sti",get_free_file_unit(), &
  unknown,write,append,in_tag = "rap_p"))
  ! culture associee
  call add(File(char(out_dir)//sep//"mod_rapportA.sti",get_free_file_unit(), &
  unknown,write,append,in_tag = "rap_a"))


  !!!!!!!!!!!!!!!! AGMIP SPECIFIC REPORT OUTPUTS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  ! PURE
  call add(File(char(out_dir)//sep//"mod_rapport_AgMIP.sti",get_free_file_unit(), &
  unknown,write,append,in_tag = "rap_agmip"))
  
  ! ASSOCIATION
  ! culture principale
  call add(File(char(out_dir)//sep//"mod_rapportP_AgMIP.sti",get_free_file_unit(), &
  unknown,write,append,in_tag = "rap_agmip_p"))
  
  ! culture associee
  call add(File(char(out_dir)//sep//"mod_rapportA_AgMIP.sti",get_free_file_unit(), &
  unknown,write,append,in_tag = "rap_agmip_a"))
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  

  ! + SPECIFIC PROFILE OUTPUTS
  !
  call add(File(char(out_dir)//sep//"mod_profil.sti",get_free_file_unit(), &
  replac,write,in_tag = "prof_sti"))

  ! OUTPUT / INPUT FILES
  !
  call def_file_path(file_path,sf%path , sf%pathrecup,"recup.tmp",sf%sticsid)
  call add(File(char(file_path),get_free_file_unit(), &
  unknown,readwrite,in_tag = "recup", in_lock = file_lock))

  ! call disp_file("recup",.TRUE.)
  
  ! snow state variables
  call add(File(char(in_dir)//sep//"snow_variables.txt",get_free_file_unit(), &
  unknown,readwrite,in_tag = "snowvar"))

  ! + DEBUG FILES
  ! cf JCP in arbrevert.f90 (TODO) ??????
  call add(File(char(in_dir)//sep//"mod_debug.sti",get_free_file_unit(), &
  unknown,readwrite,asis,in_tag = "dbg1"))

  call add(File(char(in_dir)//sep//"mod_debug2.sti",get_free_file_unit(), &
  unknown,readwrite,asis,in_tag = "dbg2"))


  ! + HISTORY
  ! history file
! DR pour le moment je ne le mets pas en append
!  sf%fichisto=File(char(out_dir)//sep//"modhistory.sti",89,unknown,readwrite,append)

  call add(File(char(out_dir)//sep//"modhistory.sti", get_free_file_unit(), &
  unknown, readwrite,asis, in_tag = "histo"))


  ! errors log file 
  call add(File(char(out_dir)//sep//"stics_errors.log", get_free_file_unit(), & 
  unknown,readwrite,asis, in_tag = "errors"))

  ! ATTENTION !!!!!!!!!!!!!!!!!!!!!!
  ! Si logInfo de Messages n"ouvre/ne ferme pas les fichiers
  ! les ouvrir ici et supprimer position = append !
  

  ! checking status for all added files
  if (ALL(sf%init_status(1:sf%files_number))) then
    ! print *, "Error, the files have not all being initialized !"
    sf%init_done = .TRUE.
  end if
  
  !call check_stics_files_init(sf)
  call check_stics_files_init()

  ! Setting writing flags values
  !call set_files_writing_flags(sf, global_write_flag)
  call set_files_writing_flags( global_write_flag )

  ! updating stics_files
  !stics_files = sf

  return
end subroutine init_stics_files

! setting flags for selecting output files
!subroutine set_files_writing_flags(sf, global_flag)
subroutine set_files_writing_flags( global_flag )
  implicit none
  !type(Stics_Files_), intent(INOUT) :: sf
  integer, intent(IN) :: global_flag
  logical :: histo, jour, rap, bilan, & 
              profil, debug, ecran, agmip
  type(File_), pointer :: a_file
  logical :: status

  ! Pointer to shared stics_files structure
  type(Stics_Files_), pointer :: sf
  !type(Stics_Files_) :: sf

  ! Pointers initialization
  sf => null()

  ! pointer association to stics_files structure
  sf => stics_files


  if (.NOT.sf%init_done) then
    print * , "Error, SticsFiles structure not initialized, aborting"
    stop
  end if

  ! if the flag is the same
  if ( sf%flagEcriture == global_flag ) then
    return
  end if

! keeping previous flags in case of second call
  histo = sf%flag_histo
  jour = sf%flag_jour
  rap = sf%flag_rap
  bilan = sf%flag_bilan 
  profil = sf%flag_profil
  debug = sf%flag_debug
  ecran = sf%flag_ecran
  agmip = sf%flag_agmip
  

  ! if set_writing_flags is called from init_files
  ! remove files and write headers
  ! histo, dbg1, dbg2
  if (.not.sf%flags_done) then
    ! old log files deletion
    ! deleting file and getting a file object at once
    !call delete_file(sf,"histo",status,a_file)
    call delete_file( "histo",status,a_file)

    ! writing file header
    if ( histo ) then
      call write_file_header(a_file,"history")
    end if

    !call delete_file(sf,"errors",status,a_file)    
    call delete_file("errors",status,a_file) 

    ! writing file header
    call write_file_header(a_file,"errors")

    
    ! writing file header
    if ( debug ) then
      !call delete_file(sf,"dbg1",status,a_file)
      call delete_file("dbg1",status,a_file)
      call write_file_header(a_file,"debug")
      !call delete_file(sf,"dbg2",status,a_file)
      call delete_file("dbg2",status,a_file)
      call write_file_header(a_file,"debug2")
    end if
  end if


  sf%flagEcriture = global_flag

  !flag_histo = .FALSE.
  call set_writing_flag(global_flag, sf%ECRITURE_HISTORIQUE, sf%flag_histo)
  !flag_jour = .FALSE.
  call set_writing_flag(global_flag, sf%ECRITURE_SORTIESJOUR, sf%flag_jour)
  !flag_rap = .FALSE.
  call set_writing_flag(global_flag, sf%ECRITURE_RAPPORTS, sf%flag_rap)
  !flag_bilan = .FALSE.
  call set_writing_flag(global_flag, sf%ECRITURE_BILAN, sf%flag_bilan)
  !flag_profil = .FALSE.
  call set_writing_flag(global_flag, sf%ECRITURE_PROFIL, sf%flag_profil)
  !flag_debug = .FALSE.
  call set_writing_flag(global_flag, sf%ECRITURE_DEBUG, sf%flag_debug)
  !flag_ecran = .FALSE.
  call set_writing_flag(global_flag, sf%ECRITURE_ECRAN, sf%flag_ecran)
  ! print *, "sticsfiles, ecriture ecran : ", global_flag, sf%ECRITURE_ECRAN, sf%flag_ecran
  !flag_agmip = .FALSE.
  call set_writing_flag(global_flag, sf%ECRITURE_AGMIP, sf%flag_agmip)


  ! if set_writing_flags is called from init_files
  ! remove files and write headers
  ! histo, dbg1, dbg2
  if (.not.sf%flags_done) then
    ! old log files deletion
    ! deleting file and getting a file object at once
    !call delete_file(sf,"histo",status,a_file)
    call delete_file("histo",status,a_file)

    ! writing file header
    if ( sf%flag_histo ) then
      call write_file_header(a_file,"history")
    end if

    !call delete_file(sf,"errors",status,a_file)
    ! writing file header
    !call write_file_header(a_file,"errors")
  
    !call delete_file(sf,"dbg1",status,a_file)
    call delete_file("dbg1",status,a_file)

    ! writing file header
    if ( sf%flag_debug ) then
      call write_file_header(a_file,"debug")
    end if

    !call delete_file(sf,"dbg2",status,a_file)
    call delete_file("dbg2",status,a_file)

    if ( sf%flag_debug ) then
      call write_file_header(a_file,"debug2")
    end if

  end if


  ! if init_files already done
  ! this is a write flags modification 
  ! a second call to set_writing_flags
  if ( sf%flags_done ) then
    if ( .NOT. sf%flag_histo) then 
      !call delete_file(sf,"histo",status,a_file)
      call delete_file("histo",status,a_file)
    end if 
    if ( .NOT. histo .AND. sf%flag_histo) then
      !call delete_file(sf,"histo",status,a_file)
      call delete_file("histo",status,a_file)
      call write_file_header(a_file,"history")
    end if
    ! TODO : add debug files ------------------------

    if ( .NOT. sf%flag_debug) then 
      ! call delete_file(sf,"dbg1",status,a_file)
      ! call delete_file(sf,"dbg2",status,a_file)
      call delete_file("dbg1",status,a_file)
      call delete_file("dbg2",status,a_file)
    end if 
    if ( .NOT. debug .AND. sf%flag_debug) then
      !call delete_file(sf,"dbg1",status,a_file)
      call delete_file("dbg1",status,a_file)
      call write_file_header(a_file,"debug")
      !call delete_file(sf,"dbg2",status,a_file)
      call delete_file("dbg2",status,a_file)
      call write_file_header(a_file,"debug2")
    end if

    ! -----------------------------------------------
  end if

  !print *, sf%flag_debug
  !stop
  
  sf%flags_done = .TRUE.

end subroutine set_files_writing_flags

! calculating a file writing flag according to a 
! global flag (sum of individual flags) and a specific flag
subroutine set_writing_flag(global_flag, file_flag, out_write_flag)
  implicit none
  integer, intent(IN) :: global_flag
  integer, intent(IN) :: file_flag
  logical, intent(OUT) :: out_write_flag

  out_write_flag = .FALSE.

  if (iand(global_flag,file_flag) > 0 ) then
    out_write_flag = .TRUE.
  end if
   
end subroutine set_writing_flag


! setting files names if the prefix is set
! the file name is calculated with the variable
! part of the name added to the prefix string
subroutine set_files_names(varname,plants,name_status)
  !subroutine set_files_names(sf,varname,plants,name_status)
  implicit none
  !type(Stics_Files_) , intent(INOUT) :: sf
  ! varname : corresponds to the variable part of the file
  ! name (== usm name)
  character(len=*), intent(IN) :: varname
  integer, intent(IN) :: plants
  logical , intent(OUT), optional :: name_status
  integer :: i
  integer :: set_name
  integer :: tag_length
  !integer :: ret
  logical :: status, tag_asso, tag_pure, file_status
  !character (len =  20) :: char_open
  type(File_),pointer :: a_file

  ! testing with shared sf pointer
! Pointer to shared stics_files structure
  type(Stics_Files_), pointer :: sf
  !type(Stics_Files_) :: sf

  ! Pointers initialization
  sf => null()

  ! pointer association to stics_files structure
  sf => stics_files



  status = .TRUE.
  !char_open = ""
  file_status = .TRUE.



  !! Setting asso flags
  if (plants > 1) then
    if (sf%flag_jour) then
      sf%asso_jour = .TRUE.
      if (sf%flag_agmip) then
        sf%asso_jour_agmip = .TRUE.
      end if
    end if

    if (sf%flag_rap) then
      sf%asso_rap = .TRUE.
      if (sf%flag_agmip) then
        sf%asso_rap_agmip = .TRUE.
      end if
    end if

    if (sf%flag_bilan) then
      sf%asso_bilan = .TRUE.
      if (sf%flag_agmip) then
        sf%asso_bilan_agmip = .TRUE.
      end if
    end if

  end if

  ! using fixed field for determining if the file var name 
  ! must be set ?
  ! using set_file_name(sf,file_tag,file_name, set_name)

  do i = 1, sf%files_number
      ! print *, sf%files(i)%fixed
    if (sf%files(i)%fixed) then
      cycle
    end if

        !call set_file_name(sf,char(sf%files(i)%tag),varname, set_name)
        call set_file_name(char(sf%files(i)%tag),varname, set_name)
        
        if ( set_name > 0 ) then
          status = .FALSE.
        end if

        tag_length = len(trim(sf%files(i)%tag))
        !print *, char(sf%files(i)%tag)

        ! daily files
        if (index(sf%files(i)%tag,"daily") > 0) then
          if ( .NOT. sf%flag_jour ) then
            cycle
          end if
        
          tag_asso = (tag_length > 5) .AND. (tag_length < 8) .AND. (plants ==2)
          tag_pure = ( tag_length == 5 ) .AND. ( plants == 1 )

        end if

        ! daily agmip files
        if (index(sf%files(i)%tag,"daily_agmip") > 0) then
          if ( .NOT. sf%flag_agmip ) then
            cycle
          end if
          
          tag_asso = (tag_length > 11) .AND. (tag_length < 14) .AND. (plants ==2)
          tag_pure = ( tag_length == 11 ) .AND. ( plants == 1 )
        end if


        ! balance files
        if (index(sf%files(i)%tag,"bilan") > 0) then
          if ( .NOT. sf%flag_bilan ) then
            cycle
          end if
          
          tag_asso = (tag_length > 5) .AND. (tag_length < 8) .AND. (plants ==2)
          tag_pure = ( tag_length == 5 ) .AND. ( plants == 1 )

        end if

        if ( tag_asso.OR.tag_pure ) then
          ! error when sf%files(i)%unit < 0
          call open_file(char(sf%files(i)%tag),a_file,file_status)
        end if

        if ( .NOT. file_status) then
          status = .FALSE.
        end if
      

  end do 
  
  if (present(name_status)) then
    name_status = status
  end if

end subroutine set_files_names


!subroutine check_stics_files_init(sf)
subroutine check_stics_files_init()
  implicit none
  !type(Stics_Files_) , intent(IN) :: sf
  logical :: error
  error = .FALSE.
  ! print *, stics_files%init_done
  !if (.NOT.stics_files%init_done) then
    !if (.NOT.sf%init_done) then
    if (.NOT.stics_files%init_done) then
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! TODO: solution secours pour ecrire dans fichier histo
    ! en alors que l"init ne s"est pas fait correctement ....
    ! print *, "Error, files initialization not complete !"
    print *, "Error, the files have not all being initialized !"
    error = .TRUE.
    !call exit(9)
  end if

  ! may be other checks !!!!!!! to be added here

  if (error) then
    call exit(9)
  end if

end subroutine check_stics_files_init


! getting array an of tags
function get_files_tags(sf)
  implicit none
  type(Stics_Files_) , intent(IN) :: sf
  type(varying_string) ,dimension(files_nb) :: get_files_tags

  ! getting tags set list 
  get_files_tags=sf%tags
  return

end function get_files_tags


! getting a file unit
!function get_file_unit(sf, file_tag)
subroutine get_file_unit( file_tag, file_unit )
  implicit none
  !type(Stics_Files_) , intent(IN) :: sf
  character(len=*) , intent(IN) :: file_tag
  integer, intent(OUT)  :: file_unit

  type(File_), pointer :: out_file
  !integer :: get_file_unit

!  call get_file(sf, var_str(file_tag), out_file)
  call c_get_file( file_tag , out_file )

  file_unit = out_file%unit

end subroutine get_file_unit


! getting the array of files units
!function get_files_units(sf)
function get_files_units()
  implicit none
  !type(Stics_Files_) , intent(IN) :: sf

  integer, dimension(files_nb) :: get_files_units
  
  ! getting tags set list 
  ! getting filed units number
  ! units_nb = count((sf%files(:)%unit /= -1) .eqv. .TRUE. )
  !  get_files_units=sf%files(:)%unit
  get_files_units=stics_files%files(:)%unit

  return
end function get_files_units


! testing if a file unit is already in the structure
!function exist_file_unit(sf, in_unit)
function exist_file_unit( in_unit)
  implicit none
  !type(Stics_Files_) , intent(IN) :: sf
  integer , intent(IN) :: in_unit

  !integer, dimension(files_nb) :: files_units
  logical :: exist_file_unit

  exist_file_unit = .FALSE.

  !files_units = get_files_units(sf)
  !files_units = sf%files(:)%unit

  !if (any(files_units == in_unit)) then
  !if (any(sf%files(:)%unit == in_unit)) then
  if (any(stics_files%files(:)%unit == in_unit)) then
    exist_file_unit = .TRUE.
  end if
  return
end function exist_file_unit


! getting an unused file unit
function get_free_file_unit()
  implicit none
  !type(Stics_Files_) , intent(IN) :: sf

  integer , dimension(files_nb) :: files_units
  integer :: get_free_file_unit

  ! get_free_file_unit = min_file_unit

  !files_units = get_files_units(sf)
  !files_units = sf%files(:)%unit
  files_units = stics_files%files(:)%unit

  if (any(files_units /= -1)) then 
    get_free_file_unit = maxval(files_units) + 1
  else
    get_free_file_unit = min_file_unit
  end if

  !print *, "files units :" ,files_units

  !  ?? add testing if not already in use, by an open file
  ! using: getFreeFileUnit ....
  return
end function get_free_file_unit


! testing if a file is already referenced 
! in the stics files structure
! for a vs
!function vs_exist_file(sf,file_tag)
function vs_exist_file(file_tag)
  implicit none
  !type(Stics_Files_) , intent(IN) :: sf
  type(varying_string), intent(IN) :: file_tag

  logical :: vs_exist_file

  !vs_exist_file = ( vs_get_file_index(sf,file_tag) > 0 )
  vs_exist_file = ( vs_get_file_index(file_tag) > 0 )

  return
end function vs_exist_file


! testing if a file is already referenced 
! in the stics files structure
! for a char
!function c_exist_file(sf,file_tag)
function c_exist_file(file_tag)
  implicit none
  !type(Stics_Files_) , intent(IN) :: sf
  character(len=*), intent(IN) :: file_tag

  logical :: c_exist_file

  !c_exist_file = vs_exist_file( sf, var_str( file_tag ) )
  c_exist_file = vs_exist_file( var_str( file_tag ) )

  return
end function c_exist_file


! getting file index in the files array
! for a vs
!function vs_get_file_index(sf,file_tag)
function vs_get_file_index(file_tag)
  implicit none
  type(varying_string), intent(IN) :: file_tag
  !type(Stics_Files_) , intent(IN) :: sf
  !type(varying_string) ,dimension(files_nb) :: tags
  integer :: vs_get_file_index
  integer :: i 
  
  vs_get_file_index = 0

  !print *, "vs_get_file_index , tag = ",char(file_tag)

  !tags=get_files_tags(sf)
  !tags = sf%tags
 
  do i = 1,files_nb
      !if ( file_tag == sf%tags(i) ) then
      if ( file_tag == stics_files%tags(i) ) then
        !print *, "found ",char(file_tag)," idx = ",i
        exit
    end if
  end do
 
  ! setting file index, if tag was found
  if (i <= files_nb) then
    vs_get_file_index = i
  end if
  
  return
 
end function vs_get_file_index


! getting file index in the files array
! for a char
!function c_get_file_index(sf,file_tag)
  function c_get_file_index(file_tag)
  implicit none
  !type(Stics_Files_) , intent(IN) :: sf
  character(len=*) , intent(IN) :: file_tag

  integer :: c_get_file_index

  !c_get_file_index = vs_get_file_index( sf, var_str( file_tag ) )
  c_get_file_index = vs_get_file_index( var_str( file_tag ) )

end function c_get_file_index


!function get_free_file_index(sf)
function get_free_file_index()
  implicit none
  !type(Stics_Files_) , intent(IN) :: sf
  integer :: get_free_file_index
  
  !get_free_file_index = get_file_index(sf,na_tag)
  get_free_file_index = get_file_index(na_tag)
  
end function get_free_file_index


! getting a file structure using his tag
! for a char
!subroutine c_get_file(sf,file_tag,out_file)
subroutine c_get_file(file_tag,out_file)  
  implicit none
  !type(Stics_Files_) , intent(IN), target :: sf
  character(len=*) , intent(IN) :: file_tag
  type(File_), pointer :: out_file
  
  out_file => null()

  !call vs_get_file(sf,var_str(file_tag),out_file)
  call vs_get_file(var_str(file_tag),out_file)
  
end subroutine c_get_file


! getting a file structure using his tag
! for a vs
!subroutine vs_get_file(sf,file_tag,out_file)
subroutine vs_get_file(file_tag,out_file)
  implicit none
  !type(Stics_Files_), intent(IN), target :: sf
  !type(Stics_Files_), intent(IN) :: sf
  type(varying_string) , intent(IN) :: file_tag
  type(File_), pointer :: out_file
  
  integer :: file_index

  out_file => null()
  !file_index = 0

  !file_index = get_file_index(sf,file_tag)
  file_index = get_file_index(file_tag)

  if ( file_index > 0 ) then
    !out_file => sf%files(file_index)
    out_file => stics_files%files(file_index)
  else
    print *, "File not found !"
  end if

end subroutine vs_get_file


! TODO : define and add interface
! for a File_ type, a pathvar, charpath
! adding a File_ in the files structure array
!subroutine add_file(sf,a_file)
subroutine add_file(a_file)
  implicit none
  !type(Stics_Files_) , intent(INOUT) :: sf
  type(File_) , intent(IN) :: a_file

  ! add status to return ?
  !logical :: add_file
  type(varying_string) :: file_tag
  integer :: lastindex
  file_tag = getFileTag(a_file)

  ! checking if tag is not "na"
  if (file_tag == "na") then
    print *, "The file tag is not set:" // char(file_tag) 
    return
  end if

  !if ( get_file_index(sf,file_tag) > 0 ) then
  if ( get_file_index(file_tag) > 0 ) then
    print *, "The file already exists in the files structure: "//char(file_tag)
    !print *, "get_file_index, value for : ",char(file_tag)," ",idx
    call dispFile(a_file,.TRUE.)
    return
  endif

  !lastindex = get_free_file_index(sf)
  lastindex = get_free_file_index()
  
  if ( ( lastindex > 0 ).AND.( lastindex <= files_nb ) ) then
    ! sf%files(lastindex) = a_file
    ! sf%tags(lastindex) = file_tag
    ! sf%init_status(lastindex) = .TRUE.
    stics_files%files(lastindex) = a_file
    stics_files%tags(lastindex) = file_tag
    stics_files%init_status(lastindex) = .TRUE.
  else
    print *, "Error, no more space to add new file in files structure !, aborting"
    stop
  end if
  
  ! sf%files_number = sf%files_number + 1
  stics_files%files_number = stics_files%files_number + 1

end subroutine add_file


! calculating a file path
! according to a dir and filename, and optionally an id
! or
! a filepath
! or
! a filename
subroutine def_file_path(file_path,in_dir, in_path,file_name,in_id)
  implicit none
  type(varying_string), intent(OUT) :: file_path
  ! character(len=*), intent(IN)  :: in_dir
  ! character(len=*), intent(IN) :: in_path
  type(varying_string), intent(IN)  :: in_dir
  type(varying_string), intent(IN) :: in_path
  character(len=*), intent(IN)  :: file_name
  !type(varying_string), intent(IN) :: file_name

  ! ID to add to the file name
  !character(len=*), intent(IN), optional :: in_id
  type(varying_string), intent(IN), optional :: in_id

  type(varying_string) :: loc_name
  type(varying_string) :: loc_id
  integer :: ib0
  integer :: ib1
  integer :: ib2
  integer :: ib3
  integer :: point_idx
  !print *, file_name

  loc_name = var_str(file_name)
  loc_id = " "
  if (present(in_id)) then 
    !loc_id = var_str(in_id)
    loc_id = in_id
  end if
  ib0 = len_trim(loc_id)
  ib1 = len_trim(loc_name)
  ib2 = len_trim(in_path)
  ib3 = len_trim(in_dir)
  !ib2 = len(trim(in_path))
  !ib3 = len(trim(in_dir))

  !print *, "def_file_path, ib2 :", ib2, &
  !" trim(in_path) :  ",trim(char(in_path)),"|"
  !print *, "def_file_path, ib3 :", ib3

  if ( (ib2 > 0) .AND. (ib3 > 0) ) then
    print *, "Error defining file path for ", trim(file_name)
    print *, "Both directory and full file path are provided !"
    print *, "Aborting."
    call exit(9)
  end if

  ! default file name if no dot and extension
  loc_name = trim(loc_name)//trim(loc_id)

  ! calculating file name if containing an extension
  if ( ib0 > 0) then
    point_idx=index(loc_name,".",.TRUE.)
    loc_name = extract(loc_name,1,(point_idx-1))//"_"// &
    extract(loc_id,1,ib0)//extract(loc_name,point_idx,ib1)
  end if

  ! Recalulating the file path if needed
  ! in_path not empty
  if (ib2 .ne. 0 ) then 
    file_path =  trim(in_path)
  else
    ! defining file path from in_dir and file name
    if (ib3 .eq. 0 ) then
        file_path = loc_name
    else
        !file_path = in_dir(1:ib3) // filesep() // loc_name
        file_path = extract(in_dir,1,ib3) // filesep() // loc_name
    endif
  endif

end subroutine def_file_path


! setting a file path using its tag
!subroutine set_file_path(sf,file_tag,file_path) !, set_path)
subroutine set_file_path(file_tag,file_path) !, set_path)  
  implicit none
  !type(Stics_Files_) , intent(INOUT) :: sf
  character(len=*), intent(IN)  :: file_tag
  character(len=*), intent(IN) :: file_path
  !integer, intent(OUT)  :: set_path

  type(File_), pointer :: a_file 

  !call get_file( sf, var_str( file_tag ), a_file )
  call get_file( var_str(file_tag) , a_file )

  call setpath( a_file, var_str( file_path ) )

end subroutine set_file_path


! setting a file name using its tag
!subroutine set_file_name(sf,file_tag,file_name, set_name)
subroutine set_file_name(file_tag,file_name, set_name)
  implicit none
  !type(Stics_Files_) , intent(INOUT) :: sf
  character(len=*), intent(IN)  :: file_tag
  character(len=*), intent(IN) :: file_name
  integer, intent(OUT)  :: set_name
  
  type(File_), pointer :: a_file 

  !call get_file( sf, var_str( file_tag ), a_file )
  call get_file( var_str(file_tag) , a_file )
  
  call setname( a_file, var_str( file_name ), set_name )

end subroutine set_file_name


! setting a file dir name using its tag
!subroutine set_dir_name(sf,file_tag,dir_name,set_dir)
subroutine set_dir_name(file_tag,dir_name,set_dir)
  implicit none
  !type(Stics_Files_) , intent(INOUT) :: sf
  character(len=*), intent(IN)  :: file_tag
  character(len=*), intent(IN) :: dir_name
  integer, intent(OUT)  :: set_dir
  
  type(File_), pointer :: a_file 

  !call get_file( sf, var_str( file_tag ), a_file )
  call get_file( var_str(file_tag) , a_file )
  
  call setdir( a_file, var_str( dir_name ), set_dir )

end subroutine set_dir_name


! setting a file description using its tag
!subroutine set_file_desc(sf,file_tag,file_desc)
subroutine set_file_desc(file_tag,file_desc)
  implicit none
  !type(Stics_Files_) , intent(INOUT) :: sf
  character(len=*), intent(IN)  :: file_tag
  character(len=*), intent(IN) :: file_desc

  type(File_), pointer :: a_file 

  !call get_file( sf, var_str( file_tag ), a_file )
  call get_file( var_str(file_tag) , a_file )

  call setdesc( a_file, var_str( file_desc ) )

end subroutine set_file_desc


! getting a file name using its tag
!subroutine get_file_name(sf,file_tag,file_name)
subroutine get_file_name(file_tag,file_name)
  implicit none
  !type(Stics_Files_) , intent(IN) :: sf
  character(len=*), intent(IN)  :: file_tag
  character(len=*), intent(OUT) :: file_name

  type(File_), pointer :: a_file 

  !call get_file(sf,var_str( file_tag ),a_file)
  call get_file( var_str(file_tag) ,a_file)

  file_name = getname(a_file)

end subroutine get_file_name


! getting a file dir name using its tag
!subroutine get_dir_name(sf,file_tag,dir_name)
subroutine get_dir_name(file_tag,dir_name)
  !type(Stics_Files_) , intent(IN) :: sf
  character(len=*), intent(IN)  :: file_tag
  character(len=*), intent(OUT) :: dir_name

  type(File_), pointer :: a_file 

  !call get_file(sf,var_str( file_tag ),a_file)
  call get_file( var_str(file_tag) ,a_file)

  dir_name = getDirName(a_file)

end subroutine get_dir_name


! TODO : for VS ? + interface
! deleting a file using its tag
!subroutine delete_file(sf,file_tag, del_file,a_file)
subroutine delete_file(file_tag, del_file,a_file)
  implicit none
  !type(Stics_Files_) , intent(IN) :: sf
  character(len=*), intent(IN)  :: file_tag
  logical, intent(OUT)  :: del_file
  !type(File_), pointer, optional :: get_a_file
  !type(File_), pointer :: get_a_file

  type(File_), pointer :: a_file 
  integer :: ret
  
  del_file = .FALSE.

  !call get_file(sf,var_str(file_tag),a_file)
  call get_file(var_str(file_tag),a_file)
 
  ret = deleteFile(a_file)

  if (ret == 0) then
    del_file = .TRUE.
  end if

  ! for getting a file object as an output
  ! if (present(get_a_file)) then
  !   nullify(get_a_file)
  !   get_a_file => a_file
  ! end if

end subroutine delete_file


! opening a file using its char tag
!subroutine c_open_file(sf,file_tag, a_file, open_file)
subroutine c_open_file(file_tag, a_file, open_file)
  implicit none
  !type(Stics_Files_) , intent(IN) :: sf
  character(len=*), intent(IN)  :: file_tag
  type(File_), pointer :: a_file
  logical, intent(OUT) :: open_file

!  call vs_open_file(sf,var_str(file_tag),a_file,open_file)
call vs_open_file( var_str(file_tag),a_file,open_file )

end subroutine c_open_file


! opening a file using its vs tag
!subroutine vs_open_file(sf,file_tag, a_file,open_file)
subroutine vs_open_file(file_tag, a_file,open_file)
  implicit none
  !type(Stics_Files_) , intent(IN) :: sf
  type(varying_string), intent(IN)  :: file_tag
  logical, intent(OUT) :: open_file

  type(File_),pointer :: a_file
  integer :: ret

  open_file = .FALSE.
  
  !call get_file(sf,file_tag,a_file)
  call get_file(file_tag,a_file)
  
  ret = openFile(a_file)

  if (ret == 0) open_file = .TRUE. 

end subroutine vs_open_file


! TODO : is_open_file !!!!!!!!!!!!!!!!!!!!!!!!
!function is_open_file(sf,file_tag)

!end function is_open_file

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


! opening a file using its char tag
!subroutine c_close_file(sf,file_tag, closed_file)
subroutine c_close_file( file_tag, closed_file ) 
  implicit none
  !type(Stics_Files_) , intent(IN) :: sf
  character(len=*), intent(IN)  :: file_tag
  logical, intent(OUT) :: closed_file

  !call vs_close_file(sf,var_str(file_tag),closed_file)
  call vs_close_file( var_str(file_tag),closed_file )

end subroutine c_close_file


! opening a file using its vs tag
!subroutine vs_close_file(sf,file_tag, closed_file)
subroutine vs_close_file(file_tag, closed_file)
  implicit none
  !type(Stics_Files_) , intent(IN) :: sf
  type(varying_string), intent(IN)  :: file_tag
  logical, intent(OUT) :: closed_file

  type(File_), pointer :: a_file
  integer :: ret
  
  closed_file = .FALSE.
  
  !call get_file(sf,file_tag,a_file)
  call get_file( file_tag,a_file )
  ret = closeFile(a_file)

  if (ret == 0) then 
    closed_file = .TRUE.
  end if
  
end subroutine vs_close_file


! displaying a file informations using its char tag   
!subroutine c_disp_file(sf,file_tag,in_force)
subroutine c_disp_file(file_tag,in_force)
  implicit none
  !type(Stics_Files_) , intent(IN) :: sf
  character(len=*), intent(IN)  :: file_tag
  logical, optional :: in_force

  logical :: force

  force = .FALSE.

  if (present(in_force)) force = in_force

  !call vs_disp_file(sf,var_str(file_tag),force)
  call vs_disp_file(var_str(file_tag),force)

end subroutine c_disp_file


! displaying a file informations using its vs tag 
!subroutine vs_disp_file(sf,file_tag,in_force)
subroutine vs_disp_file(file_tag,in_force)
  implicit none
  !type(Stics_Files_) , intent(IN) :: sf
  logical, optional :: in_force

  type(varying_string) :: file_tag
  type(File_), pointer :: a_file
  logical :: force

  force = .FALSE.
  if (present(in_force)) force = in_force

  !call get_file(sf,file_tag,a_file)
  call get_file(file_tag,a_file)
  call dispFile(a_file,force)

end subroutine vs_disp_file


! writing files footers and/or closing files
!subroutine finalize_files(sf)
  subroutine finalize_files()
  implicit none
  !type(Stics_Files_), intent(INOUT) :: sf
  type(File_), pointer :: a_file
  logical :: file_closed
  

  ! Pointer to shared stics_files structure
  type(Stics_Files_), pointer :: sf
  !type(Stics_Files_) :: sf

  ! Pointers initialization
  sf => null()

  ! pointer association to stics_files structure
  sf => stics_files



  ! TODO: may be other actions could be done here !
  ! not only writing history and error files footer ...

  if (sf%flag_histo) then
    !call get_file(sf,"histo",a_file)
    call get_file("histo",a_file)
    call write_file_footer(a_file,"history")
  end if 

  !call get_file(sf,"errors",a_file)
  call get_file("errors",a_file)
  call write_file_footer(a_file,"errors")

  ! TODO : ADD OTHER FILES dbg, balance ???
  if (sf%flag_debug) then
    !call get_file(sf,"dbg1",a_file)
    call get_file("dbg1",a_file)
    call write_file_footer(a_file,"debug")
    !call get_file(sf,"dbg2",a_file)
    call get_file("dbg2",a_file)
    call write_file_footer(a_file,"debug2")
  end if

  ! closing daily files
  if (sf%asso_jour) then
    call close_file("daily_p",file_closed)
    call close_file("daily_a",file_closed)
  else
    call close_file("daily",file_closed)
  end if

  !closing daily files agmip
  if (sf%asso_jour_agmip) then
    call close_file("daily_agmip_p",file_closed)
    call close_file("daily_agmip_a",file_closed)
  else
    call close_file("daily_agmip",file_closed)
  end if

  !add closing reports files
  if (sf%asso_rap) then
    call close_file("rap_p",file_closed)
    call close_file("rap_a",file_closed)
  else
    call close_file("rap",file_closed)
  end if

  !add closing agmip reports
  if (sf%asso_rap_agmip) then
    call close_file("rap_agmip_p",file_closed)
    call close_file("rap_agmip_a",file_closed)
  else
    call close_file("rap_agmip",file_closed)
  end if
  
  !add closing balance files
  if (sf%asso_bilan) then
    call close_file("bilan_p",file_closed)
    call close_file("bilan_a",file_closed)
  else
    call close_file("bilan",file_closed)
  end if


  ! add closing climatic file !!!!!!!
  call close_file("clim",file_closed)

  ! add closing histo file !!!!!!!
  call close_file("histo",file_closed)

end subroutine finalize_files


! getting a time string
subroutine get_timestamp(time_string)
  implicit none
  type(varying_string), intent(OUT) :: time_string

  character(10) :: time
  integer :: value(8)
  character(len=8) :: date
  
  ! writing log files headers
  call date_and_time(DATE = date, TIME = time, VALUES = value)
  ! date heure ...
  time_string=date(1:4)//"/"//date(5:6)//"/"//date(7:8)//"-"//time(1:2)//":"//time(3:4)//":"//time(5:6)

end subroutine get_timestamp


! checking if the file init routine has been already done
!subroutine check_init_done(sf, error_if_not)
subroutine check_init_done(error_if_not)
  implicit none
  !type(Stics_Files_), intent(IN) :: sf
  logical , optional :: error_if_not

  logical :: error
  
  error = .TRUE.

  if (present(error_if_not)) error = error_if_not

! if struct already initialized
  !if (.NOT.sf%init_done.AND.error) then
  if (.NOT.stics_files%init_done.AND.error) then
    print *, "Error, Stics Files structure nor initialized !, aborting"
    stop
  end if
  
end subroutine check_init_done


! writing a file header 
subroutine write_file_header(a_file,file_kind)
  implicit none
  Type(File_), pointer, intent(IN) :: a_file
  character( len = * ), intent(IN) :: file_kind
  integer :: file_unit, ret
  type(varying_string) :: date_and_time
 
  ! getting date time string
  call get_timestamp(date_and_time)

  ret = openFile(a_file)
  file_unit = a_file%unit
  write(file_unit , *) "-----------------------------------------------------------------------------------------"
  write(file_unit,*) "Stics "//trim(file_kind)//" file            Creation date: "//char(date_and_time)
  !write(file_unit,*) "STICS Version: "//char(codeversion)
  write(file_unit,*) "STICS Version: "//codeversion
  write(file_unit,*) "-----------------------------------------------------------------------------------------"
  write(file_unit,*) ""
  write(file_unit,*) ""

end subroutine write_file_header


! writing a file footer
subroutine write_file_footer(a_file,file_kind)
  implicit none
  Type(File_), pointer, intent(IN) :: a_file
  character( len = * ), intent(IN) :: file_kind
  integer :: file_unit, ret
  type(varying_string) :: date_and_time
  
  ! getting date time string
  call get_timestamp(date_and_time)

  ret = openFile(a_file)
  file_unit = a_file%unit
  write(file_unit , *) "-----------------------------------------------------------------------------------------"
  write(file_unit,*) "Stics "//trim(file_kind)//" file end            End date: "//char(date_and_time)
  write(file_unit,*) "-----------------------------------------------------------------------------------------"
  ret = closeFile(a_file)
end subroutine write_file_footer


end module SticsFiles
 
 
