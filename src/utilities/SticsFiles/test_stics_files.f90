program test_stics_files

USE Files

USE SticsFiles

type(File_) :: file1, file2, filetest
type(File_),pointer :: pfile1, pfile2, a_file
type(varying_string) :: file_tag1, file_tag2
logical :: file_exists, FORCE
character(len=255) :: cwd,filename,dirname
integer, dimension(files_nb) :: files_units
logical :: opened, deleted, closed,status
integer :: idx, i_closed, i_opened, i_setdir, i_setname

!call setDebugOn()

character(len=files_path_length) :: dir_name, file_name

nullify(pfile1)
nullify(pfile2)

file_tag2 = 'histo'

FORCE = .FALSE.


call getcwd(cwd)

! print *, trim(cwd)
! print *, isunix()


! call init(sf)

call init(stics_files)


print *, stics_files%init_status(1:stics_files%files_number)
!stop


print *, stics_files%init_status(1:stics_files%files_number)

print *, "init done: ", stics_files%init_done


!files_units = get_files_units(stics_files)
!print *, count((files_units /= -1) .eqv. .TRUE. )
!print *, files_units

!print *, get_free_file_unit(stics_files)

!file1 = get_file(stics_files,'usm')
!call disp_file('usm', in_force = FORCE)
!call dispFile(file1, in_force = FORCE)

!file_exists=exist_file(stics_files,'usm')
!print *, file_exists

!fileindex = getFileIndex(stics_files,'usm')
!print *, fileindex





! pfile2 => open_file(stics_files,'histo')
!call open_file(stics_files,'histo',pfile2,opened)
call open_file('histo',pfile2,opened)

print *, "opened : ", opened

!idx = get_file_index(stics_files,'histo')


call dispFile(pfile2, in_force = FORCE)
!call disp_file('histo', in_force = FORCE)
!call dispFile(stics_files%files(idx),in_force = FORCE)

call close_file(stics_files,file_tag2,closed)

print *, "closed : ", closed

print *, "after closing file ------------------------------------------"

!call finalize(stics_files)

!stop

!call disp_file(stics_files,'histo', in_force = FORCE)
call dispFile(pfile2, in_force = FORCE)
!call dispFile(stics_files%files(idx),in_force = FORCE)

print *, "is open ! :", isOpen(pfile2)
!stop

!call sleep(15)

print *, "------------------------------------------"
i_opened = openFile(pfile2)
call disp_file('histo', in_force = FORCE)
call dispFile(pfile2, in_force = FORCE)
!call dispFile(stics_files%files(idx),in_force = FORCE)
print *, "after opening file ------------------------------------------"

! stop


write(pfile2%unit,*) "Testing writing into file ..."

write(pfile2%unit,*) "writing with get_file"
!write(stics_files%files(idx)%unit,*) "writing using file index in the structure"


i_closed = closeFile(pfile2)

print *, "after closing file ------------------------------------------"

! stop

call disp_file('histo', in_force = FORCE)
call dispFile(pfile2, in_force = FORCE)
!call dispFile(stics_files%files(idx),in_force = FORCE)



!dir_name = getDirName(stics_files%files(idx))

!print *, "dir name: ", dir_name

!file_name = getname(stics_files%files(idx))

!print *, "file name: ", file_name


! changing file name
!call setname(pfile2,"histo_usm1.sti")
call set_file_name('histo',"histo_usm1.sti",i_setname)


call disp_file('histo', in_force = FORCE)
call dispFile(pfile2, in_force = FORCE)
!call dispFile(stics_files%files(idx),in_force = FORCE)

call get_file_name('histo',filename)
print *, filename

call get_dir_name('histo',dirname)
! print *, dirname

! call setdir(pfile2,'/home/plecharpent',i_setdir)
call set_dir_name('histo','/home/plecharpent',i_setdir)

! print *, "valeur i_setdir", i_setdir

call dispFile(pfile2, in_force = FORCE)
call disp_file('histo', in_force = FORCE)

call finalize(stics_files)




!deleted = delete_file(stics_files,'histo')



!filetest = File("/home/plecharpent/Work/TESTS/TESTS_JAVASTICS/javastics_test_diffusion/&
!                 &v140_stics_850/example/new_travail.usm", 99,  old, read,in_tag = 'usm')

!dir_name = getDirName(filetest)

!print *, "dir name: ", dir_name

!file_name = getname(filetest)

!print *, "file name: ", file_name

!call dispFile(filetest,in_force = FORCE)




!filetest = File("/home/plecharpent/Work", 99,  old, read,in_tag = 'usm', in_prefix = "test_file", in_ext = "tfx")

!dir_name = getDirName(filetest)

!print *, "dir name: ", dir_name

!file_name = getname(filetest)

!print *, "file name: ", file_name

!call dispFile(filetest,in_force = FORCE)








! With pointer
!call get_file(stics_files,'daily_agmip_p',pfile1)


!call setname(pfile1,"usm1")

!call disp_file('daily_agmip_p', in_force = FORCE)
!call dispFile(pfile1,in_force = FORCE)

!file_name = getname(pfile1)

!print *, "file name: ", file_name



! Without pointer, the file isn't modified in the structure stics_files
!call get_file(stics_files,'daily_agmip_a',filetest)


!filetest = setname(filetest,"usm1")

!call disp_file('daily_agmip_a', in_force = FORCE)
!call dispFile(filetest,in_force = FORCE)

!file_name = getname(filetest)

!print *, "file name: ", file_name


!print *, stics_files%files_number
!call dispFile(stics_files%files(28),in_force = FORCE)


call disp_file('bilan', in_force = .TRUE.)
call set_dir_name('bilan','/home/plecharpent',i_setdir)
call disp_file('bilan', in_force = .TRUE.)
call set_file_name('bilan',"usm2",i_setname)
call disp_file('bilan', in_force = .TRUE.)


call delete_file(stics_files,'histo',status,a_file)

end program test_stics_files
