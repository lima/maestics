
program System_test_driver

  use fruit
  use System_test
  integer :: failed_counts


    !#ifdef _WIN32
    !  print *,'Windows'
    !#endif
    !#ifdef __linux
    !  print *,'Linux'
    !#endif
    !#ifdef __unix
    !     print *,'Unix'
    !#endif

    
    call init_fruit


  


    ! insert calls 
    !call test_assert_os_isunix
    
    !call test_assert_os_iswindows
    
    !call test_assert_unix_os_filesep
    
    ! call test_assert_windows_os_filesep
    
    ! call test_assert_isfile
    
    ! call test_assert_isdir
    
    !call assert_change_directory
    
    !call assert_get_current_directory
    
    
    call fruit_summary

    call fruit_finalize

  call get_failed_count (failed_counts)  
  
  print *,''
  print *, 'Failed ', failed_counts

  if (failed_counts.GT.0) call exit(2)

end program System_test_driver
