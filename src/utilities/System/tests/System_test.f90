

module System_test
  use fruit
  use SticsSystem
  implicit none


contains
  
  
  
  subroutine test_assert_os_isunix
    implicit none
    ! on unix systems
    call assert_equals (isunix(), .TRUE.)
    call assert_equals (iswin(), .FALSE.)
  end subroutine test_assert_os_isunix



  subroutine test_assert_os_iswindows
    ! on windows systems
    call assert_equals (isunix(), .FALSE.)
    call assert_equals (iswin(), .TRUE.)
  end subroutine test_assert_os_iswindows
  

  
  subroutine test_assert_unix_os_filesep
    implicit none
    ! on unix systems
    call assert_equals (filesep(), '/')
    call assert_not_equals (filesep(), '\')  
  end subroutine test_assert_unix_os_filesep
  
  
  
  subroutine test_assert_windows_os_filesep
    implicit none
    ! on windows systems
    call assert_equals (filesep(), '\')
    call assert_not_equals (filesep(), '/')  
  end subroutine test_assert_windows_os_filesep
  
  
  
  subroutine test_assert_isfile
    implicit none
    character(len=system_path_length) :: a_path  
    if (isunix()) then
    ! on unix systems
       a_path='./data/test.txt'
       call assert_equals (isfile(a_path), .TRUE.)
       !print *, 'unix,isfile: ',isfile(a_path)
       a_path='data'
       call assert_equals (isfile(a_path), .FALSE.)
    else
       a_path='.\data\test.txt'
       call assert_equals (isfile(a_path), .TRUE.)
       a_path='data'
       call assert_equals (isfile(a_path), .FALSE.)
    end if
  end subroutine test_assert_isfile



  subroutine test_assert_isdir
    implicit none
    character(len=system_path_length) :: a_path
    if (isunix()) then
    ! on unix systems
       !print *, 'unix,isdir: ',isfile(a_path)
       a_path='data'
       call assert_equals (isdir(a_path), .TRUE.)
       a_path='./data/test.txt'
       call assert_equals (isdir(a_path), .FALSE.)
    else
       a_path='data'
       call assert_equals (isdir(a_path), .TRUE.)
       a_path='.\data\test.txt'
       call assert_equals (isdir(a_path), .FALSE.)
    end if
  end subroutine test_assert_isdir
   
   

   subroutine assert_change_directory
       implicit none
       character(len=system_path_length) :: new_dir, current_dir,old_dir
       integer :: is_changed_directory
       call getcwd(current_dir)
       print * , current_dir
       old_dir=current_dir
       new_dir='/home/plecharpent/Work/projet_LegoStics/legostics'
       is_changed_directory=change_directory(new_dir)
       if (is_changed_directory == 0) then 
           call getcwd(current_dir)
           print * ,'new directory ', current_dir
           call assert_equals(current_dir,'/home/plecharpent/Work/projet_LegoStics/legostics')
       else
           print * ,'Erreur de changement de repertoire vers ', new_dir
       end if
       ! returning to the origin
       is_changed_directory=change_directory(old_dir)
       call getcwd(current_dir)
       print * ,'returning to old dir :', current_dir
       call assert_equals(current_dir,old_dir)    
   end subroutine assert_change_directory
  
  
  
   subroutine assert_get_current_directory
      implicit none
      character(len=system_path_length) :: test_dir, current_dir
      call getcwd(test_dir)
      current_dir=get_current_directory()
      call assert_equals(current_dir,test_dir)
      print *,test_dir
      print *, current_dir
   end subroutine assert_get_current_directory
  

end module System_test
