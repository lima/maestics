program test_system

USE SticsSystem



! getting env var
character(len=99999) :: var_content
character(len=100) :: a_var_name
character(len=1) :: file_sep
logical :: is_unix,is_dir,is_file
character(len=255) :: filepath,dirpath
 
! get env var content
a_var_name='PATH'
var_content=getvar(a_var_name)
print *, 'Contenu de PATH : ',trim(var_content)

! recup file sep
file_sep=filesep()
print *, 'File sep ',file_sep


! OS
is_unix=isunix()
print *, 'Is unix ',is_unix

! is dir
dirpath='../Files'
is_dir=isdir(dirpath)
print *, 'Is dir ',is_dir

! is file
filepath='./data/test.txt'
is_file=isfile(filepath)
print *, 'Is file ',is_file

end program test_system
