module SticsSystem

  USE iso_varying_string
  
  implicit none
  integer, parameter :: system_path_length = 255
  
  
  type System_
     character(len=1) :: filesep
     logical :: isunix
     !character(len=200) :: copy,move,delete_dir,delete_file,recursive_delete,disp
     type(varying_string) :: copy,move,delete_dir,delete_file,recursive_delete,disp
     logical :: init_ok = .FALSE.
     ! voir pour syntaxe commandes unix / win
  end type System_
  
  type(System_),save :: sys_cmd
  
  
  
  interface init
   module procedure sys_init
  end interface init

  interface isdir
   module procedure vs_isdir, c_isdir
  end interface isdir
  
  interface isfile
   module procedure vs_isfile, c_isfile
  end interface isfile

  !interface check
  !  module procedure check_sys_cmd
  !end interface check
    
  interface execute_command
   module procedure vs_execute_command, c_execute_command
  end interface execute_command

   contains
   
   
   
  subroutine sys_init (sys)
    implicit none
    type(System_), intent(inout) :: sys
    
    if (sys%init_ok) then
      return
    end if

    sys%init_ok=.FALSE.
    sys%filesep = "/"
    sys%isunix  = .FALSE.
    
    sys%filesep=filesep()
    sys%isunix=isunix()
    
    if (sys%isunix) then
       sys%copy='cp '
       sys%move='mv '
       sys%disp='cat '
       sys%delete_file='rm '
       sys%delete_dir='rmdir '
       sys%recursive_delete='rm -fR '
    else
       sys%copy='copy '
       sys%move='move /Y'
       sys%disp='type '
       sys%delete_file='del '
       sys%delete_dir='rmdir '
       sys%recursive_delete='rmdir /S /Q '
    end if
    
    sys%init_ok=.TRUE.
  end subroutine sys_init 



  subroutine check_system()
    implicit none
    !logical, intent(out) :: init_sys_ok
    !init_sys_ok = .FALSE.
    if (sys_cmd%init_ok) then
       !init_sys_ok=.TRUE.
    else
       call init(sys_cmd)
    end if
  end subroutine check_system
   
   
   
  function isunix()
    logical :: isunix
    character(len=255) :: cwd
    isunix=.FALSE.

    cwd =""
    
    call getcwd(cwd)
    
    !print *, cwd 
    ! for unix like
    if(index(trim(cwd),'/').gt.0) then 
        isunix=.TRUE.
    endif
    
    return
  end function isunix
  
  
  
  function iswin()
    logical :: iswin
    !character(len=255) :: cwd
    iswin=.FALSE.
    
    !call getcwd(cwd)
    
    !if(index(trim(cwd),'\').gt.0)then ! for win
    !    iswin=.TRUE.
    !endif
    
    iswin = .NOT.isunix()
    
    return
  end function iswin
  
  
  
  function filesep()
    character(len=1) :: filesep
   
    if(isunix()) then ! for unix like
        filesep='/'
    else if (iswin()) then ! for win
        filesep='\'
    else
        filesep='?'
        print *, 'File separator could not be determined'
    endif
    
    return
  end function filesep
  
  
  
  function vs_isfile(a_path)
    logical :: vs_isfile,loc_exist
    type(varying_string), intent(IN) :: a_path
    vs_isfile=.FALSE.
    loc_exist=.FALSE.
    ! a_path is a directory path
    !if (isdir(a_path)) then
    !  print *, "isdir in vs_isfile ", char(a_path)
    !   return
    !end if
    ! testing if file exists
    inquire(file = trim(char(a_path)), exist = loc_exist)
    if (loc_exist) vs_isfile=.TRUE.
    return
   end function vs_isfile
  

  function c_isfile(a_path)
    character(len=*), intent(IN) :: a_path
    type(varying_string) :: vs_path
    logical :: c_isfile
    !c_isfile=.FALSE.
    vs_path = var_str(a_path)
    c_isfile = vs_isfile(vs_path)
  end function c_isfile
  
  
  function vs_isdir(a_path)
    !character(len=system_path_length) :: a_path!,cmdmsg
    type(varying_string), intent(IN) :: a_path
    !integer*4 :: exit_status
    logical :: vs_isdir, loc_exist
    vs_isdir=.FALSE.
    loc_exist=.FALSE.
    ! testing if dir exists
    inquire(file = trim(char(a_path))//filesep()//'. ', exist = loc_exist)
    !print *, "in vs_isdir : path ", trim(char(a_path))//filesep()//'. '
    !print *, "loc_exist", loc_exist
    if (loc_exist) vs_isdir=.TRUE.
  end function vs_isdir
  
  function c_isdir(a_path)
    character(len=*), intent(IN) :: a_path
    type(varying_string) :: vs_path
    logical :: c_isdir
    !c_isdir=.FALSE.
    vs_path = var_str(a_path)
    c_isdir = vs_isdir(vs_path)
  end function c_isdir
  
  ! TODO : exists interface (c_exists, vs_exists)
  ! for file and dir calls

  function getvar(a_var_name)
     character(len=100) :: a_var_name
     character(len=999) :: getvar
     call getenv(a_var_name,getvar)
     return
  end function getvar
  
  
  
  function change_directory(new_dir)
     implicit none
     character(len=system_path_length), intent(in) :: new_dir
     integer :: change_directory
     call chdir(new_dir,change_directory)
     if (change_directory > 0) print *, 'Error while changing directory to :',trim(new_dir)
     return
  end function change_directory
  
  
  
  function get_current_directory()
     implicit none
     character(len=system_path_length) :: get_current_directory
     integer  :: status
     call getcwd(get_current_directory,status)
     return
  end function get_current_directory
  

  function vs_execute_command(in_cmd)
    type(varying_string) , intent(in) :: in_cmd
    integer :: vs_execute_command
    
    call system(char(in_cmd),vs_execute_command)
    if (vs_execute_command > 0) print *, 'Error while executing command :',char(in_cmd)
    return
 end function vs_execute_command
  
  
  function c_execute_command(in_cmd)
      character(len=*), intent(in) :: in_cmd
      type(varying_string) :: loc_cmd
      integer :: c_execute_command

      loc_cmd = in_cmd
      !call system(trim(in_cmd),execute_command)
      !if (execute_command > 0) print *, 'Error while executing command :',trim(in_cmd)
      c_execute_command = vs_execute_command(loc_cmd)
      return
   end function c_execute_command
   
end module SticsSystem
 