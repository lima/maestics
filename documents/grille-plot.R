library(zoo)
library(ggplot2)

dir<-getwd() #execution in the current directory
setwd(dir) 

for(annee in c(18,19)){
for(mois in c("june","august","october","december")) {
  

 fichiertest1<-paste('2_testflx1.dat_28',mois,annee,sep="")
 fichiertest2<-paste('2_testflx2.dat_28',mois,annee,sep="")
 
#Ouverture fichier 2_hrflux1.dat pour les 5000 1ers points tests:
file1<-paste(dir,fichiertest1,sep="/")
simu_rad1<-read.table(file1, skip=20, header=FALSE)

#Ouverture fichier 2_hrflux2.dat pour les 4700 autres points tests:
file2<-paste(dir,fichiertest2,sep="/")
simu_rad2<-read.table(file2, skip=20, header=FALSE)


simu_rad <-rbind(simu_rad1,simu_rad2)
names(simu_rad) <- c("DAY","HR","PT","X","Y","Z","PAR","FBEAM","TD","TSCAT","TBEAM","TDIFF","TTOT")

resultat <- NULL
z=0
for(x in seq(from=0, to=max(simu_rad$X), by=2)){
  for(y in seq(from=2, to=max(simu_rad$Y), by=2)){
   # for(iday in 1:195){
    resultTTOT = sum(simu_rad$TTOT[simu_rad$X==x &  simu_rad$Y==y & simu_rad$Z==z]) #cumul journalier pour chaque pt x,y,z
    resultPAR = sum(simu_rad$PAR[simu_rad$X==x &  simu_rad$Y==y & simu_rad$Z==z]) #cumul journalier pour chaque pt x,y,z
    
    #  cat(x,y,100*(resultTTOT/resultPAR),"\n")
    m=matrix(c(x,y,100*(resultTTOT/resultPAR)),ncol=3)
   # m=matrix(c(x,y,resultTTOT),ncol=3)
    m=as.data.frame(m)
    resultat<-rbind(resultat,m)
    
  #  }
  }
}

#Representation graphique de la carte des TTOT cumulées :

X=resultat[,1]
Y=resultat[,2]
Per=resultat[,3]

titre <- paste('Total radiation / PAR - 28 ', mois, ' 20',annee, sep="")

cat(titre)
p <- qplot(X,Y,colour=Per,xlab="X (meters)", ylab="Y (meters)") 
s <- p + ggtitle(titre) + guides(size="none", colour=guide_colourbar(title.position = "right", title.vjust = 1)) + scale_color_distiller(palette="YlGn",  type="seq",name="%", limits=c(0,100.05),direction=-1) + theme (axis.title= element_text(size=rel(1), face="bold"), #scale_color_distiller limits=c(0,100.05),
           axis.text.x = element_text(size=rel(1), face="bold"),
           axis.text.y = element_text(size=rel(1), face="bold"))

nomfichier <- paste('carte_28',mois,annee,'.png', sep="")

ggsave(nomfichier,plot=s,height=6, width=12, units=c("cm"),dpi=600)

}
}

 #   }
#  }


#t<-rollmean(resultat[,3],195)

#line=0
#cumbisTTOT <- data.frame(x=1)
#x=136
#z=0
#for(y in seq(from=72, to=90, by=2)){

    
#    line=line+1
#    resultbisTTOT = sum(cumTTOT[])
#    cumbisTTOT[line] = resultbisTTOT
    
#    cat(x,y,z,resultbisTTOT,"\n")
    

#}


    #line=line+1
    #grid <- c(x,y,z)


#line=0

#cumTTOT <- data.frame(x=1)
#vecteurs des 12 irradiances cumulées :
#for(iday in 0:195) {
#  line=line+1
#  resultTTOT = sum(simu_rad$TTOT[simu_rad$DAY==iday])
#  cumTTOT[line] = resultTTOT
#}
