# MaeStics

The current work allows to simulate crop development in an agroforestry context using the ecophysiology model [Maespa](https://eco-sols.gitlab.cirad.fr/maespa/) and the [Stics soil crop model](https://www.sciencedirect.com/science/article/pii/S1161030102001107).

This development has been motivated by the study of [Sow et al, 2024](https://doi.org/10.1016/j.fcr.2023.109206) aiming at calibrating Stics crop model for pearl millet. This calibration highlighted a dependence of the millet yield to the solar incoming radiation and to the nitrogen fertilization.
With its ability to simulate shades produced by trees on a given plot, Maespa is used in this software to compute the incoming solar radiations at a given location. This incoming radiation is transferred to the Stics component which uses it to compute the crop yield, leading to an agroforestry simulation context (the nitrogen fertilization is not considered in this work).

- [**Documentation**](https://gitlab.cirad.fr/lima/maestics/-/wikis/Documentation)

- License : not yet choosen, must be compatible with the Cecill license of Stics and the GPL-V3 license of Maespa.

- Contributions : Celine Blitz Frayret - Guerric Le Maire with the help of Antoine Couedel and Mathilde De Freitas, Cirad.

- Contact : [celine.blitz@cirad.fr](celine.blitz@cirad.fr)
